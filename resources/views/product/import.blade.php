@extends('layouts.app')

@section('page-title')
    {{ __('Import Products') }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item"><a href="{{ route('product.index') }}">{{ __('Productos') }}</a></li>
    <li class="breadcrumb-item active">{{ __('Import Products') }}</li>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5>{{ __('Import Products') }}</h5>
                </div>
                <div class="card-body">
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form id="import-form" action="{{ route('product.import') }}" method="POST"
                        enctype="multipart/form-data" onsubmit="return disableButton()">
                        @csrf
                        <div class="form-group">
                            <label for="file">{{ __('Choose Excel File') }}</label>
                            <input type="file" name="file" id="file" class="form-control" required>
                        </div>
                        <div class="text-center mb-3">
                            <button type="submit" class="btn btn-primary" id="import-button">{{ __('Import') }}</button>
                        </div>
                    </form>

                    <div class="text-center">
                        <p>{{ __('Download an example Excel file with the correct format.') }}</p>
                        <a href="{{ asset('storage/alex-shop.xlsx') }}" class="btn btn-sm btn-primary">
                            {{ __('Download Example Excel') }}
                        </a>
                    </div>
                    
                    @push('scripts')
                        <script>
                            function disableButton() {
                                var button = document.getElementById('import-button');
                                button.disabled = true;
                                button.textContent = 'Importing...';
                                button.form.submit();
                            }
                        </script>
                    @endpush
                </div>
            </div>
        </div>
    </div>
@endsection
