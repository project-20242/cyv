@extends('layouts.app')

@section('page-title')
    {{ __('Pay Reports') }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item" aria-current="page">{{ __('Pay Reports') }}</li>
@endsection

@section('action-button')
    <div class="text-end">
        <a href="javascript:;" class="btn btn-sm btn-primary btn-icon exportChartButton" title="{{ __('Export') }}"
            data-bs-toggle="tooltip" data-bs-placement="top">
            <i class="ti ti-file-export"></i>
        </a>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-xxl-12">
            <div class="card card-body">
                <form method="GET" action="{{ route('reports.pay_report') }}">
                    <ul class="nav nav-pills gap-2 gap-3" id="pills-tab" role="tablist">
                        <li class="nav-item mb-2">
                            <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'year' ? 'active' : '' }}" name="chart_data" value="year">Year</button>
                        </li>
                        <li class="nav-item mb-2">
                            <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'last-month' ? 'active' : '' }}" name="chart_data" value="last-month">Last Month</button>
                        </li>
                        <li class="nav-item mb-2">
                            <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'this-month' ? 'active' : '' }}" name="chart_data" value="this-month">This Month</button>
                        </li>
                        <li class="nav-item mb-2">
                            <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'seven-day' ? 'active' : '' }}" name="chart_data" value="seven-day">Last 7 Days</button>
                        </li>
                        
                        <!-- Campos para el rango de fechas -->
                        
                            <li class="nav-item mb-2">
                                <input type="date" name="start_date" class="form-control" id="start_date"  value="{{ request('start_date') }}">
                            </li>
                            <li class="nav-item mb-2">
                                <input type="date" name="end_date" class="form-control" id="end_date" value="{{ request('end_date') }}" >
                            </li>
                        
                        <!-- Bot n para generar reporte con rango de fechas -->
                        <div class="col-md-2" id="filter_type" style="padding-left: 10px;">
                            <button type="submit" class="btn btn-primary label-margin chart-data generate_button" name="chart_data" value="specific-date">{{ __('Generate') }}</button>
                        </div>
                        
                        <div id="deals-staff-report" height="400" width="1100" data-color="primary" data-height="280"></div>
                    </ul>
                </form>
                

<script>
    // datos reporte
    var dataJSON = [];


    document.addEventListener('DOMContentLoaded', function () {
        var dateRange = document.getElementById('date-range');
        var buttons = document.querySelectorAll('.chart-data');

        buttons.forEach(function(button) {
            button.addEventListener('click', function() {
                if (this.value === 'specific-date') {
                    dateRange.style.display = 'block';
                } else {
                    dateRange.style.display = 'flex';
                }
            });
        });
    });
</script>

                
            </div>
        </div>
        
        <div class="col-xxl-12">
            <div class="card">
                <div class="card-header card-body table-border-style">
                    <h5>{{ __('Pay Reports') }}</h5>
                    <div class="table-responsive">
                        <table class="table dataTable">
                            <thead>
                                <tr>
                                    <th>{{ __('ID Transaction') }}</th>
                                    <th>{{ __('Transaction date') }}</th>
                                    <th>{{ __('Customer') }}</th>
                                    <th>{{ __('Amount') }}</th>
                                    <th>{{ __('Payment type') }}</th>
                                    <th>{{ __('Payment status') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($pay_report as $pay_reports)
                                    <tr>
                                        <td>{{ $pay_reports->id }}</td>
                                        <td>{{ $pay_reports->order_date }}</td>
                                        <td>{{ !empty($pay_reports->CustomerData->name) ? $pay_reports->CustomerData->name : '' }}</td>
                                        <td>{{ $pay_reports->final_price }}</td>
                                        <td>{{ $pay_reports->payment_type }}</td>
                                        <td>{{ $pay_reports->payment_status }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('custom-script')
    <script>
        var dataJSON = [];

        $(document).ready(function() {
            function insertData(){ 
                @foreach ($pay_report as $pay_reports)
                   var rowJSON = [];
                   rowJSON.push('{{ $pay_reports->id }}');
                   rowJSON.push('{{ $pay_reports->order_date }}');
                   rowJSON.push('{{ $pay_reports->user_name }}');
                   rowJSON.push('{{ $pay_reports->final_price }}');
                   rowJSON.push('{{ $pay_reports->payment_type }}');
                   rowJSON.push('{{ $pay_reports->payment_status }}'); 
                   dataJSON.push(rowJSON);
                @endforeach
            };

            insertData();

            function fetchData(value, date) {
                var csrfToken = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: "GET",
                    url: '{{ route('reports.chart') }}',
                    async: true,
                    data: {
                        _token: csrfToken,
                        chart_data: value,
                        date: date,
                    },
                    success: function(data) {  
                        $('.chart_data').html(data.html); 
                    },
                    error: function(error) {
                        console.log('Error:', error);
                    }
                });
            }

            $(".chart-data").on("click", function() {
                $(".chart-data").removeClass("active");
                $(this).addClass("active");
                var value = $(this).val();
                var date = $('.date').val();

                fetchData(value, date);
            });

            $('.date').on('change', function() {
                var selectedDate = $(this).val();
                var activeButton = $(".chart-data.active");
                var value = activeButton.val();

                fetchData(value, selectedDate);
            });

            //var firstValue = $(".chart-data:first").val();
            //$(".chart-data:first").addClass("active");
            //fetchData(firstValue);
        });

        $(document).ready(function() {
            $('.exportChartButton').click(function() {
                var tableData = [];

                $('.dataTable thead tr').each(function(index, row) {
                    var rowData = [];
                    $(row).find('th').each(function(index, column) {
                        rowData.push(`"${$(column).text()}"`);
                    });
                    tableData.push(rowData.join(','));
                }); 

                $.each(dataJSON, function(index, row) { 
                    tableData.push(row.join(','));
                });

                var csvContent = "data:text/csv;charset=utf-8,";
                csvContent += tableData.join('\n');

                var encodedUri = encodeURI(csvContent);
                var link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "Pay_Report.csv");
                document.body.appendChild(link);
                link.click();
            });
        });
    </script>
@endpush
