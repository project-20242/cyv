@extends('layouts.app')

@section('page-title')
    {{ __('Informe de los Productos más Vendidos') }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item" aria-current="page">{{ __('Informe de los productos más vendidos') }}</li>
@endsection

@section('action-button')
    <div class="text-end">
        <a href="javascript:;" class="btn btn-sm btn-primary btn-icon exportChartButton" title="{{ __('Export') }}" data-bs-toggle="tooltip" data-bs-placement="top">
            <i class="ti ti-file-export"></i>
        </a>
    </div>
@endsection
{{-- INICiO DE CAMPOS --}}
@section('content')
<div class="row">
    <div class="col-xxl-12">
      <div class="card card-body">
        <form method="GET" action="{{ route('reports.best_selling_products') }}">
            <ul class="nav nav-pills gap-2 gap-3" id="pills-tab" role="tablist">
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'year' ? 'active' : '' }}" name="chart_data" value="year">year</button>
                </li>
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'last-month' ? 'active' : '' }}" name="chart_data" value="last-month">Last month</button>
                </li>
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'this-month' ? 'active' : '' }}" name="chart_data" value="this-month">This month</button>
                </li>
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'seven-day' ? 'active' : '' }}" name="chart_data" value="seven-day">Last 7 days</button>
                </li>
                <div class="nav-item mb-2">
                    {{ Form::date('start_date', request('start_date') ?: date('Y-m-d'), ['class' => 'date form-control', 'placeholder' => 'YYYY-MM-DD']) }}
                </div>
                <div class="nav-item mb-2">
                    {{ Form::date('end_date', request('end_date') ?: date('Y-m-d'), ['class' => 'date form-control', 'placeholder' => 'YYYY-MM-DD']) }}
                </div>
                <div class="col-md-2" id="filter_type" style="padding-left:10px;">
                    <button type="submit" class="btn btn-primary label-margin chart-data generate_button" name="chart_data" value="date-range">{{ __('Generate') }}</button>
                </div>
            </ul>
        </form>
      </div>
    </div>
</div>
    <div class="row">
        <div class="card">
            <div class="card-header card-body table-border-style">
                <h5></h5>
                <div class="table-responsive">
                    <table class="table dataTable">
                        <thead>
                            <tr>  
                                <th>{{ __('Producto') }}</th>
                                <th>{{ __('Cantidad Vendida') }}</th>
                                <th>{{ __('Ingresos Generados')}}</th>
                                <th>{{ __('Porcentaje Total de Ventas') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($groupedProducts as $product)
                                <tr>
                                    <td>
                                        <span class="h6 text-m">{{ $product['name'] }}</span>
                                        <br>
                                    </td>
                                    <td>
                                        <span class="h6 text-m">{{ $product['qty'] }}</span>
                                        <br>
                                    </td>
                                    <td>
                                        <span class="h6 text-m">{{$currency}}{{ $product['total_price']}}</span>
                                    </td>
                                    <td>
                                        <span class="h6 text-m">{{ number_format($product['percentage'], 1) }} %</span>
                                        <br>
                                    </td>
    
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<script>
   $(document).ready(function() {
    $('.exportChartButton').click(function() {
        $.ajax({
            url: '{{ route('reports.export_best') }}',
            method: 'GET',
            success: function(data) {
                if (data.hasOwnProperty('error')) {
                    console.error(data.error);
                    return;
                }

                var csvContent = "data:text/csv;charset=utf-8,Producto,Cantidad Vendida,Ingresos Generados,Porcentaje Total de Ventas\n";

                if (data.length > 0) {
                    data.forEach(function(product) {
                        csvContent += `${product.name},${product.qty},${product.total_price},${product.percentage}%\n`;
                    });
                } else {
                    csvContent += "No hay informacion disponible\n";
                }

                var encodedUri = encodeURI(csvContent);
                var link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "Best Selling Products Report.csv");
                document.body.appendChild(link);
                link.click();
                document.body.removeChild(link);
            },
            error: function(xhr, status, error) {
                console.error('Error in AJAX request:', error);
            }
        });
    });
});
</script>
@endsection