@extends('layouts.app')

@section('page-title')
    {{ __('Reporte de los Productos más Vendidos por Tienda') }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item" aria-current="page">{{ __('Reporte de los Productos más Vendidos por Tienda') }}</li>
@endsection

@section('action-button')
    <div class="text-end">
        <a href="javascript:;" class="btn btn-sm btn-primary btn-icon exportChartButton" title="{{ __('Exportar') }}" data-bs-toggle="tooltip" data-bs-placement="top">
            <i class="ti ti-file-export"></i>
        </a>

        {{-- BOTÓN DE CAMBIO DE GRÁFICA
        <a href="{{ route('orders.barchart_order_report') }}" class="btn btn-sm btn-primary btn-icon" title="{{ __('Gráfica de Barras Radial') }}"
            data-bs-toggle="tooltip" data-bs-placement="top">
            <i class="ti ti-chart-bar"></i>
        </a> 
        --}}
    </div>
@endsection

{{-- INICIO DE CAMPOS --}}
@section('content')
<div class="row">
    <div class="col-xxl-12">
      <div class="card card-body">
        <form method="GET" action="{{ route('reports.sellings_stores') }}">
            <ul class="nav nav-pills gap-2 gap-3" id="pills-tab" role="tablist">
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'year' ? 'active' : '' }}" name="chart_data" value="year">year</button>
                </li>
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'last-month' ? 'active' : '' }}" name="chart_data" value="last-month">Last month</button>
                </li>
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'this-month' ? 'active' : '' }}" name="chart_data" value="this-month">This month</button>
                </li>
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'seven-day' ? 'active' : '' }}" name="chart_data" value="seven-day">Last 7 days</button>
                </li>
                <div class="nav-item mb-2">
                    {{ Form::date('start_date', request('start_date'), ['class' => 'date form-control', 'placeholder' => 'YYYY-MM-DD']) }}
                </div>
                <div class="nav-item mb-2">
                    {{ Form::date('end_date', request('end_date'), ['class' => 'date form-control', 'placeholder' => 'YYYY-MM-DD']) }}
                </div>
                <div class="col-md-2" id="filter_type" style="padding-left:10px;">
                    <button type="submit" class="btn btn-primary label-margin chart-data generate_button" name="chart_data" value="date-range">{{ __('Generate') }}</button>
                </div>
            </ul>
        </form>
      </div>
    </div>
</div>
    <div class="row">
        <div class="card">
            <div class="card-header card-body table-border-style">
                <h5></h5>
                <div class="table-responsive">
                    <table class="table dataTable" id="productTable">
                        <thead>
                            <tr>
                                <th>{{ __('Nombre de la tienda') }}</th>
                                <th>{{ __('Producto') }}</th>
                                <th>{{ __('Cantidad Vendida') }}</th>
                                <th>{{ __('Ingresos Generados') }}</th>
                                <th>{{ __('Porcentaje Total de Ventas') }}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($productosMasVendidos as $product)
                            <tr>
                                <td>
                                    <span class="h6 text-m">{{ $product['store_name'] }}</span> <!-- Nombre de la tienda -->
                                    <br>
                                </td>
                                <td>
                                    <span class="h6 text-m">{{ $product['name'] }}</span> <!-- Nombre del producto -->
                                    <br>
                                </td>
                                <td>
                                    <span class="h6 text-m">{{ $product['qty'] }}</span> <!-- Cantidad vendida -->
                                    <br>
                                </td>
                                <td>
                                    <span class="h6 text-m">${{ $product['total_price'] }}</span> <!-- Ingresos generados -->
                                </td>
                                <td>
                                    <span class="h6 text-m">{{ number_format($product['percentage'], 1) }} %</span> <!-- Porcentaje de ventas -->
                                    <br>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.17.0/xlsx.full.min.js"></script>

<script>
    $(document).ready(function() {
        $('.exportChartButton').click(function() {
            var data = [["Nombre de la tienda", "Producto", "Cantidad Vendida", "Ingresos Generados", "Porcentaje Total de Ventas"]];

            $('#productTable tbody tr').each(function() {
                var row = $(this);
                var storeName = row.find('td').eq(0).text().trim();
                var productName = row.find('td').eq(1).text().trim();
                var qty = row.find('td').eq(2).text().trim();
                var totalPrice = row.find('td').eq(3).text().replace('$', '').trim();
                var percentage = row.find('td').eq(4).text().replace('%', '').trim();

                // Formatear precio como número con dos decimales
                totalPrice = parseFloat(totalPrice).toFixed(2);

                // Asegurarse de que el porcentaje esté en el formato correcto
                percentage = parseFloat(percentage).toFixed(2) + "%";

                data.push([storeName, productName, qty, totalPrice, percentage]);
            });

            var wb = XLSX.utils.book_new();
            var ws = XLSX.utils.aoa_to_sheet(data);

            // Ajustar el ancho de las columnas
            ws['!cols'] = [
                { wch: 20 },  // Nombre de la tienda
                { wch: 30 },  // Producto
                { wch: 15 },  // Cantidad Vendida
                { wch: 20 },  // Ingresos Generados
                { wch: 25 }   // Porcentaje Total de Ventas
            ];
            XLSX.utils.book_append_sheet(wb, ws, "Reporte");

            // Generar archivo Excel
            XLSX.writeFile(wb, "Reporte_Productos_Más_Vendidos.xlsx");
        });
    });
</script>

@endsection
