@extends('layouts.app')

@section('page-title')
    {{ __('Store Performance Report') }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item" aria-current="page">{{ __('Customer Reports') }}</li>
@endsection

@section('action-button')
    <div class="text-end">
        <a href="javascript:;" class="btn btn-sm btn-primary btn-icon exportChartButton" title="{{ __('Export') }}"
            data-bs-toggle="tooltip" data-bs-placement="top">
            <i class="ti ti-file-export"></i>
        </a>
    </div>
@endsection

@section('content')
   <div class="row">
    <div class="col-xxl-12">
      <div class="card card-body">
        <form method="GET" action="{{ route('reports.store-performance') }}">
            <ul class="nav nav-pills gap-2 gap-3" id="pills-tab" role="tablist">
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'year' ? 'active' : '' }}" name="chart_data" value="year">{{ __('Year') }}</button>
                </li>
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'last-month' ? 'active' : '' }}" name="chart_data" value="last-month">{{ __('Last month') }}</button>
                </li>
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'this-month' ? 'active' : '' }}" name="chart_data" value="this-month">{{ __('This month') }}</button>
                </li>
                <li class="nav-item mb-2">
                    <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'seven-day' ? 'active' : '' }}" name="chart_data" value="seven-day">{{ __('Last 7 days') }}</button>
                </li>
                <div class="nav-item mb-2">
                    {{ Form::date('start_date', request('start_date') ?: date('Y-m-d'), ['class' => 'date form-control', 'placeholder' => 'YYYY-MM-DD']) }}
                </div>
                <div class="nav-item mb-2">
                    {{ Form::date('end_date', request('end_date') ?: date('Y-m-d'), ['class' => 'date form-control', 'placeholder' => 'YYYY-MM-DD']) }}
                </div>
                <div class="col-md-2" id="filter_type" style="padding-left:10px;">
                    <button type="submit" class="btn btn-primary label-margin chart-data generate_button" name="chart_data" value="date-range">{{ __('Generate') }}</button>
                </div>
            </ul>
        </form>
      </div>
    </div>
</div>

<div class="row">
    <div class="card">
        <div class="card-header card-body table-border-style">
            <div class="table-responsive">
                <table class="table dataTable">
                    <thead>
                        <tr>  
                            <th>{{ __('Nombre de la Tienda') }}</th>
                            <th>{{ __('Fecha') }}</th>
                            <th>{{ __('Ventas totales') }}</th>
                            <th>{{ __('Número de Pedidos') }}</th>
                            <th>{{ __('Valor Promedio del Pedido') }}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            // Ordenar las tiendas por ventas totales en orden descendente
                            $stores = $stores->sortByDesc(function($store) {
                                return $store->orders->isNotEmpty() ? $store->orders->first()->total_final_price : 0;
                            });
                        @endphp

                        @if (count($stores) > 0)
                            @foreach ($stores as $store)
                                <tr>
                                    <td><span class="h6 text-m">{{ $store->name }}</span><br></td>
                                    <td><span class="h6 text-m">{{ $store->created_at }}</span><br></td>
                                    <td><span class="h6 text-m">
                                        @if($store->orders->isNotEmpty())
                                            ${{ $store->orders->first()->total_final_price }}
                                        @else
                                            $0
                                        @endif
                                    </span><br></td>
                                    <td><span class="h6 text-m">
                                        @if($store->orders->isNotEmpty())
                                            {{ $store->orders->first()->total_order_count }}
                                        @else
                                            0
                                        @endif
                                    </span><br></td>
                                    <td><span class="h6 text-m">
                                        @if($store->orders->isNotEmpty())
                                            {{ number_format($store->orders->first()->percentage_of_total, 2) }}%
                                        @else
                                            0%
                                        @endif
                                    </span><br></td>
                                </tr>
                            @endforeach 
                        @else
                            <tr>
                                <td colspan="5" class="text-center">{{ __('No hay datos disponibles') }}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script>
    $(document).ready(function() {
        $('.exportChartButton').click(function() {
            $.ajax({
                url: "{{ route('reports.export-performance') }}",
                type: 'GET',
                success: function(response) {
                    var stores = response.stores;

                    var csvContent = "data:text/csv;charset=utf-8,";
                    csvContent += "Nombre de la Tienda,Fecha,Ventas Totales,Numero de Pedidos,Valor Promedio del Pedido\n";

                    if (stores.length === 0) {
                        csvContent += "No hay datos disponibles\n";
                    } else {
                        stores.forEach(function(store) {
                            var row = [
                                store.name,
                                store.created_at,
                                '$' + store.total_final_price, // Aquí agregamos el signo de pesos
                                store.total_order_count,
                                store.percentage_of_total
                            ];
                            csvContent += row.join(",") + "\n";
                        });
                    }

                    var encodedUri = encodeURI(csvContent);
                    var link = document.createElement("a");
                    link.setAttribute("href", encodedUri);
                    link.setAttribute("download", "Store_Performance.csv");
                    document.body.appendChild(link);
                    link.click();
                },
                error: function(error) {
                    console.error('Error al obtener los datos para exportar', error);
                    alert('Error al exportar los datos. Por favor, inténtelo de nuevo.');
                }
            });
        });
    });
</script>
@endsection