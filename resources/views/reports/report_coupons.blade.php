@extends('layouts.app')

@section('page-title')
    {{ __('Informe de Cupones de Descuento') }}
@endsection

@section('breadcrumb')
    <li class="breadcrumb-item" aria-current="page">{{ __('Informe de Cupones de Descuento') }}</li>
@endsection

@section('action-button')
    <div class="text-end">
        <a href="javascript:;" class="btn btn-sm btn-primary btn-icon exportChartButton" title="{{ __('Exportar') }}"
            data-bs-toggle="tooltip" data-bs-placement="top">
            <i class="ti ti-file-export"></i>
        </a>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-xxl-12">
            <div class="card card-body">
                <form method="GET" action="{{ route('reports.report_coupons') }}">
                    <ul class="nav nav-pills gap-2 gap-3" id="pills-tab" role="tablist">
                        <li class="nav-item mb-2">
                            <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'year' ? 'active' : '' }}" name="chart_data" value="year">{{ __('Year') }}</button>
                        </li>
                        <li class="nav-item mb-2">
                            <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'last-month' ? 'active' : '' }}" name="chart_data" value="last-month">{{ __('Last Month') }}</button>
                        </li>
                        <li class="nav-item mb-2">
                            <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'this-month' ? 'active' : '' }}" name="chart_data" value="this-month">{{ __('This Month') }}</button>
                        </li>
                        <li class="nav-item mb-2">
                            <button type="submit" class="nav-link chart-data {{ request('chart_data') == 'seven-day' ? 'active' : '' }}" name="chart_data" value="seven-day">{{ __('Seven Day') }}</button>
                        </li>
                        <li class="nav-item mb-2">
                            {{ Form::date('start_date', request('start_date') ?: date('Y-m-d'), ['class' => 'date form-control', 'placeholder' => 'YYYY-MM-DD']) }}
                        </li>
                        <li class="nav-item mb-2">
                            {{ Form::date('end_date', request('end_date') ?: date('Y-m-d'), ['class' => 'date form-control', 'placeholder' => 'YYYY-MM-DD']) }}
                        </li>
                        <div class="col-md-2" id="filter_type" style="padding-left: 10px;">
                            <button type="submit" class="btn btn-primary label-margin chart-data generate_button" name="chart_data" value="date-range">{{ __('Generar') }}</button>
                        </div>
                    </ul>
                </form>
            </div>
        </div>
        <div class="col-xxl-12">
            <div class="card">
                <div class="card-header card-body table-border-style">
                    <h5>{{ __('Reporte de Cupones de Descuento') }}</h5>
                    <div class="table-responsive">
                        <table class="table dataTable">
                            <thead>
                                <tr>
                                    <th>{{ __('Código del Cupón') }}</th>
                                    <th>{{ __('Descripción del Cupón') }}</th>
                                    <th>{{ __('Fecha de Uso') }}</th>
                                    <th>{{ __('ID del Pedido') }}</th>
                                    <th>{{ __('Monto de Descuento del Cupón') }}</th>
                                    <th>{{ __('ID Producto(s) Aplicados') }}</th>
                                    <th>{{ __('Total Después del Descuento') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if (count($report_coupons) > 0)
                                    @foreach ($report_coupons as $report_coupon)
                                        <tr>
                                            <td>{{ $report_coupon->coupon_code }}</td>
                                            <td>{{ $report_coupon->coupon_name }}</td>
                                            <td>{{ $report_coupon->updated_at }}</td>
                                            <td>{{ $report_coupon->order_id }}</td>
                                            <td>{{ $report_coupon->coupon_discount_amount }}</td>
                                            <td>{{ $report_coupon->product_order_id }}</td>
                                            <td>{{ $report_coupon->coupon_final_amount }}</td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="7" class="text-center">{{ __('No hay datos disponibles') }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('custom-script')
    <script>
        $(document).ready(function() {
            $('.exportChartButton').click(function() {
                var tableData = @json($report_coupons);

                var csvContent = "data:text/csv;charset=utf-8,";

                // Encabezados específicos
                var headers = ['Codigo del Cupon', 'Descripcion del Cupon', 'Fecha de Uso', 'ID del Pedido', 'Monto de Descuento del Cupon', 'ID Producto(s) Aplicados', 'Total Despues del Descuento'];
                csvContent += headers.join(',') + '\n';

                if (tableData.length === 0) {
                    // Si no hay datos disponibles, solo agrega una fila con el mensaje
                    csvContent += "No hay datos disponibles\n";
                } else {
                    // Filas de datos específicos
                    tableData.forEach(function(row) {
                        var rowData = [
                            row.coupon_code,
                            row.coupon_name,
                            row.updated_at,
                            row.order_id,
                            row.coupon_discount_amount,
                            row.product_order_id,
                            row.coupon_final_amount
                        ];

                        // Asegurar que los datos estén en formato CSV válido
                        var formattedRow = rowData.map(function(field) {
                            return `"${field}"`;
                        }).join(',');

                        csvContent += formattedRow + '\n';
                    });
                }

                // Crear archivo CSV y descargarlo
                var encodedUri = encodeURI(csvContent);
                var link = document.createElement("a");
                link.setAttribute("href", encodedUri);
                link.setAttribute("download", "Informe_de_Cupones_descuento.csv");
                document.body.appendChild(link);
                link.click();
            });
        });
    </script>
@endpush
