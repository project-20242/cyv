<div class="tab-pane fade show active" id="{{ $tab_name }}-home" role="tabpanel" aria-labelledby="{{ $tab_name }}-tab">
    @foreach ($top_sales as $sale)
    <div class="d-flex flex-wrap align-items-center gap-3 mb-2 justify-contant-between">
        <div class="cat-tab-text d-flex align-items-center gap-2">
            <div class="cat-img rounded-1 border overflow-hidden">
                <img src="{{ asset($sale->sale_image_path ?? '#') }}"
                    alt="product-img">
            </div>
            <p class="m-0">{{ $sale->sale_name }}</p>
        </div>
        <div class="order-price">
            <h5 class="m-0">{{ currency_format_with_sym($sale->total_sale, auth()->user()->current_store, APP_THEME()) }}</h5>
        </div>
    </div>
    @endforeach
</div>