@php
    $module = Nwidart\Modules\Facades\Module::find('ProductBarCode');
@endphp
@if(isset($module) && $module->isEnabled())
    @include('productbarcode::pages.qrcode', ['product_id' => $item->product_id ?? null ,'slug' => $slug ?? null])
@endif