@php
    $module = Nwidart\Modules\Facades\Module::find('QuickCheckout');
    $store = \App\Models\Store::where('slug',$slug)->first();
    $user = \App\Models\User::find($store->created_by);
    $enable_quick_checkout =  \App\Models\Utility::GetValueByName('enable_quick_checkout',$currentTheme, $store->id);
    $plan = \App\Models\Plan::find($user->plan_id);
                    
@endphp
@if(isset($module) && $module->isEnabled())
    {{-- Include the module blade button --}}
    @if (isset($plan)) 
        @if (strpos($plan->modules, 'QuickCheckout') !== false) 
            @if($enable_quick_checkout == 'on')
                @include('quickcheckout::theme.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
            @endif
        @endif
    @endif
@endif