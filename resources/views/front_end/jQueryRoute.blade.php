<div class="modal modal-popup" id="commanModel" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-inner lg-dialog" role="document">
            <div class="modal-content">
                <div class="popup-content">
                    <div class="modal-header  popup-header align-items-center">
                        <div class="modal-title">
                            <h6 class="mb-0" id="modelCommanModelLabel"></h6>
                        </div>
                        <button type="button" class="close close-button" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                    </div>
                </div>

            </div>
        </div>
    </div>

   
        <!-- [ Main Content ] start -->
        <div class="wrapper">
            <div class="pct-customizer">
                <div class="pct-c-btn">
                    <button class="btn btn-primary" id="pct-toggler" data-toggle="tooltip"
                        data-bs-original-title="Order Track" aria-label="Order Track">
                        <i class='fas fa-shipping-fast' style='font-size:24px;'></i>
                    </button>
                </div>
                <div class="pct-c-content">
                    <div class="pct-header bg-primary">
                        <h5 class="mb-0 f-w-500">{{ 'Order Tracking' }}</h5>
                    </div>
                    <div class="pct-body">
                        {{ Form::open(['route' => ['order.track', $slug], 'method' => 'POST', 'id' => 'choice_form', 'enctype' => 'multipart/form-data']) }}
                        <div class="form-group col-md-12">
                            {!! Form::label('order_number', __('Order Number'), ['class' => 'form-label']) !!}
                            {!! Form::text('order_number', null, ['class' => 'form-control', 'placeholder' => 'Enter Your Order Id']) !!}
                        </div>
                        <div class="form-group col-md-12">
                            {!! Form::label('email', __('Email Address'), ['class' => 'form-label']) !!}
                            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter email']) !!}
                        </div>
                        <button type="submit" class="btn justify-contrnt-end">{{ __('Submit') }}</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="addon-side-btn">
            @stack('couponListButton')
            @stack('SpinwheelButton')
        </div>
    <!-- [ Main Content ] end -->
   <!--  jQuery Validation  -->
   <script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
   <script src="{{ asset('js/jquery-cookie.min.js') }}"></script>
@stack('recentViewModelPopup')

@stack('subscribeStorePopup')
@stack('countDownPopup')
@stack('tawktoModelPopup')
@stack('purchaseNotificationPopup')
@stack('wizzchatModelPopup')
<!--scripts end here-->
@if(isset($storejs))
{!! $storejs !!}
@endif
<!--scripts start here-->
<script>
    var guest = '{{ Auth::guest() }}';
    var filterBlog = "{{ route('blogs.filter.view',$store->slug) }}";
    var cartlistSidebar = "{{ route('cart.list.sidebar',$store->slug) }}";
    var ProductCart = "{{ route('product.cart',$store->slug) }}";
    var addressbook_data = "{{ route('get.addressbook.data', $store->slug) }}";
    var shippings_data = "{{ route('get.shipping.data', $store->slug) }}";
    var get_shippings_data = "{{ route('get.shipping.data', $store->slug) }}";
    var shippings_methods = "{{ route('shipping.method', $store->slug) }}";
    var apply_coupon = "{{ route('applycoupon', $store->slug) }}";
    var paymentlist = "{{ route('paymentlist', $store->slug) }}";
    var additionalnote = "{{ route('additionalnote', $store->slug) }}";
    var state_list = "{{ route('states.list', $store->slug) }}";
    var city_list = "{{ route('city.list', $store->slug) }}";
    var changeCart = "{{ route('change.cart', $store->slug) }}";
    var wishListSidebar = "{{ route('wish.list.sidebar', $store->slug) }}";
    var removeWishlist = "{{ route('delete.wishlist', $store->slug) }}";
    var addProductWishlist = "{{ route('product.wishlist', $store->slug) }}";
    var isAuthenticated = "{{ auth('customers')->check() ? 'true' : 'false' }}";
    var removeCart = "{{  route('cart.remove', $store->slug)  }}";
    var productPrice = "{{ route('product.price', $store->slug) }}";
    var wishList = "{{ route('wish.list',$store->slug) }}";
    var whatsappNumber = "{{ $whatsapp_contact_number ?? '' }}";
    var taxes_data = "{{ route('get.tax.data', $store->slug) }}";
    var searchProductGlobaly = "{{ route('search.product', $store->slug) }}";
    var loginUrl = "{{ route('customer.login', $store->slug) }}";
    var payfast_payment_guest = "{{ route('place.order.guest', $store->slug) }}";
    var payfast_payment = "{{ route('payment.process', $store->slug) }}";
</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://d3js.org/d3.v3.min.js"></script>
<script src="{{ asset('assets/js/front-theme.js') }}" defer="defer"></script>
    @if ($store->enable_pwa_store == 'on')
        <script type="text/javascript">
            const container = document.querySelector("body")

            const coffees = [];

            if ("serviceWorker" in navigator) {
                window.addEventListener("load", function() {
                    navigator.serviceWorker
                        .register("{{ asset('serviceWorker.js') }}")
                        .then(res => console.log(""))
                        .catch(err => console.log("service worker not registered", err))

                })
            }
        </script>
    @endif
