<?php

namespace App\Imports;

use App\Models\Addon;
use App\Models\Product;
use App\Models\MainCategory;
use App\Models\ProductBrand;
use App\Models\ProductLabel;
use App\Models\Shipping;
use App\Models\Store;
use App\Models\SubCategory;
use App\Models\Tag;
use App\Models\Tax;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Log;

class ProductsImport implements ToModel, WithHeadingRow
{
    private $tag;
    private $categories;
    private $subcategories;
    private $brand;
    private $label;
    private $tax;
    private $shipping;
    private $store;
    private $storeId;
    private $userId;

    public function __construct($storeId, $userId)
    {
        $this->tag = Tag::pluck('id', 'name');
        $this->categories = MainCategory::pluck('id', 'name');
        $this->subcategories = SubCategory::pluck('id', 'name');
        $this->brand = ProductBrand::pluck('id', 'name');
        $this->label = ProductLabel::pluck('id', 'name');
        $this->tax = Tax::pluck('id','name');
        $this->shipping = Shipping::pluck('id', 'name');
        $this->store = Store::pluck('id', 'name');
        $this->storeId = $storeId;
        $this->userId = $userId;
    }

    public function model(array $row)
    {
        if (!isset($row['name']) || is_null($row['name'])) {
            Log::error('El campo "name" esta ausente o es nulo en la fila:', $row);
            return null;
        }
        if (filter_var($row['cover_image_path'], FILTER_VALIDATE_URL)) {
            // Es una URL válida
            $coverImagePath = $row['cover_image_path'];
        } else {
            // Si no es una URL, manejar como archivo local
            // Aquí podrías manejar la lógica para cargar archivos locales si es necesario
            // Por ejemplo, moviendo el archivo a una ubicación pública
            $coverImagePath = 'storage/app/public/' . $row['cover_image_path'];
        }
        Log::info($row);
            $product =  new Product([
            $trending = $row['trending'] == 'yes' ? 1 : 0,
            $status = $row['status'] == 'Active' ? 1 : 0,
            $track_stock = $row['track_stock'] == 'on' ? 1 : 0,
            $variant_product = $row['variant_product'] == 'variant' ? true : false,
            $custom_field_status = $row['custom_field_status'] == 'yes' ? true : false,
            'name' => $row['name'],
            'slug' => $row['slug'],
            // 'tag_id' => $this->tag[$row['tag']],
            'maincategory_id' => $this->categories[$row['main_category']],
            'subcategory_id' =>  $this->subcategories[$row['sub_category']],
            'brand_id' =>  $this->brand[$row['brand']],
            'label_id' =>  $this->label[$row['label']],
            'tax_id' => $this->tax[$row['tax']],
            'tax_status' => $row['tax_status'],
            'shipping_id' => $this->shipping[$row['shipping']],
            'preview_type' => $row['preview_type'],
            'preview_video' => $row['preview_video'],
            'preview_content' => $row['preview_content'],
            'trending' => $trending,
            'status' => $status,
            'video_url' => $row['video_url'],
            'track_stock' => $track_stock,
            'stock_order_status' => $row['stock_order_status'],
            'price' => $row['price'],
            'sale_price' => $row['sale_price'],
            'product_stock' => $row['product_stock'],
            'low_stock_threshold' => $row['low_stock_threshold'],
            'downloadable_product' => $row['downloadable_product'],
            'product_weight' => $row['product_weight'],
            'cover_image_path' => $coverImagePath,
            'cover_image_url' => $row['cover_image_url'],
            'stock_status' => $row['stock_status'],
            'variant_product' => $variant_product,
            'attribute_id' => $row['attribute'],
            'product_attribute' => $row['product_attribute'],
            'custom_field_status' => $custom_field_status,
            'custom_field' => $row['custom_field'],
            'description' => $row['description'],
            'detail' => $row['detail'],
            'specification' => $row['specification'],
            'average_rating' => $row['average_rating'],
            'product_type' => $row['product_type'],
            'theme_id' =>  $row['theme'],
            'store_id' =>  $this->store[$row['store']],
            // 'store_id' =>  $this->storeId,
            'created_by' => $this->userId,

        ]);
        $product->save();

        return $product;
    }
}
