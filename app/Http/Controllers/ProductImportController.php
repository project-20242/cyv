<?php

namespace App\Http\Controllers;

use App\Models\Store;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ProductsImport;
use App\Models\Product;
use Illuminate\Support\Facades\Auth;
use ZipArchive;
use File;
use Storage;

class ProductImportController extends Controller
{
     public function index()
     {
         $products = Product::all();
         // Obtener la tienda y el usuario actual
         $storeId = Auth::user()->store_id;
         $userId = Auth::id();
        
         // Obtener los productos que pertenecen a la tienda y usuario actual
         $product = Product::where('store_id', $storeId)
                            ->where('created_by', $userId)
                            ->with(['mainCategory', 'subCategory', 'brand', 'label', 'tag', 'tax', 'shipping', 'attribute', 'theme', 'store', 'user'])
                            ->get();
        
         return view('product.index', compact('product'));
     }

    public function showImportForm()
    {
        return view('product.import');
    }

    public function import(Request $request)
    {
        // Validar el archivo de importación
        $request->validate([
            'file' => 'required|mimes:xlsx,xls',
        ]);

        // Obtener la tienda y el usuario actual
        $storeId = Auth::user()->store_id;
        $userId = Auth::id();

        // Importar los productos
        Excel::import(new ProductsImport($storeId, $userId), $request->file('file'));

        // Redirigir con un mensaje de éxito
        return redirect()->route('product.index')->with('success', 'Products imported successfully.');
    }

    public function destroy($id)
    {
        // Buscar y eliminar el producto
        $product = Product::findOrFail($id);
        $product->delete();

        // Redirigir con un mensaje de éxito
        return redirect()->route('product.index')->with('success', 'Product deleted successfully');
    }
    public function showImportImage(){

        return view('product.import_image');
    }
    public function importImages(Request $request)
    {
        // Personaliza los mensajes de error para la validación
        $messages = [
            'file.required' => 'El archivo es obligatorio.',
            'file.mimes' => 'Solo se permiten archivos con extensión .zip.',
        ];
    
        // Realiza la validación del archivo
        $validator = \Validator::make($request->all(), [
            'file' => 'required|mimes:zip',
        ], $messages);
    
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();
        }
    
        // Obtener el archivo ZIP
        $file = $request->file('file');
        $zip = new ZipArchive;
    
        if ($zip->open($file->path()) === TRUE) {
            // Extraer en una carpeta temporal
            $tempPath = storage_path('app/public/uploads/temp');
            if (!File::exists($tempPath)) {
                File::makeDirectory($tempPath, 0755, true);
            }
            $zip->extractTo($tempPath);
            $zip->close();
            \Log::info('ZIP file extracted to: ' . $tempPath);
        } else {
            \Log::error('Failed to open the ZIP file');
            return redirect()->back()->withErrors(['file' => 'Failed to open the ZIP file']);
        }
    
        $subdirectories = scandir($tempPath);
        \Log::info('Subdirectories in extract path: ' . json_encode($subdirectories));
    
        foreach ($subdirectories as $subdir) {
            if ($subdir != "." && $subdir != "..") {
                $subdirPath = $tempPath . DIRECTORY_SEPARATOR . $subdir;
                if (is_dir($subdirPath)) {
                    $files = scandir($subdirPath);
                    \Log::info("Files in subdirectory ({$subdir}): " . json_encode($files));
    
                    foreach ($files as $file) {
                        if ($file != "." && $file != "..") {
                            $filePath = $subdirPath . DIRECTORY_SEPARATOR . $file;
                            \Log::info('Extracted file path: ' . $filePath);
    
                            $filename = pathinfo($filePath, PATHINFO_FILENAME);
                            $extension = pathinfo($filePath, PATHINFO_EXTENSION);
                            \Log::info('Processing file: ' . $filename . '.' . $extension);
    
                            if (preg_match('/(\d+)_([A-Z])/', $filename, $matches)) {
                                $id = $matches[1];
                                $imageSuffix = $matches[2];
                                \Log::info('Matched ID: ' . $id . ' with suffix: ' . $imageSuffix);
    
                                // Obtener el ID del tema desde la tabla product
                                $product = Product::find($id);
                                if ($product) {
                                    $themeId = $product->theme_id;
                                    $newFilePath = "themes/{$themeId}/uploads/{$id}_{$imageSuffix}.{$extension}";
                                    $storagePath = base_path($newFilePath);
                                    \Log::info('Moving file to: ' . $storagePath);
    
                                    if (!File::exists(dirname($storagePath))) {
                                        File::makeDirectory(dirname($storagePath), 0755, true);
                                    }
    
                                    if (File::move($filePath, $storagePath)) {
                                        \Log::info('File moved to: ' . $storagePath);
                                        if ($imageSuffix == 'A') {
                                            $product->cover_image_path = $newFilePath;
                                        } elseif ($imageSuffix == 'B') {
                                            $product->cover_image_url = $newFilePath;
                                        }
                                        $product->save();
                                        \Log::info('Updated product ID: ' . $id);
                                    } else {
                                        \Log::error('Failed to move the file to the new location');
                                        return redirect()->back()->withErrors(['file' => 'Failed to move the file to the new location']);
                                    }
                                // } else {
                                //     \Log::error('Product not found with ID: ' . $id);
                                //     return redirect()->back()->withErrors(['producto' => "Producto con ID {$id} no encontrado"]);
                                }
                            } else {
                                \Log::error('Filename does not match expected pattern: ' . $filename);
                            }
                        }
                    }
                }
            }
        }
    
        // Limpiar la carpeta temporal después de procesar
        File::deleteDirectory($tempPath);
    
        return redirect()->route('product.index')->with('success', 'Images imported successfully');
    }
}
