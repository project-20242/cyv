<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use App\Models\Store;
use App\Models\Plan;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rules;
use Illuminate\View\View;
use Illuminate\Support\Facades\Http;

class RegisteredUserController extends Controller
{
    public function __construct(Request $request)
    {
        if (!file_exists(storage_path() . "/installed")) {
            header('location:install');
            die;
        }
    }

    /**
     * Display the registration view.
     */
    public function create(): View
    {
        return view('auth.register');
    }

    /**
     * Handle an incoming registration request.
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request): RedirectResponse
    {
        $settings = \App\Models\Utility::Seting();
        
        // Validación de Recaptcha si está habilitado en la configuración
        if (isset($settings['RECAPTCHA_MODULE']) && $settings['RECAPTCHA_MODULE'] == 'yes') {
            $captchaResponse = $request->input('g-recaptcha-response');
            if (empty($captchaResponse)) {
                return redirect()->back()->with('status', __('Please check RECAPTCHA.'));
            }

            $captchaSecretKey = $settings['NOCAPTCHA_SECRET'] ?? null;
            $response = Http::asForm()->post('https://www.google.com/recaptcha/api/siteverify', [
                'secret' => $captchaSecretKey,
                'response' => $captchaResponse,
            ]);

            $captchaResult = $response->json();

            if (!$captchaResult['success'] || $captchaResult['score'] < 0.5) {
                return redirect()->back()->with('status', __('RECAPTCHA validation failed.'));
            }
        }
        
        // Validación de los datos del usuario
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'lowercase', 'email', 'max:255', 'unique:' . User::class],
            'password' => ['required', 'confirmed', Rules\Password::defaults()],
            'store_name' => ['required', 'string', 'max:255'],
        ]);

        // Creación del usuario
        $superAdmin = User::where('type', 'super admin')->first();

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'profile_image' => 'uploads/profile/avatar.png',
            'type' => 'admin',
            'password' => Hash::make($request->password),
            'mobile' => '',
            'default_language' => $superAdmin->default_language ?? 'en',
            'created_by' => 1,
            'theme_id' => 'grocery',
        ]);

        // Creación de la tienda asociada al usuario
        $slug = User::slugs($request->store_name);

        $store = Store::create([
            'name' => $request->store_name,
            'email' => $request->email,
            'theme_id' => $user->theme_id,
            'slug' => $slug,
            'created_by' => $user->id,
            'default_language' => $superAdmin->default_language ?? 'en'
        ]);

        $user->current_store = $store->id;
        $user->save();

        // Verificación de la validez del plan y la cuenta del usuario
        if ($user->type == 'admin') {
            $plan = Plan::find($user->plan_id);
            if ($plan) {
                if ($plan->duration != 'Unlimited') {
                    $datetime1 = new \DateTime($user->plan_expire_date);
                    $datetime2 = new \DateTime(date('Y-m-d'));
                    $interval = $datetime2->diff($datetime1);
                    $days = $interval->format('%r%a');
                    if ($days <= 0) {
                        $user->assignPlan(1);

                        return redirect()->intended(RouteServiceProvider::HOME)->with('error', __('Your Plan is expired.'));
                    }
                }

                if ($user->trial_expire_date != null) {
                    if (\Auth::user()->trial_expire_date < date('Y-m-d')) {
                        $user->assignPlan(1);

                        return redirect()->intended(RouteServiceProvider::HOME)->with('error', __('Your Trial plan Expired.'));
                    }
                }
            }
        }

        // Asignación de rol y configuración de verificación de email si está habilitada
        $role_r = Role::where('name', 'admin')->first();
        $user->addRole($role_r);

        if ($settings['email_verification'] == "on") {
            try {
                event(new Registered($user));
                Auth::login($user);
            } catch (\Exception $e) {
                $user->delete();
                return redirect('/register')->with('status', __('Email SMTP settings do not configure, please contact site admin.'));
            }
            return redirect()->route('verify-email');
        } else {
            Auth::login($user);
            return redirect(RouteServiceProvider::HOME);
        }
    }

    public function verify_email()
    {
        return view('auth.verify-email');
    }
}
