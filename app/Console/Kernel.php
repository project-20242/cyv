<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Events\Dispatcher;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [];

    /**
     * Create a new command scheduler instance.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function __construct(Application $app, Dispatcher $events)
    {
        parent::__construct($app, $events);
        
        // if ($this->app->runningInConsole()) {
        //     if (module_is_active('AuctionProduct')) {
        //         $this->commands[] = \Modules\AuctionProduct\app\Console\Auction::class;
        //     }
        // }
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        // // $schedule->command('inspire')->hourly();
        // if ($this->app->runningInConsole()) {
        //     if (module_is_active('AuctionProduct') && class_exists('\Modules\AuctionProduct\app\Console\Auction')) {
        //         $schedule->command('auction:order')->everyMinute();
        //     }
        // }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(): void
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
