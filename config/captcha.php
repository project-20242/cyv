<?php

return [
    'enabled' => env('RECAPTCHA_ENABLED', false), // Por defecto desactivado (false)
    'sitekey' => env('NOCAPTCHA_SITEKEY'),
    'secret' => env('NOCAPTCHA_SECRET'),
    'options' => [
        'timeout' => 30,
    ],
    'version' => 'v3',
];

