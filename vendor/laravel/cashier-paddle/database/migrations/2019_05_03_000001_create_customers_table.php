<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        if (!Schema::hasTable('customers')) {
            Schema::create('customers', function (Blueprint $table) {
                $table->id();
                $table->unsignedBigInteger('billable_id')->nullable();
                $table->string('billable_type')->nullable();
                $table->timestamp('trial_ends_at')->nullable();
                $table->timestamps();
    
                $table->index(['billable_id', 'billable_type']);
            });
        } else {
            Schema::table('customers', function (Blueprint $table) {
                $table->unsignedBigInteger('billable_id')->nullable();
                $table->string('billable_type')->nullable();
                $table->timestamp('trial_ends_at')->nullable();
            });
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('customers');
    }
}
