@extends('front_end.layouts.app')
@section('page-title')
{{ __('Article Page') }}
@endsection
@section('content')
@include('front_end.sections.partision.header_section')
@foreach ($blogs as $blog)

<section class="blog-page-banner common-banner-section" style="background-image:url({{asset('themes/'.$currentTheme.'/assets/images/blog-banner.png')}});">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="common-banner-content">
                            <ul class="blog-cat">
                                <li class="active">{{ __('Destacado')}}</li>
                                <li><b> {{__('Category')}}: </b> {{ $blog->category->name }}</li>
                                <li><b>{{__('Date')}}:</b> {{ $blog->created_at->format('d M,Y ') }}</li>
                            </ul>
                            <div class="section-title">
                                <h2>{!! $blog->title !!}</h2>
                            </div>
                            <p class="description">{!!$blog->short_description!!}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="article-section padding-top">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="about-user d-flex align-items-center">
                            <div class="abt-user-img">
                                <img src="{{asset('themes/'.$currentTheme.'/assets/images/john.png')}}">
                            </div>
                            <h6>
                                <span>John Doe,</span>
                                company.com
                            </h6>
                            <div class="post-lbl"><b>{{__('Category')}}:</b>{{$blog->category->name}}</div>
                            <div class="post-lbl"><b>{{__('Date')}}:</b>{{$blog->created_at->format('d M, Y ')}}</div>
                        </div>
                    </div>
                    <div class="col-md-8 col-12">
                        <div class="aticleleftbar">
                            <p>{!! html_entity_decode($blog->content) !!}</p>

                            <div class="art-auther"><b>John Doe</b>, <a href="company.com">company.com</a></div>

                            <div class="art-auther"><b>{{ __('Tags:')}}</b> {{$blog->category->name}}</div>

                            <ul class="article-socials d-flex align-items-center">
 
                                    <li><span>{{ __('Share :') }}</span></li>
                                    
                                    @for($i=0 ; $i < $section->footer->section->footer_link->loop_number ?? 1;$i++) <li>
                        <a href="{!!$section->footer->section->footer_link->social_link->{$i} ?? '#' !!}">
                            <img src=" {{  asset($section->footer->section->footer_link->social_icon->{$i}->image ?? 'themes/' . $currentTheme . '/assets/images/youtube.svg') }}"
                                alt="youtube" style="margin-bottom: inherit;">
                        </a>

                        </li>
                        @endfor
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-12">
                        <div class="articlerightbar blog-grid-section">
                            <div class="section-title">
                                <h3>{{__('Related articles')}}</h3>
                            </div>
                            @foreach ($datas as $data)
                            <div class="blog-card">
                               
                                <div class="blog-card-inner">
                                    <div class="blog-card-image">
                                    <span class="label">Articles</span>
                                    <a href="{{route('page.article',[$slug,$data->id])}}" tabindex="0">
                                        <img src="{{asset($data->cover_image_path)}}">
                                    </a>
                                    </div>
                                    <div class="blog-card-content">
                                        <div class="blog-card-content-top">
                                            <h3>
                                                <a href="{{route('page.article',[$slug,$data->id])}}" tabindex="0" class="description">{{$data->title}}
                                                </a>
                                            </h3>
                                            <p class="description">{{$data->short_description}}</p>
                                        </div>
                                        <div class="blog-card-content-bottom">
                                            <div class="blog-card-author-name">
                                                <span>@johndoe</span>
                                                <span>{{$data->created_at->format('d M,Y ')}}</span>
                                            </div>
                                            <a href="{{route('page.article',[$slug,$data->id])}}" class="btn">
                                                {{ __('READ MORE')}}
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>


@endforeach
<section class="blog-grid-section padding-top padding-bottom tabs-wrapper">
            <div class="container">
                
                <div class="section-title d-flex justify-content-between align-items-end">
                    <div>
                        <div class="subtitle">{{ __('ALL BLOGS')}}</div>
                        <h2>{{__('From our blog')}} </h2>
                    </div>
                    <a href="{{route('page.blog',$slug)}}" class="btn">
                        {{__('Read More')}}
                    </a>
                </div>
                {!! \App\Models\Blog::ArticlePageBlog($currentTheme, $slug) !!}
            </div>
        </section>
@include('front_end.sections.partision.footer_section')
@endsection