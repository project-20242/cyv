<section class="services-section padding-bottom"
    style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        @php
            $i = 0;
        @endphp
        
        <div class="row service-row">
            @for ($i = 0; $i < 3 ?? 1; $i++) <div class="col-md-4 col-sm-6 col-12 service-col">
                <div class="services-card">
                    <div class="services-card-inner">
                            <img src="{{asset($section->service_section->section->image->image->{$i} ?? '')}}"
                                alt="service">
                        <div class="services-text">
                            <h3 id="{{ ($section->service_section->section->sub_title->slug ?? '') .'_'. $i}}_preview">
                                {!!
                                $section->service_section->section->sub_title->text->{$i} ?? '' !!}
                            </h3>
                            <a href="{{route('page.product-list',$slug)}}" class="btn show-more-btn"
                                id="{{ ($section->service_section->section->button_secondary->slug ?? '') .'_'. $i}}_preview">
                                {!! $section->service_section->section->button_secondary->text->{$i} ?? '' !!} </a>
                        </div>
                    </div>
                </div>
        </div>
        @endfor
    </div>
    </div>
</section>