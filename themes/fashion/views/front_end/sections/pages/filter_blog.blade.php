@foreach ($blogs as $blog)
@if($request->cat_id == '0' || $blog->category_id == $request->cat_id)
<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 blog-itm">
    <div class="blog-inner">
        <div class="blog-img">
            <a href="{{route("page.article",[$slug,$blog->id])}}">
                <img src="{{asset($blog->cover_image_path)}}" alt="" class="cover_img{{$blog->id}} ">
            </a>
        </div>
        <div class="blog-content">
            <h3><a href="{{route("page.article",[$slug,$blog->id])}}" class="short_description">{{$blog->title}}</a>
            </h3>
            <p class="descriptions">{{$blog->short_description}}</p>
            <a href="{{route("page.article",[$slug,$blog->id])}}" class="btn-secondary" target="_blank">
                {{ __('Read more') }}
            </a>
        </div>
    </div>
</div>

@endif

@endforeach