<div class="blog-slider">
    @foreach ($landing_blogs as $blog)
    <div class="blog-itm">
        <div class="blog-inner">
            <div class="blog-img">
                <a href="{{route('page.article',[$slug,$blog->id])}}">
                    <img src="{{asset($blog->cover_image_path )}}"  alt="blog-img">
                </a>
            </div>
            <div class="blog-content">
                <h3><a href="{{route('page.article',[$slug,$blog->id])}}" class="short_description">{{$blog->title}}</a> </h3>
                <p>{{$blog->short_description}}</p>
                <a href="{{route('page.article',[$slug,$blog->id])}}" class="btn-secondary">
                    {{ __('Read more') }}
                </a>
            </div>
        </div>
    </div>
    @endforeach
</div>