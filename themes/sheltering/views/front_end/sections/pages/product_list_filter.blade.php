@foreach ($products as $product)


    <div class="col-lg-4 col-md-6 col-sm-6 col-12 product-card">
        <div class="product-card-inner">
        <div class="card-top">
            <span class="slide-label">{{ $product->ProductData->name ?? '' }}</span>
                <a href="javascript:void(0)" class="wishlist wbwish  wishbtn-globaly" product_id="{{$product->id}}" in_wishlist="{{ $product->in_whishlist ? 'remove' : 'add'}}">
                    <span class="wish-ic">
                        <i class="{{ $product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}" style='color: black'></i>
                    </span>
                </a>
                <div class="product-btn-wrp">
                    @php
                        $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                        $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                    @endphp
                    @if(isset($module) && $module->isEnabled())
                        {{-- Include the module blade button --}}
                        @include('productquickview::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                    @endif
                    @if(isset($compare_module) && $compare_module->isEnabled())
                        {{-- Include the module blade button --}}
                        @include('productcompare::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                    @endif
                </div>
        </div>
        <h4 class="product-title">
            <a href="{{url($slug.'/product/'.$product->slug)}}" class="short-description">
                {{$product->name}}
            </a>
        </h4>
        <div class="product-card-image">
            <a href="{{url($slug.'/product/'.$product->slug)}}">
                <img src="{{ asset('storage/app/public/' . $product->cover_image_path) }}" alt="Cover Image" width="100" height="100" class="default-img">
                @if($product->Sub_image($product->id)['status'] == true)
                    <img src="{{ asset($product->Sub_image($product->id)['data'][0]->image_path) }}" class="hover-img">
                @else
                    <img src="{{ asset($product->Sub_image($product->id)) }}" class="hover-img">
                @endif
            </a>
        </div>
        <div class="product-content">
            <div class="product-content-bottom d-flex align-items-center justify-content-between">
                {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $product->id) !!}
                @if ($product->variant_product == 0)
                        <div class="price">
                            <ins>{{ currency_format_with_sym(($product->sale_price ?? $product->price), $store->id, $currentTheme)}}
                            </ins>
                        </div>
                    @else
                    <div class="price">
                        <ins>{{ __('In Variant') }}</ins>
                    </div>
                @endif
            <a href="javascript:void(0)" class="btn-primary add-cart-btn addcart-btn-globaly" product_id="{{ $product->id }}" variant_id="0" qty="1">
                {{ __('Add to cart')}}
            </a>
            </div>
        </div>
        </div>
    </div>

@endforeach

@php
    $page_no = !empty($page) ? $page : 1;
@endphp

<div class="d-flex justify-content-end col-12">
    <nav class="dataTable-pagination">
        <ul class="dataTable-pagination-list">
            <li class="pagination">
            {{ $products->onEachSide(0)->links('pagination::bootstrap-4') }}
            </li>
        </ul>
    </nav>
</div>
