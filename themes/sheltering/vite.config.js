import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";
import path from "path";



export default defineConfig({
    plugins: [
        laravel({
            input: [
                "themes\sheltering\sass/app.scss",
                "themes\sheltering\js/app.js"
            ],
            buildDirectory: "sheltering",
        }),
        
        
        {
            name: "blade",
            handleHotUpdate({ file, server }) {
                if (file.endsWith(".blade.php")) {
                    server.ws.send({
                        type: "full-reload",
                        path: "*",
                    });
                }
            },
        },
    ],
    resolve: {
        alias: {
            '@': '/themes\sheltering\js',
            '~bootstrap': path.resolve('node_modules/bootstrap'),
        }
    },
    
});
