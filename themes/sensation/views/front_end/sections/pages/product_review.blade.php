@if(!empty($product_review))
<div class="review-item">
    <div class="review-item-inner">
        <div class="review-item-star d-flex align-items-center">
            <div class="review-item-star-icon">
                @for ($i = 0; $i < 5; $i++)
                    <i class="ti ti-star {{ $i < $product_review->rating_no ? 'text-warning' : '' }} "></i>
                @endfor
            </div>
            <span>{{ $product_review->rating_no }}.0<b> / 5.0 </b> </span>
        </div>
        <h3>{{ $product_review->title }}</h3>
        <p>{{ $product_review->description }}</p>
        <div class="review-bottom d-flex align-items-center">
            <div class="about-user d-flex align-items-center">
                <div class="abt-user-badge"></div>
                <p>{{!empty($product_review->UserData) ? ($product_review->UserData->first_name.',') : '' }} <span> {{ $product_review->ProductData->name }} </span> </p>
            </div>
        </div>
    </div>
</div>
@endif


