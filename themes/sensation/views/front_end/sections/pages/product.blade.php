@extends('front_end.layouts.app')
@section('page-title')
{{ __('Products') }}
@endsection
@php

$latestSales = \App\Models\Product::productSalesTag($currentTheme, $slug, $product->id);
@endphp

@section('content')
@include('front_end.sections.partision.header_section')
<section class="product-page-first-section">

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12 pdp-left-column">
                <div class="pdp-left-inner-sliders">
                    <div class="main-slider-wrp">
                        <div class="pdp-main-slider lightbox">
                            @foreach ($product->Sub_image($product->id)['data'] as $item)
                            <div class="pdp-main-slider-itm">
                                <div class="pdp-main-img">
                                    <img src="{{ asset($item->image_path) }}">
                                    <a href="{{ asset($item->image_path) }}" data-caption="{{ $product->name }}"
                                        class="open-lightbox">
                                        <svg width="11" height="11" viewBox="0 0 11 11" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M8.01711 8.89563C7.17556 9.5498 6.11811 9.93935 4.96967 9.93935C2.225 9.93935 0 7.71435 0 4.96967C0 2.225 2.225 0 4.96967 0C7.71435 0 9.93935 2.225 9.93935 4.96967C9.93935 6.11811 9.5498 7.17556 8.89563 8.01711L10.8179 9.93925C11.0605 10.1819 11.0605 10.5752 10.8179 10.8178C10.5753 11.0604 10.182 11.0604 9.93941 10.8178L8.01711 8.89563ZM8.69693 4.96967C8.69693 7.02818 7.02818 8.69693 4.96967 8.69693C2.91117 8.69693 1.24242 7.02818 1.24242 4.96967C1.24242 2.91117 2.91117 1.24242 4.96967 1.24242C7.02818 1.24242 8.69693 2.91117 8.69693 4.96967Z"
                                                fill="#12131A"></path>
                                        </svg>
                                    </a>

                                    @foreach ($latestSales as $productId => $saleData)
                                    <div class="custom-output sale-tag-product">
                                        <div class="sale_tag_icon rounded col-1 onsale">
                                            <div>{{ __('Sale!') }}</div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="pdp-thumb-wrap">
                            <div class="pdp-thumb-slider common-arrows">
                                @foreach ($product->Sub_image($product->id)['data'] as $item)
                                <div class="pdp-thumb-slider-itm">
                                    <div class="pdp-thumb-img">
                                        <img src="{{ asset($item->image_path) }}">

                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <div class="pdp-thumb-nav-wrap">
                                <div class="pdp-thumb-nav">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-12 pdp-right-column">
                <div class="product-detail-section">
                    <div class="product-detail-star-section">
                        {!! \App\Models\Testimonial::ProductReview($currentTheme, 1, $product->id) !!}
                        <div class="wishlist">
                            <a href="javascript:void(0)" class="wishbtn variant_form wishbtn-globaly"
                                product_id="{{ $product->id }}"
                                in_wishlist="{{ $product->in_whishlist ? 'remove' : 'add' }}">
                                {{ __('Add to wishlist') }}
                                <span class="wish-ic">
                                    <i class="{{ $product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"></i>
                                </span>
                            </a>
                            <div class="product-btn-wrp">
                                @php
                                    $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                    $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                @endphp
                                @if(isset($module) && $module->isEnabled())
                                    {{-- Include the module blade button --}}
                                    @include('productquickview::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                @endif
                                @if(isset($compare_module) && $compare_module->isEnabled())
                                    {{-- Include the module blade button --}}
                                    @include('productcompare::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="product-detail-title">
                        <h2 class="product-heading">{{ $product->name }}</h2>
                        <h4>{{ $product->ProductData->name }}</h4>
                    </div>

                    <div class="text-checkbox checkbox-radio d-flex align-items-center">
                        <p>{{ __('Select Scent:') }}</p>
                        @foreach ($main_pro->take(2) as $pro)
                        <div class="checkbox">
                            <input id="Select-scent-1" name="checkbox" type="checkbox" value="Mandarin">
                            <label for="Select-scent-1" class="checkbox-label">
                                <a href="{{ url($slug.'/product/'.$pro->id) }}">{{ $pro->name }}</a>
                            </label>
                        </div>
                        @endforeach
                    </div>
                    <div class="pdp-block pdp-info-block product-variant-description">
                        <p>{{ strip_tags($product->description) }}</p>
                    </div>
                    <div class="price product-price-amount">
                        <ins>
                            <ins class="min_max_price" style="display: none;">
                                {{ $currency_icon }}{{ $mi_price }} -
                                {{ $currency_icon }}{{ $ma_price }} </ins>
                        </ins>
                    </div>
                    @if ($product->variant_product == 1)
                    <h6 class="enable_option">
                        @if ($product->product_stock > 0)
                        <span class="stock">{{ $product->product_stock }}</span><small>{{ __(' in stock') }}</small>
                        @endif
                    </h6>
                    @else
                    <h6>
                        @if ($product->track_stock == 0)
                        @if ($product->stock_status == 'out_of_stock')
                        <span>{{ __('Out of Stock') }}</span>
                        @elseif ($product->stock_status == 'on_backorder')
                        <span>{{ __('Available on backorder') }}</span>
                        @else
                        <span></span>
                        @endif
                        @else
                        @if ($product->product_stock > 0)
                        <span>{{ $product->product_stock }} {{ __('  in stock') }}</span>
                        @endif
                        @endif
                    </h6>
                    @endif
                    <span class="product-price-error"></span>
                    @if ($product->track_stock == 0 && $product->stock_status == 'out_of_stock')
                    @else
                    <form class="variant_form w-100">
                        @csrf
                        <div class="prorow-lbl-qntty">
                            <div class="product-labl d-block">{{ __('quantity') }}</div>
                            <div class="qty-spinner">
                                <button type="button" data-product="{{ $product->id }}"
                                    class="quantity-decrement change_price">
                                    <svg width="12" height="2" viewBox="0 0 12 2" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0 0.251343V1.74871H12V0.251343H0Z" fill="#61AFB3">
                                        </path>
                                    </svg>
                                </button>
                                <input type="text" class="quantity" data-cke-saved-name="quantity" name="qty" value="01"
                                    min="01" max="100">
                                <button type="button" data-product="{{ $product->id }}"
                                    class="quantity-increment change_price">
                                    <svg width="12" height="12" viewBox="0 0 12 12" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path
                                            d="M6.74868 5.25132V0H5.25132V5.25132H0V6.74868H5.25132V12H6.74868V6.74868H12V5.25132H6.74868Z"
                                            fill="#61AFB3"></path>
                                    </svg>
                                </button>
                            </div>
                        </div>
                        <div class="stock_status"></div>
                        <div class="parfume-capacity">
                            @if ($product->variant_product == 1)
                            @php
                            $variant = json_decode($product->product_attribute);
                            $varint_name_array = [];
                            if (!empty($product->DefaultVariantData->variant)) {
                            $varint_name_array = explode('-', $product->DefaultVariantData->variant);
                            }
                            @endphp
                            @foreach ($variant as $key => $value)
                            @php
                            $p_variant = App\Models\Utility::ProductAttribute($value->attribute_id);
                            $attribute = json_decode($p_variant);
                            $propertyKey = 'for_variation_' . $attribute->id;
                            $variation_option = $value->$propertyKey;
                            @endphp
                            @if ($variation_option == 1)
                            <div class="product-labl w-100">{{ $attribute->name }} :
                            </div>
                            <div class=" product_variatin_option variant_loop" data-product="{{ $product->id }}"
                                name="varint[{{ $attribute->name }}]">
                                @php
                                $optionValues = [];
                                @endphp
                                @foreach ($value->values as $variant1)
                                @php
                                $parts = explode('|', $variant1);
                                @endphp
                                @foreach ($parts as $p)
                                @php
                                $id = App\Models\ProductAttributeOption::where('id', $p)->first();
                                $optionValues[] = $id->terms;
                                @endphp
                                @endforeach
                                @endforeach
                                @if (is_array($optionValues))
                                <div class="text-checkbox checkbox-radio d-flex align-items-center">
                                    <div class="checkbox">
                                        @foreach ($optionValues as $optionValue)
                                        <input type="radio" id="{{ $optionValue }}"
                                            name="varint[{{ $attribute->name }}]" value="{{ $optionValue }}"
                                            class="custom-radio-btn variont_option"
                                            {{ in_array($optionValue, $optionValues) ? 'checked' : '' }}>
                                        <label for="{{ $optionValue }}"
                                            class="checkbox-label">{{ $optionValue }}</label>
                                        @endforeach
                                    </div>
                                </div>
                                @endif
                            </div>
                            @endif
                            @endforeach
                            @endif
                        </div>
                    </form>
                    @endif

                    <div class="count-right">
                        @if ($latestSales)
                        @foreach ($latestSales as $productId => $saleData)
                        <input type="hidden" class="flash_sale_start_date" value={{ $saleData['start_date'] }}>
                        <input type="hidden" class="flash_sale_end_date" value={{ $saleData['end_date'] }}>
                        <input type="hidden" class="flash_sale_start_time" value={{ $saleData['start_time'] }}>
                        <input type="hidden" class="flash_sale_end_time" value={{ $saleData['end_time'] }}>
                        <div id="flipdown" class="flipdown"></div>
                        @endforeach
                        @endif
                        <div class="product-detail-bottom-stuff ">
                            <a href="javascript:void(0)"
                                class="addtocart-btn btn addcart-btn  addcart-btn-globaly price-wise-btn product_var_option"
                                product_id="{{ $product->id }}" variant_id="{{ $product->default_variant_id }}" qty="1">
                                <span> {{ __('Add to cart') }}</span>
                                <svg width="13" height="13" viewBox="0 0 13 13" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M9.24173 3.38727H2.75827C2.10699 3.38727 1.59075 3.93678 1.63138 4.58679L1.91365 9.10315C1.95084 9.69822 2.44431 10.1618 3.04054 10.1618H8.95946C9.55569 10.1618 10.0492 9.69822 10.0863 9.10315L10.3686 4.58679C10.4092 3.93678 9.89301 3.38727 9.24173 3.38727ZM2.75827 2.25818C1.4557 2.25818 0.423236 3.35719 0.504488 4.65722L0.78676 9.17358C0.861144 10.3637 1.84808 11.2909 3.04054 11.2909H8.95946C10.1519 11.2909 11.1389 10.3637 11.2132 9.17358L11.4955 4.65722C11.5768 3.35719 10.5443 2.25818 9.24173 2.25818H2.75827Z"
                                        fill="#ffffff"></path>
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M3.17727 2.82273C3.17727 1.26378 4.44105 0 6 0C7.55895 0 8.82273 1.26378 8.82273 2.82273V3.95182C8.82273 4.26361 8.56997 4.51636 8.25818 4.51636C7.94639 4.51636 7.69363 4.26361 7.69363 3.95182V2.82273C7.69363 1.88736 6.93537 1.12909 6 1.12909C5.06463 1.12909 4.30636 1.88736 4.30636 2.82273V3.95182C4.30636 4.26361 4.05361 4.51636 3.74182 4.51636C3.43003 4.51636 3.17727 4.26361 3.17727 3.95182V2.82273Z"
                                        fill="#ffffff"></path>
                                </svg>
                            </a>
                            <div class="price d-flex align-items-center">
                                <ins class="product_final_price ">{{  currency_format_with_sym(($product->sale_price) , $store->id, $currentTheme) }}</ins>
                                <del class="price-del d-flex align-items-center">
                                    <del class="product_orignal_price">
                                    {{  currency_format_with_sym(($product->price) , $store->id, $currentTheme) }}
                                    </del>
                            </div>
                        </div>
                        @stack('addReviewButton')
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="review-section padding-top">
    <div class="container">
        <div class="review-section-slider">
            <div class="review-slider">
                @foreach ($random_review as $review)
                <div class="review-item">
                    <div class="review-item-inner">
                        <div class="review-item-star d-flex align-items-center">
                            <div class="review-item-star-icon">
                                @for ($i = 0; $i < 5; $i++) <i
                                    class="ti ti-star {{ $i < $review->rating_no ? 'text-warning' : '' }} "></i>
                                    @endfor
                            </div>
                            <h6>{{ $review->rating_no }}.0 <span> / 5.0 </span> </h6>
                        </div>
                        <h3>{{ $review->title }}</h3>
                        <p>{{ $review->description }}</p>
                        <div class="review-bottom d-flex align-items-center">
                            <div class="about-user d-flex align-items-center">
                                <div class="abt-user-badge"></div>
                                <p>{{!empty($review->UserData) ? ($review->UserData->first_name .' ,') : '' }} <span>
                                        {{ $review->ProductData->name }} </span> </p>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>

{{-- tab section  --}}
<section class="tab-vid-section padding-top">
    <div class="container">
        <div class="tabs-wrapper">
            <div class="blog-head-row tab-nav d-flex justify-content-between">
                <div class="blog-col-left ">
                    <ul class="d-flex tabs">
                        <li class="tab-link on-tab-click active" data-tab="0"><a
                                href="javascript:;">{{ __('Description') }}</a>
                        </li>
                        @if ($product->preview_content != '')
                        <li class="tab-link on-tab-click" data-tab="1"><a href="javascript:;">{{ __('Video') }}</a>
                        </li>
                        @endif
                        <li class="tab-link on-tab-click" data-tab="2"><a
                                href="javascript:;">{{ __('Question & Answer') }}</a>
                        </li>
                        @stack('addEnquiryButton')
                        @stack('addSizeGuidelineButton')
                    </ul>
                </div>
            </div>
            <div class="tabs-container">
                <div id="0" class="tab-content active">
                    <section class="best-product-section best-product-first-sec">
                        <div>
                            <div class="row align-items-center">
                                <div class="col-md-6 col-12">
                                    <div class="best-product-left-inner">
                                        <div class="section-title">
                                            <h2> <b>{{ __('Fall in love with') }} </b> <br>
                                                {{ __('aromas increíbles') }} </h2>
                                        </div>
                                        <p>{!! strip_tags($product->description) !!}</p>
                                        <a href="{{ route('page.product-list', $slug) }}"
                                            class="btn-primary addtocart-btn btn">
                                            <span>{{ __('Buscar más productos') }}</span>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="best-product-right-inner">
                                        <div class="product-banner-image-right">
                                            <img src="{{ asset($product->cover_image_path) }}"
                                                alt="product banner right">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="best-product-section">
                        <div>
                            <div class="row align-items-center res-col-reverse">
                                <div class="col-md-6 col-12">
                                    <div class="best-product-right-inner">
                                        <div class="product-banner-image-right">
                                            <img src="{{ asset($product->downloadable_product) }}"
                                                alt="product banner right">

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="best-product-left-inner">
                                        <div class="section-title">
                                            <h2> <b>{{ __('Fall in love with') }} </b> <br>
                                                {{ __('aromas increíbles') }} </h2>
                                        </div>
                                        <p>{!! strip_tags($product->detail) !!}</p>
                                        <a href="{{ route('page.product-list', $slug) }}"
                                            class="btn-primary addtocart-btn btn">
                                            <span>{{ __('Buscar más productos') }}</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>

                @if ($product->preview_content != '')
                <div id="1" class="tab-content">
                    <div class="video-wrapper">
                        @if ($product->preview_type == 'Video Url')
                        @if (str_contains($product->preview_content, 'youtube') ||
                        str_contains($product->preview_content, 'youtu.be'))
                        @php
                        if (strpos($product->preview_content, 'src') !== false) {
                        preg_match('/src="([^"]+)"/', $product->preview_content, $match);
                        $url = $match[1];
                        $video_url = str_replace('https://www.youtube.com/embed/', '', $url);
                        } elseif (strpos($product->preview_content, 'src') == false && strpos($product->preview_content,
                        'embed') !== false) {
                        $video_url = str_replace('https://www.youtube.com/embed/', '', $product->preview_content);
                        } else {
                        $video_url = str_replace('https://youtu.be/', '',
                        str_replace('https://www.youtube.com/watch?v=', '', $product->preview_content));
                        preg_match('/[\\?\\&]v=([^\\?\\&]+)/', $product->preview_content, $matches);
                        if (count($matches) > 0) {
                        $videoId = $matches[1];
                        $video_url = strtok($videoId, '&');
                        }
                        }
                        @endphp
                        <iframe class="video-card-tag" width="100%" height="100%"
                            src="{{ 'https://www.youtube.com/embed/' }}{{ $video_url }}" title="YouTube video player"
                            frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                        @elseif(str_contains($product->preview_content, 'vimeo'))
                        @php
                        if (strpos($product->preview_content, 'src') !== false) {
                        preg_match('/src="([^"]+)"/', $product->preview_content, $match);
                        $url = $match[1];
                        $video_url = str_replace('https://player.vimeo.com/video/', '', $url);
                        } else {
                        $video_url = str_replace('https://vimeo.com/', '', $product->preview_content);
                        }
                        @endphp
                        <iframe class="video-card-tag" width="100%" height="350"
                            src="{{ 'https://player.vimeo.com/video/' }}{{ $video_url }}" frameborder="0"
                            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                        @else
                        @php
                        $video_url = $product->preview_content;
                        @endphp
                        <iframe class="video-card-tag" width="100%" height="100%" src="{{ $video_url }}"
                            title="Video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                        @endif
                        @elseif($product->preview_type == 'iFrame')
                        @if (str_contains($product->preview_content, 'youtube') ||
                        str_contains($product->preview_content, 'youtu.be'))
                        @php
                        if (strpos($product->preview_content, 'src') !== false) {
                        preg_match('/src="([^"]+)"/', $product->preview_content, $match);
                        $url = $match[1];
                        $iframe_url = str_replace('https://www.youtube.com/embed/', '', $url);
                        } else {
                        $iframe_url = str_replace('https://youtu.be/', '',
                        str_replace('https://www.youtube.com/watch?v=', '', $product->preview_content));
                        }
                        @endphp
                        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/{{ $iframe_url }}"
                            title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                        @elseif(str_contains($product->preview_content, 'vimeo'))
                        @php
                        if (strpos($product->preview_content, 'src') !== false) {
                        preg_match('/src="([^"]+)"/', $product->preview_content, $match);
                        $url = $match[1];
                        $iframe_url = str_replace('https://player.vimeo.com/video/', '', $url);
                        } else {
                        $iframe_url = str_replace('https://vimeo.com/', '', $product->preview_content);
                        }
                        @endphp
                        <iframe class="video-card-tag" width="100%" height="350"
                            src="{{ 'https://player.vimeo.com/video/' }}{{ $iframe_url }}" frameborder="0"
                            allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                        @else
                        @php
                        $iframe_url = $product->preview_content;
                        @endphp
                        <iframe class="video-card-tag" width="100%" height="100%" src="{{ $iframe_url }}"
                            title="Video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                        @endif
                        @else
                        <video controls="">
                            <source src="{{ get_file($product->preview_content, APP_THEME()) }}" type="video/mp4">
                        </video>
                        @endif
                    </div>
                </div>
                @endif

                <div id="2" class="tab-content ">
                    <div class="queary-div">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4>{{ __('Have doubts regarding this product?') }}</h4>
                            <a href="javascript:void(0)" class="btn btn-sm Question" @if (\Auth::check())
                                data-ajax-popup="true" @else data-ajax-popup="false" @endif data-size="xs"
                                data-title="Post your question"
                                data-url="{{ route('question', [$slug, $product->id]) }} " data-toggle="tooltip">
                                <i class="ti ti-plus"></i>
                                <span class="lbl">{{ __('Post Your Question') }}</span>
                            </a>
                        </div>
                        <div class="qna">
                            <br>
                            <ul>
                                @foreach ($question->take(4) as $que)
                                <li>
                                    <div class="quetion">
                                        <span class="icon que">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="305" height="266"
                                                viewBox="0 0 305 266" fill="none"
                                                class="__web-inspector-hide-shortcut__">
                                                <path
                                                    d="M152.4 256.4C222.8 256.4 283.6 216.2 300.1 158.6C303 148.8 304.4 138.6 304.4 128.4C304.4 57.7999 236.2 0.399902 152.4 0.399902C68.6004 0.399902 0.400391 57.7999 0.400391 128.4C0.600391 154.8 10.0004 180.3 27.0004 200.5C28.8004 202.7 29.3004 205.7 28.3004 208.4L6.70039 265.4L68.2004 238.4C70.4004 237.4 72.9004 237.5 75.0004 238.6C95.8004 248.9 118.4 254.9 141.5 256.1C145.2 256.3 148.8 256.4 152.4 256.4ZM104.4 120.4C104.4 85.0999 125.9 56.3999 152.4 56.3999C178.9 56.3999 200.4 85.0999 200.4 120.4C200.5 134.5 196.8 148.5 189.7 160.6L204.5 169.5C207 170.9 208.5 173.6 208.5 176.5C208.5 179.4 206.9 182 204.3 183.4C201.7 184.8 198.7 184.7 196.2 183.2L179.4 173.1C172.1 180.1 162.4 184.1 152.3 184.3C125.9 184.4 104.4 155.7 104.4 120.4Z"
                                                    fill="black" />
                                                <path
                                                    d="M164.9 164.4L156.3 159.2C152.6 156.9 151.4 152 153.7 148.3C156 144.6 160.8 143.3 164.6 145.5L176 152.4C181.6 142.7 184.6 131.6 184.4 120.4C184.4 94.3999 169.7 72.3999 152.4 72.3999C135.1 72.3999 120.4 94.3999 120.4 120.4C120.4 146.4 135.1 168.4 152.4 168.4C156.8 168.3 161.2 166.9 164.9 164.4Z"
                                                    fill="black" />
                                            </svg>
                                        </span>
                                        <div class="text">
                                            <p>
                                                {{ $que->question }}
                                            </p>
                                            <span class="user">{{ __($que->users->name) }}</span>
                                        </div>
                                    </div>
                                    <div class="answer">
                                        <span class="icon ans">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="304" height="273"
                                                viewBox="0 0 304 273" fill="none">
                                                <path
                                                    d="M304 127.3C304 126.8 304 126.2 304 125.7C304 125.2 304 124.7 303.9 124.2C301.4 55.5002 234.2 0.200195 152 0.200195C68.5 0.200195 0.6 57.1002 0 127.3C0 127.7 0 128 0 128.4C0.2 154.7 9.6 180.2 26.6 200.4C27.2 201.1 27.6 201.9 27.9 202.7C39.6 216.7 54.6 228.5 71.9 237.6C72.8 237.7 73.7 238 74.6 238.4C95.4 248.7 118 254.7 141.1 255.9C144.8 256.2 148.4 256.3 152 256.3C222.4 256.3 283.2 216.1 299.7 158.5C301.2 153.4 302.3 148.3 303 143.1C303.1 142.4 303.2 141.7 303.3 141C303.4 140.5 303.4 140.1 303.5 139.6C303.6 139 303.6 138.4 303.7 137.9C303.7 137.3 303.8 136.7 303.8 136.1C303.8 135.9 303.8 135.8 303.8 135.6C303.8 135.1 303.9 134.5 303.9 134C303.9 133.3 304 132.6 304 132C304 131.6 304 131.2 304 130.8C304 130.4 304 130 304 129.7C304 129.4 304 129.2 304 128.9V128.5C304 128.1 304 127.7 304 127.3ZM204 183.3C201.5 184.7 198.4 184.6 195.9 183.1L193.7 181.8L199.5 198.2C201 202.4 198.8 206.9 194.7 208.4C190.5 209.9 186 207.7 184.5 203.6L174.9 176.6C168.3 181.4 160.3 184.1 152.1 184.3C143.9 184.3 136.1 181.5 129.3 176.6L119.7 203.6C118.2 207.8 113.6 209.9 109.5 208.4C105.3 206.9 103.2 202.3 104.7 198.2L117 163.7C109.1 152.3 104.2 137 104.2 120.3C104.2 85.0002 125.7 56.3002 152.2 56.3002C178.7 56.3002 200.2 85.0002 200.2 120.3C200.4 134.4 196.6 148.3 189.5 160.5L204.3 169.4C206.8 170.9 208.3 173.5 208.3 176.4C208.1 179.3 206.5 181.9 204 183.3Z"
                                                    fill="black" />
                                                <path
                                                    d="M304 127.3C304 126.8 304 126.2 304 125.7C304 125.2 304 124.7 303.9 124.2C301.2 61.1002 243.4 8.7002 169.1 1.7002C168.8 2.7002 168.3 3.60019 168 4.50019C167.3 6.40019 166.6 8.20019 165.8 10.1002C165 12.0002 164.1 13.9002 163.2 15.8002C162.3 17.7002 161.4 19.4002 160.5 21.2002C159.5 23.0002 158.5 24.8002 157.5 26.5002C156.5 28.3002 155.4 30.0002 154.3 31.7002C153.2 33.4002 152 35.1002 150.8 36.7002C149.6 38.3002 148.4 40.0002 147.1 41.7002C145.8 43.3002 144.5 44.8002 143.2 46.4002C141.9 47.9002 140.5 49.5002 139.1 51.1002C137.7 52.6002 136.2 54.0002 134.8 55.5002C133.3 56.9002 131.8 58.4002 130.3 59.8002C128.8 61.2002 127.2 62.6002 125.5 63.9002C123.9 65.2002 122.3 66.6002 120.6 67.9002C118.9 69.2002 117.2 70.4002 115.4 71.7002C113.7 72.9002 112 74.1002 110.2 75.3002C108.4 76.5002 106.5 77.6002 104.6 78.7002C102.7 79.8002 101 80.9002 99.2 81.9002C97.3 82.9002 95.2 84.0002 93.2 85.0002C91.3 85.9002 89.5 86.9002 87.6 87.8002C85.5 88.8002 83.3 89.6002 81.2 90.5002C79.3 91.3002 77.4 92.1002 75.5 92.9002C73.3 93.7002 70.9 94.5002 68.6 95.2002C66.7 95.8002 64.7 96.5002 62.8 97.1002C60.4 97.8002 57.9 98.4002 55.4 99.0002C53.5 99.5002 51.6 100 49.6 100.4C47 101 44.3 101.4 41.6 101.9C39.8 102.2 37.9 102.6 36.1 102.9C33.1 103.3 30 103.6 26.9 103.9C25.3 104.1 23.8 104.3 22.2 104.4C17.5 104.7 12.7 104.9 8 104.9C6.2 104.9 4.5 104.9 2.7 104.8C0.999997 112.2 0.1 119.8 0 127.3C0 127.7 0 128 0 128.4V128.8C0 156.3 10.3 181.7 27.9 202.6C39.6 216.6 54.6 228.4 71.9 237.5C95.2 249.7 122.6 256.8 152 256.8C176.6 256.9 201 251.8 223.5 241.8C225.6 240.8 228.1 240.8 230.2 241.8L296.4 272.7L271.6 214.8C270.4 211.9 270.9 208.6 273 206.3C289.5 188.8 299.9 166.7 303 143.1C303.1 142.4 303.2 141.7 303.3 141C303.4 140.5 303.4 140.1 303.5 139.6C303.6 139 303.6 138.4 303.7 137.9C303.7 137.3 303.8 136.7 303.8 136.1C303.8 135.9 303.8 135.8 303.8 135.6C303.8 135.1 303.9 134.5 303.9 134C303.9 133.3 304 132.6 304 132C304 131.6 304 131.2 304 130.8C304 130.4 304 130 304 129.7C304 129.4 304 129.2 304 128.9V128.5C304 128.1 304 127.7 304 127.3ZM119.5 203.5C118 207.7 113.4 209.8 109.3 208.3C105.1 206.8 103 202.2 104.5 198.1L116.8 163.6L144.5 86.1002C145.6 82.9002 148.7 80.8002 152 80.8002C155.3 80.8002 158.4 82.9002 159.5 86.1002L193.7 181.7L199.5 198.1C201 202.3 198.8 206.8 194.7 208.3C190.5 209.8 186 207.6 184.5 203.5L174.9 176.5L172.1 168.8H132L129.2 176.5L119.5 203.5Z"
                                                    fill="black" />
                                                <path d="M152 112.6L137.6 152.8H166.3L152 112.6Z" fill="black" />
                                            </svg>
                                        </span>
                                        <div class="text">
                                            <p>
                                                {{ !empty($que->answers) ? $que->answers : '¡Proporcionaremos la respuesta a tu pregunta en breve!' }}
                                            </p>
                                            <span
                                                class="user">{{ !empty($que->admin->name) ? $que->admin->name : '' }}</span>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                            @if ($question->count() >= '4')
                            <div class="text-center">
                                <a href="javascript:void(0)" class="load-more-btn btn" data-ajax-popup="true"
                                    data-size="xs" data-title="Questions And Answers"
                                    data-url="{{ route('more_question', [$slug, $product->id]) }} "
                                    data-toggle="tooltip" title="{{ __('Questions And Answers') }}">
                                    {{ __('Load More') }}
                                </a>
                            </div>
                            @endif
                        </div>
                    </div>
                </div>
                @stack('addEnquiryModel')
                @stack('addSizeGuidelineModel')
            </div>
        </div>
    </div>
</section>
@stack('frequentlyproductslider')
@stack('bundleproductslider')

@include('front_end.sections.homepage.best_product_second')

@include('front_end.sections.homepage.subscribe_section')
@include('front_end.sections.partision.footer_section')
@endsection

@push('page-script')
    <script src="{{ asset('public/js/flipdown.js') }}"></script>
    <script>
        $(document).ready(function() {
            var variants = [];
            $(".product_variatin_option").each(function(index, element) {
                variants.push(element.value);
            });
            if (variants.length > 0) {
                $('.product_orignal_price').hide();
                $('.product_final_price').hide();
                $('.min_max_price').show();
                $(".enable_option").hide();
                $('.currency').hide();
            }
            if (variants.length == 0) {
                $('.product_orignal_price').show();
                $('.product_final_price').show();
                $('.min_max_price').hide();
            }


        });
    </script>
@endpush
