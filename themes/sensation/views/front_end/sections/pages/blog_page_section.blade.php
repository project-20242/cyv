@extends('front_end.layouts.app')
@section('page-title')
    {{ __('Blog Page') }}
@endsection
@section('content')
    @include('front_end.sections.partision.header_section')
    <section class="article-banner blog-page-banner common-banner-section" style="background-image:url({{ asset('themes/' . $currentTheme . '/assets/images/blog-banner.jpg')}});">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-6 col-12">
                        <div class="common-banner-content">
                            <a href="{{route('landing_page',$slug)}}" class="back-btn">
                                <span class="svg-ic">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="5" viewBox="0 0 11 5" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M10.5791 2.28954C10.5791 2.53299 10.3818 2.73035 10.1383 2.73035L1.52698 2.73048L2.5628 3.73673C2.73742 3.90636 2.74146 4.18544 2.57183 4.36005C2.40219 4.53467 2.12312 4.53871 1.9485 4.36908L0.133482 2.60587C0.0480403 2.52287 -0.000171489 2.40882 -0.000171488 2.2897C-0.000171486 2.17058 0.0480403 2.05653 0.133482 1.97353L1.9485 0.210321C2.12312 0.0406877 2.40219 0.044729 2.57183 0.219347C2.74146 0.393966 2.73742 0.673036 2.5628 0.842669L1.52702 1.84888L10.1383 1.84875C10.3817 1.84874 10.5791 2.04609 10.5791 2.28954Z" fill="white"></path>
                                    </svg>
                                </span>
                                {{__('Back to Home')}}
                            </a>
                            <ul class="blog-cat justify-content-center  ">
                                <li class="active"><a href="#">{{ __('Destacado')}}</a></li>
                            </ul>
                            <div class="section-title section-title-white text-center">
                                <h2>{{ __('Blog & Articles') }}</h2>
                                <p>{{ __('The blog and article section serves as a treasure trove of valuable information.') }}</p>
                            </div>
                            <a href="#" class="btn addtocart-btn" tabindex="0">
                            {{ __('Read More') }}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="blog-page-section tabs-wrapper padding-top padding-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="about-page-inner">
                            <div class="section-title d-flex align-items-center justify-content-between">
                                <div class="section-title-with-tab">
                                    <h2>{{__('From our blog')}}</h2>
                                </div>
                                <div class="more-product-link text-right">
                                    <a href="#" class="btn btn-primary addtocart-btn">
                                        <span>{{__('Go to Shop')}}</span>
                                    </a>
                                </div>
                            </div>
                            <div class="row">
                                @foreach ($blogs->take(8) as $blog)
                               
                                <div class="col-xl-3 col-lg-4 col-sm-6 col-12 poster-card-wrp">
                                    <div class="poster-item-1 poster-card">
                                        <div class="poster-card-inner-1">
                                            <div class="poster-content-1">
                                                <div class="poster-card-image-1">
                                                    <a href="{{route('page.article',[$slug,$blog->id])}}">
                                                        <img src="{{ asset($blog->cover_image_path) }}" alt="product-banner1" class="product-banner {{ $blog->id }}">
                                                    </a>
                                                    <h6 class="banner-date text-white">{{$blog->category->name}}</h6>
                                                </div>
                                                <div class="poster-card-details-1">
                                                    <div class="blg-subtitle"><b>{{__('Date:')}}</b>  {{$blog->created_at->format('d M, Y ')}}  </div>
                                                    <h3 class="h4"> <a href="{{route('page.article',[$slug,$blog->id])}}"> {{$blog->title}} </a> </h3>
                                                    <p>{{$blog->short_description}}</p>
                                                    <div class="poster-banner-links-1">
                                                        <a href="{{route('page.article',[$slug,$blog->id])}}" class="addtocart-btn" tabindex="0">
                                                            {{__('Read More')}}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    @include('front_end.sections.partision.footer_section')
@endsection

