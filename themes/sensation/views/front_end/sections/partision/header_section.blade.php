
<!--header start here-->
@if (\Route::current()->getName() != '')
        <header class="site-header header-style-one"  style="@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide  ?? '' }}" data-section="{{ $option->section_name  ?? '' }}"  data-store="{{ $option->store_id  ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">

    @else
        <header class="site-header header-style-one header-style-two" style="@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide  ?? '' }}" data-section="{{ $option->section_name  ?? '' }}"  data-store="{{ $option->store_id  ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    @endif
    <div class="custome_tool_bar"></div>
    <div class="announcebar">
        <div class="container">
            <div class="announce-row row align-items-center">
                <div class="annoucebar-left col-6 d-flex">
                <p id="{{ $section->header->section->title->slug ?? '' }}_preview">{!! $section->header->section->title->text ?? '' !!}</p>
                </div>
                <div class="announcebar-right col-6 d-flex justify-content-end">
                    <button id="announceclose">
                        <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false"
                            role="presentation" class="icon icon-close" viewBox="0 0 18 17">
                            <path
                                d="M.865 15.978a.5.5 0 00.707.707l7.433-7.431 7.579 7.282a.501.501 0 00.846-.37.5.5 0 00-.153-.351L9.712 8.546l7.417-7.416a.5.5 0 10-.707-.708L8.991 7.853 1.413.573a.5.5 0 10-.693.72l7.563 7.268-7.418 7.417z" fill="#000000">
                            </path>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="main-navigationbar">
        <div class="container">
            <div class="navigationbar-row d-flex align-items-center">
                <div class="logo-col">
                    <h1>
                        <a href="{{route('landing_page',$slug)}}">
                            <img src="{{asset( isset($theme_logo) && !empty($theme_logo) ? $theme_logo : 'themes/'.$currentTheme.'/assets/images/logo.png') }}" alt="Style theme">

                        </a>
                    </h1>
                </div>
                <div class="menu-items-col">
                    <ul class="main-nav">
                        @if (!empty($topNavItems))
                            @foreach ($topNavItems as $key => $nav)
                                @if (!empty($nav->children[0]))
                                    <li class="menu-lnk has-item">
                                        <a href="#">
                                            @if ($nav->title == null)
                                                {{ $nav->title }}
                                            @else
                                                {{ $nav->title }}
                                            @endif
                                        </a>
                                        <div class="menu-dropdown">
                                            <ul>
                                                @foreach ($nav->children[0] as $childNav)
                                                    @if ($childNav->type == 'custom')
                                                        <li><a href="{{ url($childNav->slug) }}" target="{{ $childNav->target }}">
                                                            @if ($childNav->title == null)
                                                                {{ $childNav->title }}
                                                            @else
                                                                {{ $childNav->title }}
                                                            @endif
                                                        </a></li>
                                                    @elseif($childNav->type == 'category')
                                                        <li><a href="{{ url($slug.'/'.$childNav->slug) }}" target="{{ $childNav->target }}">
                                                            @if ($childNav->title == null)
                                                                {{ $childNav->title }}
                                                            @else
                                                                {{ $childNav->title }}
                                                            @endif
                                                        </a></li>
                                                    @else
                                                        <li><a href="{{ route('themes.page', [$currentTheme, $childNav->slug]) }}" target="{{ $childNav->target }}">
                                                            @if ($childNav->title == null)
                                                                {{ $childNav->title }}
                                                            @else
                                                                {{ $childNav->title }}
                                                            @endif
                                                        </a></li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                @else
                                    @if ($nav->type == 'custom')
                                        <li class="">
                                            <a href="{{ url($nav->slug) }}" target="{{ $nav->target }}">
                                                @if ($nav->title == null)
                                                    {{ $nav->title }}
                                                @else
                                                    {{ $nav->title }}
                                                @endif
                                            </a>
                                        </li>
                                    @elseif($nav->type == 'category')
                                        <li class="">
                                            <a href="{{  url($slug.'/'.$nav->slug) }}" target="{{ $nav->target }}" target="{{ $nav->target }}">
                                                @if ($nav->title == null)
                                                    {{ $nav->title }}
                                                @else
                                                    {{ $nav->title }}
                                                @endif
                                            </a>
                                        </li>
                                    @else
                                        <li class="">
                                            <a href="{{ route('themes.page', [$currentTheme, $nav->slug]) }}"
                                                target="{{ $nav->target }}">
                                                @if ($nav->title == null)
                                                    {{ $nav->title }}
                                                @else
                                                    {{ $nav->title }}
                                                @endif
                                            </a>
                                        </li>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                        <li class="menu-lnk">
                            <a href="{{route('page.product-list',['storeSlug' => $slug])}}" >{{__('Shop All')}}</a>
                        </li>
                        <li class="menu-lnk has-item">
                            <a href="javascript:void()">
                               {{__(' Pages')}}
                            </a>
                            <div class="menu-dropdown">
                                <ul>
                                    @foreach ($pages as $page)
                                        <li><a href="{{ route('custom.page',[$slug, $page->page_slug]) }}">{{$page->name}}</a></li>
                                    @endforeach
                                    <li><a href="{{route('page.faq',['storeSlug' => $slug])}}">{{__('FAQs')}}</a></li>
                                    <li><a href="{{route('page.blog',['storeSlug' => $slug])}}">{{__('Blog')}}</a></li>
                                    <li><a href="{{route('page.product-list',['storeSlug' => $slug])}}">{{__('Collection')}}</a>
                                </ul>
                            </div>

                        </li>
                        <li class="menu-lnk">
                            <a href="{{route('page.contact_us',$slug)}}">
                                {{ __('Contact') }}
                            </a>
                        </li>
                    </ul>
                    <ul class="menu-right d-flex  justify-content-end">
                        @auth('customers')
                            <li class="search-header">
                                <a href="javascript:;" >
                                    <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.01711 8.89563C7.17556 9.5498 6.11811 9.93935 4.96967 9.93935C2.225 9.93935 0 7.71435 0 4.96967C0 2.225 2.225 0 4.96967 0C7.71435 0 9.93935 2.225 9.93935 4.96967C9.93935 6.11811 9.5498 7.17556 8.89563 8.01711L10.8179 9.93925C11.0605 10.1819 11.0605 10.5752 10.8179 10.8178C10.5753 11.0604 10.182 11.0604 9.93941 10.8178L8.01711 8.89563ZM8.69693 4.96967C8.69693 7.02818 7.02818 8.69693 4.96967 8.69693C2.91117 8.69693 1.24242 7.02818 1.24242 4.96967C1.24242 2.91117 2.91117 1.24242 4.96967 1.24242C7.02818 1.24242 8.69693 2.91117 8.69693 4.96967Z" fill="#12131A"/>
                                    </svg>
                                </a>
                            </li>
                            <li class="profile-header">
                                <a href="javascript:;" >
                                    <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.5 5.95841C3.72809 5.95841 2.29167 7.39483 2.29167 9.16675V10.0834C2.29167 10.3365 2.08646 10.5417 1.83333 10.5417C1.5802 10.5417 1.375 10.3365 1.375 10.0834V9.16675C1.375 6.88857 3.22183 5.04175 5.5 5.04175C7.77817 5.04175 9.625 6.88857 9.625 9.16675V10.0834C9.625 10.3365 9.4198 10.5417 9.16667 10.5417C8.91354 10.5417 8.70833 10.3365 8.70833 10.0834V9.16675C8.70833 7.39483 7.27191 5.95841 5.5 5.95841Z" fill="#12131A"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.5 5.04159C6.51252 5.04159 7.33333 4.22077 7.33333 3.20825C7.33333 2.19573 6.51252 1.37492 5.5 1.37492C4.48748 1.37492 3.66667 2.19573 3.66667 3.20825C3.66667 4.22077 4.48748 5.04159 5.5 5.04159ZM5.5 5.95825C7.01878 5.95825 8.25 4.72704 8.25 3.20825C8.25 1.68947 7.01878 0.458252 5.5 0.458252C3.98122 0.458252 2.75 1.68947 2.75 3.20825C2.75 4.72704 3.98122 5.95825 5.5 5.95825Z" fill="#12131A"/>
                                    </svg>

                                </a>
                                <div class="menu-dropdown">
                                    <ul>
                                        <li><a href="{{ route('my-account.index',$slug) }}" class="account" >{{__('My Account')}}</a></li>
                                        <li>
                                            <form method="POST" action="{{ route('customer.logout',$slug) }}" id="form_logout">
                                                @csrf
                                                <a href="#" onclick="event.preventDefault(); this.closest('form').submit();" class="account">{{__('Log Out')}}
                                                </a>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            @stack('addCompareButton')
                        @endauth
                        @guest('customers')
                            <li class="search-header">
                                <a href="javascript:;" >
                                    <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8.01711 8.89563C7.17556 9.5498 6.11811 9.93935 4.96967 9.93935C2.225 9.93935 0 7.71435 0 4.96967C0 2.225 2.225 0 4.96967 0C7.71435 0 9.93935 2.225 9.93935 4.96967C9.93935 6.11811 9.5498 7.17556 8.89563 8.01711L10.8179 9.93925C11.0605 10.1819 11.0605 10.5752 10.8179 10.8178C10.5753 11.0604 10.182 11.0604 9.93941 10.8178L8.01711 8.89563ZM8.69693 4.96967C8.69693 7.02818 7.02818 8.69693 4.96967 8.69693C2.91117 8.69693 1.24242 7.02818 1.24242 4.96967C1.24242 2.91117 2.91117 1.24242 4.96967 1.24242C7.02818 1.24242 8.69693 2.91117 8.69693 4.96967Z" fill="#12131A"/>
                                    </svg>
                                </a>
                            </li>
                            <li class="profile-header">
                                <a href="{{ route('customer.login',$slug) }}">
                                    <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.5 5.95841C3.72809 5.95841 2.29167 7.39483 2.29167 9.16675V10.0834C2.29167 10.3365 2.08646 10.5417 1.83333 10.5417C1.5802 10.5417 1.375 10.3365 1.375 10.0834V9.16675C1.375 6.88857 3.22183 5.04175 5.5 5.04175C7.77817 5.04175 9.625 6.88857 9.625 9.16675V10.0834C9.625 10.3365 9.4198 10.5417 9.16667 10.5417C8.91354 10.5417 8.70833 10.3365 8.70833 10.0834V9.16675C8.70833 7.39483 7.27191 5.95841 5.5 5.95841Z" fill="#12131A"/>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M5.5 5.04159C6.51252 5.04159 7.33333 4.22077 7.33333 3.20825C7.33333 2.19573 6.51252 1.37492 5.5 1.37492C4.48748 1.37492 3.66667 2.19573 3.66667 3.20825C3.66667 4.22077 4.48748 5.04159 5.5 5.04159ZM5.5 5.95825C7.01878 5.95825 8.25 4.72704 8.25 3.20825C8.25 1.68947 7.01878 0.458252 5.5 0.458252C3.98122 0.458252 2.75 1.68947 2.75 3.20825C2.75 4.72704 3.98122 5.95825 5.5 5.95825Z" fill="#12131A"/>
                                    </svg>
                                </a>
                            </li>
                        @endguest
                        <li class="cart-header">
                            <a href="javascript:;">
                                <span class="count">{!! \App\Models\Cart::CartCount($slug) !!} </span>
                                <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M9.24173 3.38727H2.75827C2.10699 3.38727 1.59075 3.93678 1.63138 4.58679L1.91365 9.10315C1.95084 9.69822 2.44431 10.1618 3.04054 10.1618H8.95946C9.55569 10.1618 10.0492 9.69822 10.0863 9.10315L10.3686 4.58679C10.4092 3.93678 9.89301 3.38727 9.24173 3.38727ZM2.75827 2.25818C1.4557 2.25818 0.423236 3.35719 0.504488 4.65722L0.78676 9.17358C0.861144 10.3637 1.84808 11.2909 3.04054 11.2909H8.95946C10.1519 11.2909 11.1389 10.3637 11.2132 9.17358L11.4955 4.65722C11.5768 3.35719 10.5443 2.25818 9.24173 2.25818H2.75827Z" fill="#000000"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M3.17727 2.82273C3.17727 1.26378 4.44105 0 6 0C7.55895 0 8.82273 1.26378 8.82273 2.82273V3.95182C8.82273 4.26361 8.56997 4.51636 8.25818 4.51636C7.94639 4.51636 7.69363 4.26361 7.69363 3.95182V2.82273C7.69363 1.88736 6.93537 1.12909 6 1.12909C5.06463 1.12909 4.30636 1.88736 4.30636 2.82273V3.95182C4.30636 4.26361 4.05361 4.51636 3.74182 4.51636C3.43003 4.51636 3.17727 4.26361 3.17727 3.95182V2.82273Z" fill="#000000"/>
                                </svg>
                            </a>
                        </li>
                        <li class="menu-lnk has-item lang-dropdown">
                            <a href="#">
                                <span class="drp-text">{{ Str::upper($currantLang) }}</span>
                                <div class="lang-icn">
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg" version="1.1" id="svg2223" xml:space="preserve" width="682.66669" height="682.66669" viewBox="0 0 682.66669 682.66669">
                                    <g id="g2229" transform="matrix(1.3333333,0,0,-1.3333333,0,682.66667)"><g id="g2231"><g id="g2233" clip-path="url(#clipPath2237)"><g id="g2239" transform="translate(497,256)"><path d="m 0,0 c 0,-132.548 -108.452,-241 -241,-241 -132.548,0 -241,108.452 -241,241 0,132.548 108.452,241 241,241 C -108.452,241 0,132.548 0,0 Z" style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1" id="path2241"/></g><g id="g2243" transform="translate(376,256)"><path d="m 0,0 c 0,-132.548 -53.726,-241 -120,-241 -66.274,0 -120,108.452 -120,241 0,132.548 53.726,241 120,241 C -53.726,241 0,132.548 0,0 Z" style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1" id="path2245"/></g><g id="g2247" transform="translate(256,497)"><path d="M 0,0 V -482" style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1" id="path2249"/></g><g id="g2251" transform="translate(15,256)"><path d="M 0,0 H 482" style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1" id="path2253"/></g><g id="g2255" transform="translate(463.8926,136)"><path d="M 0,0 H -415.785" style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1" id="path2257"/></g><g id="g2259" transform="translate(48.1079,377)"><path d="M 0,0 H 415.785" style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1" id="path2261"/></g></g></g></g></svg>
                                </div>
                            </a>
                            <div class="menu-dropdown">
                                <ul>
                                        @foreach ($languages as $code => $language)
                                        <li><a href="{{ route('change.languagestore', [$code]) }}"
                                                class="@if ($language == $currantLang) active-language text-primary @endif">{{  ucFirst($language) }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <div class="mobile-menu mobile-only">
                        <button class="mobile-menu-button" id="menu">
                            <div class="one"></div>
                            <div class="two"></div>
                            <div class="three"></div>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Mobile menu start here -->
        <div class="mobile-menu-wrapper">
            <div class="menu-close-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                    <path fill="#24272a" d="M19.95 16.75l-.05-.4-1.2-1-5.2-4.2c-.1-.05-.3-.2-.6-.5l-.7-.55c-.15-.1-.5-.45-1-1.1l-.1-.1c.2-.15.4-.35.6-.55l1.95-1.85 1.1-1c1-1 1.7-1.65 2.1-1.9l.5-.35c.4-.25.65-.45.75-.45.2-.15.45-.35.65-.6s.3-.5.3-.7l-.3-.65c-.55.2-1.2.65-2.05 1.35-.85.75-1.65 1.55-2.5 2.5-.8.9-1.6 1.65-2.4 2.3-.8.65-1.4.95-1.9 1-.15 0-1.5-1.05-4.1-3.2C3.1 2.6 1.45 1.2.7.55L.45.1c-.1.05-.2.15-.3.3C.05.55 0 .7 0 .85l.05.35.05.4 1.2 1 5.2 4.15c.1.05.3.2.6.5l.7.6c.15.1.5.45 1 1.1l.1.1c-.2.15-.4.35-.6.55l-1.95 1.85-1.1 1c-1 1-1.7 1.65-2.1 1.9l-.5.35c-.4.25-.65.45-.75.45-.25.15-.45.35-.65.6-.15.3-.25.55-.25.75l.3.65c.55-.2 1.2-.65 2.05-1.35.85-.75 1.65-1.55 2.5-2.5.8-.9 1.6-1.65 2.4-2.3.8-.65 1.4-.95 1.9-1 .15 0 1.5 1.05 4.1 3.2 2.6 2.15 4.3 3.55 5.05 4.2l.2.45c.1-.05.2-.15.3-.3.1-.15.15-.3.15-.45z"></path>
                </svg>
            </div>
    <div class="mobile-menu-bar">
        <ul class="mobile-only">
            <li class="mobile-item has-children">
                <a href="javascript:void()" class="acnav-label">
                    {{ __('Gifty Products')}}
                    <svg class="menu-open-arrow" xmlns="http://www.w3.org/2000/svg" width="20" height="11"
                        viewBox="0 0 20 11">
                        <path fill="#24272a"
                            d="M.268 1.076C.373.918.478.813.584.76l.21.474c.79.684 2.527 2.158 5.21 4.368 2.738 2.21 4.159 3.316 4.264 3.316.474-.053 1.158-.369 1.947-1.053.842-.631 1.632-1.42 2.474-2.368.895-.948 1.737-1.842 2.632-2.58.842-.789 1.578-1.262 2.105-1.42l.316.684c0 .21-.106.474-.316.737-.053.21-.263.421-.474.579-.053.052-.316.21-.737.474l-.526.368c-.421.263-1.105.947-2.158 2l-1.105 1.053-2.053 1.947c-1 .947-1.579 1.421-1.842 1.421-.263 0-.684-.263-1.158-.895-.526-.631-.842-1-1.052-1.105l-.737-.579c-.316-.316-.527-.474-.632-.474l-5.42-4.315L.267 2.339l-.105-.421-.053-.369c0-.157.053-.315.158-.473z">
                        </path>
                    </svg>
                    <svg class="close-menu-ioc" xmlns="http://www.w3.org/2000/svg" width="20" height="18"
                        viewBox="0 0 20 18">
                        <path fill="#24272a"
                            d="M19.95 16.75l-.05-.4-1.2-1-5.2-4.2c-.1-.05-.3-.2-.6-.5l-.7-.55c-.15-.1-.5-.45-1-1.1l-.1-.1c.2-.15.4-.35.6-.55l1.95-1.85 1.1-1c1-1 1.7-1.65 2.1-1.9l.5-.35c.4-.25.65-.45.75-.45.2-.15.45-.35.65-.6s.3-.5.3-.7l-.3-.65c-.55.2-1.2.65-2.05 1.35-.85.75-1.65 1.55-2.5 2.5-.8.9-1.6 1.65-2.4 2.3-.8.65-1.4.95-1.9 1-.15 0-1.5-1.05-4.1-3.2C3.1 2.6 1.45 1.2.7.55L.45.1c-.1.05-.2.15-.3.3C.05.55 0 .7 0 .85l.05.35.05.4 1.2 1 5.2 4.15c.1.05.3.2.6.5l.7.6c.15.1.5.45 1 1.1l.1.1c-.2.15-.4.35-.6.55l-1.95 1.85-1.1 1c-1 1-1.7 1.65-2.1 1.9l-.5.35c-.4.25-.65.45-.75.45-.25.15-.45.35-.65.6-.15.3-.25.55-.25.75l.3.65c.55-.2 1.2-.65 2.05-1.35.85-.75 1.65-1.55 2.5-2.5.8-.9 1.6-1.65 2.4-2.3.8-.65 1.4-.95 1.9-1 .15 0 1.5 1.05 4.1 3.2 2.6 2.15 4.3 3.55 5.05 4.2l.2.45c.1-.05.2-.15.3-.3.1-.15.15-.3.15-.45z">
                        </path>
                    </svg>
                </a>
                <ul class="mobile_menu_inner acnav-list">
                    <li class="menu-h-link">
                        <ul>
                            @foreach ($MainCategoryList as $category)
                                <li><a href="{{route('page.product-list',[$slug,'main_category' => $category->id ])}}">{{$category->name}}</a></li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="mobile-item">
                <a href="{{route('page.product-list',$slug)}}"> {{ __('Shop All') }} </a>
            </li>

            <li class="mobile-item has-children">
                <a href="javascript:void()" class="acnav-label">
                    {{ __('Pages')}}
                    <svg class="menu-open-arrow" xmlns="http://www.w3.org/2000/svg" width="20" height="11"
                        viewBox="0 0 20 11">
                        <path fill="#24272a"
                            d="M.268 1.076C.373.918.478.813.584.76l.21.474c.79.684 2.527 2.158 5.21 4.368 2.738 2.21 4.159 3.316 4.264 3.316.474-.053 1.158-.369 1.947-1.053.842-.631 1.632-1.42 2.474-2.368.895-.948 1.737-1.842 2.632-2.58.842-.789 1.578-1.262 2.105-1.42l.316.684c0 .21-.106.474-.316.737-.053.21-.263.421-.474.579-.053.052-.316.21-.737.474l-.526.368c-.421.263-1.105.947-2.158 2l-1.105 1.053-2.053 1.947c-1 .947-1.579 1.421-1.842 1.421-.263 0-.684-.263-1.158-.895-.526-.631-.842-1-1.052-1.105l-.737-.579c-.316-.316-.527-.474-.632-.474l-5.42-4.315L.267 2.339l-.105-.421-.053-.369c0-.157.053-.315.158-.473z">
                        </path>
                    </svg>
                    <svg class="close-menu-ioc" xmlns="http://www.w3.org/2000/svg" width="20" height="18"
                        viewBox="0 0 20 18">
                        <path fill="#24272a"
                            d="M19.95 16.75l-.05-.4-1.2-1-5.2-4.2c-.1-.05-.3-.2-.6-.5l-.7-.55c-.15-.1-.5-.45-1-1.1l-.1-.1c.2-.15.4-.35.6-.55l1.95-1.85 1.1-1c1-1 1.7-1.65 2.1-1.9l.5-.35c.4-.25.65-.45.75-.45.2-.15.45-.35.65-.6s.3-.5.3-.7l-.3-.65c-.55.2-1.2.65-2.05 1.35-.85.75-1.65 1.55-2.5 2.5-.8.9-1.6 1.65-2.4 2.3-.8.65-1.4.95-1.9 1-.15 0-1.5-1.05-4.1-3.2C3.1 2.6 1.45 1.2.7.55L.45.1c-.1.05-.2.15-.3.3C.05.55 0 .7 0 .85l.05.35.05.4 1.2 1 5.2 4.15c.1.05.3.2.6.5l.7.6c.15.1.5.45 1 1.1l.1.1c-.2.15-.4.35-.6.55l-1.95 1.85-1.1 1c-1 1-1.7 1.65-2.1 1.9l-.5.35c-.4.25-.65.45-.75.45-.25.15-.45.35-.65.6-.15.3-.25.55-.25.75l.3.65c.55-.2 1.2-.65 2.05-1.35.85-.75 1.65-1.55 2.5-2.5.8-.9 1.6-1.65 2.4-2.3.8-.65 1.4-.95 1.9-1 .15 0 1.5 1.05 4.1 3.2 2.6 2.15 4.3 3.55 5.05 4.2l.2.45c.1-.05.2-.15.3-.3.1-.15.15-.3.15-.45z">
                        </path>
                    </svg>
                </a>
                <ul class="mobile_menu_inner acnav-list">
                    <li class="menu-h-link">
                        <ul>
                            @foreach ($pages as $page)
                                <li><a href="{{ route('custom.page', [$slug,$page->page_slug]) }}">{{$page->name}}</a></li>
                            @endforeach
                            <li><a href="{{route('page.faq',$slug)}}"> {{ __('FAQs')}} </a></li>
                            <li><a href="{{route('page.blog',$slug)}}"> {{ __('Blog')}} </a></li>
                            <li><a href="{{route('page.product-list',$slug)}}"> {{ __('Collection')}} </a></li>
                        </ul>
                    </li>
                </ul>
            </li>

            <li class="menu-lnk">
                            <a href="{{route('page.contact_us',$slug)}}">
                                {{ __('Contact') }}
                            </a>
                        </li>

        </ul>
    </div>
</div>
        <!-- Mobile menu end here -->
        <div class="search-popup">
            <div class="close-search">
                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50" fill="none">
                    <path
                        d="M27.7618 25.0008L49.4275 3.33503C50.1903 2.57224 50.1903 1.33552 49.4275 0.572826C48.6647 -0.189868 47.428 -0.189965 46.6653 0.572826L24.9995 22.2386L3.33381 0.572826C2.57102 -0.189965 1.3343 -0.189965 0.571605 0.572826C-0.191089 1.33562 -0.191186 2.57233 0.571605 3.33503L22.2373 25.0007L0.571605 46.6665C-0.191186 47.4293 -0.191186 48.666 0.571605 49.4287C0.952952 49.81 1.45285 50.0007 1.95275 50.0007C2.45266 50.0007 2.95246 49.81 3.3339 49.4287L24.9995 27.763L46.6652 49.4287C47.0465 49.81 47.5464 50.0007 48.0463 50.0007C48.5462 50.0007 49.046 49.81 49.4275 49.4287C50.1903 48.6659 50.1903 47.4292 49.4275 46.6665L27.7618 25.0008Z"
                        fill="white"></path>
                </svg>
            </div>
            <div class="search-form-wrapper">
                <form>
                    <div class="form-inputs">
                        <input type="search" placeholder="Buscar Producto..." class="form-control search_input" list="products" name="search_product" id="product">
                        <datalist id="products">
                            @foreach ($search_products as $pro_id => $pros)
                                <option value="{{$pros}}"></option>
                            @endforeach
                        </datalist>

                        <button type="submit"  class="btn search_product_globaly">
                            <svg>
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M0.000169754 6.99457C0.000169754 10.8576 3.13174 13.9891 6.99473 13.9891C8.60967 13.9891 10.0968 13.4418 11.2807 12.5226C11.3253 12.6169 11.3866 12.7053 11.4646 12.7834L17.0603 18.379C17.4245 18.7432 18.015 18.7432 18.3792 18.379C18.7434 18.0148 18.7434 17.4243 18.3792 17.0601L12.7835 11.4645C12.7055 11.3864 12.6171 11.3251 12.5228 11.2805C13.442 10.0966 13.9893 8.60951 13.9893 6.99457C13.9893 3.13157 10.8577 0 6.99473 0C3.13174 0 0.000169754 3.13157 0.000169754 6.99457ZM1.86539 6.99457C1.86539 4.1617 4.16187 1.86522 6.99473 1.86522C9.8276 1.86522 12.1241 4.1617 12.1241 6.99457C12.1241 9.82743 9.8276 12.1239 6.99473 12.1239C4.16187 12.1239 1.86539 9.82743 1.86539 6.99457Z">
                                </path>
                            </svg>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!--header end here-->
    </header>

@push('page-script')
@endpush
