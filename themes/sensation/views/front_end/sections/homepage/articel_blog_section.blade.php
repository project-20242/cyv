<section class="home-article-blog-section padding-top" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class= "section-top w-100 d-flex align-items-center justify-content-between">
            <div class="section-title-left">
                <div class="section-title">
                    <h2 id="{{ $section->articel_blog->section->title->slug ?? ''}}_preview">
                        {!! $section->articel_blog->section->title->text ?? ''!!}
                    </h2>
                </div>
                <p id="{{ $section->articel_blog->section->description->slug ?? '' }}_preview">
                    {!! $section->articel_blog->section->description->text ?? '' !!}
                </p>
            </div>
            <div class="section-title-right">
                <a href="{{ route('page.product-list',$slug) }}" class="link-btn btn">
                    {!! $section->articel_blog->section->button->text ?? '' !!}
                </a>
            </div>
        </div>
        <div class="row home-main-blog">
            @foreach($MainCategoryList as $key => $MainCategory)
                @if($key <= 2)
                    <div class="col-lg-4 col-md-6 col-sm-6 col-cx-12 col-12">
                        <div class="home-article-blog">
                            <div class="home-article-blog-image">
                                <img src="{{ asset($MainCategory->image_path) }}" alt="article blog">
                            </div>

                            @if($key == 0 || $key == 2)
                                <div class="home-article-blog-content" style="background-color: var(--second-color);">
                                    <div class="section-title">
                                        <h3> <b> {{$MainCategory->name}} </b>   </h3>
                                    </div>
                                    <a href="{{route('page.product-list',[$slug,'main_category' => $MainCategory->id ])}}" class="article-blog-show-more-link">{{__('Show more')}}</a>
                                </div>
                            @endif
                            @if($key == 1)
                                <div  class="home-article-blog-content blog-content-top" style="background-color: var(--second-color);">
                                    <div class="section-title">
                                        <h3> <b> {{$MainCategory->name}} </b>  </h3>
                                    </div>
                                    <a href="{{route('page.product-list',[$slug,'main_category' => $MainCategory->id ])}}" class="article-blog-show-more-link">{{__('Show more')}}</a>
                                </div>
                            @endif
                        </div>
                    </div>
                @endif
            @endforeach
        </div>
    </div>
</section>
