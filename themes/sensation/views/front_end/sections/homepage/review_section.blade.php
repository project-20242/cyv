<section class="review-section padding-bottom padding-top"style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 col-12">
                        <div class="review-section-header">
                            <div class="section-title text-center d-flex align-items-center justify-content-center">
                            <h2 class="title" id="{{ $section->review->section->title->slug ?? '' }}_preview">{!!
                            $section->review->section->title->text ?? '' !!}</h2>
                            <p id="{{ $section->review->section->description->slug ?? '' }}_preview">{!!
                            $section->review->section->description->text ?? '' !!} </p>
                            </div>
                            <div class="review-section-slider">
                                <div class="review-slider">
                                    @foreach($reviews as $review)
                                        <div class="review-item">
                                            <div class="review-item-inner">
                                                <div class="review-item-star d-flex align-items-center">
                                                    <div class="review-item-star-icon">
                                                        @for ($i = 0; $i < 5; $i++)
                                                            <i class="ti ti-star {{ $i < $review->rating_no ? 'text-warning' : '' }} "></i>
                                                        @endfor
                                                    </div>
                                                    <span>{{ $review->rating_no }}.0<b> / 5.0 </b> </span>
                                                </div>
                                                <h3>{{ $review->title }}</h3>
                                                <p>{{ $review->description }}</p>
                                                <div class="review-bottom d-flex align-items-center">
                                                    <div class="about-user d-flex align-items-center">
                                                        <div class="abt-user-badge"><img src="{{ asset('themes/' . $currentTheme . '/assets/images/client.png') }}"
                                                            alt="reviewer-img"></div>
                                                        <p>{{!empty($review->UserData) ? ($review->UserData->first_name.',') : '' }} <span> {{ $review->ProductData->name }} </span> </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>