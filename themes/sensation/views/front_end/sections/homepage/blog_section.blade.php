<section class="home-blog-section padding-top padding-bottom" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12">
                <div class="home-blog-section-header">
                    <div
                        class="section-title text-center section-title-white d-flex align-items-center justify-content-center">
                        <h2 id="{{ $section->blog->section->title->slug ?? ''}}_preview">
                            {!! $section->blog->section->title->text ?? ''!!}</h2>
                        <p id="{{ $section->blog->section->description->slug ?? '' }}_preview">{!!
                            $section->blog->section->description->text ?? '' !!} </p>
                    </div>
                </div>

                <div class="about-blog-section-slider">
                    {!! \App\Models\Blog::HomePageBlog($currentTheme, $slug, 4) !!}
                </div>
            </div>
        </div>
    </div>
</section>