<section class="one-col-variant-bg padding-top padding-bottom" style="background-image: url({{ asset($section->variant_background->section->image->image ?? 'themes/'.$currentTheme.'/assets/images/banner-2.png')}}); position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}"
    data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}"
    data-section="{{ $option->section_name ?? '' }}" data-store="{{ $option->store_id ?? '' }}"
    data-theme="{{ $option->theme_id ?? '' }}">

    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-12 one-col-variant-bg-main">
                <div class="one-variant-left">
                    <div class="section-title section-title-white">
                        <h2 id="{{ $section->variant_background->section->title->slug ?? '' }}_preview">
                            {!! $section->variant_background->section->title->text ?? '' !!}
                        </h2>
                    </div>
                    <p id="{{ $section->variant_background->section->description->slug ?? '' }}_preview">
                        {!! $section->variant_background->section->description->text ?? '' !!}
                    </p>
                    <div class="banner-links">
                        <a href="{{ route('page.product-list',$slug) }}" class="btn white-btn btn-secondary" tabindex="0">
                            {!! $section->variant_background->section->button->text ?? '' !!}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
