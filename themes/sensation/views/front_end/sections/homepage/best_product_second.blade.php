<section class="bestseller-product-section pdp-bestseller-sec recent-product-section tabs-wrapper padding-bottom"
    style="@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="bestseller-product-inner">
                    <div class="section-title d-flex align-items-center justify-content-between">
                        <div class="section-title-with-tab">
                            <h2> <b> {{ __('Recent') }} </b> {{ __('products') }} </h2>

                        </div>
                        <div class="bestseller-product-col">
                            <ul class="d-flex tabs">
                                @foreach ($MainCategory as $cat_key => $category)
                                <li class="tab-link {{ $cat_key == 0 ? 'active' : '' }}" data-tab="category_{{ $cat_key }}">
                                   <a href="javascript:;">{{ __('All Products') }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </div>

                    </div>
                    <div class="tabs-container">
                        @foreach ($MainCategory as $cat_k => $category)
                        <div id="category_{{ $cat_k }}" class="tab-content {{ $cat_k == 0 ? 'active' : '' }}">
                            <div class="bestseller-product-slider">
                                @foreach ($homeproducts as $homeproduct)

                                @if ($cat_k == '0' || $homeproduct->ProductData->id == $cat_k)
                                <div class="bestseller-item product-card">
                                    <div class="product-card-inner">
                                        <div class="product-card-image">
                                            <a href="{{ url($slug.'/product/'.$homeproduct->slug) }}">
                                                <img src="{{ asset($homeproduct->cover_image_path) }}"
                                                    class="default-img">
                                                @if ($homeproduct->Sub_image($homeproduct->id)['status'] == true)
                                                <img src="{{ asset($homeproduct->Sub_image($homeproduct->id)['data'][0]->image_path) }}"
                                                    class="hover-img">
                                                @else
                                                <img src="{{ asset($homeproduct->Sub_image($homeproduct->id)) }}"
                                                    class="hover-img">
                                                @endif
                                            </a>
                                            {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $homeproduct->id) !!}
                                                    <div class="like-items-icon">
                                                        <a class="wishlist wishbtn wishbtn-globaly"
                                                            href="javascript:void(0)" title="Wishlist" tabindex="0"
                                                            product_id="{{ $homeproduct->id }}"
                                                            in_wishlist="{{ $homeproduct->in_whishlist ? 'remove' : 'add' }}">
                                                            <span class="wish-ic">
                                                                <i
                                                                    class="{{ $homeproduct->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"></i>
                                                            </span>
                                                        </a>
                                                        <div class="product-btn-wrp">
                                                            @php
                                                                $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                                $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                                            @endphp
                                                            @if(isset($module) && $module->isEnabled())
                                                                {{-- Include the module blade button --}}
                                                                @include('productquickview::pages.button', ['product_slug' => $homeproduct->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                            @endif
                                                            @if(isset($compare_module) && $compare_module->isEnabled())
                                                                {{-- Include the module blade button --}}
                                                                @include('productcompare::pages.button', ['product_slug' => $homeproduct->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                            @endif
                                                        </div>
                                                    </div>
                                        </div>
                                        <div class="product-content text-center">
                                            <div class="product-content-top">
                                                <h3 class="product-title">
                                                    <a href="{{ url($slug.'/product/'.$homeproduct->slug) }}">
                                                        {{ $homeproduct->name }}
                                                    </a>
                                                </h3>
                                            </div>
                                            <div
                                                class="product-content-bottom d-flex align-items-center justify-content-center">
                                                @if ($homeproduct->variant_product == 0)
                                                <div class="price">
                                                    <ins>{{ $currency_icon }}{{ $homeproduct->sale_price ?? $homeproduct->price }}</ins>
                                                </div>
                                                @else
                                                <div class="price">
                                                    <ins>{{ __('In Variant') }}</ins>
                                                </div>
                                                @endif
                                                <div
                                                    class="d-flex flex-wrap text-checkbox checkbox-radio align-items-center justify-content-center w-100">
                                                </div>
                                                <a href="javascript:void(0)"
                                                    class="addtocart-btn btn variant_form addcart-btn-globaly"
                                                    product_id="{{ $homeproduct->id }}" variant_id="0" qty="1">
                                                    <span> {{ __('Add to cart') }}</span>
                                                    <svg width="13" height="13" viewBox="0 0 13 13" fill="none"
                                                        xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                            d="M9.24173 3.38727H2.75827C2.10699 3.38727 1.59075 3.93678 1.63138 4.58679L1.91365 9.10315C1.95084 9.69822 2.44431 10.1618 3.04054 10.1618H8.95946C9.55569 10.1618 10.0492 9.69822 10.0863 9.10315L10.3686 4.58679C10.4092 3.93678 9.89301 3.38727 9.24173 3.38727ZM2.75827 2.25818C1.4557 2.25818 0.423236 3.35719 0.504488 4.65722L0.78676 9.17358C0.861144 10.3637 1.84808 11.2909 3.04054 11.2909H8.95946C10.1519 11.2909 11.1389 10.3637 11.2132 9.17358L11.4955 4.65722C11.5768 3.35719 10.5443 2.25818 9.24173 2.25818H2.75827Z"
                                                            fill="#ffffff"></path>
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                            d="M3.17727 2.82273C3.17727 1.26378 4.44105 0 6 0C7.55895 0 8.82273 1.26378 8.82273 2.82273V3.95182C8.82273 4.26361 8.56997 4.51636 8.25818 4.51636C7.94639 4.51636 7.69363 4.26361 7.69363 3.95182V2.82273C7.69363 1.88736 6.93537 1.12909 6 1.12909C5.06463 1.12909 4.30636 1.88736 4.30636 2.82273V3.95182C4.30636 4.26361 4.05361 4.51636 3.74182 4.51636C3.43003 4.51636 3.17727 4.26361 3.17727 3.95182V2.82273Z"
                                                            fill="#ffffff"></path>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
