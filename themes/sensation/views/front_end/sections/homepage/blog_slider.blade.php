<div class="poster-slider-1 ">
    @foreach($landing_blogs as $blog)
   
    <div class="poster-item-1 poster-card">
        <div class="poster-card-inner-1">
            <div class="poster-content-1">
                <div class="poster-card-image-1">
                    <a href="{{route('page.article',[$slug,$blog->id])}}">
                        <img src="{{ asset($blog->cover_image_path ) }}" alt="product-banner1" class="product-banner">

                    </a>
                    <h6 class="banner-date text-white">{{ __('Men') }}</h6>
                </div>
                <div class="poster-card-details-1">
                    <div class="blg-subtitle"> {{$blog->created_at->format('d M, Y ')}} </div>
                    <h3 class="h4"> <a href="javascript:;">{{ $blog->title }}</a> </h3>
                    <p> {{ $blog->short_description }} </p>
                    <div class="poster-banner-links-1">
                        <a href="{{route('page.article',[$slug,$blog->id])}}" class="addtocart-btn" tabindex="0">
                           {{__('Leer Más')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endforeach
</div>
