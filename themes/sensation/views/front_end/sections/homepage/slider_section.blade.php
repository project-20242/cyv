<section class="home-section" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="home-banner-image">
        <img src="{{ asset($section->slider->section->background_image->image ?? '') }}" class="img-fluid {{ ($section->slider->section->background_image->slug ?? '').'_preview'}}" alt="main-banner">
    </div>

    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-10 col-12">
                <div class="banner-content-slider">
                    @for ($i = 0; $i < $section->slider->loop_number ?? 1; $i++)

                        <div class="banner-content-item">
                            <div class="banner-content-inner">
                                <div class="banner-content-top">
                                    <h5 id="{{ ($section->slider->section->title->slug ?? '').'_'. $i }}_preview">
                        {!! $section->slider->section->title->text->{$i} ?? "" !!}</h5>
                                    <h2 id="{{ ($section->slider->section->sub_title->slug ?? '').'_'. $i }}_preview">
                        {!! $section->slider->section->sub_title->text->{$i} ?? "" !!} </h2>
                                    <p id="{{ ($section->slider->section->description->slug ?? '').'_'. $i }}_preview">{!! ($section->slider->section->description->text->{$i} ?? '') !!}</p>
                                </div>
                                <div class="banner-links">
                                    <a href="{{route('page.product-list',$slug)}}" class="btn btn-primary white-btn" tabindex="0" id="{{ ($section->slider->section->button_first->slug ?? '').'_'. $i }}_preview">
                        {!! $section->slider->section->button_first->text->{$i} ?? "" !!}
                                    </a>
                                    <a href="{{route('page.product-list',$slug)}}" class="btn btn-secondary white-btn" tabindex="0" id="{{ ($section->slider->section->button_second->slug ?? '').'_'. $i }}_preview">
                        {!! $section->slider->section->button_second->text->{$i} ?? "" !!}
                                    </a>
                                </div>
                            </div>
                        </div>
                    @endfor
                </div>
            </div>
            <div class="col-lg-4 col-md-2 col-12"></div>
        </div>
    </div>
</section>
