<section class="product-categories-section tabs-wrapper padding-top padding-bottom" style="@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="section-title d-flex align-items-center justify-content-between">
            <div class="section-title-left">
                <h2 id="{{ $section->bestseller_slider->section->title->slug ?? ''}}_preview">
                    {!! $section->bestseller_slider->section->title->text ?? ''!!}
                </h2>
            </div>
            <div class="section-title-right">
                <a href="{{ route('page.product-list',$slug) }}" class="link-btn btn btn-primary">{!! $section->bestseller_slider->section->button->text ?? ''!!}</a>
            </div>
        </div>
        <div class="-flex align-items-center justify-content-between">
            <div class="bestseller-product-col">
                <ul class="d-flex tabs">
                    @foreach($category_options as $key =>$cat)
                        @if(isset($categoryIds) && count($categoryIds) > 0)
                            @if(in_array($key, $categoryIds))
                                <li class="tab-link {{ $key == 0 ? 'active' : '' }}" data-tab="{{ $key }}">
                                    <a href="javascript:;">{{ $cat }}</a>
                                </li>
                            @endif
                        @else
                            <li class="tab-link {{ $key == 0 ? 'active' : '' }}" data-tab="{{ $key }}">
                                <a href="javascript:;">{{ $cat }}</a>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="tabs-container">
            @foreach ($category_options as $cat_k => $category)
                <div id="{{ $cat_k }}" class="tab-content tab-cat-id {{$cat_k == 0 ? 'active' : ''}}">
                    <div class="bestseller-product-slider">
                        @foreach($products as $product)
                            @if($cat_k == '0' ||  $product->ProductData->id == $cat_k)
                                <div class="bestseller-item product-card">
                                    <div class="product-card-inner">
                                        <div class="product-card-image">
                                            <a href="{{url($slug.'/product/'.$product->slug)}}">
                                                <img src="{{asset($product->cover_image_path)}}" class="default-img">
                                                @if ($product->Sub_image($product->id)['status'] == true)
                                                <img src="{{ asset($product->Sub_image($product->id)['data'][0]->image_path ?? '') }}"
                                                    class="hover-img">
                                            @else
                                                <img src="{{ asset($product->Sub_image($product->id) ?? '') }}" class="hover-img">
                                            @endif
                                            </a>
                                            {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $product->id) !!}
                                            <div class="like-items-icon">
                                                <a class="wishlist wishbtn wishbtn-globaly" href="javascript:void(0)" title="Wishlist" tabindex="0"  product_id="{{$product->id}}" in_wishlist="{{ $product->in_whishlist ? 'remove' : 'add'}}">
                                                    <span class="wish-ic">
                                                        <i class="{{ $product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"></i>
                                                    </span>
                                                </a>
                                                <div class="product-btn-wrp">
                                                    @php
                                                        $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                        $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                                    @endphp
                                                    @if(isset($module) && $module->isEnabled())
                                                        {{-- Include the module blade button --}}
                                                        @include('productquickview::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                    @endif
                                                    @if(isset($compare_module) && $compare_module->isEnabled())
                                                        {{-- Include the module blade button --}}
                                                        @include('productcompare::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="product-content text-center">
                                            <div class="product-content-top">
                                                <h3 class="product-title">
                                                    <a href="{{url($slug.'/product/'.$product->slug)}}" >
                                                    {{$product->name}}
                                                    </a>
                                                </h3>
                                            </div>
                                            <div class="product-content-bottom d-flex align-items-center justify-content-center">
                                                @if($product->variant_product == 0)
                                                    <div class="price">
                                                        <ins>{{  currency_format_with_sym(($product->sale_price ?? $product->price) , $store->id, $currentTheme) }}</ins>
                                                    </div>
                                                @else
                                                    <div class="price">
                                                        <ins>{{__('In Variant')}}</ins>
                                                    </div>
                                                @endif
                                                <a href="JavaScript:void(0)" class="addtocart-btn btn addcart-btn-globaly" product_id="{{ $product->id }}" variant_id="0" qty="1" >
                                                        {{__('Add to cart')}}
                                                    <svg width="13" height="13" viewBox="0 0 13 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M9.24173 3.38727H2.75827C2.10699 3.38727 1.59075 3.93678 1.63138 4.58679L1.91365 9.10315C1.95084 9.69822 2.44431 10.1618 3.04054 10.1618H8.95946C9.55569 10.1618 10.0492 9.69822 10.0863 9.10315L10.3686 4.58679C10.4092 3.93678 9.89301 3.38727 9.24173 3.38727ZM2.75827 2.25818C1.4557 2.25818 0.423236 3.35719 0.504488 4.65722L0.78676 9.17358C0.861144 10.3637 1.84808 11.2909 3.04054 11.2909H8.95946C10.1519 11.2909 11.1389 10.3637 11.2132 9.17358L11.4955 4.65722C11.5768 3.35719 10.5443 2.25818 9.24173 2.25818H2.75827Z" fill="#ffffff"></path>
                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M3.17727 2.82273C3.17727 1.26378 4.44105 0 6 0C7.55895 0 8.82273 1.26378 8.82273 2.82273V3.95182C8.82273 4.26361 8.56997 4.51636 8.25818 4.51636C7.94639 4.51636 7.69363 4.26361 7.69363 3.95182V2.82273C7.69363 1.88736 6.93537 1.12909 6 1.12909C5.06463 1.12909 4.30636 1.88736 4.30636 2.82273V3.95182C4.30636 4.26361 4.05361 4.51636 3.74182 4.51636C3.43003 4.51636 3.17727 4.26361 3.17727 3.95182V2.82273Z" fill="#ffffff"></path>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>


