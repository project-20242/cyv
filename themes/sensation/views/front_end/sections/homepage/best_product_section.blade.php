<section class="best-product-section padding-top" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-7 col-12">
                            <div class="best-product-left-inner">
                                <div class="section-title">
                                    <h2 id="{{ $section->best_product->section->title->slug ?? '' }}_preview"> {!!
                $section->best_product->section->title->text ?? '' !!} </h2>
                                </div>
                                <p  id="{{ $section->best_product->section->description->slug ?? '' }}_preview"> {!!
                $section->best_product->section->description->text ?? '' !!} </p>
                                <div class="select-scent text-checkbox checkbox-radio">
                                    <p id="{{ $section->best_product->section->sub_title->slug ?? '' }}_preview"> {!!
                $section->best_product->section->sub_title->text ?? '' !!} </p>
                                    @foreach ($categories as $category)
                                        <div class="checkbox">
                                            <input id="Select-scent-1" name="checkbox" type="checkbox" value="Mandarin">
                                            <label for="Select-scent-1" class="checkbox-label">
                                                <a href="{{route('page.product-list',[$slug,'main_category' => $category->id ])}}">{{ucfirst($category->name)}}</a></label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-12">
                            <div class="best-product-right-inner">
                                <div class="product-banner-image-right">
                                    <img src="{{asset( $section->best_product->section->image->image ?? '' )}}" id="{{ $section->best_product->section->image->slug ?? '' }}_preview" alt="product banner right">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>