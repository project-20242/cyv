
@foreach ($blogs as $key => $blog)
@if($request->cat_id == '0' || $blog->category_id == $request->cat_id)

<div class="col-lg-3 col-md-4 col-sm-6 col-12 blog-itm">
            <div class="blog-card-itm">
                <div class="blog-card-itm-inner">
                    <div class="blog-card-image">
                        <a href="{{route('page.article',[$slug,$blog->id])}}" tabindex="0">
                            <img src="{{ asset($blog->cover_image_path) }}" class="default-img {{ $blog->id }}">
                        </a>
                        <div class="tip-lable">
                            <span>{{$blog->category->name}}</span>
                        </div>
                    </div>
                    <div class="blog-card-content">
                        <div class="blog-card-heading-detail">
                            <span>{{$blog->created_at->format('d M,Y ')}} / John Doe</span>
                        </div>
                        <h4>
                            <a class="title" href="{{route('page.article',[$slug,$blog->id])}}" tabindex="0">
                                {{$blog->title}}
                            </a>
                        </h4>
                        <p class="description"> {{$blog->short_description}}</p>
                        <div class="blog-card-bottom">
                            <a href="{{route('page.article',[$slug,$blog->id])}}" class=" btn" tabindex="0">
                                {{__('Read More')}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @endforeach
