@extends('front_end.layouts.app')
@section('page-title')
{{ __('Article Page') }}
@endsection
@section('content')
@include('front_end.sections.partision.header_section')
@foreach ($blogs as $blog)
<section class="blog-page-banner article-banner common-banner-section" style="background-image:url({{ asset('themes/' . $currentTheme . '/assets/images/blog-page-banner.jpg')}});">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-7 col-md-8 col-12">
                        <div class="common-banner-content text-center">
                            <a href="{{route('landing_page',$slug)}}" class="back-btn">
                                <span class="svg-ic">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="5" viewBox="0 0 11 5" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M10.5791 2.28954C10.5791 2.53299 10.3818 2.73035 10.1383 2.73035L1.52698 2.73048L2.5628 3.73673C2.73742 3.90636 2.74146 4.18544 2.57183 4.36005C2.40219 4.53467 2.12312 4.53871 1.9485 4.36908L0.133482 2.60587C0.0480403 2.52287 -0.000171489 2.40882 -0.000171488 2.2897C-0.000171486 2.17058 0.0480403 2.05653 0.133482 1.97353L1.9485 0.210321C2.12312 0.0406877 2.40219 0.044729 2.57183 0.219347C2.74146 0.393966 2.73742 0.673036 2.5628 0.842669L1.52702 1.84888L10.1383 1.84875C10.3817 1.84874 10.5791 2.04609 10.5791 2.28954Z" fill="white"></path>
                                    </svg>
                                </span>
                                {{__('Back to Home')}}
                            </a>
                            <ul class="blog-cat justify-content-center">
                                <li class="active"><a href="#">{{ __('Featured')}}</a></li>
                                <li><a href="#"><b>{{__('Category:')}}</b>{{$blog->category->name}}</a></li>
                                <li><a href="#"><b>{{__('Date:')}}</b>  {{$blog->created_at->format('d M, Y ')}}</a></li>
                            </ul>
                            <div class="section-title text-center">
                                <h2>{{$blog->title}}</h2>
                            </div>
                            <p>{{$blog->short_description}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="article-section padding-bottom padding-top">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="about-user d-flex align-items-center">
                            <div class="abt-user-img">
                                <img src="{{ asset('themes/' . $currentTheme . '/assets/images/john.png')}}">
                            </div>
                            <h6>
                                <span>{{__('John Doe,')}}</span>
                                 {{__('company.com')}}
                            </h6>
                            <div class="post-lbl"><b> {{ __('Category:')}} </b> {{$blog->category->name}}</div>
                            <div class="post-lbl"><b> {{ __('Date:')}} </b> {{$blog->created_at->format('d M, Y ')}}</div>
                        </div>
                    </div>
                    <div class="col-md-8 col-12">
                        <div class="aticleleftbar">
                            {!! html_entity_decode($blog->content) !!}
                            <div class="art-auther"><b> {{ __('Tags:')}} </b>{{$blog->category->name}}</div>
                           
                                <ul class="article-socials d-flex align-items-center">
                                <li><span>{{__('Share:') }}</span></li>

@for($i=0 ; $i < $section->footer->section->footer_link->loop_number ?? 1;$i++) <li>
    <a href="{!!$section->footer->section->footer_link->social_link->{$i} ?? '#' !!}">
        <img src=" {{  asset($section->footer->section->footer_link->social_icon->{$i}->image ?? 'themes/' . $currentTheme . '/assets/images/youtube.svg') }}"
            alt="youtube">
    </a>

    </li>
    @endfor
                                </ul>
                        </div>
                    </div>
                    <div class="col-md-4 col-12 blog-section blog-itm">
                        <div class="articlerightbar">
                            @foreach ($datas->take(2) as $data)
                                <div class="blog-card-itm">
                                    <div class="blog-card-itm-inner">
                                        <div class="blog-card-image">
                                            <a href="{{route('page.article',[$slug,$data->id])}}" tabindex="0">
                                                <img src="{{asset($data->cover_image_path)}} " class="default-img">
                                            </a>
                                            <div class="tip-lable">
                                                <span>{{$blog->category->name}}</span>
                                            </div>
                                        </div>
                                        <div class="blog-card-content">
                                            <div class="blog-card-heading-detail">
                                                <span>{{$blog->created_at->format('d M,Y ')}}  / John Doe</span>
                                            </div>
                                            <h4>
                                                <a class="title" href="{{route('page.article',[$slug,$data->id])}}" tabindex="0">
                                                    {{$data->title}}
                                                </a>
                                            </h4>
                                            <p class="description">{{$data->short_description}}</p>
                                            <div class="blog-card-bottom">
                                                <a href="{{route('page.article',[$slug,$data->id])}}" class=" btn" tabindex="0">
                                                    {{__('Read More')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </section>

@endforeach
<section class="blog-section article-page padding-top padding-bottom">
    <div class="container">
        <div class="section-title">
            <h2>
                {{__('From our blog')}}
            </h2>
        </div>
        <div class="about-card-slider flex-slider">
            {!! \App\Models\Blog::HomePageBlog($currentTheme, $slug, 10) !!}
        </div>
    </div>
</section>
@include('front_end.sections.partision.footer_section')
@endsection