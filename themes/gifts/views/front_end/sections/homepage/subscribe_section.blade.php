<section class="subscribe-section padding-bottom" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
<div class="custome_tool_bar"></div>
            <div class="container">
                    <div class="row align-items-center">
                        <div class="col-md-6 col-12">
                            <div class="subscribe-column">
                                <div class="section-title">
                                    <h2 id="{{ ($section->subscribe->section->title->slug ?? '') }}_preview">{!!
                                $section->subscribe->section->title->text ?? "" !!}</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-12">
                            <div class="subscribe-section-form">
                                <p id="{{ ($section->subscribe->section->description->slug ?? '') }}_preview">{!!
                                $section->subscribe->section->description->text ?? "" !!}</p>
                                <form class="footer-subscribe-form" action="{{ route('newsletter.store', $slug) }}"
                                    method="post">
                                    @csrf
                                    <div class="input-wrapper">
                                        <input type="email" placeholder="Escribe tu correo electronico..." name="email">
                                        <button type="submit" class="btn-subscibe">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17"
                                                viewBox="0 0 17 17" fill="none">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M13.9595 9.00095C14.2361 8.72433 14.2361 8.27584 13.9595 7.99921L9.70953 3.74921C9.43291 3.47259 8.98441 3.47259 8.70779 3.74921C8.43117 4.02584 8.43117 4.47433 8.70779 4.75095L12.4569 8.50008L8.70779 12.2492C8.43117 12.5258 8.43117 12.9743 8.70779 13.2509C8.98441 13.5276 9.4329 13.5276 9.70953 13.2509L13.9595 9.00095ZM4.04286 13.2509L8.29286 9.00095C8.56948 8.72433 8.56948 8.27584 8.29286 7.99921L4.04286 3.74921C3.76624 3.47259 3.31775 3.47259 3.04113 3.74921C2.7645 4.02583 2.7645 4.47433 3.04113 4.75095L6.79026 8.50008L3.04112 12.2492C2.7645 12.5258 2.7645 12.9743 3.04112 13.2509C3.31775 13.5276 3.76624 13.5276 4.04286 13.2509Z"
                                                    fill="#0A062D"></path>
                                            </svg>
                                        </button>
                                    </div>
                                </form>
                                {{-- <div class="checkbox-custom"> --}}
                                {{-- <input type="checkbox" class="" id="subsection"> --}}
                                <label for="subsection" id="{{ ($section->subscribe->section->sub_title->slug ?? '') }}_preview">{!!
                                $section->subscribe->section->sub_title->text ?? "" !!}</label>
                                {{-- </div> --}}
                            </div>
                        </div>
                    </div>
            </div>
        </section>
