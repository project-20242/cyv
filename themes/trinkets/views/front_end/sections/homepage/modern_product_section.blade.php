<section class="product-second-section" style="@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>

    <img id="{{ $section->modern_product->section->image->slug }}_preview"
        src="{{ asset($section->modern_product->section->image->text ?? '') }}" class="background-img" alt="img">
    <div class="container">
        <div class=" banner-content-inner">
            <div class="section-title">
                <h2 id="{{ $section->modern_product->section->title->slug }}_preview">{!! $section->modern_product->section->title->text ?? '' !!}</h2>
            </div>
            <p id="{{ $section->modern_product->section->description->slug }}_preview">{!! $section->modern_product->section->description->text ?? '' !!}</p>
        </div>
        <div class="row product-list-row">
            {!! \App\Models\Product::GetLatestProduct($currentTheme, $slug, 2) !!}
        </div>
    </div>
</section>
