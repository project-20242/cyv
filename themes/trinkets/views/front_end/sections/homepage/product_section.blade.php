<section class="product-prescription-section-two padding-bottom"
    style="@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-7 col-sm-6  col-12">
                <div class="left-side-image">
                    <img id="{{ $section->product->section->image->slug }}_preview"
                        src="{{ asset($section->product->section->image->text ?? '') }}">
                </div>
            </div>
            <div class="col-lg-5 col-sm-6 col-12 right-col">
                <div class=" banner-content-inner">
                    <div class="section-title">
                        <h2 id="{{ $section->product->section->title->slug }}_preview">{!! $section->product->section->title->text ?? '' !!}</h2>
                    </div>
                    <p id="{{ $section->product->section->description->slug }}_preview">{!! $section->product->section->description->text ?? '' !!}</p>
                    <a href="{{ route('page.product-list', $slug) }}"
                        class="btn white-btn" id="{{ $section->product->section->button->slug }}_preview">
                        {!! $section->product->section->button->text ?? '' !!}
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
