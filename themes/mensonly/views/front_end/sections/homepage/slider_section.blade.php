<section class="home-banner-section padding-top padding-bottom"
    style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="offset-container offset-left">
        <div class="home-banner-slider">
            @for ($i = 0; $i < $section->slider->loop_number ?? 1; $i++)
                <div class="banner-itm">
                    <div class="banner-itm-inner">
                        <div class="row col-reverse no-gutters align-items-center">
                            <div class="col-md-6 col-12">
                                <div class="banner-left-col">
                                    <div class="section-title">
                                        <h3 class="subtitle"
                                            id="{{ ($section->slider->section->sub_title->slug ?? '').'_'. $i }}_preview">
                                            {!!
                                            ($section->slider->section->sub_title->text->{$i} ?? '') !!}</h3>
                                        <span class="subtitle-second"
                                            id="{{ ($section->slider->section->button_first->slug ?? '').'_'. $i }}_preview">{!!
                                            ($section->slider->section->button_first->text->{$i} ?? '') !!}</span>
                                        <h2 id="{{ ($section->slider->section->title->slug ?? '').'_'. $i }}_preview">
                                            {!!
                                            $section->slider->section->title->text->{$i} ?? '' !!}
                                        </h2>
                                    </div>
                                    <p id="{{ ($section->slider->section->description->slug ?? '').'_'. $i}}_preview">
                                        {!!
                                        ($section->slider->section->description->text->{$i} ?? '') !!}</p>
                                    <div class="button-wrappper d-flex">
                                        <a href="{{ route('page.product-list', $slug) }}" class="btn" tabindex="0"
                                            id="{{ ($section->slider->section->button->slug ?? '').'_'. $i }}_preview">{!!
                                            ($section->slider->section->button->text->{$i} ?? '') !!}
                                            <svg xmlns="http://www.w3.org/2000/svg" width="17" height="14"
                                                viewBox="0 0 17 14" fill="none">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M13.4504 8.90368H6.43855C5.48981 8.90395 4.67569 8.22782 4.50176 7.29517L3.58917 2.35144C3.53142 2.03558 3.25368 1.80784 2.93263 1.81308H1.40947C1.04687 1.81308 0.75293 1.51913 0.75293 1.15654C0.75293 0.793942 1.04687 0.5 1.40947 0.5H2.94577C3.8945 0.499732 4.70862 1.17586 4.88255 2.10852L5.79514 7.05225C5.85289 7.3681 6.13063 7.59584 6.45168 7.59061H13.4569C13.778 7.59584 14.0557 7.3681 14.1135 7.05225L14.9407 2.58779C14.9761 2.3943 14.923 2.19512 14.7958 2.04506C14.6686 1.89499 14.4808 1.80986 14.2842 1.81308H6.66177C6.29917 1.81308 6.00523 1.51913 6.00523 1.15654C6.00523 0.793942 6.29917 0.5 6.66177 0.5H14.2776C14.8633 0.499835 15.4187 0.760337 15.793 1.2108C16.1673 1.66126 16.3218 2.25494 16.2144 2.83071L15.3872 7.29517C15.2132 8.22782 14.3991 8.90395 13.4504 8.90368ZM9.28827 11.5304C9.28827 10.4426 8.40644 9.56081 7.31866 9.56081C6.95606 9.56081 6.66212 9.85475 6.66212 10.2173C6.66212 10.5799 6.95606 10.8739 7.31866 10.8739C7.68125 10.8739 7.97519 11.1678 7.97519 11.5304C7.97519 11.893 7.68125 12.187 7.31866 12.187C6.95606 12.187 6.66212 11.893 6.66212 11.5304C6.66212 11.1678 6.36818 10.8739 6.00558 10.8739C5.64299 10.8739 5.34904 11.1678 5.34904 11.5304C5.34904 12.6182 6.23087 13.5 7.31866 13.5C8.40644 13.5 9.28827 12.6182 9.28827 11.5304ZM13.2277 12.8432C13.2277 12.4806 12.9338 12.1867 12.5712 12.1867C12.2086 12.1867 11.9146 11.8928 11.9146 11.5302C11.9146 11.1676 12.2086 10.8736 12.5712 10.8736C12.9338 10.8736 13.2277 11.1676 13.2277 11.5302C13.2277 11.8928 13.5217 12.1867 13.8843 12.1867C14.2468 12.1867 14.5408 11.8928 14.5408 11.5302C14.5408 10.4424 13.659 9.56055 12.5712 9.56055C11.4834 9.56055 10.6016 10.4424 10.6016 11.5302C10.6016 12.6179 11.4834 13.4998 12.5712 13.4998C12.9338 13.4998 13.2277 13.2058 13.2277 12.8432Z"
                                                    fill="#0A062D"></path>
                                            </svg>
                                        </a>
                                        <a href="javascript:void()" class="play-btn">
                                            <img src="{{ asset('themes/' . $currentTheme . '/assets/img/video.png') }}"
                                                alt="">
                                            <span>play video</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="banner-right-col">
                                    <div class="hero-main-img">
                                        <img src="{{ asset($section->slider->section->image->image->{$i} ?? '') }}"
                                            class="img-fluid {{ ($section->slider->section->image->slug ?? '').'_'.$i}}_preview"
                                            alt="HeroImage-1">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endfor
        </div>
        <div class="slider-navgation">
            <div class="slider-nav"></div>
            <span class="pagingInfo"></span>
            <div class="progress" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="50">
                <span class="slider__label sr-only">50% completado</span>
            </div>
        </div>
    </div>
</section>