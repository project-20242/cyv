import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";
import path from "path";



export default defineConfig({
    plugins: [
        laravel({
            input: [
                "themes\chocolate\sass/app.scss",
                "themes\chocolate\js/app.js"
            ],
            buildDirectory: "chocolate",
        }),
        
        
        {
            name: "blade",
            handleHotUpdate({ file, server }) {
                if (file.endsWith(".blade.php")) {
                    server.ws.send({
                        type: "full-reload",
                        path: "*",
                    });
                }
            },
        },
    ],
    resolve: {
        alias: {
            '@': '/themes\chocolate\js',
            '~bootstrap': path.resolve('node_modules/bootstrap'),
        }
    },
    
});
