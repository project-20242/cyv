<section class="best-product padding-bottom" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
        <div class="container">

            <div class="section-title d-flex align-items-center justify-content-between">
                <h2 class="title" id="{{ $section->best_product_second->section->title->slug ?? '' }}_preview"> {!!
                                    $section->best_product_second->section->title->text ?? '' !!}</h2>
                <a href="{{route('page.product-list',$slug)}}" class="btn desk-only" id="{{ $section->best_product_second->section->button->slug ?? '' }}_preview"> {!!
                                    $section->best_product_second->section->button->text ?? '' !!}
                    <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6" viewBox="0 0 4 6" fill="none">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                            fill="white"></path>
                    </svg>
                </a>
            </div>

            <div class="row product-list">
                <div class="col-lg-4 col-xl-3 col-md-6 col-sm-6 col-12">
                    @foreach($MainCategoryList->take(1) as $category)
                    <div class="category-card second-style">
                        <div class="category-img">
                            <img src="{{asset($category->image_path)}}" alt="Nuts">
                        </div>
                        <div class="category-card-body">
                            <div class="title-wrapper">
                                <h3 class="title">
                                    <a href="{{route('page.product-list',[$slug,'main_category' => $category->id ])}}">{!! $category->name !!}</a>
                                </h3>
                            </div>
                            <div class="btn-wrapper">
                                <a href="{{route('page.product-list',$slug)}}" class="btn">{{ __('Show more')}} <svg
                                        xmlns="http://www.w3.org/2000/svg" width="4" height="6" viewBox="0 0 4 6"
                                        fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                            fill="white"></path>
                                    </svg></a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                @foreach($homeproducts->take(3) as $product)
                <div class="col-lg-4 col-xl-3 col-md-6 col-sm-6 product-card col-12">

                    <div class="product-card-inner">
                        <div class="product-card-image">
                            <a href="{{route('page.product',[$slug,$product->id])}}">
                                <img src="{{ asset($product->cover_image_path ) }}" class="default-img">
                            </a>
                            {{-- <div class="custom-output">
                                @foreach ($latestSales as $productId => $saleData)
                                    <div class="new-labl  danger">
                                        @if ($saleData['discount_type'] == 'flat')
                                            -{{ $saleData['discount_amount'] }}{{ $currency_icon }}
                                        @elseif ($saleData['discount_type'] == 'percentage')
                                            -{{ $saleData['discount_amount'] }}%
                                        @endif
                                    </div>
                                @endforeach
                            </div> --}}
                            <div class="new-labl  danger">
                    {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $product->id) !!}
                </div>
                        </div>
                        <div class="product-content">
                            <div class="product-content-top d-flex align-items-end">
                                <div class="product-content-left">
                                    {{-- <div class="product-type">{{$product->label->name ?? ''}}</div>--}}
                                    <h3 class="product-title">
                                        <a href="{{route('page.product',[$slug,$product->id])}}">
                                            {{$product->name}}
                                        </a>
                                    </h3>
                                    <div class="reviews-stars-wrap d-flex align-items-center">
                                        @if (!empty($product->average_rating))
                                            @for ($i = 0; $i < 5; $i++)
                                                <i class="fa fa-star {{ $i < $product->average_rating ? '' : 'text-warning' }} "></i>
                                            @endfor
                                            <span><b>{{ $product->average_rating }}.0</b> / 5.0</span>
                                        @else
                                            @for ($i = 0; $i < 5; $i++)
                                                <i lass="fa fa-star {{ $i < $product->average_rating ? '' : 'text-warning' }} "></i>
                                            @endfor
                                            <span><b>{{ $product->average_rating }}.0</b> / 5.0</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="product-content-center">
                                @if ($product->variant_product == 0)
                                    <div class="price">
                                        <ins class="text-danger">{{ currency_format_with_sym(($product->sale_price ?? $product->price), $store->id, $currentTheme)}} <span class="currency-type">{{$currency}}</span></ins>
                                        <del>{{$product->price}} {{$currency}}</del>
                                    </div>
                                @else
                                    <div class="price">
                                        <ins>{{ __('In Variant') }}</ins>
                                    </div>
                                @endif
                            </div>
                            <div class="product-content-bottom d-flex align-items-center justify-content-between">
                                <button class="btn addcart-btn-globaly" product_id="{{ $product->id }}" variant_id="0" qty="1">
                                    <span>{{ __('Add to cart')}} </span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6" viewBox="0 0 4 6"
                                        fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                            fill="white"></path>
                                    </svg>
                                </button>
                                    <a href="javascript:void(0)" class="wishlist-btn wbwish  wishbtn-globaly" product_id="{{$product->id}}" in_wishlist="{{ $product->in_whishlist ? 'remove' : 'add'}}">
                                        <span class="wish-ic">
                                            <i class="{{ $product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"
                                                style='color: #fff'></i>
                                        </span>
                                    </a>
                                    <div class="product-btn-wrp">
                                        @php
                                            $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                            $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                        @endphp
                                        @if(isset($module) && $module->isEnabled())
                                            {{-- Include the module blade button --}}
                                            @include('productquickview::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                        @endif
                                        @if(isset($compare_module) && $compare_module->isEnabled())
                                            {{-- Include the module blade button --}}
                                            @include('productcompare::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                        @endif
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
            <div class="mobile-only d-flex justify-content-center text-center">
                <a href="{{route('page.product-list',$slug)}}" class="btn" id="{{ $section->best_product_second->section->button_left->slug ?? '' }}_preview"> {!!
                                    $section->best_product_second->section->button_left->text ?? '' !!}
                    <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6" viewBox="0 0 4 6" fill="none">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                            fill="white"></path>
                    </svg>
                </a>
            </div>
        </div>
    </section>
