
<section class="padding-top shop-product-second padding-bottom"    style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">

            <div class="section-title text-center">
                <h2 class="title" id="{{ $section->product_category->section->title->slug ?? '' }}_preview">{!! $section->product_category->section->title->text ?? '' !!}</h2>
                <div class="descripion" id="{{ $section->product_category->section->description->slug ?? '' }}_preview">
                    <p>{{ $section->product_category->section->description->text ?? '' }}</p>
                </div>
            </div>

        <div class="row row-gap">
            @foreach ($discount_products->take(8) as $homeproduct)

                <div class="col-lg-4 col-xl-3 col-md-6 col-sm-6 product-card col-12">
                    <div class="product-card-inner no-back">
                        <div class="product-card-image">
                            <a href="{{ url($slug.'/product/'.$homeproduct->slug) }}">
                                <img src="{{ asset($homeproduct->cover_image_path) }}"
                                    class="default-img">
                                @if ($homeproduct->Sub_image($homeproduct->id)['status'] == true)
                                    <img src="{{ asset($homeproduct->Sub_image($homeproduct->id)['data'][0]->image_path) }}"
                                        class="hover-img">
                                @else
                                    <img src="{{ asset($homeproduct->Sub_image($homeproduct->id)) }}"
                                        class="hover-img">
                                @endif
                            </a>
                            <div class="new-labl">
                                {{ $homeproduct->ProductData->name }}
                            </div>
                        </div>
                        <div class="product-content">
                            <div class="product-content-top ">
                                <div class="custom-output">
                                {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $homeproduct->id) !!}
                                </div>
                                <h3 class="product-title short_description">
                                    <a href="{{ url($slug.'/product/'.$homeproduct->slug) }}">
                                        {{ $homeproduct->name }}
                                    </a>
                                </h3>
                                <div class="reviews-stars-wrap d-flex align-items-center justify-content-center">
                                    @if (!empty($homeproduct->average_rating))
                                        <div class="reviews-stars-outer">
                                            @for ($i = 0; $i < 5; $i++)
                                                <i
                                                    class="fa fa-star review-stars {{ $i < $homeproduct->average_rating ? '' : 'text-warning' }} "></i>
                                            @endfor
                                        </div>
                                        <div class="point-wrap">
                                            <span class="review-point"><b>{{ $homeproduct->average_rating }}.0</b> / 5.0</span>
                                        </div>
                                    @else
                                        <div class="reviews-stars-outer">
                                            @for ($i = 0; $i < 5; $i++)
                                                <i
                                                    class="fa fa-star review-stars {{ $i < $homeproduct->average_rating ? '' : 'text-warning' }} "></i>
                                            @endfor
                                        </div>
                                        <div class="point-wrap">
                                            <span class="review-point"><b>{{ $homeproduct->average_rating }}.0</b> / 5.0</span>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <div class="product-content-center">
                                @if ($homeproduct->variant_product == 0)
                                    <div class="price">
                                        <ins>{{ currency_format_with_sym(($homeproduct->sale_price ?? $homeproduct->price) , $store->id, $currentTheme) }} </ins>
                                        <del>{{ currency_format_with_sym($homeproduct->price , $store->id, $currentTheme) }}</del>{{-- {{ $currency }}--}}
                                    </div>
                                @else
                                    <div class="price">
                                        <ins>{{ __('In Variant') }}</ins>
                                    </div>
                                @endif
                            </div>
                            <div class="product-content-bottom d-flex align-items-center justify-content-between">
                                <div class="bottom-select  d-flex align-items-center justify-content-between">
                                    <div class="cart-btn-wrap">
                                        <button class="btn addcart-btn-globaly"
                                            product_id="{{ $homeproduct->id }}" variant_id="0" qty="1">
                                            <span> {{ __('Add to cart') }}</span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6"
                                                viewBox="0 0 4 6" fill="none">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                                    fill="white"></path>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                                <button href="javascript:void(0)" class="wishlist-btn wbwish  wishbtn-globaly"
                                    product_id="{{ $homeproduct->id }}"
                                    in_wishlist="{{ $homeproduct->in_whishlist ? 'remove' : 'add' }}">
                                    <span class="wish-ic">
                                        <i
                                            class="{{ $homeproduct->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"></i>
                                    </span>
                                </button>
                                <div class="product-btn-wrp">
                                    @php
                                        $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                        $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                    @endphp
                                    @if(isset($module) && $module->isEnabled())
                                        {{-- Include the module blade button --}}
                                        @include('productquickview::pages.button', ['product_slug' => $homeproduct->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                    @endif
                                    @if(isset($compare_module) && $compare_module->isEnabled())
                                        {{-- Include the module blade button --}}
                                        @include('productcompare::pages.button', ['product_slug' => $homeproduct->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
