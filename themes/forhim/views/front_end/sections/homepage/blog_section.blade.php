<section class="home-blog-section padding-top"  style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>

        <div class="container">
                <div class="section-title d-flex justify-content-between align-items-center">
                    <div class="section-title-left">
                    <h2 id="{{ $section->blog->section->title->slug ?? '' }}_preview">{!!
                     $section->blog->section->title->text ?? '' !!}</h2>
                    </div>
                    <div class="section-title-center">
                    <p id="{{ $section->blog->section->description->slug ?? '' }}_preview">{!!
                            $section->blog->section->description->text ?? '' !!}</p>
                    </div>
                    <div class="section-title-right">
                        <a href="{{route('page.product-list',$slug)}}" class="btn-transparent"  id="{{ $section->blog->section->button->slug ?? '' }}_preview"> {!!
                         $section->blog->section->button->text ?? '' !!}
                        </a>
                    </div>
                </div>
            <div class="blog-two-row-slider common-arrows">
                {!! \App\Models\Blog::HomePageBlog($currentTheme, $slug,12) !!}
            </div>
        </div>
    </section>
