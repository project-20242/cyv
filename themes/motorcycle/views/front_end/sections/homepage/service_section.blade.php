<section class="services-section"  style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
        <div class="container">
            <div class="section-title d-flex justify-content-between align-items-center">
                <h2>{{ __('Bestsellers') }}</h2>
                <a href="{{route('page.product-list',$slug)}}" class="btn show-more-btn">
                       {{ __('Show More') }}
                </a>
            </div>
           <div class="row service-row">
                @for ($i = 0; $i < 3 ?? 1; $i++)
                    <div class="col-md-4 col-sm-6 col-12 service-col">
                        <div class="service-card">
                            <div class="img-wrapper">
                                <img src="{{asset($section->service_section->section->image->image->{$i} ?? '')}}" alt="service">
                            </div>
                            <h3 id="{{ ($section->service_section->section->sub_title->slug ?? '') .'_'. $i}}_preview">{!! $section->service_section->section->sub_title->text->{$i} ?? '' !!}
                            </h3>
                            <p id="{{ ($section->service_section->section->description->slug ?? '') .'_'. $i}}_preview">{!! $section->service_section->section->description->text->{$i} ?? '' !!}</p>
                            <a href="{{route('page.product-list',$slug)}}" class="btn whit-btn" id="{{ ($section->service_section->section->button->slug ?? '') .'_'. $i}}_preview">
                                {!! $section->service_section->section->button->text->{$i} ?? '' !!} </a>
                        </div>
                    </div>
                @endfor
            </div> 
        </div>
    </section>


