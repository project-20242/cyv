<section class="logo-slider-section padding-top padding-bottom"  style="position: relative;@if(isset($option) && $option->is_hide == 1)  opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
        <div class="container">
            <div class="logo-slider-main">
                @if(!empty($homepage_main_logo['homepage-logo-logo-text']))
                    @for ($i = 0; $i <= 10; $i++)
                        <div class="logo-slides">
                            <h4>{{$homepage_logo}}</h4>
                        </div>
                    @endfor
                @else
                    @for ($i = 0; $i <= 10; $i++)
                        <div class="logo-slides">
                            <img src="{{ asset((isset($section->logo_slider->section->image->image->{$i}) && !empty($section->logo_slider->section->image->image->{$i})) ? $section->logo_slider->section->image->image->{$i} : 'themes/' . $currentTheme. '/assets/images/logo.png') }}" alt="">
                        </div>
                    @endfor
                @endif
            </div>
        </div>
</section>