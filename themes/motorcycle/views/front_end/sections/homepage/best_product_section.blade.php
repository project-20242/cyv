<section
    style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="best-pro-section  @if(request()->route()->getName() != 'landing_page') padding-top @endif padding-bottom">
    <div class="offset-container offset-right">
        <div class="row no-gutters">
            <div class="col-md-6 left-side">
                <div class="img-wrapper">
                    <img src="{{ asset($section->best_product->section->image->image ?? '') }}" alt="bike">
                </div>
            </div>
            <div class="col-md-6 right-side">
                <div class="right-content">
                    <span class="sub-title"
                        id="{{ ($section->best_product->section->sub_title->slug ?? '')}}_preview">
                        {!! $section->best_product->section->sub_title->text ?? '' !!}
                        </span>
                    <div class="section-title">
                        <h2 id="{{ ($section->best_product->section->title->slug ?? '')}}_preview">
                            {!! $section->best_product->section->title->text ?? '' !!}</h2>
                    </div>
                    <p id="{{ ($section->best_product->section->description->slug ?? '')}}_preview">
                        {!! $section->best_product->section->description->text ?? '' !!}</p>
                    <a href="{{route('page.product-list',$slug)}}" class="btn"
                        id="{{ $section->best_product->section->button->slug ?? '' }}_preview">
                        {!! $section->best_product->section->button->text ?? '' !!}
                    </a>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>