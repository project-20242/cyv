@extends('front_end.layouts.app')
@section('page-title')
{{ __('Article Page') }}
@endsection
@section('content')
@include('front_end.sections.partision.header_section')
@foreach ($blogs as $blog)
<section class="blog-page-banner common-banner-section"
    style="background-image:url({{ asset('themes/' . $currentTheme . '/assets/images/blog-banner.png')}});">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="common-banner-content">
                    <ul class="blog-cat">
                        <li class="active"><a href="#"> {{ __('Destacado')}} </a></li>
                        <li><a href="#"><b> {{ __('Category:')}} </b> {{$blog->category->name}}</a></li>
                        <li><a href="#"><b> {{ __('Date:')}} </b> {{$blog->created_at->format('d M, Y ')}}</a></li>
                    </ul>
                    <div class="section-title">
                        <h3>{{$blog->title}}</h3>
                    </div>
                    <div class="about-user d-flex align-items-center">
                    <div class="abt-user-img">
                        <img src="{{asset('themes/'.$currentTheme.'/assets/images/john.png')}}" />
                    </div>
                    <h6>
                        <span>{{__('John Doe')}},</span>
                        {{__('company.com')}}
                    </h6>
                </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="article-section padding-top">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="about-user d-flex align-items-center">
                    <div class="abt-user-img">
                        <img src="{{asset('themes/'.$currentTheme.'/assets/images/john.png')}}" />
                    </div>
                    <h6>
                        <span>{{__('John Doe')}},</span>
                        {{__('company.com')}}
                    </h6>
                    <div class="post-lbl"><b> {{ __('Category:')}} </b> {{$blog->category->name}}</div>
                    <div class="post-lbl"><b> {{ __('Date:')}} </b> {{$blog->created_at->format('d M, Y ')}}</div>
                </div>
                {{-- <div class="section-title">
                       <h2>{{$blog->title}}</h2>
            </div> --}}
        </div>
        <div class="col-md-8 col-12">
            <div class="aticleleftbar">
                {!! html_entity_decode($blog->content) !!}
                <div class="art-auther">
                    <div class="art-auther"><b>{{__('Tags:')}} </b> {{$blog->category->name}}</div>
                </div>

                <ul class="article-socials d-flex align-items-center">
                    <li><span>{{__('Share:') }}</span></li>

                    @for($i=0 ; $i < $section->footer->section->footer_link->loop_number;$i++) <li>
                            <a href="{!!$section->footer->section->footer_link->social_link->{$i} ?? '#' !!}">
                                <img src=" {{  asset($section->footer->section->footer_link->social_icon->{$i}->image ?? 'themes/' . $currentTheme . '/assets/images/youtube.svg') }}"
                                    alt="youtube">
                            </a>

                        </li>
                        @endfor
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="articlerightbar">
                <div class="section-title">
                    <h2>{{ __('Related articles')}}</h2>
                </div>
                <div class="row blog-grid">
                    @foreach ($datas->take(2) as $data)
                    <div class="col-md-12 col-sm-6 col-12 blog-card">
                            <div class="blog-card-inner">
                                <div class="blog-media">
                                    <span class="new-labl bg-second">{{__('Food')}}</span>
                                    <a href="#">
                                        <img src="{{asset($blog->cover_image_path)}}">
                                    </a>
                                </div>
                                <div class="blog-content">
                                    <h4>
                                        <a class="description" href="{{route('page.article',[$slug,$data->id])}}">
                                            {{$data->title}}
                                        </a>
                                    </h4>
                                    <p class="description">{{$data->short_description}}</p>

                                    <div class="blog-lbl-row d-flex align-items-center justify-content-between">
                                        <a class="btn blog-btn" href="#">
                                            {{__('Read more')}}
                                            <svg xmlns="http://www.w3.org/2000/svg" width="3" height="6"
                                                viewBox="0 0 3 6" fill="none">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M0.15976 0.662719C-0.0532536 0.879677 -0.0532536 1.23143 0.15976 1.44839L1.68316 3L0.15976 4.55161C-0.0532533 4.76856 -0.0532532 5.12032 0.15976 5.33728C0.372773 5.55424 0.718136 5.55424 0.931149 5.33728L2.84024 3.39284C3.05325 3.17588 3.05325 2.82412 2.84024 2.60716L0.931149 0.662719C0.718136 0.445761 0.372773 0.445761 0.15976 0.662719Z"
                                                    fill="white" />
                                            </svg>
                                        </a>
                                        <div class="author-info">
                                            <strong class="auth-name">{{__('John Doe,')}}</strong>
                                            <span class="date">{{$blog->created_at->format('d M,Y ')}}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endforeach
<section class="blog-grid-section tabs-wrapper padding-bottom padding-top">
    <div class="container">
    {!! \App\Models\Blog::ArticlePageBlog($currentTheme, $slug) !!}
    </div>
</section>
@include('front_end.sections.partision.footer_section')
@endsection