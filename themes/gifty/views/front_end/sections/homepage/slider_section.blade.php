<section class="home-banner-section padding-top wrapper" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <img src="{{ asset($section->slider->section->image->image ?? '' ) }}" id="{{ $section->slider->section->image->slug ?? '' }}_preview" class="main-banner" alt="image">
    <div class="container">
        <div class="row">
            @if (!empty($latest_product))
            <div class="col-lg-4 col-md-6 col-sm-6 col-12 product-card banner-card">
                <div class="product-card-inner">
                    <div class="product-card-image image-inslide">
                        <div class="product-card-image-slider">
                            @if ($latest_product->Sub_image($latest_product->id)['status'] == true)
                            <img src="{{ asset($latest_product->cover_image_path) }}">
                            <div class="main-img">
                                <a href="{{ url($slug.'/product/'.$latest_product->slug) }}">
                                    <img
                                        src="{{ asset($latest_product->Sub_image($latest_product->id)['data'][0]->image_path) }}">
                                </a>
                            </div>
                            @else
                            <img src="{{ asset($latest_product->Sub_image($latest_product->id)) }}">
                            @endif
                        </div>
                        <div class="new-labl  danger">
                            {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $latest_product->id) !!}
                        </div>
                    </div>
                    <div class="product-content">
                        <div class="product-content-top d-flex align-items-end">
                            <div class="product-content-left">
                                <div class="product-subtitle">{{!empty($latest_product->ProductData) ? $latest_product->ProductData->name : ''}}</div>
                                <h3 class="product-title">
                                    <a href="{{ url($slug.'/product/'.$latest_product->slug) }}"
                                        class="short_description">
                                        {{ $latest_product->name }}
                                    </a>
                                </h3>
                            </div>
                            @if ($latest_product->variant_product == 0)
                            <div class="price">
                                <ins>{{ currency_format_with_sym(($latest_product->sale_price ?? $latest_product->price), $store->id, $currentTheme)}} </ins>
                            </div>
                            @else
                            <div class="price">
                                <ins>{{ __('In Variant') }}</ins>
                            </div>
                            @endif
                        </div>
                        <div class="product-content-bottom d-flex align-items-center justify-content-between">
                            <button class="btn red-btn addcart-btn-globaly" tabindex="0"
                                product_id="{{ $latest_product->id }}" variant_id="0" qty="1">
                                {{ __('Add to cart') }}
                                <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6" viewBox="0 0 4 6"
                                    fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                        fill="white" />
                                </svg>
                            </button>
                            <button href="javascript:void(0)" class="wishlist-btn wbwish  wishbtn-globaly"
                                product_id="{{ $latest_product->id }}"
                                in_wishlist="{{ $latest_product->in_whishlist ? 'remove' : 'add' }}">
                                <span class="wish-ic">
                                    <i
                                        class="{{ $latest_product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }} wishlist-icon"></i>
                                </span>
                            </button>
                            <div class="product-btn-wrp">
                                @php
                                    $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                    $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                @endphp
                                @if(isset($module) && $module->isEnabled())
                                    {{-- Include the module blade button --}}
                                    @include('productquickview::pages.button', ['product_slug' => $latest_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                @endif
                                @if(isset($compare_module) && $compare_module->isEnabled())
                                    {{-- Include the module blade button --}}
                                    @include('productcompare::pages.button', ['product_slug' => $latest_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="col-lg-4 col-md-6 col-sm-6 col-12 banner-card">
                @foreach ($MainCategoryList->take(1) as $category)
                    <div class="pro-card">
                        <div class="pro-card-image">
                            <img src="{{ asset($category->image_path) }}" alt="images">
                        </div>
                        <div class="pro-card-content">
                            <div class="title-wrap">
                                <h3 class="title">
                                    <a href="{{ route('page.product-list', [$slug,'main_category' => $category->id]) }}">{!!
                                        $category->name !!}</a>
                                </h3>
                            </div>
                            <div class="btn-wrapper">
                                <a href="{{ route('page.product-list', [$slug,'main_category' => $category->id]) }}"
                                    class="btn red-btn">
                                    {{ __('Show more') }}
                                    <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6" viewBox="0 0 4 6"
                                        fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                            fill="white"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="col-lg-4 col-md-12 col-sm-12 col-12">
                <div class="row">
                    @foreach ($products->take(2) as $homepage_product)
                        <div class="col-lg-12 col-xl-12 col-md-6 col-sm-6 col-12 product-right-first">
                            <div class="product-card-right">
                                <div class="product-img">
                                    <img src="{{asset($homepage_product->cover_image_path) }}" alt="Potato chips with onion">
                                </div>
                                <div class="product-card-body">
                                    <div class="card-head">
                                        <div class="badge-wrapper">
                                                <div class="new-labl  danger">
                                                    {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $homepage_product->id) !!}
                                                </div>
                                        </div>
                                    </div>
                                    <div class="title-wrapper">
                                        <div class="product-subtitle">{{!empty($homepage_product->ProductData) ? $homepage_product->ProductData->name : ''}}
                                        </div>
                                        <h3 class="title">
                                            <a
                                                href="{{ url($slug.'/product/'.$homepage_product->slug) }}">{{ $homepage_product->name }}</a>
                                        </h3>
                                    </div>
                                    <div class="card-footer">
                                        @if ($homepage_product->variant_product == 0)
                                        <div class="price-wrap">

                                            <ins class="price">{{ currency_format_with_sym(($homepage_product->sale_price ?? $homepage_product->price), $store->id, $currentTheme)}}
                                            <del>{{ currency_format_with_sym(($homepage_product->price ?? $homepage_product->price) , $store->id, $currentTheme) }}</del>
                                        </div>
                                        @else
                                        <div class="price-wrap">
                                            <ins>{{ __('In Variant') }}</ins>
                                        </div>
                                        @endif
                                        <div class="btn-wrapper">
                                            <button class="btn addcart-btn-globaly red-btn"
                                                product_id="{{ $homepage_product->id }}" variant_id="0" qty="1">
                                                {{ __('Add to cart') }}
                                                <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6"
                                                    viewBox="0 0 4 6" fill="none">
                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                        d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                                        fill="white"></path>
                                                </svg>
                                            </button>
                                            <button href="javascript:void(0)" class="wishlist-btn wbwish  wishbtn-globaly"
                                                product_id="{{ $homepage_product->id }}"
                                                in_wishlist="{{ $homepage_product->in_whishlist ? 'remove' : 'add' }}">
                                                <span class="wish-ic">
                                                    <i class="{{ $homepage_product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"
                                                        style="color: red"></i>
                                                </span>
                                            </button>
                                            <div class="product-btn-wrp">
                                                @php
                                                    $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                    $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                                @endphp
                                                @if(isset($module) && $module->isEnabled())
                                                    {{-- Include the module blade button --}}
                                                    @include('productquickview::pages.button', ['product_slug' => $homepage_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                @endif
                                                @if(isset($compare_module) && $compare_module->isEnabled())
                                                    {{-- Include the module blade button --}}
                                                    @include('productcompare::pages.button', ['product_slug' => $homepage_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
