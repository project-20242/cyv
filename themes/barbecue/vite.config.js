import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";
import path from "path";



export default defineConfig({
    plugins: [
        laravel({
            input: [
                "themes\barbecue\sass/app.scss",
                "themes\barbecue\js/app.js"
            ],
            buildDirectory: "barbecue",
        }),
        
        
        {
            name: "blade",
            handleHotUpdate({ file, server }) {
                if (file.endsWith(".blade.php")) {
                    server.ws.send({
                        type: "full-reload",
                        path: "*",
                    });
                }
            },
        },
    ],
    resolve: {
        alias: {
            '@': '/themes\barbecue\js',
            '~bootstrap': path.resolve('node_modules/bootstrap'),
        }
    },
    
});
