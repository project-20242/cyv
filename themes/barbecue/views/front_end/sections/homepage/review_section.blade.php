<section class="testimonials-section padding-top padding-bottom" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="section-title d-flex justify-content-between align-items-center ">
            <h2 id="{{ $section->review->section->title->slug ?? '' }}_preview">{!!
                        $section->review->section->title->text ?? '' !!}</h2>
            <a href="{{ route('page.product-list', $slug) }}" class="btn-secondary show-more-btn" id="{{ $section->review->section->button->slug ?? '' }}_preview"> {!!
                         $section->review->section->button->text ?? '' !!}
                <svg xmlns="http://www.w3.org/2000/svg" width="11" height="8" viewBox="0 0 11 8" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M6.92546 0.237956C6.69464 0.00714337 6.32042 0.00714327 6.08961 0.237955C5.8588 0.468767 5.8588 0.842988 6.08961 1.0738L9.01507 3.99926L6.08961 6.92471C5.8588 7.15552 5.8588 7.52974 6.08961 7.76055C6.32042 7.99137 6.69464 7.99137 6.92545 7.76055L10.2688 4.41718C10.4996 4.18636 10.4996 3.81214 10.2688 3.58133L6.92546 0.237956ZM1.91039 0.237955C1.67958 0.00714327 1.30536 0.00714337 1.07454 0.237956C0.843732 0.468768 0.843733 0.842988 1.07454 1.0738L4 3.99925L1.07454 6.92471C0.843732 7.15552 0.843733 7.52974 1.07455 7.76055C1.30536 7.99137 1.67958 7.99137 1.91039 7.76055L5.25377 4.41718C5.48458 4.18637 5.48458 3.81214 5.25377 3.58133L1.91039 0.237955Z"
                        fill="white"></path>
                </svg>
            </a>
        </div>
        <div class="testimonials-main">
            <div class="testimonials-slider flex-slider">
                @foreach ($reviews as $review)
                <div class="testimonials-slides">
                    <div class="testi-inner">
                        <div class="left-content blog-card-content">
                            <div class="ratings d-flex align-items-center">
                                @for ($i = 0; $i < 5; $i++) <i
                                    class="fa fa-star {{ $i < $review->rating_no ? 'text-warning' : '' }} "></i>
                                    @endfor
                                    <span>{{ $review->rating_no }}.0 / 5.0</span>
                            </div>
                            <h3>{{ $review->title }}</h3>
                            <p>{{ $review->description }}</p>
                            <span class="user-name"><a href="#">{{ !empty($review->UserData->first_name) ? $review->UserData->first_name : '' }},
                                </a>
                                {{ $review->ProductData->type }} </span>
                        </div>
                        <div class="img-wrapper">
                            <img
                                src="{{ asset('/' . !empty($review->ProductData) ? get_file($review->ProductData->cover_image_path, $currentTheme) : '') }}">
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>
