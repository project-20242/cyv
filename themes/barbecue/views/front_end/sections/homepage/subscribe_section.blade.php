<section class="newsletter-section"
    style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class=" container">
        <div class="bg-sec">
            <img src="{{ asset($section->subscribe->section->image->image ?? 'themes/'.$currentTheme.'/assets/img/banner-sec-7.png')}}"
                            id="{{ $section->subscribe->section->image->slug ?? '' }}_preview" class="banner-img" alt="newsletter img">
            <div class="contnent">
            <h2 id="{{ $section->subscribe->section->title->slug ?? '' }}_preview">{!! $section->subscribe->section->title->text ?? '' !!}</h2>
            <p id="{{ $section->subscribe->section->description->slug ?? '' }}_preview">{!! $section->subscribe->section->description->text ?? '' !!}</p>
            {!! \App\Models\Newsletter::Subscribe($currentTheme, $slug, $section) !!}
            </div>
        </div>
</section>