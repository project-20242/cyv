<section class="more-pro-banner-section"
    style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <img src="{{ asset($section->variant_background->section->image->image ?? 'themes/')}}" class="mid-bg-img" 
        id="{{ $section->variant_background->section->image->slug ?? '' }}_preview" alt="cat-img">
    <div class="container">
        <div class="inner-content dark-p">
            <div class="section-title">
                <span class="sub-title"id="{{ $section->variant_background->section->sub_title->slug ?? '' }}_preview">{!!
                   $section->variant_background->section->sub_title->text ?? '' !!} </span>
                <h2 id="{{ $section->variant_background->section->title->slug ?? '' }}_preview">
                    {!! $section->variant_background->section->title->text ?? '' !!}</h2>
            </div>
             <p id="{{ $section->variant_background->section->description->slug ?? '' }}_preview">{!!
                  $section->variant_background->section->description->text ?? '' !!} </p>
        </div>
    </div>
</section>