<div class="row">
    @foreach ($products as $product)
    <div class="col-lg-4 col-md-6 col-sm-6 col-12 product-card">
        <div class="product-card-inner">
            <div class="card-top">
                <div class="custom-output">
                    {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $product->id) !!}
                </div>
                <div class="like-items-icon">
                    <a class="add-wishlist wishbtn wishbtn-globaly" href="javascript:void(0)" title="Wishlist"
                        tabindex="0" product_id="{{ $product->id }}"
                        in_wishlist="{{ $product->in_whishlist ? 'remove' : 'add' }}">
                        <div class="wish-ic">
                            <i class="{{ $product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"></i>
                        </div>
                    </a>
                    <div class="product-btn-wrp">
                        @php
                            $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                            $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                        @endphp
                        @if(isset($module) && $module->isEnabled())
                            {{-- Include the module blade button --}}
                            @include('productquickview::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                        @endif
                        @if(isset($compare_module) && $compare_module->isEnabled())
                            {{-- Include the module blade button --}}
                            @include('productcompare::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                        @endif
                    </div>  
                </div>
            </div>
            <div class="product-card-image">
                <a href="{{url($slug.'/product/'.$product->slug)}}">
                    <img src="{{asset($product->cover_image_path)}}" class="default-img">
                </a>
            </div>
            <div class="card-bottom">
                <div class="product-title">
                    <span class="sub-title">{{!empty($product->ProductData) ? $product->ProductData->name : ''}} </span>
                    <h3>
                        <a class="product-title1" href="{{ url($slug.'/product/'.$product->slug) }}">
                            {{ $product->name }}
                        </a>
                    </h3>
                </div>
                @if ($product->variant_product == 0)
                <div class="price">
                    <ins>{{ currency_format_with_sym(($product->sale_price ?? $product->price), $store->id, $currentTheme)}}<span class="currency-type">{{ $currency }}</span></ins>
                </div>
                @else
                <div class="price">
                    <ins>{{ __('In Variant') }}</ins>
                </div>
                @endif
                <a href="JavaScript:void(0)" class="btn addtocart-btn addcart-btn-globaly"
                    product_id="{{ $product->id }}" variant_id="0" qty="1">
                    {{ __('Add to cart') }}
                    <svg xmlns="http://www.w3.org/2000/svg" width="11" height="8" viewBox="0 0 11 8" fill="none">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M6.92546 0.237956C6.69464 0.00714337 6.32042 0.00714327 6.08961 0.237955C5.8588 0.468767 5.8588 0.842988 6.08961 1.0738L9.01507 3.99926L6.08961 6.92471C5.8588 7.15552 5.8588 7.52974 6.08961 7.76055C6.32042 7.99137 6.69464 7.99137 6.92545 7.76055L10.2688 4.41718C10.4996 4.18636 10.4996 3.81214 10.2688 3.58133L6.92546 0.237956ZM1.91039 0.237955C1.67958 0.00714327 1.30536 0.00714337 1.07454 0.237956C0.843732 0.468768 0.843733 0.842988 1.07454 1.0738L4 3.99925L1.07454 6.92471C0.843732 7.15552 0.843733 7.52974 1.07455 7.76055C1.30536 7.99137 1.67958 7.99137 1.91039 7.76055L5.25377 4.41718C5.48458 4.18637 5.48458 3.81214 5.25377 3.58133L1.91039 0.237955Z"
                            fill="white" />
                    </svg>
                </a>
            </div>
        </div>
    </div>
    @endforeach
</div>
@php
$page_no = !empty($page) ? $page : 1;
@endphp
<!-- a Tag for previous page -->
{{ $products->onEachSide(0)->links('pagination::bootstrap-4') }}
