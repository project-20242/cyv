<form class="footer-subscribe-form" action="{{ route('newsletter.store', $slug) }}" method="post">
    @csrf
    <div class="contnent">
        <div class="input-box">
            <input type="email" placeholder="ESCRIBE TU DIRECCIÓN DE CORREO ELECTRÓNICO..." name="email">
            <button>
                {{ __('SUBSCRIBE') }}
            </button>
        </div>
        <div class="checkbox-custom">
            <input type="checkbox" id="subscibecheck">
            <label for="subscibecheck" id="{{ $section->subscribe->section->sub_title->slug ?? '' }}_preview">
                {!! $section->subscribe->section->sub_title->text ?? '' !!}
            </label>
        </div>
    </div>
</form>