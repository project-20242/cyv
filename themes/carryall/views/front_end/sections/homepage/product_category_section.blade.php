<section class="tabs-section tabs-section-second tabs-wrapper padding-bottom" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="section-title d-flex justify-content-between align-items-center ">
             <h2 id="{{ $section->product_category->section->title->slug ?? ''}}_preview">
                        {!! $section->product_category->section->title->text ?? ''!!}</h2>
            <ul class="tabs">
                @foreach ($category_options as $cat_key => $category)
                <li class="{{ $cat_key == 0 ? 'active' : '' }}" data-tab="{{ $cat_key }}">
                    <a href="javascript:" class="btn-secondary">
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20" fill="none">
                            <path
                                d="M19.4324 0.980437L19.4089 0.949187C19.0222 0.468706 18.4792 0.148385 17.8737 0.0390077C17.2722 -0.0664637 16.651 0.0429141 16.1198 0.355422C12.3423 2.58204 7.65082 2.58204 3.87338 0.355422C3.34602 0.0429141 2.72491 -0.0664637 2.12334 0.0390077C1.51785 0.148385 0.974869 0.468706 0.588141 0.949187L0.564703 0.976531C0.0998473 1.55467 -0.0915638 2.30469 0.0412521 3.03518C0.174068 3.76566 0.615485 4.39849 1.25222 4.7735C3.13117 5.8829 5.17029 6.60558 7.26018 6.94152V8.58609C7.26018 9.47284 7.98286 10.1955 8.8696 10.1955H9.41258V14.4769C8.72897 14.7191 8.24068 15.3714 8.24068 16.1332V19.4145C8.24068 19.7387 8.5024 20.0004 8.82663 20.0004H11.1704C11.4947 20.0004 11.7564 19.7387 11.7564 19.4145V16.1332C11.7564 15.3675 11.2681 14.7191 10.5845 14.4769V10.1955H11.1275C12.0142 10.1955 12.7369 9.47284 12.7369 8.58609V6.94152C14.8229 6.60558 16.862 5.8829 18.7448 4.7735C19.3816 4.39849 19.823 3.76566 19.9558 3.03518C20.0886 2.30469 19.8972 1.55467 19.4324 0.980437ZM10.5845 18.8285H9.41258V16.1332C9.41258 15.8089 9.6743 15.5472 9.99853 15.5472C10.3228 15.5472 10.5845 15.8089 10.5845 16.1332V18.8285ZM8.8696 9.02361C8.6274 9.02361 8.43209 8.82829 8.43209 8.58609V7.08996C8.76022 7.12121 9.08835 7.14075 9.41649 7.15247V9.02751H8.8696V9.02361ZM11.5689 8.58609C11.5689 8.82829 11.3736 9.02361 11.1314 9.02361H10.5845V7.14856C10.9126 7.13684 11.2408 7.11731 11.5689 7.08606V8.58609ZM18.8034 2.82423C18.7331 3.21877 18.4948 3.55863 18.1511 3.76176C13.1236 6.73058 6.87345 6.73058 1.84598 3.76566C1.50223 3.56253 1.26394 3.22268 1.19362 2.82814C1.12331 2.4336 1.22488 2.02734 1.47488 1.71483L1.49832 1.68358C1.76786 1.34763 2.1624 1.17185 2.56475 1.17185C2.80695 1.17185 3.05305 1.23435 3.27571 1.36717C3.95151 1.76561 4.66246 2.10156 5.38904 2.375L4.88122 2.88283C4.65074 3.1133 4.65074 3.48441 4.88122 3.71097C4.9945 3.82426 5.14685 3.88285 5.29529 3.88285C5.44373 3.88285 5.59608 3.82426 5.70936 3.71097L6.64689 2.77345C7.18206 2.91408 7.72894 3.02346 8.27974 3.09377L8.10395 3.26956C7.87348 3.50003 7.87348 3.87113 8.10395 4.0977C8.21724 4.21099 8.36959 4.26958 8.51803 4.26958C8.66647 4.26958 8.81882 4.21099 8.9321 4.0977L9.83056 3.20315C9.88525 3.20315 9.94384 3.20315 9.99853 3.20315C10.5571 3.20315 11.1157 3.16799 11.6704 3.09377L11.5025 3.26174C11.272 3.48831 11.2681 3.85942 11.4947 4.08989C11.6079 4.20708 11.7603 4.26568 11.9126 4.26568C12.0611 4.26568 12.2095 4.21099 12.3228 4.0977L13.7799 2.66407C13.7877 2.65626 13.7955 2.64845 13.8033 2.64064C14.8151 2.33594 15.7956 1.90624 16.7174 1.36326C17.3151 1.01169 18.0651 1.1445 18.4948 1.67967L18.5183 1.71092C18.7722 2.02734 18.8777 2.42969 18.8034 2.82423Z"
                                fill="#729454" />
                        </svg>
                        {{ $category }}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
        <div class="tabs-container">
            @foreach ($category_options as $cat_k => $category)
            <div id="{{ $cat_k }}" class="tab-content tab-cat-id {{ $cat_k == 0 ? 'active' : '' }}">
                <div class="product-tab-slider f_blog">
                    @foreach ($products as $product)
                    @if($cat_k == '0' || $product->ProductData->id == $cat_k)
                    <div class="shop-protab-item">
                        <div class="product-card">
                            <div class="product-card-inner">
                                <div class="card-top">
                                    <div class="custom-output">
                                        {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $product->id) !!}
                                    </div>
                                    <div class="like-items-icon">
                                        <a class="add-wishlist wishbtn wishbtn-globaly" href="javascript:void(0)"
                                            title="Wishlist" tabindex="0" product_id="{{ $product->id }}"
                                            in_wishlist="{{ $product->in_whishlist ? 'remove' : 'add' }}">
                                            <div class="wish-ic">
                                                <i
                                                    class="{{ $product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"></i>
                                            </div>
                                        </a>
                                        <div class="product-btn-wrp">
                                            @php
                                                $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                            @endphp
                                            @if(isset($module) && $module->isEnabled())
                                                {{-- Include the module blade button --}}
                                                @include('productquickview::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                            @endif
                                            @if(isset($compare_module) && $compare_module->isEnabled())
                                                {{-- Include the module blade button --}}
                                                @include('productcompare::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="product-card-image">
                                    <a href="{{ url($slug.'/product/'.$product->slug)}}">
                                        <img src="{{asset($product->cover_image_path)}}" class="default-img">
                                    </a>
                                </div>
                                <div class="card-bottom">
                                    <div class="product-title">
                                        <span class="sub-title">{{!empty($product->ProductData) ? $product->ProductData->name : ''}} </span>
                                        <h3>
                                            <a class="product-title1"
                                                href="{{ url($slug.'/product/'.$product->slug) }}">
                                                {{ $product->name }}
                                            </a>
                                        </h3>
                                    </div>
                                    @if ($product->variant_product == 0)
                                    <div class="price">
                                        <ins>{{ currency_format_with_sym(($product->sale_price ?? $product->price), $store->id, $currentTheme)}}<span
                                                class="currency-type">{{ $currency }}</span></ins>
                                    </div>
                                    @else
                                    <div class="price">
                                        <ins>{{ __('In Variant') }}</ins>
                                    </div>
                                    @endif
                                    <a href="JavaScript:void(0)" class="btn addtocart-btn addcart-btn-globaly"
                                        product_id="{{ $product->id }}" variant_id="0" qty="1">
                                        {{ __('Add to cart') }}
                                        <svg xmlns="http://www.w3.org/2000/svg" width="11" height="8" viewBox="0 0 11 8"
                                            fill="none">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M6.92546 0.237956C6.69464 0.00714337 6.32042 0.00714327 6.08961 0.237955C5.8588 0.468767 5.8588 0.842988 6.08961 1.0738L9.01507 3.99926L6.08961 6.92471C5.8588 7.15552 5.8588 7.52974 6.08961 7.76055C6.32042 7.99137 6.69464 7.99137 6.92545 7.76055L10.2688 4.41718C10.4996 4.18636 10.4996 3.81214 10.2688 3.58133L6.92546 0.237956ZM1.91039 0.237955C1.67958 0.00714327 1.30536 0.00714337 1.07454 0.237956C0.843732 0.468768 0.843733 0.842988 1.07454 1.0738L4 3.99925L1.07454 6.92471C0.843732 7.15552 0.843733 7.52974 1.07455 7.76055C1.30536 7.99137 1.67958 7.99137 1.91039 7.76055L5.25377 4.41718C5.48458 4.18637 5.48458 3.81214 5.25377 3.58133L1.91039 0.237955Z"
                                                fill="white" />
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>

    </div>
</section>
