<footer class="site-footer"
    style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide  ?? '' }}" data-section="{{ $option->section_name  ?? '' }}"
    data-store="{{ $option->store_id  ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        @if(isset($whatsapp_setting_enabled) && !empty($whatsapp_setting_enabled))
        <div class="floating-wpp"></div>
        @endif
        <div class="footer-row">
            @if(isset($section->footer->section->footer_menu_type))
            @for ($i = 0; $i < $section->footer->section->footer_menu_type->loop_number ?? 1; $i++)
                @if(isset($section->footer->section->footer_menu_type->footer_title->{$i}))
                <div class="footer-col footer-link footer-link-{{$i+1}}">
                    <div class="footer-widget">
                        <h2> {{ $section->footer->section->footer_menu_type->footer_title->{$i} ?? ''}} </h2>
                        @php
                        $footer_menu_id = $section->footer->section->footer_menu_type->footer_menu_ids->{$i} ?? '';
                        $footer_menu = get_nav_menu($footer_menu_id);
                        @endphp
                        <ul>
                            @if (!empty($footer_menu))
                            @foreach ($footer_menu as $key => $nav)
                            @if ($nav->type == 'custom')
                            <li><a href="{{ url($nav->slug) }}" target="{{ $nav->target }}">
                                    @if ($nav->title == null)
                                    {{ $nav->title }}
                                    @else
                                    {{ $nav->title }}
                                    @endif
                                </a></li>
                            @elseif($nav->type == 'category')
                            <li><a href="{{ route('themes.page', [$slug, $nav->slug]) }}" target="{{ $nav->target }}">
                                    @if ($nav->title == null)
                                    {{ $nav->title }}
                                    @else
                                    {{ $nav->title }}
                                    @endif
                                </a></li>
                            @else
                            <li><a href="{{ route('themes.page', [$slug, $nav->slug]) }}" target="{{ $nav->target }}">
                                    @if ($nav->title == null)
                                    {{ $nav->title }}
                                    @else
                                    {{ $nav->title }}
                                    @endif
                                </a>
                            </li>
                            @endif
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                @endif
                @endfor
                @endif
                <div class="footer-col footer-subscribe-col">
                    <div class="footer-widget">
                        <h2 
                            id="{{ $section->footer->section->newsletter_title->slug ?? '' }}_preview">
                            {!! $section->footer->section->newsletter_title->text ?? '' !!}</h2>
                        <form class="footer-subscribe-form" action='{{ route("newsletter.store",$slug) }}'
                            method="post">
                            @csrf
                            <div class="contnent">
                                <div class="input-box">
                                    <input type="email" placeholder="ESCRIBE TU DIRECCIÓN DE CORREO ELECTRÓNICO..." name="email">
                                    <button>
                                        {{__('SUBSCRIBE')}}
                                    </button>
                                </div>
                                <div class="checkbox-custom">
                                    <input type="checkbox" id="subscibecheck">
                                    <label for="subscibecheck"
                                        id="{{ $section->footer->section->newsletter_sub_title->slug ?? '' }}_preview">
                                        {!! $section->footer->section->newsletter_sub_title->text ?? '' !!}
                                    </label>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="footer-col social-icons">
                    <div class="footer-widget">
                        <h2 id="{{ $section->footer->section->title->slug ?? '' }}_preview"> {!!
                            $section->footer->section->title->text ?? '' !!}</h2>
                        @if(isset($section->footer->section->footer_link))
                        <ul class="social-ul align-items-center d-flex">
                            @for ($i = 0; $i < $section->footer->section->footer_link->loop_number ?? 1; $i++)
                                <li>
                                    <a href="{{ $section->footer->section->footer_link->social_link->{$i} ?? '#'}}"
                                        target="_blank" id="social_link_{{ $i }}">
                                        <img src="{{ asset($section->footer->section->footer_link->social_icon->{$i}->image ?? 'themes/' . $currentTheme . '/assets/images/youtube.svg') }}"
                                            class="{{ 'social_icon_'. $i .'_preview' }}" alt="icon"
                                            id="social_icon_{{ $i }}">
                                    </a>
                                </li>
                                @endfor
                        </ul>
                        @endif
                    </div>
                </div>
        </div>
        <div class="footer-bottom">
            <div class="row align-items-center">
                <div class="col-sm-6 col-12">
                    <p id="{{ $section->footer->section->copy_right->slug ?? '' }}_preview"> {!!
                        $section->footer->section->copy_right->text ?? '' !!}</p>
                </div>
                <div class="col-sm-6 col-12">
                    <ul class="policy-links d-flex align-items-center justify-content-end">
                        <li><a href="https://apps.rajodiya.com/ecommercego-saas/barbecue/page/privacy-policy">Privacy
                                Policy </a> </li>
                        <li><a href="https://apps.rajodiya.com/ecommercego-saas/barbecue/page/refund-policy">Refund
                                Policy </a> </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    <div class="cookie">
        <p id="{{ $section->footer->section->footer_cookie->slug ?? '' }}_preview">{!! $section->footer->section->footer_cookie->text !!}</p>
        <button class="cookie-close">
            <svg xmlns="http://www.w3.org/2000/svg" width="8" height="8" viewBox="0 0 8 8" fill="none">
                <path fill-rule="evenodd" clip-rule="evenodd"
                    d="M7.20706 0.707107L6.49995 0L3.60354 2.89641L0.707134 0L2.67029e-05 0.707107L2.89644 3.60352L0 6.49995L0.707107 7.20706L3.60354 4.31062L6.49998 7.20706L7.20708 6.49995L4.31065 3.60352L7.20706 0.707107Z"
                    fill="#30383D"></path>
            </svg>
        </button>
    </div>
</footer>
<!--footer end here-->
<div class="overlay "></div>

<!--cart popup start here-->
<div class="cartDrawer cartajaxDrawer">
</div>

<!--cart popup ends here-->
<div class="overlay wish-overlay"></div>
<div class="wishDrawer wishajaxDrawer"></div>