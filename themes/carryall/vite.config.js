import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";
import path from "path";



export default defineConfig({
    plugins: [
        laravel({
            input: [
                "themes\carryall\sass/app.scss",
                "themes\carryall\js/app.js"
            ],
            buildDirectory: "carryall",
        }),
        
        
        {
            name: "blade",
            handleHotUpdate({ file, server }) {
                if (file.endsWith(".blade.php")) {
                    server.ws.send({
                        type: "full-reload",
                        path: "*",
                    });
                }
            },
        },
    ],
    resolve: {
        alias: {
            '@': '/themes\carryall\js',
            '~bootstrap': path.resolve('node_modules/bootstrap'),
        }
    },
    
});
