import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";
import path from "path";



export default defineConfig({
    plugins: [
        laravel({
            input: [
                "themes\coffee\sass/app.scss",
                "themes\coffee\js/app.js"
            ],
            buildDirectory: "coffee",
        }),
        
        
        {
            name: "blade",
            handleHotUpdate({ file, server }) {
                if (file.endsWith(".blade.php")) {
                    server.ws.send({
                        type: "full-reload",
                        path: "*",
                    });
                }
            },
        },
    ],
    resolve: {
        alias: {
            '@': '/themes\coffee\js',
            '~bootstrap': path.resolve('node_modules/bootstrap'),
        }
    },
    
});
