<section class="subscribe-section" style="@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>

    <div class="container">
        <div class="subscribe-banner">
            <img id="{{ $section->subscribe->section->image->slug ?? '' }}_preview" src="{!! asset($section->subscribe->section->image->image) ?? '' !!}"
                class="subscribe-bnr">
            <div class="subscribe-column">
                <div class="section-title">
                    <div class="subtitle">{{ __('CoffeStote') }}</div>

                    <h2 id="{{ $section->subscribe->section->title->slug ?? '' }}_preview">
                        {!! $section->subscribe->section->title->text ?? '' !!}</h2>
                </div>
                <p id="{{ $section->subscribe->section->sub_title->slug ?? '' }}_preview">
                    {!! $section->subscribe->section->sub_title->text ?? '' !!}</p>
                <form class="footer-subscribe-form" action="{{ route('newsletter.store', $slug) }}" method="post">
                    @csrf
                    <div class="input-wrapper">
                        <input type="email" placeholder="Escribe tu dirección de correo electrónico" name="email">
                        <button class="btn-subscibe"
                            id="{{ $section->subscribe->section->button->slug ?? '' }}_preview">
                            {!! $section->subscribe->section->button->text ?? '' !!}</button>
                    </div>
                    <label for="subsection" id="{{ $section->subscribe->section->description->slug ?? '' }}_preview">
                        {!! $section->subscribe->section->description->text ?? '' !!}</label>
                </form>
            </div>
        </div>
    </div>
</section>
