
@foreach ($landing_blogs as $blog)
    <div class="blog-itm">
        <div class="blog-itm-inner">
            <div class="blog-img">
                <a href="{{route('page.article',[$slug,$blog->id])}}">
                    <img src="{{ asset($blog->cover_image_path) }}">
                    <span class="blg-lbl">{{ __('ARTICLES') }}</span>
                </a>
            </div>
            <div class="blog-content">
                <div class="blog-content-top">
                    <span class="blog-itm-cat">{{ __('COFFEE') }}</span>
                    <h3>
                        <a href="{{route('page.article',[$slug,$blog->id])}}" class="description">{!! $blog->title !!}
                        </a>
                    </h3>
                    <p class="description">{!!$blog->short_description!!}</p>
                </div>
                <div class="blog-contnt-bottom">
                    <a href="{{route('page.article',[$slug,$blog->id])}}" class="link-btn">{{ __('READ MORE')}}</a>
                </div>
            </div>
        </div>
    </div>
@endforeach
