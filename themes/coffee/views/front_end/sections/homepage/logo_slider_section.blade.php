<section class="our-partner-section"
    style="position: relative; @if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-4 col-12">
                <div class="partner-left-column">
                    <div class="section-title">
                        <div class="subtitle" id="{{ $section->logo_slider->section->sub_title->slug ?? '' }}_preview">
                                {!! $section->logo_slider->section->sub_title->text ?? '' !!}
                        </div>
                        <h2 id="{{ $section->logo_slider->section->title->slug ?? '' }}_preview">
                                {!! $section->logo_slider->section->title->text ?? '' !!}</h2>
                    </div>
                </div>
            </div>
            <div class="col-md-8 col-12">
                <div class="partner-right-column">
                    <div class="parner-lbl">
                        {{ __('PARTNERS') }}
                    </div>
                    <div class="partner-slider">
                        @for ($i = 0; $i < 7; $i++)
                            <div class="partner-itm">
                                <a href="#">
                                    <img src="{{ asset($section->logo_slider->section->image->image->{0}) ?? '' }}"
                                        alt="theeme logo">
                                </a>
                            </div>
                        @endfor

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
