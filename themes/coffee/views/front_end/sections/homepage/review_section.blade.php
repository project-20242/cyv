<section class="vertical-tab-section tabs-wrapper"
    style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <img src="{{ asset('themes/coffee/assets/images/leftcoffee.png') }}" alt="coffee image" class="bst-midimg">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-12 col-12">
                <div class="vertical-tab-left coffeethree-column-box">
                    <div class="section-title">
                        <div class="subtitle" id="{{ $section->best_product->section->sub_title->slug ?? '' }}_preview">
                            {!! $section->best_product->section->sub_title->text ?? '' !!}
                        </div>
                        <h2 id="{{ $section->review->section->title->slug ?? '' }}_preview">
                        {!! $section->review->section->title->text ?? '' !!}</h2>
                    </div>
                    <p id="{{ $section->review->section->description->slug ?? '' }}_preview">
                        {!! $section->review->section->description->text ?? '' !!}</p>

                    <a href="{{ route('page.product-list', $slug) }}" class="link-btn"
                        id="{{ $section->review->section->button->slug ?? '' }}_preview">
                        {!! $section->review->section->button->text ?? '' !!}</a>
                </div>
            </div>
            <div class="col-xl-4 col-lg-3 col-md-4 col-sm-5 col-12">
                <div class="vertical-tab-center">
                    <div class="subtitle">{{__('Choose the origin of the coffee:')}} </div>
                    <ul class="cat-tab tabs">

                        @foreach (collect($languages)->take(5) as $code => $language)
                        <li class="tab-link" data-tab="{{ucFirst($language)}}"><a href="javascript:;">{{ucFirst($language)}} </a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-xl-4 col-lg-5 col-md-8 col-sm-7 col-12">
                <div class="tabs-container">
                    <div id="spain" class="tab-content active">
                        <div class="testimonial-slider">
                            @foreach ($reviews as $review)
                            <div class="testimonial-itm">
                                <div class="testimonial-itm-inner">
                                    <div class="testimonial-itm-image">
                                        <a href="{{ url($slug.'/product/'.$review->ProductData->slug) }}">
                                            <img src="{{ asset('/' . !empty($review->ProductData) ? asset($review->ProductData->cover_image_path) : '') }}"
                                                alt="review">
                                        </a>
                                    </div>
                                    <div class="testimonial-itm-content">
                                        <div class="testimonial-content-top">
                                            <h4>{{ $review->title }}</h4>
                                            <p>{{ $review->description }}</p>
                                        </div>
                                        <div class="testimonial-bottom d-flex align-items-center">
                                            <span class="client-name">John Doe, Client</span>
                                            <div class="testimonial-str d-flex align-items-center">
                                                @for ($i = 0; $i < 5; $i++) <i
                                                    class="ti ti-star {{ $i < $review->rating_no ? 'text-warning' : 'text-white' }}">
                                                    </i>
                                                    @endfor
                                                    <span><b>{{ $review->rating_no }}</b> / 5.0</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>