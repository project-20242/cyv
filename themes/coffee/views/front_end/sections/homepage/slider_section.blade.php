<section style="@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div  class="first-video-section">
        <video src="{!! asset('themes/coffee/assets/images/video1.mp4') !!}" loop="" autoplay="" muted="muted" playsinline="" controlslist="nodownload"></video>
        <div class="fixed-slider-left">
            <div class="slides-numbers" style="display: block;">
                <span class="active">01</span> / <span class="total"></span>
            </div>
            <div class="line"></div>
            <div class="slider-inner-text">
                <h5> {{ __('CoffeeStore') }} </h5>
            </div>
        </div>
        <div class="container">
            <div class="banner-slider">
               
            @for ($i = 0; $i < objectToArray($section->slider->loop_number); $i++)
                  
                    <div>
                        <div class="row align-items-start">
                            <div class="col-lg-3 col-md-6 col-sm-6  col-12"></div>
                            <div class="col-lg-6 col-md-12 col-12 banner-center-content">
                                <div class="banner-content-inner text-center">
                                <div class="section-title">
                                        <div class="subtitle" id="{{ ($section->slider->section->sub_title->slug->{$i} ?? '').'_'.$i }}_preview">
                                    {!! $section->slider->section->sub_title->text->{$i} ?? '' !!}</div>
                                        <h2 id="{{ ($section->slider->section->title->slug->{$i} ?? '').'_'.$i }}_preview">
                                    {!! $section->slider->section->title->text->{$i} ?? '' !!}</h2>
                                    </div>
                                    <p id="{{ ($section->slider->section->description->slug->{$i} ?? '').'_'.$i }}_preview">
                                    {{ $section->slider->section->description->text->{$i} ?? '' }}</p>
                                    <a href="{{route('page.product-list',$slug)}}" class="link-btn" id="{{ ($section->slider->section->button->slug->{$i} ?? '').'_'.$i }}_preview">
                                    {!! $section->slider->section->button->text->{$i} ?? '' !!}</a>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-6 col-sm-6  col-12"></div>
                        </div>
                    </div>
                @endfor
            </div>
        </div>
    </div>
    </section>