<section
    style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="sections-wrapper">
    <div class="bestseller-section tabs-wrapper padding-top">
        <img src="{{ asset('themes/coffee/assets/images/leftcoffee.png') }}" class="bst-leftimg">
        <div class="container">
            <div class="section-title d-flex align-items-center justify-content-between">
                <div class="section-title-left">
                    <ul class="cat-tab tabs">
                        @foreach ($category_options->take(6) as $cat_key => $category)
                            <li class="tab-link {{ $cat_key == 0 ? 'active' : '' }}" data-tab="{{ $cat_key }}">
                                <a href="javascript:;">{{ __('All Products') }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
                <a href="{{ route('page.product-list', $slug) }}" class="link-btn">{{ __('Show more products') }}</a>
            </div>
            <div class="tabs-container">
                @foreach ($category_options as $cat_k => $category)
                    <div id="{{ $cat_k }}" class="tab-content {{ $cat_k == 0 ? 'active' : '' }}">
                        <div class="bestpro-slider">
                            @foreach ($products as $all_product)
                                @if ($cat_k == '0' || $all_product->ProductData->id == $cat_k)
                                    <div class="bestpro-itm product-card  card-direction-row">
                                        <div class="product-card-inner">
                                            <div class="product-img">
                                                <a href="{{ url($slug.'/product/'.$all_product->slug) }}">
                                                    <img
                                                        src="{{ asset($all_product->cover_image_path) }}">
                                                        <span class="offer-lbl"> {{!empty($all_product->ProductData) ? $all_product->ProductData->name : ''}} </span>
                                                    <div class="custom-output">
                                                            {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $all_product->id) !!}
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-content-top">
                                                    <div class="d-flex justify-content-end wsh-wrp">
                                                        <a href="javascript:void(0)"
                                                            class="wishlist wbwish  wishbtn-globaly"
                                                            product_id="{{ $all_product->id }}"
                                                            in_wishlist="{{ $all_product->in_whishlist ? 'remove' : 'add' }}">
                                                            <span class="wish-ic">
                                                                <i class="{{ $all_product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"
                                                                    style='color: rgb(255, 254, 254)'></i>
                                                            </span>
                                                        </a>
                                                    </div>
                                                    <div class="subtitle">{{!empty($all_product->ProductData) ? $all_product->ProductData->name : ''}}</div>
                                                    <h3><a href="{{ url($slug.'/product/'.$all_product->slug) }}"
                                                            class="description">{{ $all_product->name }}</a>
                                                    </h3>
                                                </div>
                                                <div class="product-content-bottom">
                                                    <div
                                                        class="main-price d-flex align-items-center justify-content-between">
                                                        @if ($all_product->variant_product == 0)
                                                            <div class="price">
                                                                <ins>{{ currency_format_with_sym(($all_product->sale_price ?? $all_product->price), $store->id, $currentTheme)}}</ins>
                                                            </div>
                                                        @else
                                                            <div class="price">
                                                                <ins>{{ __('In Variant') }}</ins>
                                                            </div>
                                                        @endif
                                                        <a href="javascript:void(0)"
                                                            class="link-btn addcart-btn-globaly" type="submit"
                                                            product_id="{{ $all_product->id }}"
                                                            variant_id="{{ $all_product->default_variant_id }}"
                                                            qty="1">
                                                            + {{ __('ADD TO CART') }}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="center-btn-view-all d-flex justify-content-center">
                <a href="{{ route('page.product-list', $slug) }}" class="link-btn">{{ __('Show more products') }}</a>
            </div>
        </div>
    </div>
    <div class="banner-with-products padding-top">
        <div class="container">
            <div class="beans-video-wrp">
                <video id="{{ $section->best_product->section->background_image->slug }}"
                        src="{!! asset($section->best_product->section->background_image->image) !!}" loop="" autoplay="" muted="muted" playsinline=""
                    controlslist="nodownload"></video>
                <div class="row">
                    <div class="col-md-4 col-12">
                        <div class="banner-product-details coffeethree-column-box">
                            <div class="section-title">
                                <div
                                    class="subtitle"id="{{ $section->best_product->section->sub_title->slug ?? '' }}_preview">
                                    {!! $section->best_product->section->sub_title->text ?? '' !!}
                                </div>
                                <h2 id="{{ $section->best_product->section->title->slug ?? '' }}_preview">
                                    {!! $section->best_product->section->title->text ?? '' !!}</h2>
                            </div>
                            <p>{{ $section->best_product->section->description->text ?? '' }}</p>
                            <a href="{{ route('page.product-list', $slug) }}"class="link-btn"
                                id="{{ $section->best_product->section->button->slug ?? '' }}_preview">
                                {!! $section->best_product->section->button->text ?? '' !!}</a>
                        </div>
                    </div>
                    @foreach ($home_page_products as $homepage_product)
                        <div class="col-md-4 col-sm-6 col-12">
                            <div class="product-card  card-direction-row">
                                <div class="product-card-inner">
                                    <div class="product-img">
                                        <a href="{{ url($slug.'/product/'.$homepage_product->slug) }}">
                                            <img src="{{ asset($homepage_product->cover_image_path) }}">
                                            <span class="offer-lbl"> {{!empty($all_product->ProductData) ? $all_product->ProductData->name : ''}} </span>
                                            <div class="custom-output">
                                             {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $homepage_product->id) !!}
                                            </div>
                                        </a>
                                    </div>
                                    <div class="product-content">
                                        <div class="product-content-top">
                                            <div class="d-flex justify-content-end wsh-wrp">
                                                <a href="javascript:void(0)" class="wishlist wbwish  wishbtn-globaly"
                                                    product_id="{{ $homepage_product->id }}"
                                                    in_wishlist="{{ $homepage_product->in_whishlist ? 'remove' : 'add' }}">
                                                    <span class="wish-ic">
                                                        <i class="{{ $homepage_product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"
                                                            style='color: rgb(255, 254, 254)'></i>
                                                    </span>
                                                </a>
                                                <div class="product-btn-wrp">
                                                    @php
                                                        $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                        $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                                    @endphp
                                                    @if(isset($module) && $module->isEnabled())
                                                        {{-- Include the module blade button --}}
                                                        @include('productquickview::pages.button', ['product_slug' => $homepage_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                    @endif
                                                    @if(isset($compare_module) && $compare_module->isEnabled())
                                                        {{-- Include the module blade button --}}
                                                        @include('productcompare::pages.button', ['product_slug' => $homepage_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="subtitle">{{!empty($homepage_product->ProductData) ? $homepage_product->ProductData->name : ''}}</div>
                                            <h3>
                                                <a
                                                    href="{{ url($slug.'/product/'.$homepage_product->slug) }}">
                                                    {{ $homepage_product->name }}
                                                </a>
                                            </h3>
                                        </div>
                                        <div class="product-content-bottom">
                                            <div class="main-price d-flex align-items-center justify-content-between">
                                                @if ($homepage_product->variant_product == 0)
                                                    <div class="price">
                                                        <ins>{{ currency_format_with_sym(($homepage_product->sale_price ?? $homepage_product->price), $store->id, $currentTheme)}}{{ $currency }}</ins>
                                                    </div>
                                                @else
                                                    <div class="price">
                                                        <ins>{{ __('In Variant') }}</ins>
                                                    </div>
                                                @endif
                                                <a href="javascript:void(0)" class="link-btn addcart-btn-globaly"
                                                    type="submit" product_id="{{ $homepage_product->id }}"
                                                    variant_id="{{ $homepage_product->default_variant_id }}"
                                                    qty="1">
                                                    + {{ __('ADD TO CART') }}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
