<section class="coffee-three-col-section"
    style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-4 col-12">
                <div class="coffeethree-column-box">
                    <div class="section-title">
                        <div class="subtitle">{{ __('coffeeStore') }}
                        </div>
                        <h2 id="{{ $section->product->section->title->slug ?? '' }}_preview">
                            {!! $section->product->section->title->text ?? '' !!}</h2>
                    </div>
                    <p id="{{ $section->product->section->description->slug ?? '' }}_preview">
                        {!! $section->product->section->description->text ?? '' !!}</p>
                    <a href="{{ route('page.blog', $slug) }}" class="link-btn"
                        id="{{ $section->product->section->button->slug ?? '' }}_preview">
                        {!! $section->product->section->button->text ?? '' !!}</a>
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-12">
                <div class="coffee-mug-animation">
                    <svg width="80px" height="73px" viewBox="0 0 31 73">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g class="smokes" transform="translate(2.000000, 2.000000)" stroke="#BEBEBE"
                                stroke-width="5">
                                <g class="smoke-1">
                                    <path id="Shape1"
                                        d="M0.5,8.8817842e-16 C0.5,8.8817842e-16 3.5,5.875 3.5,11.75 C3.5,17.625 0.5,17.625 0.5,23.5 C0.5,29.375 3.5,29.375 3.5,35.25 C3.5,41.125 0.5,41.125 0.5,47">
                                    </path>
                                </g>
                                <g class="smoke-2">
                                    <path id="Shape2"
                                        d="M0.5,8.8817842e-16 C0.5,8.8817842e-16 3.5,5.875 3.5,11.75 C3.5,17.625 0.5,17.625 0.5,23.5 C0.5,29.375 3.5,29.375 3.5,35.25 C3.5,41.125 0.5,41.125 0.5,47">
                                    </path>
                                </g>
                                <g class="smoke-3">
                                    <path id="Shape3"
                                        d="M0.5,8.8817842e-16 C0.5,8.8817842e-16 3.5,5.875 3.5,11.75 C3.5,17.625 0.5,17.625 0.5,23.5 C0.5,29.375 3.5,29.375 3.5,35.25 C3.5,41.125 0.5,41.125 0.5,47">
                                    </path>
                                </g>
                            </g>
                        </g>
                    </svg>
                    <img id="{{ $section->product->section->image->slug ?? '' }}_preview"
                        src="{!! asset($section->product->section->image->image) ?? '' !!}">
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-12">
                @if (!empty($latest_product))
                    <div class="product-card  card-direction-row">
                        <div class="product-card-inner">
                            <div class="product-img">
                                <a href="{{ url($slug.'/product/'.$latest_product->slug) }}">
                                    <img src="{{ asset($latest_product->cover_image_path) }}">
                                    <span class="offer-lbl"> {{!empty($latest_product->ProductData) ? $latest_product->ProductData->name : ''}} </span>
                                    <div class="custom-output">
                                    {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $latest_product->id) !!}
                                    </div>
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="product-content-top">
                                    <div class="d-flex justify-content-end wsh-wrp">
                                        <a href="javascript:void(0)" class="wishlist wbwish  wishbtn-globaly"
                                            product_id="{{ $latest_product->id }}"
                                            in_wishlist="{{ $latest_product->in_whishlist ? 'remove' : 'add' }}">
                                            <span class="wish-ic">
                                                <i class="{{ $latest_product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"
                                                    style='color: rgb(255, 254, 254)'></i>
                                            </span>
                                        </a>
                                        <div class="product-btn-wrp">
                                            @php
                                                $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                            @endphp
                                            @if(isset($module) && $module->isEnabled())
                                                {{-- Include the module blade button --}}
                                                @include('productquickview::pages.button', ['product_slug' => $latest_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                            @endif
                                            @if(isset($compare_module) && $compare_module->isEnabled())
                                                {{-- Include the module blade button --}}
                                                @include('productcompare::pages.button', ['product_slug' => $latest_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                            @endif
                                        </div>
                                    </div>
                                    <div class="subtitle">{{!empty($latest_product->ProductData) ? $latest_product->ProductData->name : ''}}</div>
                                    <h3><a
                                            href="{{ url($slug.'/product/'.$latest_product->slug) }}">{{ $latest_product->name }}</a>
                                    </h3>

                                </div>
                                <div class="product-content-bottom">
                                    <div class="main-price d-flex align-items-center justify-content-between">
                                        @if ($latest_product->variant_product == 0)
                                            <div class="price">
                                                <ins>{{ currency_format_with_sym(($latest_product->sale_price ?? $latest_product->price), $store->id, $currentTheme)}}</ins>
                                            </div>
                                        @else
                                            <div class="price">
                                                <ins>{{ __('In Variant') }}</ins>
                                            </div>
                                        @endif
                                        <a href="javascript:void(0)" class="link-btn addcart-btn-globaly" type="submit"
                                            product_id="{{ $latest_product->id }}"
                                            variant_id="{{ $latest_product->default_variant_id }}" qty="1">
                                            + {{ __('ADD TO CART') }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>

</section>
