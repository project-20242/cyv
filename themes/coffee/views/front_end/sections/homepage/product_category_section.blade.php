<section class="product-categories-section padding-top"
    style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="section-title d-flex align-items-center justify-content-between">
            <div class="section-title-left">
                <div class="subtitle" id="{{ $section->product_category->section->sub_title->slug ?? '' }}_preview">
                    {!! $section->product_category->section->sub_title->text ?? '' !!}
                </div>
                <h2 id="{{ $section->product_category->section->title->slug ?? '' }}_preview">
                    {!! $section->product_category->section->title->text ?? '' !!}</h2>
            </div>
            <a href="{{ route('page.product-list', $slug) }}" class="link-btn"
                id="{{ $section->product_category->section->button->slug ?? '' }}_preview">
                {!! $section->product_category->section->button->text ?? '' !!}
            </a>
        </div>
        <div class="pro-categorie-slider bottom-arrow">
            @foreach ($bestSeller as $bestSellers)
                <div class="pro-cate-itm product-card">
                    <div class="product-card-inner">
                        <div class="product-img">
                            <a href="{{ url($slug.'/product/'.$bestSellers->slug) }}">
                                <img src="{{ asset($bestSellers->cover_image_path) }}">
                                <span class="offer-lbl"> {{!empty($bestSellers->ProductData) ? $bestSellers->ProductData->name : ''}} </span>
                                <div class="custom-output">
                                {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $bestSellers->id) !!}
                                </div>
                            </a>
                        </div>
                        <div class="product-content">
                            <div class="product-content-top">
                                <div class="d-flex justify-content-between wsh-wrp">
                                <div class="subtitle">{{!empty($bestSellers->ProductData) ? $bestSellers->ProductData->name : ''}}</div>
                                    <a href="javascript:void(0)" class="wishlist wbwish  wishbtn-globaly"
                                        product_id="{{ $bestSellers->id }}"
                                        in_wishlist="{{ $bestSellers->in_whishlist ? 'remove' : 'add' }}">
                                        <span class="wish-ic">
                                            <i class="{{ $bestSellers->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"
                                                style='color: rgb(255, 254, 254)'></i>
                                        </span>
                                    </a>
                                    <div class="product-btn-wrp">
                                        @php
                                            $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                            $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                        @endphp
                                        @if(isset($module) && $module->isEnabled())
                                            {{-- Include the module blade button --}}
                                            @include('productquickview::pages.button', ['product_slug' => $bestSellers->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                        @endif
                                        @if(isset($compare_module) && $compare_module->isEnabled())
                                            {{-- Include the module blade button --}}
                                            @include('productcompare::pages.button', ['product_slug' => $bestSellers->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                        @endif
                                    </div>
                                </div>

                                <h3><a href="{{ url($slug.'/product/'.$bestSellers->slug) }}"
                                        class="short-description">{{ $bestSellers->name }}</a></h3>
                            </div>
                            <div class="product-content-bottom">
                                <div class="main-price d-flex align-items-center justify-content-between">
                                    @if ($bestSellers->variant_product == 0)
                                        <div class="price">
                                            <ins>{{ currency_format_with_sym(($bestSellers->sale_price ?? $bestSellers->price), $store->id, $currentTheme)}}</ins>
                                        </div>
                                    @else
                                        <div class="price">
                                            <ins>{{ __('In Variant') }}</ins>
                                        </div>
                                    @endif
                                    <a href="javascript:void(0)" class="link-btn addcart-btn-globaly" type="submit"
                                        product_id="{{ $bestSellers->id }}"
                                        variant_id="{{ $bestSellers->default_variant_id }}" qty="1">
                                        + {{ __('ADD TO CART') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="center-btn-view-all d-flex justify-content-center">
            <a href="{{ route('page.product-list', $slug) }}" class="link-btn"
                id="{{ $section->product_category->section->button->slug ?? '' }}_preview">
                {!! $section->product_category->section->button->text ?? '' !!}
            </a>
        </div>
    </div>
</section>
