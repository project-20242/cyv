
@foreach ($blogs as $key => $blog)
@if($request->cat_id == '0' || $blog->category_id == $request->cat_id)

<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 blog-itm">
        <div class="blog-itm-inner">
            <div class="blog-img">
                <a href="{{route('page.article',[$slug,$blog->id])}}">
                    <img src="{{ asset($blog->cover_image_path) }}">
                    <span class="blg-lbl">ARTICLES</span>
                </a>
            </div>
            <div class="blog-content">
                <div class="blog-content-top">
                    <span class="blog-itm-cat">{{$blog->category->name}}</span>
                    <h3><a href="{{route('page.article',[$slug,$blog->id])}}" class="description">{!! $blog->title !!}</a></h3>
                    <p class="description">{{$blog->short_description}}</p>
                </div>
                <div class="blog-contnt-bottom">
                    <a href="{{route('page.article',[$slug,$blog->id])}}" class="link-btn"> {{ __('READ MORE')}}</a>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endforeach
