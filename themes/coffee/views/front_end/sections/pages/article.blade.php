@extends('front_end.layouts.app')
@section('page-title')
{{ __('Article Page') }}
@endsection
@section('content')
@include('front_end.sections.partision.header_section')
@foreach ($blogs as $blog)
<section class="blog-page-banner common-banner-section"
    style="background-image:url({{asset('themes/'.$currentTheme.'/assets/images/blog-banner.jpg')}});">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="common-banner-content">
                    <ul class="blog-cat">
                        <li class="active">{{__('Featured')}}</li>
                        <li><b> {{__('Category')}}: </b> {{ $blog->category->name }}</li>
                        <li><b>{{__('Date')}}:</b> {{ $blog->created_at->format('d M,Y ') }}</li>
                    </ul>
                    <div class="section-title">
                        <h2>{!! $blog->title !!}</h2>
                    </div>
                    <p class="description">{!!$blog->short_description!!}</p>
                    <a href="#" class="btn-secondary white-btn">
                        <span class="btn-txt">{{__('READ MORE')}}</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="article-section padding-top padding-bottom">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="about-user d-flex align-items-center">
                    <div class="abt-user-img">
                        <img src="{{asset('themes/'.$currentTheme.'/assets/images/insta-pro.png')}}">
                    </div>
                    <h6>
                        <span>John Doe,</span>
                        company.com
                    </h6>
                    <div class="post-lbl"><b>{{__('Category')}}:</b>{{$blog->category->name}}</div>
                    <div class="post-lbl"><b>{{__('Date')}}:</b>{{$blog->created_at->format('d M, Y ')}}</div>
                </div>

            </div>
            <div class="col-md-8 col-12">
                <div class="aticleleftbar">
                    <p>{!! html_entity_decode($blog->content) !!}</p>

                    <div class="art-auther"><b>John Doe</b>, <a href="company.com">company.com</a></div>

                    <div class="art-auther"><b>{{ __('Tags:')}}</b> {{$blog->category->name}}</div>

                    <ul class="article-socials d-flex align-items-center">

                        <li><span>{{__('Share:')}}</span></li>

                        @for ($i = 0; $i < $section->footer->section->footer_link->loop_number ?? 1; $i++)
                            <li>
                                <a href="{{ $section->footer->section->footer_link->social_link->{$i} ?? '#'}}"
                                    target="_blank">
                                    <img src="{{ asset($section->footer->section->footer_link->social_icon->{$i}->image ?? 'themes/' . $currentTheme . '/assets/images/youtube.svg') }}"
                                        class="{{ 'social_icon_'. $i .'_preview' }}" alt="icon" style="width:60%;">
                                </a>
                            </li>
                            @endfor
                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="articlerightbar blog-grid-section">
                    <div class="section-title">
                        <h3>{{__('Related articles')}}</h3>
                    </div>
                    <div class="row blog-grid">
                        @foreach ($datas->take(2) as $data)

                        <div class="blog-itm col-md-12 col-sm-6 col-12">
                            <div class="blog-itm-inner">
                                <div class="blog-img">
                                    <a href="{{route('page.article',[$slug,$data->id])}}" tabindex="0">
                                        <img src="{{asset($data->cover_image_path)}}">
                                        <span class="blg-lbl">ARTICLES</span>
                                    </a>
                                </div>
                                <div class="blog-content">
                                    <div class="blog-content-top">
                                        <span class="blog-itm-cat">{{$data->category->name}}</span>
                                        <h3>
                                            <a href="{{route('page.article',[$slug,$data->id])}}" tabindex="0"
                                                class="description">{{$data->title}}
                                            </a>
                                        </h3>
                                        <p class="descriptions">{{$data->short_description}}</p>
                                    </div>
                                    <div class="blog-contnt-bottom">
                                        <a href="{{route('page.article',[$slug,$data->id])}}" class="link-btn">
                                            {{ __('View Blog')}}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>



<hr class="article-line">
@endforeach
<section class="padding-top blog-grid-section padding-bottom">
    <div class="container">
        <div class="section-title">
            <h2>{{ __('Latest Blogs')}}</h2>
        </div>
        <div class="blog-slider2">
            @foreach ($l_articles as $blog)

            <div class="blog-itm">
                <div class="blog-itm-inner">
                    <div class="blog-img">
                        <a href="{{route('page.article',[$slug,$blog->id])}}" tabindex="0">
                            <img src="{{asset($blog->cover_image_path)}}">
                            <span class="blg-lbl">{{ __('ARTICLES')}}</span>
                        </a>
                    </div>
                    <div class="blog-content">
                        <div class="blog-content-top">
                            <span class="blog-itm-cat">{{$blog->category->name}}</span>
                            <h3>
                                <a href="{{route('page.article',[$slug,$blog->id])}}" tabindex="0"
                                    class="description">{!!$blog->title!!}
                                </a>
                            </h3>
                            <p class="description">{{$blog->short_description}}</p>
                        </div>
                        <div class="blog-contnt-bottom">
                            <a href="{{route('page.article',[$slug,$blog->id])}}" class="link-btn">
                                {{ __('View Blog')}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>


@include('front_end.sections.partision.footer_section')
@endsection