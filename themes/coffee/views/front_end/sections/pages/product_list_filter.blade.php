<div class="row">
    @foreach($products as $product)

    <div class="col-lg-4 col-md-4 col-sm-6 col-12 product-card theme-colored-card">
    <div class="product-card-inner">
        <div class="product-img">
            <a href="{{url($slug.'/product/'.$product->slug)}}">
                <img src="{{ asset($product->cover_image_path) }}">
                <div class="custom-output">
                {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $product->id) !!}
                </div>
            </a>
        </div>
        <div class="product-content">
            <div class="product-content-top">
                <div class="d-flex justify-content-end wsh-wrp">
                    <a href="javascript:void(0)" class="wishlist wbwish  wishbtn-globaly" product_id="{{$product->id}}" in_wishlist="{{ $product->in_whishlist ? 'remove' : 'add'}}">
                        <span class="wish-ic">
                            <i class="{{ $product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}" style='color: 00000'></i>
                        </span>
                    </a>
                    <div class="product-btn-wrp">
                        @php
                            $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                            $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                        @endphp
                        @if(isset($module) && $module->isEnabled())
                            {{-- Include the module blade button --}}
                            @include('productquickview::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                        @endif
                        @if(isset($compare_module) && $compare_module->isEnabled())
                            {{-- Include the module blade button --}}
                            @include('productcompare::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                        @endif
                    </div>
                </div>
                <div class="subtitle">{{$product->ProductData->name}}</div>
                <h3><a href="{{url($slug.'/product/'.$product->slug)}}" class="description">{{$product->name}}</a></h3>
            </div>
            <div class="product-content-bottom">
                <div class="main-price d-flex align-items-center justify-content-between">
                    @if ($product->variant_product == 0)
                        <div class="price">
                            <ins>{{ currency_format_with_sym(($product->sale_price ?? $product->price), $store->id, $currentTheme)}}</ins>
                        </div>
                    @else
                        <div class="price">
                            <ins>{{ __('In Variant') }}</ins>
                        </div>
                    @endif
                    <a href="javascript:void(0)" class="link-btn addcart-btn-globaly" type="submit" product_id="{{ $product->id }}" variant_id="{{ $product->default_variant_id }}" qty="1">
                        + {{ __('ADD TO CART')}}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
    @endforeach
</div>

@php
    $page_no = !empty($page) ? $page : 1;
@endphp

<div class="d-flex justify-content-center col-12 " style="justify-content: flex-end;">
    <nav class="dataTable-pagination">
        <ul class="dataTable-pagination-list">
            <li class="pagination" style="margin-left: auto;">
            {{ $products->onEachSide(0)->links('pagination::bootstrap-4') }}
            </li>
        </ul>
    </nav>
</div>

