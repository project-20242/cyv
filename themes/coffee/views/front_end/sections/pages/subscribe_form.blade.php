<form class="footer-subscribe-form" action='{{ route("newsletter.store",$slug) }}' method="post">
    @csrf
    <div class="input-box">
        <input type="email" placeholder="Escribe tu dirección de correo electrónico..." name="email">
        <button type="submit">
            {{ __('Subscribe')}}
        </button>
    </div>
        <label for="subscribecheck" id="{{ $section->subscribe->section->sub_title->slug ?? '' }}_preview">
            {!! $section->subscribe->section->sub_title->text ?? '' !!}
        </label>
</form>
