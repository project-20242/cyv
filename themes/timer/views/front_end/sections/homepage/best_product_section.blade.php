<section class="bestseller-section product-card-section padding-bottom" style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="product-card-head d-flex align-items-center justify-content-between">
            <div class="section-title">
                <span class="subtitle" id="{{ ($section->best_product->section->sub_title->slug ?? '') }}_preview">{!!$section->best_product->section->sub_title->text ?? '' !!}</span>
                <h2  id="{{ ($section->best_product->section->title->slug ?? '') }}_preview">{!! $section->best_product->section->title->text ?? ''!!}</h2>
            </div>
            <p id="{{ ($section->best_product->section->description->slug ?? '') }}_preview">{!! $section->best_product->section->description->text ?? '' !!}</p>
            <a href="{{ route('page.product-list', $slug) }}" class="btn" tabindex="0"  id="{{ ($section->best_product->section->button->slug ?? '') }}_preview">
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8.39025 12.636V11.06L14.2425 4.756C15.2525 3.668 15.2525 1.904 14.2425 0.816001C13.2324 -0.272 11.5949 -0.272 10.5848 0.816001L0.539316 11.637C-0.612777 12.878 0.203182 15 1.83249 15H6.19566C7.4077 15 8.39025 13.9416 8.39025 12.636ZM6.92719 11.0601L5.03558 9.02244L1.57385 12.7514C1.34343 12.9996 1.50663 13.424 1.83249 13.424H6.19566C6.59968 13.424 6.92719 13.0712 6.92719 12.636V11.0601ZM6.13287 7.84044L11.6194 1.9304C12.058 1.45787 12.7693 1.45787 13.2079 1.9304C13.6466 2.40294 13.6466 3.16907 13.2079 3.6416L7.72144 9.55164L6.13287 7.84044Z" fill="#CDC6BE"></path>
                </svg>
                {!!  $section->best_product->section->button->text ?? ''!!}
            </a>
        </div>
        <div class="bg-black product-card-reverse product-two-row-slider">
            @foreach ($products as $product)
                <div class="product-card-slider-main">
                    <div class="bestseller-itm product-card">
                        <div class="product-card-inner">
                            <div class="pro-img">
                                <a href="{{ url($slug.'/product/'. $product->slug) }}">
                                    <img src="{{ asset($product->cover_image_path) }}">
                                </a>
                            </div>
                            <div class="pro-content">
                                <div class="pro-content-inner">
                                    <div class="pro-content-top">
                                        <div class="content-title">
                                            <div class="subtitle">
                                                <span>{{!empty($product->ProductData) ? $product->ProductData->name : ''}} </span>
                                                {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $product->id) !!}
                                                    <a href="javascript:void(0)" class=" wishbtn wishbtn-globaly"
                                                        product_id="{{ $product->id }}"
                                                        in_wishlist="{{ $product->in_whishlist ? 'remove' : 'add' }}">
                                                        <span class="wish-ic">
                                                            <i
                                                                class="{{ $product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"></i>
                                                        </span>
                                                    </a>
                                                    <div class="product-btn-wrp">
                                                        @php
                                                            $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                            $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                                        @endphp
                                                        @if(isset($module) && $module->isEnabled())
                                                            {{-- Include the module blade button --}}
                                                            @include('productquickview::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                        @endif
                                                        @if(isset($compare_module) && $compare_module->isEnabled())
                                                            {{-- Include the module blade button --}}
                                                            @include('productcompare::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                        @endif
                                                    </div>
                                            </div>
                                            <h4><a href="{{ url($slug.'/product/'. $product->slug) }}">{{ $product->name }}</a></h4>
                                        </div>
                                        {{-- <div class="order-select">
                                            <div class="checkbox check-product">
                                                <input id="checkbox-7" name="radio" type="checkbox" value=".blue" checked>
                                                <label for="checkbox-7" class="checkbox-label">Aluminium border</label>
                                            </div>
                                            <div class="checkbox check-product">
                                                <input id="checkbox-8" name="radio" type="checkbox" value=".blue">
                                                <label for="checkbox-8" class="checkbox-label">Gold border</label>
                                            </div>
                                        </div> --}}
                                    </div>
                                    @if ($product->variant_product == 0)
                                        <div class="price">
                                            <ins>{{ currency_format_with_sym(($product->sale_price ?? $product->price) , $store->id, $currentTheme) }}</ins>
                                        </div>
                                    @else
                                        <div class="price">
                                            <ins>{{ __('In Variant') }}</ins>
                                        </div>
                                    @endif
                                    <a href="javascript:void(0)" class="btn-secondary addcart-btn-globaly"
                                        product_id="{{ $product->id }}" variant_id="0" qty="1">
                                        {{ __('Add to cart') }}
                                        <svg viewBox="0 0 10 5">
                                            <path
                                                d="M2.37755e-08 2.57132C-3.38931e-06 2.7911 0.178166 2.96928 0.397953 2.96928L8.17233 2.9694L7.23718 3.87785C7.07954 4.031 7.07589 4.28295 7.22903 4.44059C7.38218 4.59824 7.63413 4.60189 7.79177 4.44874L9.43039 2.85691C9.50753 2.78197 9.55105 2.679 9.55105 2.57146C9.55105 2.46392 9.50753 2.36095 9.43039 2.28602L7.79177 0.69418C7.63413 0.541034 7.38218 0.544682 7.22903 0.702329C7.07589 0.859976 7.07954 1.11192 7.23718 1.26507L8.1723 2.17349L0.397965 2.17336C0.178179 2.17336 3.46059e-06 2.35153 2.37755e-08 2.57132Z">
                                            </path>
                                        </svg>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="single-btn">
            <a href="{{ route('page.product-list', $slug) }}" class="btn" tabindex="0" d="{{ ($section->best_product->section->button_second->slug ?? '') }}_preview">
                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M8.39025 12.636V11.06L14.2425 4.756C15.2525 3.668 15.2525 1.904 14.2425 0.816001C13.2324 -0.272 11.5949 -0.272 10.5848 0.816001L0.539316 11.637C-0.612777 12.878 0.203182 15 1.83249 15H6.19566C7.4077 15 8.39025 13.9416 8.39025 12.636ZM6.92719 11.0601L5.03558 9.02244L1.57385 12.7514C1.34343 12.9996 1.50663 13.424 1.83249 13.424H6.19566C6.59968 13.424 6.92719 13.0712 6.92719 12.636V11.0601ZM6.13287 7.84044L11.6194 1.9304C12.058 1.45787 12.7693 1.45787 13.2079 1.9304C13.6466 2.40294 13.6466 3.16907 13.2079 3.6416L7.72144 9.55164L6.13287 7.84044Z" fill="#CDC6BE"></path>
                </svg>
                {!! $section->best_product->section->button_second->text ?? ''  !!}
            </a>
        </div>
    </div>
</section>
