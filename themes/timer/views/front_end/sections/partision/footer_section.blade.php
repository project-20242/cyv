<footer
    class="site-footer"style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">

        <div class="footer-row">

            <div class="footer-col footer-subscribe-col flex-grow-1">
                <div class="footer-widget">
                    <div class="footer-subscribe">
                        <div class="subtitle" id="{{ $section->subscribe->section->sub_title->slug ?? '' }}_preview">
                            {!! $section->subscribe->section->sub_title->text ?? '' !!}</div>
                        <h3 id="{{ $section->subscribe->section->title->slug ?? '' }}_preview">
                            {!! $section->subscribe->section->title->text ?? '' !!}</h3>
                    </div>
                    <p id="{{ $section->subscribe->section->description->slug ?? '' }}_preview">
                        {!! $section->subscribe->section->description->text ?? '' !!}</p>
                    <div class="footer-subscribe-form">
                        <form class="" action="{{ route('newsletter.store', $slug) }}" method="post">
                            @csrf
                            <div class="input-wrapper">
                                <input type="email" placeholder="ESCRIBE TU DIRECCIÓN DE CORREO ELECTRÓNICO..." name="email">
                                <button class="btn-subscibe">{{ __('Subscribe') }}</button>
                            </div>
                            <label for="123"
                                id="{{ $section->subscribe->section->sub_description->slug ?? '' }}_preview">{!! $section->subscribe->section->sub_description->text ?? '' !!}</label>
                        </form>
                    </div>
                </div>
            </div>


            @if (isset($section->footer->section->footer_menu_type))
                @for ($i = 0; $i < $section->footer->section->footer_menu_type->loop_number ?? 1; $i++)
                    <div class="footer-col footer-link footer-link-1">
                        <div class="footer-widget">
                            <h4>{{ $section->footer->section->footer_menu_type->footer_title->{$i} ?? '' }}</h4>
                            @php
                                $footer_menu_id = $section->footer->section->footer_menu_type->footer_menu_ids->{$i} ?? '';
                                $footer_menu = get_nav_menu($footer_menu_id);
                            @endphp
                                @if (!empty($footer_menu))
                                <ul>
                                    @foreach ($footer_menu as $key => $nav)
                                        @if ($nav->type == 'custom')
                                            <li>
                                                <a href="{{ url($nav->slug) }}" target="{{ $nav->target }}">
                                                    @if ($nav->title == null)
                                                        {{ $nav->title }}
                                                    @else
                                                        {{ $nav->title }}
                                                    @endif
                                                </a>
                                            </li>
                                        @elseif($nav->type == 'category')
                                            <li><a href="{{ route('themes.page', [$currentTheme, $nav->slug]) }}"
                                                    target="{{ $nav->target }}">
                                                    @if ($nav->title == null)
                                                        {{ $nav->title }}
                                                    @else
                                                        {{ $nav->title }}
                                                    @endif
                                                </a></li>
                                        @else
                                            <li><a href="{{ route('themes.page', [$currentTheme, $nav->slug]) }}"
                                                    target="{{ $nav->target }}">
                                                    @if ($nav->title == null)
                                                        {{ $nav->title }}
                                                    @else
                                                        {{ $nav->title }}
                                                    @endif
                                                </a>
                                            </li>
                                        @endif
                                        @endforeach
                                    </ul>
                                @endif
                        </div>
                    </div>
                @endfor
            @endif

            <div class="footer-col footer-link footer-link-3">
                <div class="footer-widget">
                    <h4> {{__('Share:')}} </h4>
                    <ul class="footer-list-social" role="list">
                        @if (isset($section->footer->section->footer_link))
                            <ul class="social-links">
                                @for ($i = 0; $i < $section->footer->section->footer_link->loop_number ?? 1; $i++)
                                    <li>
                                        <a href="{{ $section->footer->section->footer_link->social_link->{$i} ?? '#' }}"
                                            target="_blank" id="social_link_{{ $i }}">
                                            <img src="{{ asset($section->footer->section->footer_link->social_icon->{$i}->image ?? 'themes/' . $currentTheme . '/assets/images/youtube.svg') }}"
                                                class="{{ 'social_icon_' . $i . '_preview' }}" alt="icon"
                                                id="social_icon_{{ $i }}">
                                        </a>
                                    </li>
                                @endfor
                            </ul>
                        @endif
                    </ul>
                </div>
            </div>

        </div>
</footer>

<!--footer end here-->
<div class="overlay"></div>

<!--cart popup start here-->
<div class="cartDrawer cartajaxDrawer">
</div>

<div class="overlay wish-overlay"></div>
<div class="wishDrawer wishajaxDrawer">
</div>
