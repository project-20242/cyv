<section class="form-section form-bg padding-top padding-bottom"
style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <img  src="{{asset($section->subscribe->section->image->image ?? '') }}"
    class="subcribe-bg" id="{{ ($section->subscribe->section->image->slug ?? '').'_preview'}}" alt="image" >
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="left-slide-itm-inner">
                    <div class="section-title">
                        <span class="subtitle"  id="{{ ($section->subscribe->section->sub_title->slug ?? '') }}_preview">{!! $section->subscribe->section->sub_title->text ?? ""  !!}</span>
                        <h2  id="{{ ($section->subscribe->section->title->slug ?? '') }}_preview">{!! $section->subscribe->section->title->text ?? '' !!}</h2>
                    </div>
                    <p id="{{ ($section->subscribe->section->description->slug ?? '') }}_preview">{!! $section->subscribe->section->description->text ?? '' !!}</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-12">
                <form class="" action="{{ route('newsletter.store', $slug) }}" method="post">
                    @csrf
                    <div class="form-subscribe">
                        <span>{{ __('Type your email:') }}</span>
                        <div class="input-wrapper">
                            <input type="email" placeholder="ESCRIBE TU DIRECCIÓN DE CORREO ELECTRÓNICO..." name="email">
                            <button class="btn-subscibe">{{ __('Subscribe') }}</button>
                        </div>
                        <label for="FotterCheckbox" id="{{ ($section->subscribe->section->sub_description->slug ?? '') }}_preview">{!!  $section->subscribe->section->sub_description->text ?? "" !!}</label>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
