<div class="home-blog-slider white-dots">
@foreach ($landing_blogs as $blog)
    <div class="blog-itm">    
        <div class="blog-itm-inner">
            <div class="blog-img">
                <a href="{{route('page.article',[$slug,$blog->id])}}" class="img-wrapper">
                    <img src="{{asset($blog->cover_image_path)}}">
                </a>
            </div>
            <div class="blog-caption">
                <h5>
                    <a  class="description" href="{{route('page.article',[$slug,$blog->id])}}">{!! $blog->title !!}</a>
                </h5>
                <p >{{$blog->short_description}}
                </p>
                <div class="blog-lbl-row d-flex">
                <div class="blog-labl">
                        @John
                     </div>
                    <span class="blog-labl">{{ $blog->created_at->format('d M,Y ') }}</span>
                </div>
               
                <a class="btn blog-btn" href="{{route('page.article',[$slug,$blog->id])}}">
                    {{ __('Read more')}}
                </a>
            </div>
        </div>
    </div>  
@endforeach
</div>

