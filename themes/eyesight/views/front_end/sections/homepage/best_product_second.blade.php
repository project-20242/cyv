<section class="video-bottom-section" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
        <div class="container">
            <div class="bg-black product-card-reverse bestsell-slider">
                @foreach ($bestSeller as $bestSellers)
                    <div class="bestseller-itm product-card">
                        <div class="product-card-inner">
                            <div class="product-image">
                                <a href="{{ url($slug.'/product/'.$bestSellers->slug) }}">
                                    <img src="{{ asset($bestSellers->cover_image_path) }}">
                                </a>
                            </div>
                            <div class="product-content">
                                <div class="product-cont-top">
                                    <div class="subtitle">{{ $bestSellers->tag_api }}</div>
                                    {{-- <div class="custom-output"> --}}

                                    {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $latest_product->id) !!}
                                    {{-- </div> --}}
                                    <h3>
                                        <a
                                            href="{{ url($slug.'/product/'.$bestSellers->slug) }}">{{ $bestSellers->name }}</a>
                                    </h3>
                                    <div class="product-btn-wrp">
                                        @php
                                            $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                            $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                        @endphp
                                        @if(isset($module) && $module->isEnabled())
                                            {{-- Include the module blade button --}}
                                            @include('productquickview::pages.button', ['product_slug' => $bestSellers->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                        @endif
                                        @if(isset($compare_module) && $compare_module->isEnabled())
                                            {{-- Include the module blade button --}}
                                            @include('productcompare::pages.button', ['product_slug' => $bestSellers->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                        @endif
                                    </div>
                                </div>
                                <div class="product-cont-bottom">
                                    <div class="price-btn">
                                        @if ($bestSellers->variant_product == 0)
                                            <span class="price">
                                                <ins>{{ currency_format_with_sym(($bestSellers->sale_price ?? $bestSellers->price), $store->id, $currentTheme)}}</ins>
                                            </span>
                                        @else
                                            <span class="price">
                                                <ins>{{ __('In Variant') }}</ins>
                                            </span>
                                        @endif
                                        <button class="cart-button addcart-btn-globaly" type="submit"
                                            product_id="{{ $bestSellers->id }}"
                                            variant_id="0" qty="1">
                                            <svg width="20" height="20" viewBox="0 0 20 20">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M15.7424 6H4.25797C3.10433 6 2.1899 6.97336 2.26187 8.12476L2.76187 16.1248C2.82775 17.1788 3.70185 18 4.75797 18H15.2424C16.2985 18 17.1726 17.1788 17.2385 16.1248L17.7385 8.12476C17.8104 6.97336 16.896 6 15.7424 6ZM4.25797 4C1.95069 4 0.121837 5.94672 0.265762 8.24951L0.765762 16.2495C0.89752 18.3577 2.64572 20 4.75797 20H15.2424C17.3546 20 19.1028 18.3577 19.2346 16.2495L19.7346 8.24951C19.8785 5.94672 18.0496 4 15.7424 4H4.25797Z">
                                                </path>
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M5 5C5 2.23858 7.23858 0 10 0C12.7614 0 15 2.23858 15 5V7C15 7.55228 14.5523 8 14 8C13.4477 8 13 7.55228 13 7V5C13 3.34315 11.6569 2 10 2C8.34315 2 7 3.34315 7 5V7C7 7.55228 6.55228 8 6 8C5.44772 8 5 7.55228 5 7V5Z">
                                                </path>
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
