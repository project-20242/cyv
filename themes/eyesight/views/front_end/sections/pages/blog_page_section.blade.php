@extends('front_end.layouts.app')
@section('page-title')
{{ __('Blog Page') }}
@endsection
@section('content')
@include('front_end.sections.partision.header_section')
<section class="blog-page-banner common-banner-section"
    style="background-image:url({{ asset('themes/'.$currentTheme.'/assets/images/blog-banner.jpg')}});">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="common-banner-content">
                    <ul class="blog-cat">
                        <li class="active">{{ __('Destacado')}}</li>
                        {{-- <li><b>Category:</b> Fashion</li>
                            <li><b>Date:</b> 12 Mar, 2022</li> --}}
                    </ul>
                    <div class="section-title">
                       <h2>{{ __('Blog & Articles') }}</h2> 
                    </div>
                    <p>{{ __('The blog and article section serves as a treasure trove of valuable information.') }}</p>
                    <a href="#" class="btn-secondary white-btn">
                        <span class="btn-txt">{{ __('Read More') }}</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="blog-grid-section padding-top tabs-wrapper">
    <div class="container">
        <div class="section-title">
            <div class="subtitle">{{ __('ALL  PRODUCTS')}}</div>
            <h2>{{ __('From')}} <b> {{ __('our blog')}}</b></h2>
        </div>
        <div class="blog-head-row d-flex justify-content-between">
            <div class="blog-col-left">
                <ul class="d-flex tabs">
                    @foreach ($BlogCategory as $cat_key => $category)
                    <li class="tab-link on-tab-click {{$cat_key == 0 ? 'active' : ''}}" data-tab="{{ $cat_key }}">
                        <a href="javascript:;">{{ __('All Products') }}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="blog-col-right d-flex align-items-center justify-content-end">
                <span class="select-lbl"> {{ __('Sort by') }} </span>
                <select class="position">
                    <option value="lastest"> {{ __('Lastest') }} </option>
                    <option value="new"> {{ __('new') }} </option>
                </select>
            </div>
        </div>
        <div class="tabs-container">
            @foreach ($BlogCategory as $cat_k => $category)
            <div id="{{ $cat_k }}" class="tab-content {{$cat_k == 0 ? 'active' : ''}} ">
                <div class="blog-grid-row row f_blog">

                    @foreach ($blogs as $key => $blog)
                    @if($cat_k == '0' || $blog->category_id == $cat_k)
                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 blog-itm">
                        <div class="blog-itm-inner">
                            <div class="blog-img">
                                <a href="{{ route("page.article",[$slug,$blog->id]) }}">
                                    <img src="{{asset($blog->cover_image_path)}}" alt="" width="120"
                                        class="cover_img{{$blog->id}}">
                                </a>
                                <span class="blg-lbl">{{__('ACCESSORIES')}}</span>
                            </div>
                            <div class="blog-caption">
                                <h5><a href="{{ route("page.article",[$slug,$blog->id]) }}" class="description">
                                        {{$blog->title}} </a></h5>
                                <p>{!! $blog->short_description !!}</p>
                                <div class="blog-lbl-row d-flex">
                                    <div class="blog-labl">
                                        <b> {{ __('Category:') }} </b> {{$blog->category->name}}
                                    </div>
                                    <div class="blog-labl">
                                        <b> {{ __('Date:') }} </b> {{$blog->created_at->format('d M,Y ')}}
                                    </div>
                                </div>
                                <a href="{{ route("page.article",[$slug,$blog->id]) }}"
                                    class="btn">{{ __('SHOW MORE')}}</a>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

@include('front_end.sections.partision.footer_section')
@endsection