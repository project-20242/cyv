<section class="best-product-section padding-top {{ $option->class_name }}" style="position: relative;@if($option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order }}" data-id="{{ $option->order }}" data-value="{{ $option->id }}" data-hide="{{ $option->is_hide }}">
    <div class="custome_tool_bar"></div>            
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 col-12">
                <div class="section-title-left">
                    <div class="section-title">
                        <h2> <b>Enamórate de </b> <br> aromas increíbles </h2>
                    </div>
                    <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de la industria desde el año 1500.</p>
                </div>
                <div class="banner-links">
                    
                    <a href="product-list.html" class="btn" tabindex="0">
                        Search more products
                    </a>
                </div>
            </div>
            <div class="col-md-6 col-12">
                <div class="best-product-right-inner">
                    <div class="product-banner-image-right">
                    
                        <img src="{{ asset('themes/' . $currentTheme . '/assets/images/product-banner-right-1.png') }}" alt="product banner right">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
