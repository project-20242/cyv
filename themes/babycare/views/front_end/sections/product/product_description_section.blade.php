<section class="pdp-descrip-section padding-top {{ $option->class_name }}" style="position: relative;@if($option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order }}" data-id="{{ $option->order }}" data-value="{{ $option->id }}" data-hide="{{ $option->is_hide }}">
    <div class="custome_tool_bar"></div>   
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-6 col-12">
                <div class="pdp-descrp-content">
                    <div class="section-title">
                        <h2>Descripción</h2>
                    </div>
                    <p>Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de la industria desde el año 1500.</p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12">
                <div class="product-description-content m-0">
                    <div class="section-title">
                        <h2>Info:</h2>
                    </div>
                    <div class="product-info">
                        <ul>
                            <li>
                                <h6>SKU: <b>PER_2251161222</b></h6>
                            </li>
                            <li>
                                <h6>CATEGORÍA: <b>Perfumes</b></h6>
                            </li>
                            <li>
                                <h6>ETIQUETAS: <b>SCENT, PERFUMES</b></h6>
                            </li>
                        </ul>
                        <ul>
                            <li>
                                <h6>TAMAÑO DEL FRASCO: <b>PER_2251161222</b></h6>
                            </li>
                            <li>
                                <h6>COLOR DEL LÍQUIDO: <b>Perfumes</b></h6>
                            </li>
                            <li>
                                <h6>AROMA: <b>CEDRO, CÍTRICO</b></h6>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
