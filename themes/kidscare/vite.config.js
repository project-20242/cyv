import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";
import path from "path";



export default defineConfig({
    plugins: [
        laravel({
            input: [
                "themes\kidscare\sass/app.scss",
                "themes\kidscare\js/app.js"
            ],
            buildDirectory: "kidscare",
        }),
        
        
        {
            name: "blade",
            handleHotUpdate({ file, server }) {
                if (file.endsWith(".blade.php")) {
                    server.ws.send({
                        type: "full-reload",
                        path: "*",
                    });
                }
            },
        },
    ],
    resolve: {
        alias: {
            '@': '/themes\kidscare\js',
            '~bootstrap': path.resolve('node_modules/bootstrap'),
        }
    },
    
});
