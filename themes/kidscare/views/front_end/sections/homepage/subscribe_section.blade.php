<section class="subscription-section padding-top padding-bottom"
    style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>

    <img src=" {{ asset($homepage_banner_image_left ?? 'themes/kidscare/assets/img/sub-left.png') }}" class="sub-left">
    <img
        src=" {{ asset($homepage_banner_image_right ?? 'themes/kidscare/assets/img/sub-right.png') }}"class="sub-right">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-5 col-12">
                <div class="subscription-left-column">
                    <div class="section-title">
                        <div class="subtitle" id="{{ $section->subscribe->section->sub_title->slug ?? '' }}_preview">
                            {!! $section->subscribe->section->sub_title->text ?? '' !!}</div>
                        <h2 id="{{ $section->subscribe->section->title->slug ?? '' }}_preview">
                            {!! $section->subscribe->section->title->text ?? '' !!}
                        </h2>
                    </div>
                </div>
            </div>
            <div class="col-md-7 col-12">
                <form class="footer-subscribe-form" action="{{ route('newsletter.store', $slug) }}" method="post">
                    @csrf
                    <div class="input-wrapper">
                        <input type="email" placeholder="Correo Electronico..." name="email">
                        <button type="submit" class="btn-subscibe">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20"
                                fill="none">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M4.97863e-08 9.99986C-7.09728e-06 10.4601 0.373083 10.8332 0.83332 10.8332L17.113 10.8335L15.1548 12.7358C14.8247 13.0565 14.817 13.584 15.1377 13.9142C15.4584 14.2443 15.986 14.2519 16.3161 13.9312L19.7474 10.5979C19.9089 10.441 20.0001 10.2254 20.0001 10.0002C20.0001 9.77496 19.9089 9.55935 19.7474 9.40244L16.3161 6.0691C15.986 5.74841 15.4584 5.75605 15.1377 6.08617C14.817 6.41628 14.8247 6.94387 15.1548 7.26456L17.1129 9.1668L0.833346 9.16654C0.373109 9.16653 7.24653e-06 9.53962 4.97863e-08 9.99986Z"
                                    fill="#183A40" />
                            </svg>
                        </button>
                    </div>
                    <div class="checkbox-custom">
                        <p id="{{ $section->subscribe->section->description->slug ?? '' }}_preview">
                            {!! $section->subscribe->section->description->text ?? '' !!}
                        </p>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
