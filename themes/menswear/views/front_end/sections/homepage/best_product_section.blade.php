<section class="men-skincare-section padding-top padding-bottom" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
        <div class="offset-container offset-left">
                <div class="row no-gutters">
                    <div class="col-xl-4 col-lg-5 col-md-6 col-12 skincare-left-box">
                        <div class="skincare-left">

                            <div class="section-title">
                                <div class="subtitle" id="{{ $section->best_product->section->sub_title->slug ?? '' }}_preview">{!! $section->best_product->section->sub_title->text ?? '' !!}</div>
                                <h2 id="{{ $section->best_product->section->title->slug ?? '' }}">{!!
                    $section->best_product->section->title->text ?? '' !!}</h2>
                            </div>
                            <div class="skincare-pro ">
                                <div class="skincare-pro-full">
                                    <div class="main-hiro-pro-slider bg-light">
                                        @foreach ($all_products->take(5)  as $homepage_product)
                                            <div class="hiro-pro-itm product-card ">
                                                <div class="product-card-inner">

                                                    <div class="product-image">
                                                        <a href="{{ url($slug.'/product/'.$homepage_product->slug) }}">
                                                            <img
                                                                src="{{ asset($homepage_product->cover_image_path) }}">
                                                        </a>
                                                    </div>
                                                    <div class="product-content">
                                                        <div class="product-cont-top">
                                                            <div class="subtitle">{{ !empty($homepage_product->ProductData) ? $homepage_product->ProductData->name : ''}}</div>
                                                            <div class="product-btn-wrp">
                                                                @php
                                                                    $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                                    $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                                                @endphp
                                                                @if(isset($module) && $module->isEnabled())
                                                                    {{-- Include the module blade button --}}
                                                                    @include('productquickview::pages.button', ['product_slug' => $homepage_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                                @endif
                                                                @if(isset($compare_module) && $compare_module->isEnabled())
                                                                    {{-- Include the module blade button --}}
                                                                    @include('productcompare::pages.button', ['product_slug' => $homepage_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                                @endif
                                                            </div>
                                                            <div class="custom-output">
                                                            {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $latest_product->id) !!}
                                                            </div>
                                                            <h3><a
                                                                    href="{{ url($slug.'/product/'.$homepage_product->slug) }}">{{ $homepage_product->name }}</a>
                                                            </h3>
                                                        </div>
                                                        <div class="product-cont-bottom">
                                                            <div class="price-btn">
                                                                @if ($homepage_product->variant_product == 0)
                                                                    <span class="price">
                                                                        <ins>{{ currency_format_with_sym(($homepage_product->sale_price ?? $homepage_product->price), $store->id, $currentTheme)}}</ins>
                                                                    </span>
                                                                @else
                                                                    <span class="price">
                                                                        <ins>{{ __('In Variant') }}</ins>
                                                                    </span>
                                                                @endif
                                                                <button class="cart-button addcart-btn-globaly"
                                                                    type="submit"
                                                                    product_id="{{ $homepage_product->id }}"
                                                                    variant_id="0"
                                                                    qty="1">
                                                                    <svg width="20" height="20"
                                                                        viewBox="0 0 20 20">
                                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                                            d="M15.7424 6H4.25797C3.10433 6 2.1899 6.97336 2.26187 8.12476L2.76187 16.1248C2.82775 17.1788 3.70185 18 4.75797 18H15.2424C16.2985 18 17.1726 17.1788 17.2385 16.1248L17.7385 8.12476C17.8104 6.97336 16.896 6 15.7424 6ZM4.25797 4C1.95069 4 0.121837 5.94672 0.265762 8.24951L0.765762 16.2495C0.89752 18.3577 2.64572 20 4.75797 20H15.2424C17.3546 20 19.1028 18.3577 19.2346 16.2495L19.7346 8.24951C19.8785 5.94672 18.0496 4 15.7424 4H4.25797Z">
                                                                        </path>
                                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                                            d="M5 5C5 2.23858 7.23858 0 10 0C12.7614 0 15 2.23858 15 5V7C15 7.55228 14.5523 8 14 8C13.4477 8 13 7.55228 13 7V5C13 3.34315 11.6569 2 10 2C8.34315 2 7 3.34315 7 5V7C7 7.55228 6.55228 8 6 8C5.44772 8 5 7.55228 5 7V5Z">
                                                                        </path>
                                                                    </svg>
                                                                </button>
                                                            </div>
                                                            <a href="{{ url($slug.'/product/'.$homepage_product->slug) }}"
                                                                class="link-btn">
                                                                {{ __('Read More') }}
                                                                <svg width="6" height="5" viewBox="0 0 6 5"
                                                                    fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                                        d="M5.89017 2.75254C6.03661 2.61307 6.03661 2.38693 5.89017 2.24746L3.64017 0.104605C3.49372 -0.0348681 3.25628 -0.0348681 3.10984 0.104605C2.96339 0.244078 2.96339 0.470208 3.10984 0.609681L5.09467 2.5L3.10984 4.39032C2.96339 4.52979 2.96339 4.75592 3.10984 4.8954C3.25628 5.03487 3.49372 5.03487 3.64016 4.8954L5.89017 2.75254ZM0.640165 4.8954L2.89017 2.75254C3.03661 2.61307 3.03661 2.38693 2.89017 2.24746L0.640165 0.104605C0.493719 -0.0348682 0.256282 -0.0348682 0.109835 0.104605C-0.0366115 0.244078 -0.0366115 0.470208 0.109835 0.609681L2.09467 2.5L0.109835 4.39032C-0.0366117 4.52979 -0.0366117 4.75592 0.109835 4.8954C0.256282 5.03487 0.493719 5.03487 0.640165 4.8954Z">
                                                                    </path>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="skincare-pro-full">
                                    <div class="main-hiro-pro-slider bg-light">
                                        @foreach ($all_products->take(5) as $homepage_product)

                                            <div class="hiro-pro-itm product-card ">
                                                <div class="product-card-inner">
                                                    <div class="product-image">
                                                        <a href="{{ url($slug.'/product/'.$homepage_product->slug) }}">
                                                            <img
                                                                src="{{ asset($homepage_product->cover_image_path) }}">
                                                        </a>
                                                    </div>
                                                    <div class="product-content">
                                                        <div class="product-cont-top">
                                                            <div class="subtitle">{{ !empty($homepage_product->ProductData) ? $homepage_product->ProductData->name : ''}}</div>
                                                            <div class="product-btn-wrp">
                                                                @php
                                                                    $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                                    $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                                                @endphp
                                                                @if(isset($module) && $module->isEnabled())
                                                                    {{-- Include the module blade button --}}
                                                                    @include('productquickview::pages.button', ['product_slug' => $homepage_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                                @endif
                                                                @if(isset($compare_module) && $compare_module->isEnabled())
                                                                    {{-- Include the module blade button --}}
                                                                    @include('productcompare::pages.button', ['product_slug' => $homepage_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                                @endif
                                                            </div>
                                                            <div class="custom-output">
                                                            {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $latest_product->id) !!}
                                                            </div>
                                                            <h3><a href="product.html">{{ $homepage_product->name }}</a>
                                                            </h3>
                                                        </div>
                                                        <div class="product-cont-bottom">
                                                            <div class="price-btn">
                                                                @if ($homepage_product->variant_product == 0)
                                                                    <span class="price">
                                                                        <ins>{{ currency_format_with_sym(($homepage_product->sale_price ?? $homepage_product->price), $store->id, $currentTheme)}}</ins>
                                                                    </span>
                                                                @else
                                                                    <span class="price">
                                                                        <ins>{{ __('In Variant') }}</ins>
                                                                    </span>
                                                                @endif
                                                                <button class="cart-button addcart-btn-globaly"
                                                                    type="submit"
                                                                    product_id="{{ $homepage_product->id }}"
                                                                    variant_id="0"
                                                                    qty="1">
                                                                    <svg width="20" height="20"
                                                                        viewBox="0 0 20 20">
                                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                                            d="M15.7424 6H4.25797C3.10433 6 2.1899 6.97336 2.26187 8.12476L2.76187 16.1248C2.82775 17.1788 3.70185 18 4.75797 18H15.2424C16.2985 18 17.1726 17.1788 17.2385 16.1248L17.7385 8.12476C17.8104 6.97336 16.896 6 15.7424 6ZM4.25797 4C1.95069 4 0.121837 5.94672 0.265762 8.24951L0.765762 16.2495C0.89752 18.3577 2.64572 20 4.75797 20H15.2424C17.3546 20 19.1028 18.3577 19.2346 16.2495L19.7346 8.24951C19.8785 5.94672 18.0496 4 15.7424 4H4.25797Z">
                                                                        </path>
                                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                                            d="M5 5C5 2.23858 7.23858 0 10 0C12.7614 0 15 2.23858 15 5V7C15 7.55228 14.5523 8 14 8C13.4477 8 13 7.55228 13 7V5C13 3.34315 11.6569 2 10 2C8.34315 2 7 3.34315 7 5V7C7 7.55228 6.55228 8 6 8C5.44772 8 5 7.55228 5 7V5Z">
                                                                        </path>
                                                                    </svg>
                                                                </button>
                                                            </div>
                                                            <a href="{{ url($slug.'/product/'.$homepage_product->slug) }}"
                                                                class="link-btn">
                                                                {{ __('Read More') }}
                                                                <svg width="6" height="5" viewBox="0 0 6 5"
                                                                    fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                                        d="M5.89017 2.75254C6.03661 2.61307 6.03661 2.38693 5.89017 2.24746L3.64017 0.104605C3.49372 -0.0348681 3.25628 -0.0348681 3.10984 0.104605C2.96339 0.244078 2.96339 0.470208 3.10984 0.609681L5.09467 2.5L3.10984 4.39032C2.96339 4.52979 2.96339 4.75592 3.10984 4.8954C3.25628 5.03487 3.49372 5.03487 3.64016 4.8954L5.89017 2.75254ZM0.640165 4.8954L2.89017 2.75254C3.03661 2.61307 3.03661 2.38693 2.89017 2.24746L0.640165 0.104605C0.493719 -0.0348682 0.256282 -0.0348682 0.109835 0.104605C-0.0366115 0.244078 -0.0366115 0.470208 0.109835 0.609681L2.09467 2.5L0.109835 4.39032C-0.0366117 4.52979 -0.0366117 4.75592 0.109835 4.8954C0.256282 5.03487 0.493719 5.03487 0.640165 4.8954Z">
                                                                    </path>
                                                                </svg>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-7 col-md-6 col-12">
                        <div class="skincare-right">
                            <img src="{{ asset($section->best_product->section->image->image ?? '') }}" id="{{ $section->best_product->section->image->slug ?? '' }}_preview">
                        </div>
                    </div>
                </div>

        </div>
    </section>
