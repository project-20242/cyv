<section class="home-cat-section padding-top  padding-bottom" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-12 home-cat-left">
                    <div class="home-cat-left-inner">
                        @foreach ($products->take(2) as $home_product)
                            <div class="home-cat-box">
                                <a href="{{ route('page.product-list', $slug) }}">
                                    <img src="{{ asset($home_product->cover_image_path) }}">
                                    <div class="home-cat-text">
                                        <div class="subtitle"> {{!empty($home_product->ProductData) ? $home_product->ProductData->name : ''}} </div>
                                        <h3 id="{{ $section->category->section->title->slug ?? '' }}_preview">{!! $section->category->section->title->text ?? '' !!}</h3>
                                        <div class="link-btn justify-content-start">
                                            {{ __('See More') }}
                                            <svg width="6" height="5" viewBox="0 0 6 5" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M5.89017 2.75254C6.03661 2.61307 6.03661 2.38693 5.89017 2.24746L3.64017 0.104605C3.49372 -0.0348681 3.25628 -0.0348681 3.10984 0.104605C2.96339 0.244078 2.96339 0.470208 3.10984 0.609681L5.09467 2.5L3.10984 4.39032C2.96339 4.52979 2.96339 4.75592 3.10984 4.8954C3.25628 5.03487 3.49372 5.03487 3.64016 4.8954L5.89017 2.75254ZM0.640165 4.8954L2.89017 2.75254C3.03661 2.61307 3.03661 2.38693 2.89017 2.24746L0.640165 0.104605C0.493719 -0.0348682 0.256282 -0.0348682 0.109835 0.104605C-0.0366115 0.244078 -0.0366115 0.470208 0.109835 0.609681L2.09467 2.5L0.109835 4.39032C-0.0366117 4.52979 -0.0366117 4.75592 0.109835 4.8954C0.256282 5.03487 0.493719 5.03487 0.640165 4.8954Z">
                                                </path>
                                            </svg>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-8 col-12 home-cat-right">
                        <div class="home-cat-right-inner">
                            <div class="section-title">
                                <h3 id="{{ $section->category->section->title->slug ?? '' }}_preview">{!! $section->category->section->title->text ?? '' !!}</h3>
                            </div>
                            <p id="{{ $section->category->section->description->slug ?? '' }}_preview">{!! $section->category->section->description->text ?? '' !!}</p>
                            <a href="{{ route('page.product-list', $slug) }}" class="btn" id="{{ $section->category->section->button->slug ?? '' }}_preview">{!! $section->category->section->button->text ?? '' !!}</a>
                        </div>
                </div>
            </div>
        </div>
    </section>







    
