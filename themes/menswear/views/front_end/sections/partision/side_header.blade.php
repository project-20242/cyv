<!--header start here-->
<header class="site-header header-style-one dark-header" style="@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide  ?? '' }}" data-section="{{ $option->section_name  ?? '' }}"  data-store="{{ $option->store_id  ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
 
 <div class="custome_tool_bar"></div>
        <div class="announcebar">
            <div class="container">
                <div class="announce-row no-gutters row align-items-center">
                    <div class=" col-6">
                        <div class="annoucebar-left justify-content-between">
                        <p id="{{ $section->header->section->title->slug ?? '' }}_preview">{!! $section->header->section->title->text ?? '' !!}</p>
                        </div>
                    </div>
                    <div class=" col-6 d-flex justify-content-end">
                        <div class="announcebar-right d-flex align-items-center justify-content-end">
                            <ul class="d-flex align-items-center">
                                @foreach ($pages as $page)
                                    @if ($page->page_status == 'custom_page')
                                        <li class="menu-lnk" style="color : black;"> <a
                                                href="{{ route('custom.page', [$slug,$page->page_slug]) }}">{{ $page->name }}</a>
                                        </li>
                                    @else
                                    @endif
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <div class="main-navigationbar">
        <div class="container">
            <div class="navigationbar-row d-flex align-items-center">
                <div class="menu-items-col row align-items-center no-gutters">
                    <div class="col-md-6 col-2 d-flex align-items-center">
                        <div class="hide-on-tblt">
                            <div class="home-link" id="{{ $section->header->section->sub_title->slug ?? '' }}_preview">{!! $section->header->section->sub_title->text ?? '' !!}
                            </div>
                            <div class="fixed-headr-left">
                                <div class="logo-col">
                                    <h1>
                                        <a href="{{ route('landing_page',$slug) }}">
                                            <img src="{{ asset('themes/' . $currentTheme . '/assets/images/logo.png') }}"
                                                alt="logo">
                                        </a>
                                    </h1>
                                </div>
                                <div class="cart-header desk-only">
                                    <a href="javascript:;">
                                        <svg width="20" height="20" viewBox="0 0 20 20">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M15.7424 6H4.25797C3.10433 6 2.1899 6.97336 2.26187 8.12476L2.76187 16.1248C2.82775 17.1788 3.70185 18 4.75797 18H15.2424C16.2985 18 17.1726 17.1788 17.2385 16.1248L17.7385 8.12476C17.8104 6.97336 16.896 6 15.7424 6ZM4.25797 4C1.95069 4 0.121837 5.94672 0.265762 8.24951L0.765762 16.2495C0.89752 18.3577 2.64572 20 4.75797 20H15.2424C17.3546 20 19.1028 18.3577 19.2346 16.2495L19.7346 8.24951C19.8785 5.94672 18.0496 4 15.7424 4H4.25797Z">
                                            </path>
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M5 5C5 2.23858 7.23858 0 10 0C12.7614 0 15 2.23858 15 5V7C15 7.55228 14.5523 8 14 8C13.4477 8 13 7.55228 13 7V5C13 3.34315 11.6569 2 10 2C8.34315 2 7 3.34315 7 5V7C7 7.55228 6.55228 8 6 8C5.44772 8 5 7.55228 5 7V5Z">
                                            </path>
                                        </svg>
                                        <span class="desk-only icon-lable cart-price"
                                            id="sub_total_main_page">{{ 0 }} </span>{{ $currency }}
                                        <span class="count">{!! \App\Models\Cart::CartCount($slug) !!} </span>
                                    </a>

                                </div>
                                <ul class="main-nav">
                                
                                    <li class="menu-lnk">
                                        <a href="#">
                                            {{ __('Contact') }}
                                        </a>
                                    </li>

                                    {{-- <li class="menu-lnk">
                                        <a href="{{route('page.about')}}">
                                            {{ __('Blog') }}
                                        </a> --}}

                                    @foreach ($pages as $page)
                                        @if ($page->page_slug == 'about')
                                            <li class="menu-lnk"><a
                                                    href="{{ route('custom.page', [$slug,$page->page_slug]) }}">{{ $page->name }}</a>
                                            </li>
                                        @else
                                        @endif
                                    @endforeach
                                    {{-- </li> --}}

                                    <li class="menu-lnk">
                                        <a href="{{ route('page.product-list',$slug) }}"> {{ __('Shop All') }} </a>
                                    </li>
                                </ul>
                                <div class="socials d-flex align-items-center justify-content-center">
                                    <a href="https://www.facebook.com/" target="_blank">FB</a>
                                    IN<a href="https://twitter.com/" target="_blank">TW</a>
                                </div>
                            </div>
                        </div>

                        <div class="show-on-tblt">
                            <div class="logo-col">
                                <h1>
                                    <a href="{{ route('landing_page',$slug) }}">
                                        <img src="{{ asset((isset($theme_logo) && !empty($theme_logo)) ? $theme_logo : 'themes/' . $currentTheme . '/assets/images/logo.png') }}"
                                            alt="logo" style="filter: drop-shadow(1px 3px 8px #011c4b);">
                                    </a>
                                </h1>

                            </div>

                            <ul class="main-nav">
                            @if (!empty($topNavItems))
                                    @foreach ($topNavItems as $key => $nav)
                                        @if (!empty($nav->children[0]))
                                            <li class="menu-lnk has-item">
                                                <a href="#">
                                                    <span class="link-icon">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13"
                                                            viewBox="0 0 13 13" fill="none">
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M6.19178 10.2779C6.37663 10.1508 6.6207 10.1508 6.80555 10.2779L7.11244 10.4889C7.45128 10.7219 7.79006 10.8333 8.12365 10.8333C8.72841 10.8333 9.38816 10.4492 9.95049 9.66199C10.5068 8.8832 10.832 7.88189 10.832 7.04167C10.832 5.47332 9.69207 4.33333 8.12365 4.33333C7.68321 4.33333 7.28574 4.4228 6.9418 4.57698L6.72023 4.6763C6.57928 4.73948 6.41805 4.73949 6.2771 4.6763L6.05553 4.57698C5.71158 4.4228 5.31409 4.33333 4.87365 4.33333C3.30527 4.33333 2.16536 5.47328 2.16536 7.04167C2.16536 7.88191 2.49061 8.88322 3.04688 9.662C3.60918 10.4492 4.2689 10.8333 4.87365 10.8333C5.20724 10.8333 5.54603 10.7219 5.88489 10.4889L6.19178 10.2779ZM6.49866 3.58842C6.98579 3.37006 7.53471 3.25 8.12365 3.25C10.2904 3.25 11.9154 4.875 11.9154 7.04167C11.9154 9.20833 10.2904 11.9167 8.12365 11.9167C7.53471 11.9167 6.98579 11.7166 6.49866 11.3816C6.01152 11.7166 5.4626 11.9167 4.87365 11.9167C2.70694 11.9167 1.08203 9.20833 1.08203 7.04167C1.08203 4.875 2.70694 3.25 4.87365 3.25C5.4626 3.25 6.01152 3.37006 6.49866 3.58842Z"
                                                                fill="white" />
                                                            <path
                                                                d="M4.60286 6.5C4.45329 6.5 4.33203 6.62126 4.33203 6.77083V7.3125C4.33203 7.76123 4.6958 8.125 5.14453 8.125C5.29411 8.125 5.41536 8.00374 5.41536 7.85417V7.3125C5.41536 6.86377 5.0516 6.5 4.60286 6.5Z"
                                                                fill="white" />
                                                            <path
                                                                d="M8.39453 6.5C8.54411 6.5 8.66536 6.62126 8.66536 6.77083V7.3125C8.66536 7.76123 8.3016 8.125 7.85286 8.125C7.70329 8.125 7.58203 8.00374 7.58203 7.85417V7.3125C7.58203 6.86377 7.9458 6.5 8.39453 6.5Z"
                                                                fill="white" />
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M4.33333 2.16732C5.04453 2.16732 5.64901 2.62419 5.8693 3.26046C5.96717 3.54315 6.20085 3.79232 6.5 3.79232C6.79915 3.79232 7.04732 3.54736 6.98819 3.25411C6.73859 2.01617 5.64482 1.08398 4.33333 1.08398H3.79167C3.49251 1.08398 3.25 1.3265 3.25 1.62565C3.25 1.92481 3.49251 2.16732 3.79167 2.16732H4.33333Z"
                                                                fill="white" />
                                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                                d="M10.832 2.34787C10.832 1.64985 10.2662 1.08398 9.56814 1.08398H7.94314C6.84624 1.08398 5.95703 1.9732 5.95703 3.0701C5.95703 3.76812 6.52289 4.33398 7.22092 4.33398H8.84592C9.94282 4.33398 10.832 3.44477 10.832 2.34787ZM9.56814 2.16732C9.66786 2.16732 9.7487 2.24816 9.7487 2.34787C9.7487 2.84646 9.34451 3.25065 8.84592 3.25065H7.22092C7.1212 3.25065 7.04036 3.16981 7.04036 3.0701C7.04036 2.57151 7.44455 2.16732 7.94314 2.16732H9.56814Z"
                                                                fill="white" />
                                                        </svg>
                                                    </span>
                                                    @if ($nav->title == null)
                                                        {{ $nav->title }}
                                                    @else
                                                        {{ $nav->title }}
                                                    @endif
                                                </a>
                                                <div class="menu-dropdown">
                                                    <ul>
                                                        @foreach ($nav->children[0] as $childNav)
                                                            @if ($childNav->type == 'custom')
                                                                <li><a href="{{ url($childNav->slug) }}" target="{{ $childNav->target }}">
                                                                    @if ($childNav->title == null)
                                                                        {{ $childNav->title }}
                                                                    @else
                                                                        {{ $childNav->title }}
                                                                    @endif
                                                                </a></li>
                                                            @elseif($childNav->type == 'category')
                                                                <li><a href="{{ route('themes.page', [$currentTheme, $childNav->slug]) }}" target="{{ $childNav->target }}">
                                                                    @if ($childNav->title == null)
                                                                        {{ $childNav->title }}
                                                                    @else
                                                                        {{ $childNav->title }}
                                                                    @endif
                                                                </a></li>
                                                            @else
                                                                <li><a href="{{ route('themes.page', [$currentTheme, $childNav->slug]) }}" target="{{ $childNav->target }}">
                                                                    @if ($childNav->title == null)
                                                                        {{ $childNav->title }}
                                                                    @else
                                                                        {{ $childNav->title }}
                                                                    @endif
                                                                </a></li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            </li>
                                        @else
                                            @if ($nav->type == 'custom')
                                                <li class="">
                                                    <a href="{{ url($nav->slug) }}" target="{{ $nav->target }}">
                                                        <span class="link-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                                                                viewBox="0 0 12 12" fill="none">
                                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                                    d="M4.56302 1.77712L1.28211 7.74242C0.74609 8.717 0.965747 9.90911 2.01111 10.289C2.88666 10.6072 4.14849 10.8874 5.87734 10.8874C7.6062 10.8874 8.86803 10.6072 9.74358 10.289C10.7889 9.90911 11.0086 8.717 10.4726 7.74242L7.19167 1.77712C6.62178 0.740959 5.13291 0.74096 4.56302 1.77712ZM2.63113 7.36469L5.43924 2.25904C5.6292 1.91365 6.12549 1.91365 6.31545 2.25904L9.12353 7.36463C9.08275 7.38595 9.03729 7.40802 8.98671 7.4305C8.50912 7.64275 7.58466 7.8874 5.87727 7.8874C4.16989 7.8874 3.24543 7.64275 2.76784 7.4305C2.71731 7.40804 2.67188 7.38599 2.63113 7.36469ZM9.6054 8.24103C9.54015 8.27572 9.46944 8.31026 9.39284 8.34431C8.74543 8.63205 7.66989 8.8874 5.87727 8.8874C4.08466 8.8874 3.00912 8.63205 2.36171 8.34431C2.28515 8.31028 2.21448 8.27575 2.14925 8.24109C1.99556 8.5295 1.97529 8.80525 2.02288 8.99278C2.06462 9.1573 2.15842 9.27857 2.35268 9.34917C3.11297 9.62548 4.25867 9.88744 5.87734 9.88744C7.49602 9.88744 8.64172 9.62548 9.40201 9.34917C9.59627 9.27857 9.69007 9.1573 9.73181 8.99278C9.7794 8.80524 9.75913 8.52947 9.6054 8.24103Z"
                                                                    fill="white" />
                                                                <path
                                                                    d="M8.39022 5.92066L8.87339 6.79915C8.42112 7.01872 7.90166 7.06027 7.4188 6.91362C6.91125 6.75947 6.48574 6.41002 6.23585 5.94214C5.98596 5.47426 5.93217 4.92627 6.08632 4.41873C6.22818 3.95164 6.53545 3.55402 6.94822 3.29883L7.43139 4.17732C7.24654 4.30509 7.1091 4.4922 7.04316 4.70933C6.96609 4.9631 6.99298 5.23709 7.11793 5.47104C7.24287 5.70498 7.45563 5.8797 7.7094 5.95678C7.93445 6.02513 8.1754 6.01171 8.39022 5.92066Z"
                                                                    fill="white" />
                                                                <path
                                                                    d="M5.5 6.5C5.5 6.77614 5.27614 7 5 7C4.72386 7 4.5 6.77614 4.5 6.5C4.5 6.22386 4.72386 6 5 6C5.27614 6 5.5 6.22386 5.5 6.5Z"
                                                                    fill="white" />
                                                            </svg>
                                                        </span>
                                                        @if ($nav->title == null)
                                                            {{ $nav->title }}
                                                        @else
                                                            {{ $nav->title }}
                                                        @endif
                                                    </a>
                                                </li>
                                            @elseif($nav->type == 'category')
                                                <li class="">
                                                    <a href="{{ route('themes.page', [$currentTheme, $nav->slug]) }}" target="{{ $nav->target }}" target="{{ $nav->target }}">
                                                        <span class="link-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                                                                viewBox="0 0 12 12" fill="none">
                                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                                    d="M4.56302 1.77712L1.28211 7.74242C0.74609 8.717 0.965747 9.90911 2.01111 10.289C2.88666 10.6072 4.14849 10.8874 5.87734 10.8874C7.6062 10.8874 8.86803 10.6072 9.74358 10.289C10.7889 9.90911 11.0086 8.717 10.4726 7.74242L7.19167 1.77712C6.62178 0.740959 5.13291 0.74096 4.56302 1.77712ZM2.63113 7.36469L5.43924 2.25904C5.6292 1.91365 6.12549 1.91365 6.31545 2.25904L9.12353 7.36463C9.08275 7.38595 9.03729 7.40802 8.98671 7.4305C8.50912 7.64275 7.58466 7.8874 5.87727 7.8874C4.16989 7.8874 3.24543 7.64275 2.76784 7.4305C2.71731 7.40804 2.67188 7.38599 2.63113 7.36469ZM9.6054 8.24103C9.54015 8.27572 9.46944 8.31026 9.39284 8.34431C8.74543 8.63205 7.66989 8.8874 5.87727 8.8874C4.08466 8.8874 3.00912 8.63205 2.36171 8.34431C2.28515 8.31028 2.21448 8.27575 2.14925 8.24109C1.99556 8.5295 1.97529 8.80525 2.02288 8.99278C2.06462 9.1573 2.15842 9.27857 2.35268 9.34917C3.11297 9.62548 4.25867 9.88744 5.87734 9.88744C7.49602 9.88744 8.64172 9.62548 9.40201 9.34917C9.59627 9.27857 9.69007 9.1573 9.73181 8.99278C9.7794 8.80524 9.75913 8.52947 9.6054 8.24103Z"
                                                                    fill="white" />
                                                                <path
                                                                    d="M8.39022 5.92066L8.87339 6.79915C8.42112 7.01872 7.90166 7.06027 7.4188 6.91362C6.91125 6.75947 6.48574 6.41002 6.23585 5.94214C5.98596 5.47426 5.93217 4.92627 6.08632 4.41873C6.22818 3.95164 6.53545 3.55402 6.94822 3.29883L7.43139 4.17732C7.24654 4.30509 7.1091 4.4922 7.04316 4.70933C6.96609 4.9631 6.99298 5.23709 7.11793 5.47104C7.24287 5.70498 7.45563 5.8797 7.7094 5.95678C7.93445 6.02513 8.1754 6.01171 8.39022 5.92066Z"
                                                                    fill="white" />
                                                                <path
                                                                    d="M5.5 6.5C5.5 6.77614 5.27614 7 5 7C4.72386 7 4.5 6.77614 4.5 6.5C4.5 6.22386 4.72386 6 5 6C5.27614 6 5.5 6.22386 5.5 6.5Z"
                                                                    fill="white" />
                                                            </svg>
                                                        </span>
                                                        @if ($nav->title == null)
                                                            {{ $nav->title }}
                                                        @else
                                                            {{ $nav->title }}
                                                        @endif
                                                    </a>
                                                </li>
                                            @else
                                                <li class="">
                                                    <a href="{{ route('themes.page', [$currentTheme, $nav->slug]) }}"
                                                        target="{{ $nav->target }}">
                                                        <span class="link-icon">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12"
                                                                viewBox="0 0 12 12" fill="none">
                                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                                    d="M4.56302 1.77712L1.28211 7.74242C0.74609 8.717 0.965747 9.90911 2.01111 10.289C2.88666 10.6072 4.14849 10.8874 5.87734 10.8874C7.6062 10.8874 8.86803 10.6072 9.74358 10.289C10.7889 9.90911 11.0086 8.717 10.4726 7.74242L7.19167 1.77712C6.62178 0.740959 5.13291 0.74096 4.56302 1.77712ZM2.63113 7.36469L5.43924 2.25904C5.6292 1.91365 6.12549 1.91365 6.31545 2.25904L9.12353 7.36463C9.08275 7.38595 9.03729 7.40802 8.98671 7.4305C8.50912 7.64275 7.58466 7.8874 5.87727 7.8874C4.16989 7.8874 3.24543 7.64275 2.76784 7.4305C2.71731 7.40804 2.67188 7.38599 2.63113 7.36469ZM9.6054 8.24103C9.54015 8.27572 9.46944 8.31026 9.39284 8.34431C8.74543 8.63205 7.66989 8.8874 5.87727 8.8874C4.08466 8.8874 3.00912 8.63205 2.36171 8.34431C2.28515 8.31028 2.21448 8.27575 2.14925 8.24109C1.99556 8.5295 1.97529 8.80525 2.02288 8.99278C2.06462 9.1573 2.15842 9.27857 2.35268 9.34917C3.11297 9.62548 4.25867 9.88744 5.87734 9.88744C7.49602 9.88744 8.64172 9.62548 9.40201 9.34917C9.59627 9.27857 9.69007 9.1573 9.73181 8.99278C9.7794 8.80524 9.75913 8.52947 9.6054 8.24103Z"
                                                                    fill="white" />
                                                                <path
                                                                    d="M8.39022 5.92066L8.87339 6.79915C8.42112 7.01872 7.90166 7.06027 7.4188 6.91362C6.91125 6.75947 6.48574 6.41002 6.23585 5.94214C5.98596 5.47426 5.93217 4.92627 6.08632 4.41873C6.22818 3.95164 6.53545 3.55402 6.94822 3.29883L7.43139 4.17732C7.24654 4.30509 7.1091 4.4922 7.04316 4.70933C6.96609 4.9631 6.99298 5.23709 7.11793 5.47104C7.24287 5.70498 7.45563 5.8797 7.7094 5.95678C7.93445 6.02513 8.1754 6.01171 8.39022 5.92066Z"
                                                                    fill="white" />
                                                                <path
                                                                    d="M5.5 6.5C5.5 6.77614 5.27614 7 5 7C4.72386 7 4.5 6.77614 4.5 6.5C4.5 6.22386 4.72386 6 5 6C5.27614 6 5.5 6.22386 5.5 6.5Z"
                                                                    fill="white" />
                                                            </svg>
                                                        </span>
                                                        @if ($nav->title == null)
                                                            {{ $nav->title }}
                                                        @else
                                                            {{ $nav->title }}
                                                        @endif
                                                    </a>
                                                </li>
                                            @endif
                                        @endif
                                    @endforeach
                                @endif
                                <li class="menu-lnk">
                                    <a href="{{ route('page.product-list',$slug) }}"> {{ __('Shop All') }} </a>
                                </li>
                                <li class="menu-lnk has-item">
                                    <a href="#">
                                        {{ __('Pages') }}
                                    </a>
                                    <div class="menu-dropdown">
                                        <ul>
                                            @foreach ($pages as $page)
                                                <li><a
                                                        href="{{ route('custom.page', [$slug,$page->page_slug]) }}">{{ $page->name }}</a>
                                                </li>
                                            @endforeach
                                            <li><a href="{{ route('page.faq',$slug) }}"> {{ __('FAQs') }} </a></li>
                                            <li><a href="{{ route('page.blog',$slug) }}"> {{ __('Blog') }} </a></li>
                                            <li><a href="{{ route('page.product-list',$slug) }}"> {{ __('Collection') }} </a>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li class="menu-lnk">
                                    <a href="#">
                                        {{ __('Contact') }}
                                    </a>
                                </li>
                            </ul>

                        </div>
                    </div>
                    <div class="col-md-6 col-10">
                        <ul class="menu-right d-flex justify-content-end">
                            @auth('customers')
                                <li>
                                    <a href="javascript:;" title="wish" class="wish-header">
                                        <svg width="15" height="12" viewBox="0 0 15 12">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M7.88653 2.64206C7.62555 2.89251 7.21347 2.89251 6.95248 2.64206L6.48546 2.19388C5.93882 1.6693 5.20036 1.349 4.38425 1.349C2.70793 1.349 1.349 2.70793 1.349 4.38425C1.349 5.99134 2.21896 7.31837 3.47486 8.4087C4.73184 9.49996 6.23468 10.2237 7.13261 10.5931C7.32035 10.6703 7.51866 10.6703 7.70641 10.5931C8.60433 10.2237 10.1072 9.49996 11.3642 8.4087C12.6201 7.31836 13.49 5.99133 13.49 4.38425C13.49 2.70793 12.1311 1.349 10.4548 1.349C9.63866 1.349 8.90019 1.6693 8.35356 2.19388L7.88653 2.64206ZM7.41951 1.22056C6.63176 0.464599 5.56226 0 4.38425 0C1.9629 0 0 1.9629 0 4.38425C0 8.67965 4.7015 11.0517 6.61941 11.8406C7.13592 12.0531 7.70309 12.0531 8.21961 11.8406C10.1375 11.0517 14.839 8.67964 14.839 4.38425C14.839 1.9629 12.8761 0 10.4548 0C9.27675 0 8.20725 0.464599 7.41951 1.22056Z">
                                            </path>
                                        </svg>
                                        <span class="desk-only icon-lable">{{ __('WISHLIST') }}</span>
                                    </a>
                                </li>
                            @endauth
                            <li class="search-header">
                                <a href="javascript:;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19"
                                        viewBox="0 0 19 19" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M0.000169754 6.99457C0.000169754 10.8576 3.13174 13.9891 6.99473 13.9891C8.60967 13.9891 10.0968 13.4418 11.2807 12.5226C11.3253 12.6169 11.3866 12.7053 11.4646 12.7834L17.0603 18.379C17.4245 18.7432 18.015 18.7432 18.3792 18.379C18.7434 18.0148 18.7434 17.4243 18.3792 17.0601L12.7835 11.4645C12.7055 11.3864 12.6171 11.3251 12.5228 11.2805C13.442 10.0966 13.9893 8.60951 13.9893 6.99457C13.9893 3.13157 10.8577 0 6.99473 0C3.13174 0 0.000169754 3.13157 0.000169754 6.99457ZM1.86539 6.99457C1.86539 4.1617 4.16187 1.86522 6.99473 1.86522C9.8276 1.86522 12.1241 4.1617 12.1241 6.99457C12.1241 9.82743 9.8276 12.1239 6.99473 12.1239C4.16187 12.1239 1.86539 9.82743 1.86539 6.99457Z"
                                            fill="#183A40" />
                                    </svg>
                                    @auth('customers')
                                        <span class="desk-only icon-lable" style="margin-right: -23px;">
                                            {{ __('Buscar') }} </span>
                                    @endauth
                                    @guest('customers')
                                        <span class="desk-only icon-lable"> {{ __('Buscar') }} </span>
                                    @endguest
                                </a>
                            </li>
                            <li class="profile-nav-menu">
                                <ul class="main-nav top-main-nav profile-nav">
                                    <li class="menu-lnk has-item">
                                        <a href="#">
                                            <span>{{ Str::upper($currantLang) }}</span>
                                        </a>
                                        <div class="menu-dropdown" style="padding: initial;">
                                            <ul>
                                            @foreach ($languages as $code => $language)
                                        <li><a href="{{ route('change.languagestore', [$code]) }}"
                                                class="dropdown-item @if ($language == $currantLang) active-language text-primary @endif" style="color: black; width: fit-content;"
                                                         >{{  ucFirst($language) }}</a>
                                        </li>
                                    @endforeach

                                               
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                            @auth('customers')
                            <li class="profile-nav-menu">
                                <ul class="main-nav top-main-nav profile-nav">
                                    <li class="menu-lnk has-item">
                                        <a href="#">
                                            <span>{{ __('My profile') }}</span>
                                        </a>
                                        <div class="menu-dropdown" style="padding: initial;">
                                            <ul>
                                                <li>
                                                    <a href="{{ route('my-account.index',$slug) }}"
                                                        style="margin-left: 17px; width: fit-content; color: black;">{{ __('My Account') }}</a>
                                                </li>
                                                <li>
                                                    <form method="POST" action="{{ route('customer.logout',$slug) }}"
                                                        id="form_logout">
                                                        @csrf
                                                        <a href="#"
                                                            onclick="event.preventDefault(); this.closest('form').submit();"
                                                            style="color: black; width: fit-content;"
                                                            class="dropdown-item">
                                                            {{ __('Log Out') }}
                                                        </a>
                                                    </form>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            </li>

                            @endauth

                            @guest('customers')
                                <li class="profile-header">
                                    <a href="{{ route('customer.login',$slug) }}">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="22"
                                            viewBox="0 0 16 22" fill="none">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M13.3699 21.0448H4.60183C4.11758 21.0448 3.72502 20.6522 3.72502 20.168C3.72502 19.6837 4.11758 19.2912 4.60183 19.2912H13.3699C13.8542 19.2912 14.2468 18.8986 14.2468 18.4143V14.7756C14.2026 14.2836 13.9075 13.8492 13.4664 13.627C10.0296 12.2394 6.18853 12.2394 2.75176 13.627C2.31062 13.8492 2.01554 14.2836 1.9714 14.7756V20.168C1.9714 20.6522 1.57883 21.0448 1.09459 21.0448C0.610335 21.0448 0.217773 20.6522 0.217773 20.168V14.7756C0.256548 13.5653 0.986136 12.4845 2.09415 11.9961C5.95255 10.4369 10.2656 10.4369 14.124 11.9961C15.232 12.4845 15.9616 13.5653 16.0004 14.7756V18.4143C16.0004 19.8671 14.8227 21.0448 13.3699 21.0448ZM12.493 4.38406C12.493 1.96281 10.5302 0 8.10892 0C5.68767 0 3.72486 1.96281 3.72486 4.38406C3.72486 6.80531 5.68767 8.76812 8.10892 8.76812C10.5302 8.76812 12.493 6.80531 12.493 4.38406ZM10.7393 4.38483C10.7393 5.83758 9.56159 7.01526 8.10884 7.01526C6.6561 7.01526 5.47841 5.83758 5.47841 4.38483C5.47841 2.93208 6.6561 1.75439 8.10884 1.75439C9.56159 1.75439 10.7393 2.93208 10.7393 4.38483Z"
                                                fill="#183A40" />
                                        </svg>
                                        <span class="desk-only icon-lable">{{ __('Login') }}</span>
                                    </a>
                                </li>
                            @endguest
                            <li class="cart-header">
                                 <a href="javascript:;">
                                     <svg xmlns="http://www.w3.org/2000/svg" width="19" height="17"
                                         viewBox="0 0 19 17" fill="none">
                                         <path fill-rule="evenodd" clip-rule="evenodd"
                                             d="M15.5698 10.627H6.97178C5.80842 10.6273 4.81015 9.79822 4.59686 8.65459L3.47784 2.59252C3.40702 2.20522 3.06646 1.92595 2.67278 1.93238H0.805055C0.360435 1.93238 0 1.57194 0 1.12732C0 0.682701 0.360435 0.322266 0.805055 0.322266H2.68888C3.85224 0.321937 4.85051 1.15101 5.0638 2.29465L6.18282 8.35672C6.25364 8.74402 6.5942 9.02328 6.98788 9.01686H15.5778C15.9715 9.02328 16.3121 8.74402 16.3829 8.35672L17.3972 2.88234C17.4407 2.64509 17.3755 2.40085 17.2195 2.21684C17.0636 2.03283 16.8334 1.92843 16.5922 1.93238H7.2455C6.80088 1.93238 6.44044 1.57194 6.44044 1.12732C6.44044 0.682701 6.80088 0.322266 7.2455 0.322266H16.5841C17.3023 0.322063 17.9833 0.641494 18.4423 1.19385C18.9013 1.74622 19.0907 2.4742 18.959 3.18021L17.9447 8.65459C17.7314 9.79822 16.7331 10.6273 15.5698 10.627ZM10.466 13.8478C10.466 12.5139 9.38464 11.4326 8.05079 11.4326C7.60617 11.4326 7.24573 11.7931 7.24573 12.2377C7.24573 12.6823 7.60617 13.0427 8.05079 13.0427C8.49541 13.0427 8.85584 13.4032 8.85584 13.8478C8.85584 14.2924 8.49541 14.6528 8.05079 14.6528C7.60617 14.6528 7.24573 14.2924 7.24573 13.8478C7.24573 13.4032 6.88529 13.0427 6.44068 13.0427C5.99606 13.0427 5.63562 13.4032 5.63562 13.8478C5.63562 15.1816 6.71693 16.2629 8.05079 16.2629C9.38464 16.2629 10.466 15.1816 10.466 13.8478ZM15.2963 15.4579C15.2963 15.0133 14.9358 14.6528 14.4912 14.6528C14.0466 14.6528 13.6862 14.2924 13.6862 13.8478C13.6862 13.4032 14.0466 13.0427 14.4912 13.0427C14.9358 13.0427 15.2963 13.4032 15.2963 13.8478C15.2963 14.2924 15.6567 14.6528 16.1013 14.6528C16.5459 14.6528 16.9064 14.2924 16.9064 13.8478C16.9064 12.5139 15.8251 11.4326 14.4912 11.4326C13.1574 11.4326 12.076 12.5139 12.076 13.8478C12.076 15.1816 13.1574 16.2629 14.4912 16.2629C14.9358 16.2629 15.2963 15.9025 15.2963 15.4579Z"
                                             fill="white" />
                                     </svg>
                                     <span class="desk-only icon-lable">{{ __('Cart') }}</span>
                                     <span class="desk-only" style="margin-left: auto;">(</span>
                                     <span class="count" style="margin-left: auto;">
                                         {{ \App\Models\Cart::CartCount($slug) }} </span>
                                     <span class="desk-only" style="margin-left: auto;">)</span>
                                 </a>
                             </li>
                            <li class="mobile-menu mobile-only">
                                <button class="mobile-menu-button" id="menu">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="45" height="22"
                                        viewBox="0 0 45 22" fill="none">
                                        <line y1="1" x2="45" y2="1" stroke="white"
                                            stroke-width="2" />
                                        <line y1="11" x2="45" y2="11" stroke="white"
                                            stroke-width="2" />
                                        <line y1="21" x2="45" y2="21" stroke="white"
                                            stroke-width="2" />
                                    </svg>
                                </button>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="search-popup">
        <div class="close-search">
            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"
                fill="none">
                <path
                    d="M27.7618 25.0008L49.4275 3.33503C50.1903 2.57224 50.1903 1.33552 49.4275 0.572826C48.6647 -0.189868 47.428 -0.189965 46.6653 0.572826L24.9995 22.2386L3.33381 0.572826C2.57102 -0.189965 1.3343 -0.189965 0.571605 0.572826C-0.191089 1.33562 -0.191186 2.57233 0.571605 3.33503L22.2373 25.0007L0.571605 46.6665C-0.191186 47.4293 -0.191186 48.666 0.571605 49.4287C0.952952 49.81 1.45285 50.0007 1.95275 50.0007C2.45266 50.0007 2.95246 49.81 3.3339 49.4287L24.9995 27.763L46.6652 49.4287C47.0465 49.81 47.5464 50.0007 48.0463 50.0007C48.5462 50.0007 49.046 49.81 49.4275 49.4287C50.1903 48.6659 50.1903 47.4292 49.4275 46.6665L27.7618 25.0008Z"
                    fill="white"></path>
            </svg>
        </div>
        <div class="search-form-wrapper">
            <form>
                <div class="form-inputs">
                    <input type="search" placeholder="Buscar Producto..." class="form-control search_input"
                        list="products" name="search_product" id="product">
                    <datalist id="products">
                        {{-- @foreach ($search_products as $pro_id => $pros)
                            <option value="{{ $pros }}"></option>
                        @endforeach --}}
                    </datalist>

                    <button type="submit" class="btn search_product_globaly">
                        <svg>
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M0.000169754 6.99457C0.000169754 10.8576 3.13174 13.9891 6.99473 13.9891C8.60967 13.9891 10.0968 13.4418 11.2807 12.5226C11.3253 12.6169 11.3866 12.7053 11.4646 12.7834L17.0603 18.379C17.4245 18.7432 18.015 18.7432 18.3792 18.379C18.7434 18.0148 18.7434 17.4243 18.3792 17.0601L12.7835 11.4645C12.7055 11.3864 12.6171 11.3251 12.5228 11.2805C13.442 10.0966 13.9893 8.60951 13.9893 6.99457C13.9893 3.13157 10.8577 0 6.99473 0C3.13174 0 0.000169754 3.13157 0.000169754 6.99457ZM1.86539 6.99457C1.86539 4.1617 4.16187 1.86522 6.99473 1.86522C9.8276 1.86522 12.1241 4.1617 12.1241 6.99457C12.1241 9.82743 9.8276 12.1239 6.99473 12.1239C4.16187 12.1239 1.86539 9.82743 1.86539 6.99457Z">
                            </path>
                        </svg>
                    </button>
                </div>
            </form>
        </div>
    </div>
</header>
<!--header end here-->