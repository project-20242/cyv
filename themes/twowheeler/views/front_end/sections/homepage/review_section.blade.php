<section style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="testimonials-section padding-bottom">
        <div class="container">
        <div class="section-title d-flex justify-content-between align-items-center">
             <h2 id="{{ ($section->review->section->title->slug ?? '') }}_preview">{!!
                $section->review->section->title->text ?? '' !!}</h2>
        </div>
            <div class="testi-main-slider">
                @foreach($reviews as $review)
                <div class="testi-slides">
                    <div class="testi-inner">
                        <div class="img-wrapper">
                            <a href="{{route('page.product',[$slug,$review->id])}}">
                                <img src="{{ asset(!empty($review->ProductData) ? $review->ProductData->cover_image_path : '' ) }}"
                                    alt="testimonial-product">
                            </a>
                        </div>
                        <div class="ratings d-flex align-items-center">
                            @for ($i = 0; $i < 5; $i++) <i
                                class="fa fa-star {{ $i < $review->rating_no ? 'text-warning' : '' }}"></i>
                                @endfor
                                <span><b>{{$review->rating_no}}.0</b> / 5.0</span>
                        </div>
                        <h3>{!! $review->title !!}</h3>
                        <p>{{$review->description}}</p>
                        {{-- <h6>WSR-690 Superbike</h6> --}}
                        <span class="user-name"><a
                                href="#">{{!empty($review->UserData->first_name) ? $review->UserData->first_name : '' }}</a>
                            Cliente </span>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</section>