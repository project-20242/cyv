@extends('front_end.layouts.app')
@section('page-title')
    {{ __('Blog Page') }}
@endsection
@section('content')
    @include('front_end.sections.partision.header_section')
    <section class="blog-page-banner common-banner-section"
            style="background-image:url({{ asset('themes/' . $currentTheme . '/assets/images/blog-banner.png') }});">
            <div class="container">
               
                <div class="row">
                    <div class="col-md-6 col-12">
                        <div class="common-banner-content">
                            <ul class="blog-cat">
                                <li class="active"><a href="#"> {{ __('Destacado') }} </a></li>
                            </ul>
                            <div class="section-title">
                                <h2>{{ __('Blog & Articles') }}  </h2>
                            </div>
                            <p>{{ __('The blog and article section serves as a treasure trove of valuable information.') }} </p>
                            <a href="#" class="btn">
                                <span class="btn-txt">{{ __('Read More') }}</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="blog-grid-section padding-top padding-bottom tabs-wrapper">
            <div class="container">
                
                <div class="section-title d-flex justify-content-between align-items-end">
                    <div>
                        <div class="subtitle">{{ __('ALL BLOGS') }}</div>
                        <h2>{{ __('From our blog') }} </h2>
                    </div>
                    <a href="#" class="btn">
                        {{ __('Read More') }}
                    </a>
                </div>
                <div class="blog-head-row d-flex justify-content-between">
                    <div class="blog-col-left">
                        <ul class="d-flex tabs">
                            @foreach ($BlogCategory as $cat_key =>  $category)
                                <li class="tab-link on-tab-click {{$cat_key == 0 ? 'active' : ''}}" data-tab="{{ $cat_key }}">
                                   <a href="javascript:;">{{ __('All Products') }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="blog-col-right d-flex align-items-center justify-content-end">
                        <span class="select-lbl"> {{ __('Sort by') }} </span>
                        <select class="position">
                            <option value="lastest"> {{ __('Últimos') }} </option>
                            <option value="new"> {{ __('new') }} </option>
                        </select>
                    </div>
                </div>
                <div class="tabs-container">
                    @foreach ($BlogCategory as $cat_k => $category)
                    <div id="{{ $cat_k }}" class="tab-content tab-cat-id {{$cat_k == 0 ? 'active' : ''}}">
                        <div class="blog-grid-row row f_blog">
                        @foreach ($blogs as $key => $blog)
                            @if($cat_k == '0' || $blog->category_id == $cat_k)

                                <div class="col-lg-3 col-md-4 col-sm-6 col-12 blog-card">
                                    <div class="blog-card-inner">
                                        <div class="blog-card-image">
                                            <span class="label">{{$blog->category->name}}</span>
                                            <a href="{{route('page.article',[$slug,$blog->id])}}" tabindex="0">
                                                <img src="{{ asset($blog->cover_image_path) }}" class="default-img" width="120"
                                                    class="cover_img{{ $blog->id }}">
                                            </a>
                                        </div>
                                        <div class="blog-card-content">
                                            <div class="blog-card-content-top">
                                                <h3>
                                                    <a href="{{route('page.article',[$slug,$blog->id])}}" tabindex="0" class="short-description">
                                                        {!! $blog->title !!}</b>
                                                    </a>
                                                </h3>
                                                <p class="description">{{$blog->short_description}}</p>
                                            </div>
                                            <div class="blog-card-content-bottom">
                                                <div class="blog-card-author-name">
                                                    <span>@johndoe</span>
                                                    <span>{{ $blog->created_at->format('d M,Y ') }}</span>
                                                </div>
                                                <a href="{{route('page.article',[$slug,$blog->id])}}" class="btn">
                                                    {{ __('View blog')}}
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach

                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </section>

    @include('front_end.sections.partision.footer_section')
@endsection

