<section class="best-product  @if(request()->route()->getName() == 'landing_page') padding-bottom @elseif (request()->route()->getName() != 'landing_page') padding-top @endif" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="section-title d-flex align-items-center justify-content-between">
            <h2 id="{{ $section->best_product->section->title->slug ?? '' }}_preview"> {!!
                                    $section->best_product->section->title->text ?? '' !!}</h2>
            <a href="{{ route('page.product-list',$slug) }}" class="btn red-btn" id="{{ $section->best_product->section->button->slug ?? '' }}_preview"> {!!
                                     $section->best_product->section->button->text ?? '' !!}
                <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6" viewBox="0 0 4 6" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                        fill="white"></path>
                </svg>
            </a>
        </div>
        <div class="row">
            @foreach ($MainCategoryList->take(1) as $category)
            <div class="col-lg-3 col-md-4 col-sm-6 col-12 category-widget">
                <div class="category-widget-inner second-style">
                    <div class="category-img">
                        <img src="{{ asset($category->image_path) }}" loading="lazy">
                    </div>
                    <div class="category-card-body">
                        <div class="title-wrapper">
                            <h3 class="title">
                                <a href="{{ route('page.product-list', [$slug,'main_category' => $category->id]) }}">{!!
                                    $category->name !!}</a>
                            </h3>
                        </div>
                        <div class="btn-wrapper">
                            <a href="{{ route('page.product-list',$slug) }}" class="btn red-btn"id="{{ $section->best_product->section->button_left->slug ?? '' }}_preview"> {!!
                                     $section->best_product->section->button_left->text ?? '' !!}
                                <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6" viewBox="0 0 4 6"
                                    fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                        fill="white"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="col-lg-9 col-md-8 col-sm-6 col-12">
                <div class="best-product-slider">
                    @foreach ($home_products as $home_product)
                        <div class="product-card">
                            <div class="product-card-inner">
                                <div class="product-card-image">
                                    <a href="{{ route('page.product', [$slug,$home_product->slug]) }}">
                                        <img src="{{ asset($home_product->cover_image_path) }}"
                                            class="default-img">
                                        <div class="new-labl  danger">
                                            {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $home_product->id) !!}
                                        </div>
                                    </a>
                                </div>
                                <div class="product-content">
                                    <div class="product-content-top d-flex align-items-end">
                                        <div class="product-content-left">
                                            <div class="product-subtitle">{{ $home_product->ProductData->name ?? '' }}</div>
                                            <h3 class="product-title">
                                                <a href="{{ route('page.product', [$slug,$home_product->slug]) }}" class="short_description">
                                                    {{ $home_product->name }}
                                                </a>
                                            </h3>
                                            <div class="reviews-stars-wrap d-flex align-items-center">
                                                @if (!empty($home_product->average_rating))
                                                    @for ($i = 0; $i < 5; $i++)
                                                        <i class="fa fa-star {{ $i < $home_product->average_rating ? '' : 'text-warning' }} ">
                                                        </i>
                                                    @endfor
                                                <span><b>{{ $home_product->average_rating }}.0</b> / 5.0</span>
                                                @else
                                                    @for ($i = 0; $i < 5; $i++)
                                                        <i class="fa fa-star {{ $i < $home_product->average_rating ? '' : 'text-warning' }} ">
                                                        </i>
                                                    @endfor
                                                <span><b>{{ $home_product->average_rating }}.0</b> / 5.0</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="product-content-center">
                                        @if ($home_product->variant_product == 0)
                                            <div class="price">
                                                <ins>{{ currency_format_with_sym(($home_product->sale_price ?? $home_product->price), $store->id, $currentTheme)}} </ins>
                                            </div>
                                        @else
                                            <div class="price">
                                                <ins>{{ __('In Variant') }}</ins>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="product-content-bottom d-flex align-items-center justify-content-between">
                                        <button class="btn addcart-btn-globaly" product_id="{{ $home_product->id }}" variant_id="0" qty="1">
                                            <span>{{ __('Add to cart') }} </span>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6"
                                                viewBox="0 0 4 6" fill="none">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                                    fill="white"></path>
                                            </svg>
                                        </button>
                                            <a href="javascript:void(0)"
                                                class="wishlist-btn wbwish  wishbtn-globaly" product_id="{{ $home_product->id }}" in_wishlist="{{ $home_product->in_whishlist ? 'remove' : 'add' }}">
                                                <span class="wish-ic">
                                                    <i class="{{ $home_product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"
                                                        style='color: #000000'></i>
                                                </span>
                                            </a>
                                            <div class="product-btn-wrp">
                                                @php
                                                    $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                    $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                                @endphp
                                                @if(isset($module) && $module->isEnabled())
                                                    {{-- Include the module blade button --}}
                                                    @include('productquickview::pages.button', ['product_slug' => $home_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                @endif
                                                @if(isset($compare_module) && $compare_module->isEnabled())
                                                    {{-- Include the module blade button --}}
                                                    @include('productcompare::pages.button', ['product_slug' => $home_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                @endif
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
