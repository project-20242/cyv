@if (\Route::current()->getName() != 'landing_page')
{{-- site-header header-style-one header-style-two home-header --}}
<header class="site-header header-style-one header-style-three home-header"
    style="@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    @else
    <header class="site-header header-style-one home-header"
        style="@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
        data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}"
        data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}"
        data-section="{{ $option->section_name ?? '' }}" data-store="{{ $option->store_id ?? '' }}"
        data-theme="{{ $option->theme_id ?? '' }}">
        @endif
        <div class="custome_tool_bar"></div>
        <div class="announce-bar">
            <div class="container">
                <div class="announcebar-inner">
                    <div class="row align-items-center">
                        <div class="col-md-6 col-12">
                            <p>
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="12" viewBox="0 0 12 12"
                                    fill="none">
                                    <g clip-path="url(#clip0_1_508)">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M11.8704 1.16417C12.0074 1.31521 12.0389 1.53425 11.9501 1.71777L7.59593 10.7178C7.52579 10.8628 7.3901 10.9651 7.23141 10.9926C7.07272 11.0202 6.91047 10.9696 6.79555 10.8568L5.06335 9.1561L3.34961 10.6517C3.2065 10.7766 3.00497 10.8096 2.82949 10.737C2.654 10.6643 2.53486 10.4984 2.52199 10.3089L2.31069 7.19698L0.246224 5.98083C0.0797703 5.88277 -0.0153014 5.6976 0.00202164 5.50519C0.0193447 5.31278 0.145963 5.14756 0.327254 5.08081L11.3273 1.03081C11.5186 0.960374 11.7335 1.01314 11.8704 1.16417ZM3.34923 7.73073L3.45141 9.23561L4.34766 8.45343L3.87393 7.98831L3.34923 7.73073ZM4.91243 7.60651L6.9991 9.65524L10.0489 3.35145L4.91243 7.60651ZM8.93151 2.97851L1.66281 5.65471L3.02908 6.45956L4.09673 6.98368L8.93151 2.97851Z"
                                            fill="black" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_1_508">
                                            <rect width="12" height="12" fill="white" />
                                        </clipPath>
                                    </defs>
                                </svg>
                                {!! $section->header->section->title->text ?? '' !!}
                            </p>
                        </div>
                        <div class="col-md-6 col-12">
                            <ul class="d-flex align-items-center justify-content-end">
                                <li>
                                    <a href="tel:1234567890">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14"
                                            viewBox="0 0 14 14" fill="none">
                                            <g clip-path="url(#clip0_1_562)">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M2.71108 1.3729C3.46413 0.619858 4.70955 0.708369 5.34852 1.56034L6.40918 2.97455C6.93171 3.67126 6.86243 4.64618 6.24662 5.26199L5.46462 6.04398C5.5404 6.24124 5.78989 6.67412 6.54962 7.43386C7.30935 8.19359 7.74224 8.44307 7.9395 8.51885L8.72149 7.73686C9.3373 7.12105 10.3122 7.05176 11.0089 7.5743L12.4231 8.63496C13.2751 9.27393 13.3636 10.5194 12.6106 11.2724C12.3658 11.5172 12.3243 11.5586 11.9158 11.9672C11.4994 12.3836 10.6152 12.7728 9.72011 12.8117C8.31934 12.8726 6.41664 12.2506 4.07475 9.90873C1.73285 7.56684 1.11087 5.66414 1.17177 4.26337C1.20565 3.48415 1.45339 2.62832 2.01953 2.07097C2.42483 1.65915 2.47754 1.60644 2.71108 1.3729ZM2.33734 4.31404C2.29512 5.28503 2.70433 6.8884 4.89971 9.08377C7.09508 11.2791 8.69845 11.6884 9.66944 11.6461C10.5759 11.6067 11.0621 11.1688 11.0908 11.1423L11.7856 10.4474C12.0366 10.1964 12.0071 9.78128 11.7231 9.56829L10.3089 8.50763C10.0767 8.33345 9.75172 8.35655 9.54645 8.56182C9.23717 8.87109 9.02606 9.08534 8.6128 9.49703C7.75435 10.3522 6.28489 8.81904 5.72466 8.25882C5.20982 7.74398 3.64086 6.22786 4.48561 5.37309C4.48728 5.37141 4.71481 5.14388 5.42166 4.43703C5.62693 4.23176 5.65002 3.90679 5.47585 3.67455L4.41519 2.26034C4.2022 1.97635 3.78706 1.94685 3.53604 2.19786C3.30501 2.42889 3.04355 2.69035 2.84193 2.89337C2.42696 3.3112 2.35954 3.80328 2.33734 4.31404Z"
                                                    fill="#12131A" />
                                            </g>
                                            <defs>
                                                <clipPath id="clip0_1_562">
                                                    <rect width="14" height="14" fill="white" />
                                                </clipPath>
                                            </defs>
                                        </svg>
                                        {!! $section->header->section->support_value->text ?? '' !!}
                                    </a>
                                </li>
                                {{-- <li>
                                <a href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M7.5 8.125C5.08375 8.125 3.125 10.0838 3.125 12.5V13.75C3.125 14.0952 2.84518 14.375 2.5 14.375C2.15482 14.375 1.875 14.0952 1.875 13.75V12.5C1.875 9.3934 4.3934 6.875 7.5 6.875C10.6066 6.875 13.125 9.3934 13.125 12.5V13.75C13.125 14.0952 12.8452 14.375 12.5 14.375C12.1548 14.375 11.875 14.0952 11.875 13.75V12.5C11.875 10.0838 9.91625 8.125 7.5 8.125Z" fill="#12131A"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M7.5 6.875C8.88071 6.875 10 5.75571 10 4.375C10 2.99429 8.88071 1.875 7.5 1.875C6.11929 1.875 5 2.99429 5 4.375C5 5.75571 6.11929 6.875 7.5 6.875ZM7.5 8.125C9.57107 8.125 11.25 6.44607 11.25 4.375C11.25 2.30393 9.57107 0.625 7.5 0.625C5.42893 0.625 3.75 2.30393 3.75 4.375C3.75 6.44607 5.42893 8.125 7.5 8.125Z" fill="#12131A"/>
                                    </svg>
                                    Log In
                                </a>
                            </li> --}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-navigationbar">
            <div class="container">
                <div class="navigationbar-row d-flex align-items-center">
                    <div class="logo-col">
                        <h1>
                            <a href="{{ route('landing_page', $slug) }}">
                                <img src="{{ asset((isset($theme_logo) && !empty($theme_logo)) ? $theme_logo : 'themes/'.$currentTheme.'/assets/img/logo.svg') }}"
                                    alt="Nav brand">
                            </a>
                        </h1>
                    </div>
                    <div class="menu-items-col">
                        <ul class="main-nav">
                            @if (!empty($topNavItems))
                            @foreach ($topNavItems as $key => $nav)
                                @if (!empty($nav->children[0]))
                                    <li class="menu-lnk has-item">
                                        <a href="#">
                                            @if ($nav->title == null)
                                                {{ $nav->title }}
                                            @else
                                                {{ $nav->title }}
                                            @endif
                                        </a>
                                        <div class="menu-dropdown">
                                            <ul>
                                                @foreach ($nav->children[0] as $childNav)
                                                    @if ($childNav->type == 'custom')
                                                        <li><a href="{{ url($childNav->slug) }}" target="{{ $childNav->target }}">
                                                            @if ($childNav->title == null)
                                                                {{ $childNav->title }}
                                                            @else
                                                                {{ $childNav->title }}
                                                            @endif
                                                        </a></li>
                                                    @elseif($childNav->type == 'category')
                                                        <li><a href="{{ url($slug.'/'.$childNav->slug) }}" target="{{ $childNav->target }}">
                                                            @if ($childNav->title == null)
                                                                {{ $childNav->title }}
                                                            @else
                                                                {{ $childNav->title }}
                                                            @endif
                                                        </a></li>
                                                    @else
                                                        <li><a href="{{ route('themes.page', [$currentTheme, $childNav->slug]) }}" target="{{ $childNav->target }}">
                                                            @if ($childNav->title == null)
                                                                {{ $childNav->title }}
                                                            @else
                                                                {{ $childNav->title }}
                                                            @endif
                                                        </a></li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                @else
                                    @if ($nav->type == 'custom')
                                        <li class="">
                                            <a href="{{ url($nav->slug) }}" target="{{ $nav->target }}">
                                                @if ($nav->title == null)
                                                    {{ $nav->title }}
                                                @else
                                                    {{ $nav->title }}
                                                @endif
                                            </a>
                                        </li>
                                    @elseif($nav->type == 'category')
                                        <li class="">
                                            <a href="{{  url($slug.'/'.$nav->slug) }}" target="{{ $nav->target }}" target="{{ $nav->target }}">
                                                @if ($nav->title == null)
                                                    {{ $nav->title }}
                                                @else
                                                    {{ $nav->title }}
                                                @endif
                                            </a>
                                        </li>
                                    @else
                                        <li class="">
                                            <a href="{{ route('themes.page', [$currentTheme, $nav->slug]) }}"
                                                target="{{ $nav->target }}">
                                                @if ($nav->title == null)
                                                    {{ $nav->title }}
                                                @else
                                                    {{ $nav->title }}
                                                @endif
                                            </a>
                                        </li>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                            <li class="menu-lnk has-item">
                                <a href="#">
                                    {{ __('Pages') }}
                                </a>
                                <div class="menu-dropdown">
                                    <ul>
                                        <li><a href="{{route('page.faq',['storeSlug' => $slug])}}">{{__('FAQs')}}</a>
                                        </li>
                                        <li><a href="{{route('page.blog',['storeSlug' => $slug])}}">{{__('Blog')}}</a>
                                        </li>
                                        <li><a
                                                href="{{route('page.product-list',['storeSlug' => $slug])}}">{{__('Collection')}}</a>
                                        </li>
                                        <li><a
                                                href="{{route('page.contact_us',['storeSlug' => $slug])}}">{{__('Contact')}}</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <ul class="menu-right d-flex">
                            <li class="search-header">
                                <a href="javascript:;">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19" viewBox="0 0 19 19"
                                        fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M0.000169754 6.99457C0.000169754 10.8576 3.13174 13.9891 6.99473 13.9891C8.60967 13.9891 10.0968 13.4418 11.2807 12.5226C11.3253 12.6169 11.3866 12.7053 11.4646 12.7834L17.0603 18.379C17.4245 18.7432 18.015 18.7432 18.3792 18.379C18.7434 18.0148 18.7434 17.4243 18.3792 17.0601L12.7835 11.4645C12.7055 11.3864 12.6171 11.3251 12.5228 11.2805C13.442 10.0966 13.9893 8.60951 13.9893 6.99457C13.9893 3.13157 10.8577 0 6.99473 0C3.13174 0 0.000169754 3.13157 0.000169754 6.99457ZM1.86539 6.99457C1.86539 4.1617 4.16187 1.86522 6.99473 1.86522C9.8276 1.86522 12.1241 4.1617 12.1241 6.99457C12.1241 9.82743 9.8276 12.1239 6.99473 12.1239C4.16187 12.1239 1.86539 9.82743 1.86539 6.99457Z"
                                            fill="#183A40"></path>
                                    </svg>
                                </a>
                            </li>

                            @auth('customers')
                            <li class="profile-header">
                                <a href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="22" viewBox="0 0 16 22" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.3699 21.0448H4.60183C4.11758 21.0448 3.72502 20.6522 3.72502 20.168C3.72502 19.6837 4.11758 19.2912 4.60183 19.2912H13.3699C13.8542 19.2912 14.2468 18.8986 14.2468 18.4143V14.7756C14.2026 14.2836 13.9075 13.8492 13.4664 13.627C10.0296 12.2394 6.18853 12.2394 2.75176 13.627C2.31062 13.8492 2.01554 14.2836 1.9714 14.7756V20.168C1.9714 20.6522 1.57883 21.0448 1.09459 21.0448C0.610335 21.0448 0.217773 20.6522 0.217773 20.168V14.7756C0.256548 13.5653 0.986136 12.4845 2.09415 11.9961C5.95255 10.4369 10.2656 10.4369 14.124 11.9961C15.232 12.4845 15.9616 13.5653 16.0004 14.7756V18.4143C16.0004 19.8671 14.8227 21.0448 13.3699 21.0448ZM12.493 4.38406C12.493 1.96281 10.5302 0 8.10892 0C5.68767 0 3.72486 1.96281 3.72486 4.38406C3.72486 6.80531 5.68767 8.76812 8.10892 8.76812C10.5302 8.76812 12.493 6.80531 12.493 4.38406ZM10.7393 4.38483C10.7393 5.83758 9.56159 7.01526 8.10884 7.01526C6.6561 7.01526 5.47841 5.83758 5.47841 4.38483C5.47841 2.93208 6.6561 1.75439 8.10884 1.75439C9.56159 1.75439 10.7393 2.93208 10.7393 4.38483Z" fill="#183A40"></path>
                                    </svg>
                                </a>
                                <div class="menu-dropdown">
                                    <ul>
                                        <li class="menu-lnk has-item"><a
                                                href="{{ route('my-account.index',$slug) }}">{{ __('My Account') }}</a>
                                        </li>
                                        <li>
                                            <form method="POST" action="{{ route('customer.logout',$slug) }}"
                                                id="form_logout">
                                                <a href="#"
                                                    onclick="event.preventDefault(); this.closest('form').submit();"
                                                    class="dropdown-item">
                                                    @csrf
                                                    {{ __('Log Out') }}
                                                </a>
                                            </form>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            @stack('addCompareButton')
                            @endauth
                            @guest('customers')
                            <li class="profile-header">
                                <a href="{{ route('customer.login',$slug) }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="22" viewBox="0 0 16 22"
                                        fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M13.3699 21.0448H4.60183C4.11758 21.0448 3.72502 20.6522 3.72502 20.168C3.72502 19.6837 4.11758 19.2912 4.60183 19.2912H13.3699C13.8542 19.2912 14.2468 18.8986 14.2468 18.4143V14.7756C14.2026 14.2836 13.9075 13.8492 13.4664 13.627C10.0296 12.2394 6.18853 12.2394 2.75176 13.627C2.31062 13.8492 2.01554 14.2836 1.9714 14.7756V20.168C1.9714 20.6522 1.57883 21.0448 1.09459 21.0448C0.610335 21.0448 0.217773 20.6522 0.217773 20.168V14.7756C0.256548 13.5653 0.986136 12.4845 2.09415 11.9961C5.95255 10.4369 10.2656 10.4369 14.124 11.9961C15.232 12.4845 15.9616 13.5653 16.0004 14.7756V18.4143C16.0004 19.8671 14.8227 21.0448 13.3699 21.0448ZM12.493 4.38406C12.493 1.96281 10.5302 0 8.10892 0C5.68767 0 3.72486 1.96281 3.72486 4.38406C3.72486 6.80531 5.68767 8.76812 8.10892 8.76812C10.5302 8.76812 12.493 6.80531 12.493 4.38406ZM10.7393 4.38483C10.7393 5.83758 9.56159 7.01526 8.10884 7.01526C6.6561 7.01526 5.47841 5.83758 5.47841 4.38483C5.47841 2.93208 6.6561 1.75439 8.10884 1.75439C9.56159 1.75439 10.7393 2.93208 10.7393 4.38483Z"
                                            fill="#183A40"></path>
                                    </svg>
                                </a>
                            </li>
                            @endguest
                            @auth('customers')
                            <li class="wish-header wishlist-header">
                                <a href="#">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16"
                                        fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M10.6295 3.52275C10.2777 3.85668 9.7223 3.85668 9.37055 3.52275L8.74109 2.92517C8.00434 2.22574 7.00903 1.79867 5.90909 1.79867C3.64974 1.79867 1.81818 3.61057 1.81818 5.84567C1.81818 7.98845 2.99071 9.75782 4.68342 11.2116C6.37756 12.6666 8.40309 13.6316 9.61331 14.1241C9.86636 14.2271 10.1336 14.2271 10.3867 14.1241C11.5969 13.6316 13.6224 12.6666 15.3166 11.2116C17.0093 9.75782 18.1818 7.98845 18.1818 5.84567C18.1818 3.61057 16.3503 1.79867 14.0909 1.79867C12.991 1.79867 11.9957 2.22574 11.2589 2.92517L10.6295 3.52275ZM10 1.62741C8.93828 0.619465 7.49681 0 5.90909 0C2.64559 0 0 2.6172 0 5.84567C0 11.5729 6.33668 14.7356 8.92163 15.7875C9.61779 16.0708 10.3822 16.0708 11.0784 15.7875C13.6633 14.7356 20 11.5729 20 5.84567C20 2.6172 17.3544 0 14.0909 0C12.5032 0 11.0617 0.619465 10 1.62741Z"
                                            fill="#545454" />
                                    </svg>
                                    <span class="count">{!! \App\Models\Wishlist::WishCount($currentTheme) !!}</span>
                                </a>
                            </li>
                            @endauth
                            <li class="menu-lnk has-item lang-dropdown">
                                <a href="#">
                                    @php \Log::info(['header firdt currantLang' => $currantLang]) @endphp
                                    <span class="drp-text">{{ Str::upper($currantLang) }}</span>
                                    <div class="lang-icn">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg"
                                            version="1.1" id="svg2223" xml:space="preserve" width="682.66669"
                                            height="682.66669" viewBox="0 0 682.66669 682.66669">
                                            <g id="g2229" transform="matrix(1.3333333,0,0,-1.3333333,0,682.66667)">
                                                <g id="g2231">
                                                    <g id="g2233" clip-path="url(#clipPath2237)">
                                                        <g id="g2239" transform="translate(497,256)">
                                                            <path
                                                                d="m 0,0 c 0,-132.548 -108.452,-241 -241,-241 -132.548,0 -241,108.452 -241,241 0,132.548 108.452,241 241,241 C -108.452,241 0,132.548 0,0 Z"
                                                                style="fill:none;stroke:#FEBD2F;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2241" />
                                                        </g>
                                                        <g id="g2243" transform="translate(376,256)">
                                                            <path
                                                                d="m 0,0 c 0,-132.548 -53.726,-241 -120,-241 -66.274,0 -120,108.452 -120,241 0,132.548 53.726,241 120,241 C -53.726,241 0,132.548 0,0 Z"
                                                                style="fill:none;stroke:#FEBD2F;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2245" />
                                                        </g>
                                                        <g id="g2247" transform="translate(256,497)">
                                                            <path d="M 0,0 V -482"
                                                                style="fill:none;stroke:#FEBD2F;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2249" />
                                                        </g>
                                                        <g id="g2251" transform="translate(15,256)">
                                                            <path d="M 0,0 H 482"
                                                                style="fill:none;stroke:#FEBD2F;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2253" />
                                                        </g>
                                                        <g id="g2255" transform="translate(463.8926,136)">
                                                            <path d="M 0,0 H -415.785"
                                                                style="fill:none;stroke:#FEBD2F;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2257" />
                                                        </g>
                                                        <g id="g2259" transform="translate(48.1079,377)">
                                                            <path d="M 0,0 H 415.785"
                                                                style="fill:none;stroke:#FEBD2F;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2261" />
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                </a>
                                <div class="menu-dropdown">
                                    <ul>
                                        @php \Log::info(['header currantLang' => $currantLang]) @endphp
                                        @foreach ($languages as $code => $language)
                                        <li><a href="{{ route('change.languagestore', [$code]) }}"
                                                class="@if ($language == $currantLang) active-language text-primary @endif">{{  ucFirst($language) }}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                            <li class="cart-header">
                                <a href="javascript:;">
                                    <span class="desk-only icon-lable"><b>{{__('Cart:')}}</b>
                                        <span id="sub_total_main_page">{{0}}</span>
                                        {{ $currency }}</span>
                                    <div>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="19" height="17"
                                            viewBox="0 0 19 17" fill="none">
                                            <path fill-rule="evenodd" clip-rule="evenodd"
                                                d="M15.5698 10.627H6.97178C5.80842 10.6273 4.81015 9.79822 4.59686 8.65459L3.47784 2.59252C3.40702 2.20522 3.06646 1.92595 2.67278 1.93238H0.805055C0.360435 1.93238 0 1.57194 0 1.12732C0 0.682701 0.360435 0.322266 0.805055 0.322266H2.68888C3.85224 0.321937 4.85051 1.15101 5.0638 2.29465L6.18282 8.35672C6.25364 8.74402 6.5942 9.02328 6.98788 9.01686H15.5778C15.9715 9.02328 16.3121 8.74402 16.3829 8.35672L17.3972 2.88234C17.4407 2.64509 17.3755 2.40085 17.2195 2.21684C17.0636 2.03283 16.8334 1.92843 16.5922 1.93238H7.2455C6.80088 1.93238 6.44044 1.57194 6.44044 1.12732C6.44044 0.682701 6.80088 0.322266 7.2455 0.322266H16.5841C17.3023 0.322063 17.9833 0.641494 18.4423 1.19385C18.9013 1.74622 19.0907 2.4742 18.959 3.18021L17.9447 8.65459C17.7314 9.79822 16.7331 10.6273 15.5698 10.627ZM10.466 13.8478C10.466 12.5139 9.38464 11.4326 8.05079 11.4326C7.60617 11.4326 7.24573 11.7931 7.24573 12.2377C7.24573 12.6823 7.60617 13.0427 8.05079 13.0427C8.49541 13.0427 8.85584 13.4032 8.85584 13.8478C8.85584 14.2924 8.49541 14.6528 8.05079 14.6528C7.60617 14.6528 7.24573 14.2924 7.24573 13.8478C7.24573 13.4032 6.88529 13.0427 6.44068 13.0427C5.99606 13.0427 5.63562 13.4032 5.63562 13.8478C5.63562 15.1816 6.71693 16.2629 8.05079 16.2629C9.38464 16.2629 10.466 15.1816 10.466 13.8478ZM15.2963 15.4579C15.2963 15.0133 14.9358 14.6528 14.4912 14.6528C14.0466 14.6528 13.6862 14.2924 13.6862 13.8478C13.6862 13.4032 14.0466 13.0427 14.4912 13.0427C14.9358 13.0427 15.2963 13.4032 15.2963 13.8478C15.2963 14.2924 15.6567 14.6528 16.1013 14.6528C16.5459 14.6528 16.9064 14.2924 16.9064 13.8478C16.9064 12.5139 15.8251 11.4326 14.4912 11.4326C13.1574 11.4326 12.076 12.5139 12.076 13.8478C12.076 15.1816 13.1574 16.2629 14.4912 16.2629C14.9358 16.2629 15.2963 15.9025 15.2963 15.4579Z"
                                                fill="white"></path>
                                        </svg>
                                        <span class="count">0</span>
                                    </div>
                                </a>
                            </li>

                        </ul>
                        <div class="mobile-menu mobile-only">
                            <button class="mobile-menu-button">
                                <div class="one"></div>
                                <div class="two"></div>
                                <div class="three"></div>
                            </button>
                        </div>

                        <div class="search-popup">
                            <div class="close-search">
                                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50"
                                    fill="none">
                                    <path
                                        d="M27.7618 25.0008L49.4275 3.33503C50.1903 2.57224 50.1903 1.33552 49.4275 0.572826C48.6647 -0.189868 47.428 -0.189965 46.6653 0.572826L24.9995 22.2386L3.33381 0.572826C2.57102 -0.189965 1.3343 -0.189965 0.571605 0.572826C-0.191089 1.33562 -0.191186 2.57233 0.571605 3.33503L22.2373 25.0007L0.571605 46.6665C-0.191186 47.4293 -0.191186 48.666 0.571605 49.4287C0.952952 49.81 1.45285 50.0007 1.95275 50.0007C2.45266 50.0007 2.95246 49.81 3.3339 49.4287L24.9995 27.763L46.6652 49.4287C47.0465 49.81 47.5464 50.0007 48.0463 50.0007C48.5462 50.0007 49.046 49.81 49.4275 49.4287C50.1903 48.6659 50.1903 47.4292 49.4275 46.6665L27.7618 25.0008Z"
                                        fill="white"></path>
                                </svg>
                            </div>
                            <div class="search-form-wrapper">
                                <form>
                                    <div class="form-inputs">
                                        <input type="search" placeholder="Buscar Producto..."
                                            class="form-control search_input" list="products" name="search_product"
                                            id="product">
                                        <datalist id="products">
                                        </datalist>
                                        <button type="submit" class="btn">
                                            <svg>
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M0.000169754 6.99457C0.000169754 10.8576 3.13174 13.9891 6.99473 13.9891C8.60967 13.9891 10.0968 13.4418 11.2807 12.5226C11.3253 12.6169 11.3866 12.7053 11.4646 12.7834L17.0603 18.379C17.4245 18.7432 18.015 18.7432 18.3792 18.379C18.7434 18.0148 18.7434 17.4243 18.3792 17.0601L12.7835 11.4645C12.7055 11.3864 12.6171 11.3251 12.5228 11.2805C13.442 10.0966 13.9893 8.60951 13.9893 6.99457C13.9893 3.13157 10.8577 0 6.99473 0C3.13174 0 0.000169754 3.13157 0.000169754 6.99457ZM1.86539 6.99457C1.86539 4.1617 4.16187 1.86522 6.99473 1.86522C9.8276 1.86522 12.1241 4.1617 12.1241 6.99457C12.1241 9.82743 9.8276 12.1239 6.99473 12.1239C4.16187 12.1239 1.86539 9.82743 1.86539 6.99457Z">
                                                </path>
                                            </svg>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
  


    </header>

    <!--header start here-->