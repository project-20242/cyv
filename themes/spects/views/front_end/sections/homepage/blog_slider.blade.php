@foreach ($landing_blogs as $blog)
    <div class="blog-itm">
        <div class="blog-inner">
            <div class="blog-img">
                <a href="{{route('page.article',['storeSlug'=>$slug, 'id'=>$blog->id])}}">
                    <img src="{{asset($blog->cover_image_path )}}">
                </a>
            </div>
            <div class="blog-content">
                <h4><a href="{{route('page.article',['storeSlug'=>$slug, 'id'=>$blog->id])}}">{{$blog->title}}</a> </h4>
                <p>{{$blog->short_description}}</p>
                <a href="{{route('page.article',['storeSlug'=>$slug, 'id'=>$blog->id])}}" class="btn-secondary">
                    {{ __('Read more') }}
                </a>
            </div>
        </div>
    </div>
@endforeach
