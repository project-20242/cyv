<!--footer start here-->
<footer class="site-footer" style="@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide  ?? '' }}" data-section="{{ $option->section_name  ?? '' }}"
    data-store="{{ $option->store_id  ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <img src="{{ asset($section->footer->section->background_image->image ?? '')}}" class="footer-bg"
        id="{{ $section->footer->section->background_image->slug ?? '' }}_preview"  alt="">
    <div class="container">
        @if($whatsapp_setting_enabled)
        <div class="floating-wpp"></div>
        @endif
        <div class="footer-row">
            <div class="footer-col footer-subscribe-col">
                <div class="footer-widget">
                    <div class="footer-subscribe">
                        <div class="subtitle" id="{{ $section->footer->section->sub_title->slug ?? '' }}_preview">
                            {!! $section->footer->section->sub_title->text ?? '' !!}</div>
                        <h2 id="{{ $section->footer->section->newsletter_title->slug ?? '' }}_preview">
                            {!! $section->footer->section->newsletter_title->text ?? '' !!}
                        </h2>
                    </div>
                    <p id="{{ $section->footer->section->description->slug ?? '' }}_preview">
                        {!! $section->footer->section->description->text ?? '' !!}
                    </p>
                    <form class="footer-subscribe-form" action="{{ route('newsletter.store',$slug) }}" method="post">
                        @csrf
                        <div class="input-wrapper">
                            <input type="email" placeholder="ESCRIBE TU DIRECCIÓN DE CORREO ELECTRÓNICO..." name="email">
                            <button type="submit" class="btn-subscibe"> {{ __('SUBSCRIBE') }}
                            </button>
                        </div>
                        <div class="">
                            {{-- <input type="checkbox" class="" id="PlaceCheckbox"> --}}
                            <label id="{{ $section->footer->section->newsletter_description->slug ?? '' }}_preview">
                                {!! $section->footer->section->newsletter_description->text ?? '' !!}</label>
                        </div>
                    </form>
                </div>
            </div>
            @if(isset($section->footer->section->footer_menu_type))
            @for ($i = 0; $i < $section->footer->section->footer_menu_type->loop_number ?? 1; $i++)
                @if(isset($section->footer->section->footer_menu_type->footer_title->{$i}))
                <div class="footer-col footer-link footer-link-{{$i+1}}">
                    <div class="footer-widget">
                        <h2> {{ $section->footer->section->footer_menu_type->footer_title->{$i} ?? ''}} </h2>
                        @php
                        $footer_menu_id = $section->footer->section->footer_menu_type->footer_menu_ids->{$i} ?? '';
                        $footer_menu = get_nav_menu($footer_menu_id);
                        @endphp
                        <ul>
                            @if (!empty($footer_menu))
                            @foreach ($footer_menu as $key => $nav)
                            @if ($nav->type == 'custom')
                            <li><a href="{{ url($nav->slug) }}" target="{{ $nav->target }}">
                                    @if ($nav->title == null)
                                    {{ $nav->title }}
                                    @else
                                    {{ $nav->title }}
                                    @endif
                                </a></li>
                            @elseif($nav->type == 'category')
                            <li><a href="{{ route('themes.page', [$slug, $nav->slug]) }}" target="{{ $nav->target }}">
                                    @if ($nav->title == null)
                                    {{ $nav->title }}
                                    @else
                                    {{ $nav->title }}
                                    @endif
                                </a></li>
                            @else
                            <li><a href="{{ route('themes.page', [$slug, $nav->slug]) }}" target="{{ $nav->target }}">
                                    @if ($nav->title == null)
                                    {{ $nav->title }}
                                    @else
                                    {{ $nav->title }}
                                    @endif
                                </a>
                            </li>
                            @endif
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                @endif
                @endfor
                @endif
                <div class="footer-col footer-link footer-link-3">
                    <div class="footer-widget">
                        <h2 id="{{ $section->footer->section->title->slug ?? '' }}_preview"> {!!
                            $section->footer->section->title->text ?? '' !!}</h2>
                        @if(isset($section->footer->section->footer_link))
                        <ul class="header-list-social">
                            @for ($i = 0; $i < $section->footer->section->footer_link->loop_number ?? 1; $i++)
                                <li>
                                    <a href="{{ $section->footer->section->footer_link->social_link->{$i} ?? '#'}}"
                                        target="blank" id="social_link{{ $i }}">
                                        <img src="{{ asset($section->footer->section->footer_link->social_icon->{$i}->image ?? 'themes/' . $currentTheme . '/assets/images/youtube.svg') }}"
                                            class="{{ 'social_icon_'. $i .'_preview' }}" alt="icon"
                                            id="social_icon_{{ $i }}">
                                    </a>
                                </li>
                            @endfor
                        </ul>
                        @endif
                    </div>
                </div>
        </div>
    </div>
</footer>
<!--footer end here-->
<div class="overlay "></div>
<!--cart popup start here-->

<!--cart popup ends here-->
<!-- Mobile menu start here -->
<div class="mobile-menu-wrapper">
    <div class="menu-close-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="35" height="34" viewBox="0 0 35 34" fill="none">
            <line x1="2.29695" y1="1.29289" x2="34.1168" y2="33.1127" stroke="white" stroke-width="2" />
            <line x1="0.882737" y1="33.1122" x2="32.7025" y2="1.29242" stroke="white" stroke-width="2" />
        </svg>
    </div>
    <div class="mobile-menu-bar">
        <div class="steps-theme-navigation header-desk-only">
            <h5 class="stp-head">{{ __('NAVIGATION') }}</h5>
            <div class="stp-nav d-flex align-items-center">

                @foreach ($MainCategoryList->take(3) as $category)
                <div class="stp-navlink ">
                    <a
                        href="{{ route('page.product-list', [$slug,'main_category' => $category->id]) }}">{{ $category->name }}</a>
                </div>
                @endforeach
            </div>
            <div class="stp-menu-widget featured-coll-widget">
                <h5> <b>{{ __('Destacado') }} </b> {{ __('collections') }}</h5>
                <ul>
                    @foreach ($products as $key => $featured)
                    <li class="d-flex">
                        <div class="fea-coll-img">
                            <a
                                href="{{ route('page.product-list', [$slug,'main_category' => $category->id, 'sub_category' => $featured->id]) }}">
                                <img src="{{ asset($featured->image_path) }}">
                            </a>
                        </div>
                        <div class="fea-coll-contnt d-flex align-items-center">
                            <div class="fea-coll-contnt-left">
                                <h6><a
                                        href="{{ route('page.product-list', [$slug,'main_category' => $category->id, 'sub_category' => $featured->id]) }}">
                                        {{ $featured->name }}</a></h6>
                            </div>
                            <a href="{{ route('page.product-list', [$slug,'main_category' => $category->id, 'sub_category' => $featured->id]) }}"
                                class="btn white-btn">
                                <span>{{ __('MORE') }}</span>
                                <svg viewBox="0 0 10 5">
                                    <path
                                        d="M2.37755e-08 2.57132C-3.38931e-06 2.7911 0.178166 2.96928 0.397953 2.96928L8.17233 2.9694L7.23718 3.87785C7.07954 4.031 7.07589 4.28295 7.22903 4.44059C7.38218 4.59824 7.63413 4.60189 7.79177 4.44874L9.43039 2.85691C9.50753 2.78197 9.55105 2.679 9.55105 2.57146C9.55105 2.46392 9.50753 2.36095 9.43039 2.28602L7.79177 0.69418C7.63413 0.541034 7.38218 0.544682 7.22903 0.702329C7.07589 0.859976 7.07954 1.11192 7.23718 1.26507L8.1723 2.17349L0.397965 2.17336C0.178179 2.17336 3.46059e-06 2.35153 2.37755e-08 2.57132Z">
                                    </path>
                                </svg>
                            </a>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="stp-menu-widget featured-coll-widget">
                <h5> <b>{{ __('More') }} </b> {{ __('categories') }}:</h5>
                <ul class="pulse-arrow">
                    <li class="filter_products" data-value="best-selling">
                        <a
                            href="{{ route('page.product-list', [$slug,'filter_product' => 'best-selling']) }}">{{ __('Bestsellers') }}</a>
                    </li>
                    <li class="filter_products" data-value="best-selling">
                        <a
                            href="{{ route('page.product-list', [$slug,'filter_product' => 'trending']) }}">{{ __('Trending product') }}<span></span></a>
                    </li>
                </ul>
            </div>
            <div class="stp-menu-widget featured-coll-widget">
                <h5> <b>{{ __('Share us,') }} </b> {{ __('be us') }}</h5>

                <ul class="header-list-social" role="list">
                    @if(isset($section->footer->section->footer_link))
                    @for ($i = 0; $i < $section->footer->section->footer_link->loop_number ?? 1; $i++)
                        <li>
                            <a href="{{ $section->footer->section->footer_link->social_link->{$i} ?? '#'}}"
                                target="blank" id="social_link{{ $i }}">
                                <img src="{{ asset($section->footer->section->footer_link->social_icon->{$i}->image ?? 'themes/' . $currentTheme . '/assets/images/youtube.svg') }}"
                                    class="{{ 'social_icon_'. $i .'preview' }}" alt="icon" id="social_icon{{ $i }}">
                            </a>
                        </li>
                        @endfor
                        @endif
                </ul>
            </div>
        </div>
        <ul class="header-mobile-only">
            <li class="mobile-item has-children">
                <a href="#" class="acnav-label">
                    {{ __(' Shop All') }}
                    <svg class="menu-open-arrow" xmlns="http://www.w3.org/2000/svg" width="20" height="11"
                        viewBox="0 0 20 11">
                        <path fill="#24272a"
                            d="M.268 1.076C.373.918.478.813.584.76l.21.474c.79.684 2.527 2.158 5.21 4.368 2.738 2.21 4.159 3.316 4.264 3.316.474-.053 1.158-.369 1.947-1.053.842-.631 1.632-1.42 2.474-2.368.895-.948 1.737-1.842 2.632-2.58.842-.789 1.578-1.262 2.105-1.42l.316.684c0 .21-.106.474-.316.737-.053.21-.263.421-.474.579-.053.052-.316.21-.737.474l-.526.368c-.421.263-1.105.947-2.158 2l-1.105 1.053-2.053 1.947c-1 .947-1.579 1.421-1.842 1.421-.263 0-.684-.263-1.158-.895-.526-.631-.842-1-1.052-1.105l-.737-.579c-.316-.316-.527-.474-.632-.474l-5.42-4.315L.267 2.339l-.105-.421-.053-.369c0-.157.053-.315.158-.473z">
                        </path>
                    </svg>
                    <svg class="close-menu-ioc" xmlns="http://www.w3.org/2000/svg" width="20" height="18"
                        viewBox="0 0 20 18">
                        <path fill="#24272a"
                            d="M19.95 16.75l-.05-.4-1.2-1-5.2-4.2c-.1-.05-.3-.2-.6-.5l-.7-.55c-.15-.1-.5-.45-1-1.1l-.1-.1c.2-.15.4-.35.6-.55l1.95-1.85 1.1-1c1-1 1.7-1.65 2.1-1.9l.5-.35c.4-.25.65-.45.75-.45.2-.15.45-.35.65-.6s.3-.5.3-.7l-.3-.65c-.55.2-1.2.65-2.05 1.35-.85.75-1.65 1.55-2.5 2.5-.8.9-1.6 1.65-2.4 2.3-.8.65-1.4.95-1.9 1-.15 0-1.5-1.05-4.1-3.2C3.1 2.6 1.45 1.2.7.55L.45.1c-.1.05-.2.15-.3.3C.05.55 0 .7 0 .85l.05.35.05.4 1.2 1 5.2 4.15c.1.05.3.2.6.5l.7.6c.15.1.5.45 1 1.1l.1.1c-.2.15-.4.35-.6.55l-1.95 1.85-1.1 1c-1 1-1.7 1.65-2.1 1.9l-.5.35c-.4.25-.65.45-.75.45-.25.15-.45.35-.65.6-.15.3-.25.55-.25.75l.3.65c.55-.2 1.2-.65 2.05-1.35.85-.75 1.65-1.55 2.5-2.5.8-.9 1.6-1.65 2.4-2.3.8-.65 1.4-.95 1.9-1 .15 0 1.5 1.05 4.1 3.2 2.6 2.15 4.3 3.55 5.05 4.2l.2.45c.1-.05.2-.15.3-.3.1-.15.15-.3.15-.45z">
                        </path>
                    </svg>
                </a>


                <ul class="mobile_menu_inner acnav-list">
                    <li class="menu-h-link">
                        <ul>
                            @foreach ($MainCategoryList as $category)
                            <li><a
                                    href="{{ route('page.product-list', [$slug,'main_category' => $category->id]) }}">{{ $category->name }}</a>
                            </li>
                            @endforeach
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="{{ route('page.product-list',$slug) }}">{{ __('Collection') }}</a></li>

            <li class="mobile-item has-children">
                <a href="#" class="acnav-label">
                    {{ __('Pages') }}
                    <svg class="menu-open-arrow" xmlns="http://www.w3.org/2000/svg" width="20" height="11"
                        viewBox="0 0 20 11">
                        <path fill="#24272a"
                            d="M.268 1.076C.373.918.478.813.584.76l.21.474c.79.684 2.527 2.158 5.21 4.368 2.738 2.21 4.159 3.316 4.264 3.316.474-.053 1.158-.369 1.947-1.053.842-.631 1.632-1.42 2.474-2.368.895-.948 1.737-1.842 2.632-2.58.842-.789 1.578-1.262 2.105-1.42l.316.684c0 .21-.106.474-.316.737-.053.21-.263.421-.474.579-.053.052-.316.21-.737.474l-.526.368c-.421.263-1.105.947-2.158 2l-1.105 1.053-2.053 1.947c-1 .947-1.579 1.421-1.842 1.421-.263 0-.684-.263-1.158-.895-.526-.631-.842-1-1.052-1.105l-.737-.579c-.316-.316-.527-.474-.632-.474l-5.42-4.315L.267 2.339l-.105-.421-.053-.369c0-.157.053-.315.158-.473z">
                        </path>
                    </svg>
                    <svg class="close-menu-ioc" xmlns="http://www.w3.org/2000/svg" width="20" height="18"
                        viewBox="0 0 20 18">
                        <path fill="#24272a"
                            d="M19.95 16.75l-.05-.4-1.2-1-5.2-4.2c-.1-.05-.3-.2-.6-.5l-.7-.55c-.15-.1-.5-.45-1-1.1l-.1-.1c.2-.15.4-.35.6-.55l1.95-1.85 1.1-1c1-1 1.7-1.65 2.1-1.9l.5-.35c.4-.25.65-.45.75-.45.2-.15.45-.35.65-.6s.3-.5.3-.7l-.3-.65c-.55.2-1.2.65-2.05 1.35-.85.75-1.65 1.55-2.5 2.5-.8.9-1.6 1.65-2.4 2.3-.8.65-1.4.95-1.9 1-.15 0-1.5-1.05-4.1-3.2C3.1 2.6 1.45 1.2.7.55L.45.1c-.1.05-.2.15-.3.3C.05.55 0 .7 0 .85l.05.35.05.4 1.2 1 5.2 4.15c.1.05.3.2.6.5l.7.6c.15.1.5.45 1 1.1l.1.1c-.2.15-.4.35-.6.55l-1.95 1.85-1.1 1c-1 1-1.7 1.65-2.1 1.9l-.5.35c-.4.25-.65.45-.75.45-.25.15-.45.35-.65.6-.15.3-.25.55-.25.75l.3.65c.55-.2 1.2-.65 2.05-1.35.85-.75 1.65-1.55 2.5-2.5.8-.9 1.6-1.65 2.4-2.3.8-.65 1.4-.95 1.9-1 .15 0 1.5 1.05 4.1 3.2 2.6 2.15 4.3 3.55 5.05 4.2l.2.45c.1-.05.2-.15.3-.3.1-.15.15-.3.15-.45z">
                        </path>
                    </svg>
                </a>
                <ul class="mobile_menu_inner acnav-list">
                    <li class="menu-h-link">
                        <ul>
                            @foreach ($pages as $page)
                            <li><a href="{{ route('custom.page', [$slug,$page->page_slug]) }}">{{$page->name}}</a></li>
                            @endforeach
                            <li><a href="{{ route('page.faq',$slug) }}">{{ __('FAQs') }}</a></li>
                            <li><a href="{{ route('page.blog',$slug) }}">{{ __('Blog') }}</a></li>
                        </ul>
                    </li>

                </ul>
            </li>
            @foreach ($pages as $page)
            <li><a href="{{ env('APP_URL') . 'page/' . $page->page_slug }}">{{ $page->name }}</a></li>
            @endforeach

        </ul>
    </div>
</div>
<div class="overlay cart-overlay"></div>
<div class="cartDrawer cartajaxDrawer">
</div>

<div class="overlay wish-overlay"></div>
<div class="wishDrawer wishajaxDrawer">
</div>