<section style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="place-section place-section-second padding-top padding-bottom">
        <div class="row no-gutters justify-content-between align-items-center flex-dairection">
            <div class="col-lg-4 col-md-4 col-12">
                <div class="place-left">
                    <img src="{{ asset($section->subscribe->section->image->image ?? '') }}" class="place-left-one"
                        alt="">
                    <img src="{{ asset($section->subscribe->section->image->image ?? '') }}" class="place-left-two"
                        alt="">
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="footer-widget">
                    <div class="footer-subscribe">
                        <div class="subtitle" id="{{ $section->subscribe->section->sub_title->slug ?? '' }}">{!!
                            $section->subscribe->section->sub_title->text ?? '' !!}</div>
                        <h2 id="{{ $section->subscribe->section->title->slug ?? '' }}">{!!
                            $section->subscribe->section->title->text ?? '' !!}</h2>
                    </div>
                    <p id="{{ $section->subscribe->section->description->slug ?? '' }}">{!!
                        $section->subscribe->section->description->text ?? '' !!}</p>
                    <form class="footer-subscribe-form" action="{{ route('newsletter.store', $slug) }}" method="post">
                        @csrf
                        <div class="input-wrapper">
                            <input type="email" placeholder="ESCRIBE TU DIRECCIÓN DE CORREO ELECTRÓNICO..." name="email">
                            <button type="submit" class="btn-subscibe"> {{ __('SUBSCRIBE') }}
                            </button>
                        </div>
                        <div class="">
                            {{-- <input type="checkbox" class="" id="PlaceCheckbox"> --}}
                            <label for=""
                                id="{{ $section->subscribe->section->newsletter_description->slug ?? '' }}">{!!
                                $section->subscribe->section->newsletter_description->text ?? '' !!}</label>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-12">
                <div class="place-right">
                    <div class="place-right-image">
                        <img src="{{ asset($section->subscribe->section->image->image ?? '') }}" class="" alt="">
                    </div>
                    <div class="place-right-image">
                        <img src="{{ asset($section->subscribe->section->image->image ?? '') }}" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>