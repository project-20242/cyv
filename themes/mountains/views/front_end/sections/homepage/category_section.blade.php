<section class="about-product padding-top"
    style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="row align-items-center justify-content-center">
        <div class="col-lg-6 col-12">
            <div class="about card">
                @foreach ($products->take(1) as $product)
                <div class="about-product-main">
                    <div class="about-product-img">
                        <a href="{{ url($slug.'/product/'.$product->slug) }}">
                            <img src="{{ asset($product->cover_image_path) }}" alt="">
                        </a>
                    </div>
                    <div class="about-product-content">
                        <div class="about-subtitle">
                            <div class="about-subtitle-inner">
                                <div class="subtitle-pointer">
                                    <img src="{{ asset('themes/' . $currentTheme . '/assets/images/slider-inner-line-right.png') }}"
                                        alt="">
                                    <span>{{ __('PRODUCTO DESTACADO') }}</span>
                                </div>
                                {{--  <div class="custom-output">
                                {{!empty($product->ProductData) ? $product->ProductData->name : ''}}
                                </div> --}}
                            </div>
                            <div class="about-title">
                                <h3>
                                    <a href="{{ url($slug.'/product/'.$product->slug) }}">{{ $product->name }}</a>
                                </h3>
                                <div class="about-itm-datail">
                                    @if ($product->variant_id != 0)
                                    <b> {!! \App\Models\ProductVariant::variantlist($product->variant_id) !!} </b>
                                    @endif
                                    @if ($product->variant_product == 0)
                                    <div class="price">
                                        <ins>{{ $currency }} {{ currency_format_with_sym(($product->sale_price ?? $product->price) , $store->id, $currentTheme) }}</ins>
                                        <del>{{ $currency }}{{ currency_format_with_sym(($product->price ?? $product->price) , $store->id, $currentTheme) }}</del>
                                    </div>
                                    @else
                                    <div class="price">
                                        <ins>{{ __('In Variant') }}</ins>
                                    </div>
                                    @endif
                                    <a href="javascript:void(0)" class="btn addcart-btn-globaly"
                                        product_id="{{ $product->id }}" variant_id="0" qty="1">
                                        {{ __('Add to cart') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                @foreach ($products->take(1) as $product)
                <div class="about-product-main about-product-bg">
                    <div class="about-product-img">
                        <a href="{{ url($slug.'/product/'.$product->slug) }}">
                            <img src='{{ asset($product->cover_image_path) }}' alt="">
                        </a>
                    </div>
                    <div class="about-product-content">
                        <div class="about-subtitle">
                            <div class="about-subtitle-inner">
                                <div class="subtitle-pointer">
                                    <img src="{{ asset('themes/' . $currentTheme . '/assets/images/slider-inner-line-right.png') }}"
                                        alt="">
                                    <span>{{ __('PRODUCTO DESTACADO') }}</span>
                                </div>
                                {{--  <div class="custom-output">
                                {{!empty($product->ProductData) ? $product->ProductData->name : ''}}
                                </div> --}}
                            </div>
                            <div class="about-title">
                                <h3>
                                    <a href="{{ url($slug.'/product/'.$product->slug) }}">{{ $product->name }}</a>
                                </h3>
                                <div class="about-itm-datail">
                                    @if ($product->variant_id != 0)
                                    <b> {!! \App\Models\ProductVariant::variantlist($product->variant_id) !!} </b>
                                    @endif
                                    @if ($product->variant_product == 0)
                                    <div class="price">
                                        <ins>{{ $currency }} {{ currency_format_with_sym(($product->sale_price ?? $product->price) , $store->id, $currentTheme) }}</ins>
                                        <del>{{ $currency }} {{ currency_format_with_sym(($product->price ?? $product->price) , $store->id, $currentTheme) }}</del>
                                    </div>
                                    @else
                                    <div class="price">
                                        <ins>{{ __('In Variant') }}</ins>
                                    </div>
                                    @endif
                                    <a href="javascript:void(0)" class="btn btn-secondary addcart-btn-globaly"
                                        product_id="{{ $product->id }}" variant_id="0" qty="1">
                                        {{ __('Add to cart') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
        @foreach ($all_products->take(1) as $product)
        <div class="col-lg-6 col-12">
            <div class="about-product-main-wrp">
                <div class="about-product-content">
                    <div class="about-subtitle">
                        <div class="about-subtitle-inner">
                            <div class="subtitle-pointer">

                                <img src="{{ asset('themes/' . $currentTheme . '/assets/images/slider-inner-line-right.png') }}"
                                    alt="">
                                <span>{{ __('PRODUCTO DESTACADO') }}</span><br>

                            </div>
                            {{--  <div class="custom-output">
                            {{!empty($product->ProductData) ? $product->ProductData->name : ''}}
                            </div> --}}
                        </div>
                        <div class="about-title">
                            <h3>
                                <a href="{{ url($slug.'/product/'.$product->slug) }}">{{ $product->name }}</a>
                            </h3>
                            <div class="about-itm-datail">
                                @if ($product->variant_id != 0)
                                {{-- <b>  {!! \App\Models\ProductVariant::-variantlist($product->variant_id) !!} </b> --}}
                                @endif
                                @if ($product->variant_product == 0)
                                <div class="price">
                                    <ins>{{ $currency }} {{ currency_format_with_sym(($product->sale_price ?? $product->price) , $store->id, $currentTheme) }}</ins>
                                    <del>{{ $currency }} {{ currency_format_with_sym(($product->price ?? $product->price) , $store->id, $currentTheme) }}</del>
                                </div>
                                @else
                                <div class="price">
                                    <ins>{{ __('In Variant') }}</ins>
                                </div>
                                @endif
                                <a href="javascript:void(0)" class="btn addcart-btn-globaly"
                                    product_id="{{ $product->id }}" variant_id="0" qty="1">
                                    {{ __('Add to cart') }}
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="about-product-img-wrp">
                    <a href="{{ url($slug.'/product/'.$product->slug) }}">
                        <img src="{{ asset($product->cover_image_path) }}" alt="">
                    </a>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
