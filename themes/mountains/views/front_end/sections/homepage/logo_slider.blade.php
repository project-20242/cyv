<section class="merge-client-section padding-top"
    style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <!-- <img src="{{ asset('themes/' . $currentTheme  . '/assets/img/Client-logo-pattern.png') }}" class="client-ptrn"
        alt="">  -->
    <div class="client-logo-section common-arrows padding-bottom">
        <div class="container">
            <div class="client-logo-slider">
                @if (!empty($homepage_main_logo['homepage-logo-logo']))
                @for ($i = 0; $i < count($homepage_main_logo['homepage-logo-logo']); $i++) @php foreach
                    ($homepage_main_logo['inner-list'] as $homepage_main_logo_value) { if
                    ($homepage_main_logo_value['field_slug']=='homepage-logo-logo' ) {
                    $homepage_logo=$homepage_main_logo_value['field_default_text']; } if
                    (!empty($homepage_main_logo[$homepage_main_logo_value['field_slug']])) { if
                    ($homepage_main_logo_value['field_slug']=='homepage-logo-logo' ) {
                    $homepage_logo=$homepage_main_logo[$homepage_main_logo_value['field_slug']][$i]['field_prev_text'];
                    } } } @endphp <div class="client-logo-item">
                    <a href="#" tabindex="0">
                        <img src="{{ asset($homepage_logo) }}" alt="logo">
                    </a>
            </div>
            @endfor
            @else
            @for ($i = 0; $i <= 6; $i++) <div class="client-logo-item">
                <a href="#" tabindex="0">
                    <img src="{{ asset((isset($section->logo_slider->section->image->image->{$i}) && !empty($section->logo_slider->section->image->image->{$i})) ? $section->logo_slider->section->image->image->{$i} : 'themes/' . $currentTheme. '/assets/images/clietn-logo.png') }}" alt="">
                </a>
               </div>
            @endfor
            @endif
    </div>
</section>