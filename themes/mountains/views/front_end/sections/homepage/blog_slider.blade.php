<div class="blog-card-slider">
    @foreach ($landing_blogs->take(5) as $blogs)
        <div class="about-card-main">
            <div class="blog-card-inner">
                <div class="blog-card">
                    <div class="blog-card-image">
                        <a href="{{route('page.article',[$slug,$blogs->id])}}">
                            <img src="{{asset($blogs->cover_image_path )}}" class="default-img">
                        </a>
                        <div class="tip-lable">
                            <span>{{ __('CONSEJOS') }}</span>
                        </div>
                    </div>
                    <div class="blog-card-content">
                        <div class="blog-card-heading-detail">
                            <span>{{ __('AUTHOR:') }}  JOHN DOE </span>
                            <span>{{ __('DATE:') }} {{ date("d M Y", strtotime($blogs->created_at))}}</span>
                        </div>
                        <h3 class="blog_text">
                            <a href="{{route('page.article',[$slug,$blogs->id])}}" >
                                {{ $blogs->title }}
                            </a>
                        </h3>
                        <p class="blog_descrip">
                            {{ $blogs->short_description }}
                        </p>
                        <div class="blog-card-bottom">
                            <a href="{{route('page.article',[$slug,$blogs->id])}}" class=" btn">
                               {{__('Leer Más')}}
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
</div>