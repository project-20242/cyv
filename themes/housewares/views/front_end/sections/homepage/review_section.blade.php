<section class="testimonials-section padding-top padding-bottom"style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="section-title padding-top">
            <h2>
                <b id="{{ $section->review->section->title->slug ?? '' }}_preview">{!! $section->review->section->title->text ?? '' !!}</b>
            </h2>
        </div>
        <div class="testimonial-slider flex-slider">
            @foreach ($reviews as $review)
                <div class="testimonial-itm">
                    <div class="testimonial-itm-inner">
                        <div class="testimonial-itm-content">
                            <span>{{ !empty($review->UserData) ? $review->UserData->first_name : '' }},
                               </span>
                            <div class="testimonial-content-top">
                                <h3 class="testimonial-title">
                                    {{ $review->title }}
                                </h3>
                            </div>
                            <p class="descriptions">{{ $review->description }}</p>
                            <div class="testimonial-star">
                                <div class="d-flex align-items-center">
                                    @for ($i = 0; $i < 5; $i++)
                                        <i
                                            class="fa fa-star {{ $i < $review->rating_no ? 'text-warning' : '' }} "></i>
                                    @endfor
                                    <span class="star-count">{{ $review->rating_no }}.5 / <b> 5.0</b></span>
                                </div>
                            </div>
                        </div>
                        <div class="testimonial-itm-image">
                            <a href="#" tabindex="0">
                                <img src="{{ asset('/' . !empty($review->ProductData) ? $review->ProductData->cover_image_path : '') }}"
                                    class="default-img" alt="review">
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
