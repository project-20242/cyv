<section class="image-accessories-section padding-top padding-bottom"
    style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="row align-items-center justify-content-between order">
            <div class="col-lg-5 col-md-6 col-12">
                <div class="accessories-image">
                    <img src="{{ asset($section->best_product_second->section->image->image ?? '')}}" alt=""
                        id="{{ ($section->best_product_second->section->image->slug ?? '') }}_preview">
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-12">
                <div class="accessories-right-side-inner">
                    <div class="accessories-right-side-content">
                        <div class="section-title">
                            <span class="subtitle"
                                id="{{ ($section->best_product_second->section->sub_title->slug ?? '') }}_preview">{!!$section->best_product_second->section->sub_title->text
                                ?? '' !!}</span>
                            <h2 id="{{ ($section->best_product_second->section->title->slug ?? '') }}_preview">
                                {!!$section->best_product_second->section->title->text ?? '' !!}</h2>
                        </div>
                        <p id="{{ ($section->best_product_second->section->description->slug ?? '') }}_preview">
                            {!!$section->best_product_second->section->description->text ?? '' !!}</p>
                        <a href="{{route('page.product-list',$slug)}}" class="btn-secondary" tabindex="0"
                            id="{{ ($section->best_product_second->section->button->slug ?? '') }}_preview">{!!$section->best_product_second->section->button->text
                            ?? '' !!}
                            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15"
                                fill="none">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M8.39025 12.636V11.06L14.2425 4.756C15.2525 3.668 15.2525 1.904 14.2425 0.816001C13.2324 -0.272 11.5949 -0.272 10.5848 0.816001L0.539316 11.637C-0.612777 12.878 0.203182 15 1.83249 15H6.19566C7.4077 15 8.39025 13.9416 8.39025 12.636ZM6.92719 11.0601L5.03558 9.02244L1.57385 12.7514C1.34343 12.9996 1.50663 13.424 1.83249 13.424H6.19566C6.59968 13.424 6.92719 13.0712 6.92719 12.636V11.0601ZM6.13287 7.84044L11.6194 1.9304C12.058 1.45787 12.7693 1.45787 13.2079 1.9304C13.6466 2.40294 13.6466 3.16907 13.2079 3.6416L7.72144 9.55164L6.13287 7.84044Z"
                                    fill="#CDC6BE"></path>
                            </svg>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>