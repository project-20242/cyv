
@foreach ($landing_blogs as $blog)

    <div class="blog-card-itm">
        <div class="blog-card-itm-inner">
            <div class="blog-card-image">

                <a href="{{ route('page.article', ['storeSlug' => $slug, 'id' => $blog->id]) }}">
                    <img src="{{ asset($blog->cover_image_path) }}" alt="" class="default-img">
                </a>
                <div class="tip-lable">
                    <span>{{$blog->category->name}}</span>
                </div>
            </div>
            <div class="blog-card-content">
                <div class="blog-card-heading-detail">
                    <span>{{$blog->created_at->format('d M,Y ')}} / John Doe</span>
                </div>
                <h3>
                    <a class="title" href="{{ route('page.article', ['storeSlug' => $slug, 'id' => $blog->id]) }}" tabindex="0">
                        {{$blog->title}}
                    </a>
                </h3>
                <p class="description">{{$blog->short_description}}</p>
                <div class="blog-card-bottom">
                    <a href="{{ route('page.article', ['storeSlug' => $slug, 'id' => $blog->id]) }}" class=" btn" tabindex="0">
                        {{__('Read More')}}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endforeach
