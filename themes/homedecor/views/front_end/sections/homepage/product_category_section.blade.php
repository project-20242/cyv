<section class="interiors-design-section" style="@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="row align-items-center">
            <div class="col-md-3 col-12">
                <div class="section-title">
                    <h2 id="{{ $section->product_category->section->title->slug ?? '' }}_preview">
                        {!! $section->product_category->section->title->text ?? '' !!}
                    </h2>
                </div>
            </div>
            <div class="col-md-5 col-12">
                <div class=interiors-title-center>
                    <p id="{{ $section->product_category->section->sub_title->slug ?? '' }}_preview">
                        {!! $section->product_category->section->sub_title->text ?? '' !!}
                    </p>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <a href="{{ route('page.product-list', $slug) }}" class="btn-secondary btn-secondary-theme-color"
                    id="{{ $section->product_category->section->button->slug ?? '' }}_preview">
                    {!! $section->product_category->section->button->text ?? '' !!}
                    <svg xmlns="http://www.w3.org/2000/svg" width="8" height="10" viewBox="0 0 4 6"
                        fill="none">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                            fill=""></path>
                    </svg>
                </a>
            </div>
        </div>

        <div class="row padding-top">
            @foreach ($MainCategoryList->take(2) as $key => $MainCategory)
                <div class="col-md-6 col-sm-6 col-12">
                    <div class="interiors-design-wrapper">
                        <img src="{{asset($MainCategory->image_path)}}" class="design-bg-img">
                        <div class="row align-items-flex-end">
                            <div class="col-lg-5 col-12">
                                <div class="columnl-left-media-inner">
                                    <div class="column-left-media-content">
                                        <div class="section-title">
                                            <h3>{{ $MainCategory->name }}</h3>
                                        </div>
                                        <a href="{{ route('page.product-list', [$slug, 'main_category' => $MainCategory->slug]) }}"
                                            class="link-btn" tabindex="0">
                                            {{ __('Show More') }}
                                            <svg xmlns="http://www.w3.org/2000/svg" width="8" height="10"
                                                viewBox="0 0 4 6" fill="none">
                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                    d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                                    fill=""></path>
                                            </svg>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @php
                                $prod = App\Models\Product::where('maincategory_id', $MainCategory->id)
                                    ->where('theme_id', $currentTheme)
                                    ->limit(1)
                                    ->get();
                            @endphp
                            @foreach ($prod as $pro)
                                @if ($pro->maincategory_id == $MainCategory->id)
                                    <div class="col-lg-7 col-12">
                                        <div class="columnl-right-caption-inner">
                                            <div class="product-card-inner">
                                                <div class="product-card-image">
                                                    <a href="{{ url($slug.'/product/'.$pro->slug) }}"
                                                        class="product-img">
                                                        <img src="{{ asset($pro->cover_image_path) }}"
                                                            class="default-img" alt="fan">
                                                    </a>
                                                    <div class="new-labl">
                                                        <a href="javascript:void(0)" class="wishbtn wishbtn-globaly"
                                                            product_id="{{ $pro->id }}"
                                                            in_wishlist="{{ $pro->in_whishlist ? 'remove' : 'add' }}">
                                                            <span class="wish-ic">
                                                                <i class="{{ $pro->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"
                                                                    style="color: black;"></i>
                                                            </span>
                                                        </a>
                                                        <div class="product-btn-wrp">
                                                            @php
                                                                $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                                $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                                            @endphp
                                                            @if(isset($module) && $module->isEnabled())
                                                                {{-- Include the module blade button --}}
                                                                @include('productquickview::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                            @endif
                                                            @if(isset($compare_module) && $compare_module->isEnabled())
                                                                {{-- Include the module blade button --}}
                                                                @include('productcompare::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="product-content">
                                                    <div class="product-content-top">
                                                        <div class="review-star">
                                                            <span>{{ $pro->ProductData->name }}</span>
                                                            <div class="d-flex align-items-center">
                                                                @for ($i = 0; $i < 5; $i++)
                                                                    <i
                                                                        class="fa fa-star review-stars {{ $i < $pro->average_rating ? 'text-warning' : '' }} "></i>
                                                                @endfor
                                                            </div>
                                                        </div>
                                                        <h3 class="product-title">
                                                            <a href="{{ url($slug.'/product/'.$pro->slug) }}">
                                                                {{ $pro->name }}
                                                            </a>
                                                        </h3>
                                                    </div>
                                                    <div class="product-content-bottom pro-bottom-price">
                                                        @if ($pro->variant_product == 0)
                                                            <div class="price">
                                                                <ins>{{ currency_format_with_sym(($pro->sale_price ?? $pro->price) , $store->id, $currentTheme) }}
                                                                    <span
                                                                        class="currency-type">{{ $currency }}</span>
                                                                </ins>
                                                            </div>
                                                        @else
                                                            <div class="price">
                                                                <ins>{{ __('In Variant') }}</ins>
                                                            </div>
                                                        @endif
                                                        <br>
                                                        <div class="custom-output">
                                                            @php
                                                                date_default_timezone_set('Asia/Kolkata');
                                                                $currentDateTime = \Carbon\Carbon::now();
                                                                $sale_product = \App\Models\FlashSale::where('theme_id', $currentTheme)
                                                                    ->where('store_id', getCurrentStore())
                                                                    ->where('is_active', 1)
                                                                    ->get();
                                                                $latestSales = [];

                                                                foreach ($sale_product as $flashsale) {
                                                                    $saleEnableArray = json_decode($flashsale->sale_product, true);
                                                                    $startDate = \Carbon\Carbon::parse($flashsale->start_date . ' ' . $flashsale->start_time);
                                                                    $endDate = \Carbon\Carbon::parse($flashsale->end_date . ' ' . $flashsale->end_time);

                                                                    if ($endDate < $startDate) {
                                                                        $endDate->addDay();
                                                                    }
                                                                    $currentDateTime->setTimezone($startDate->getTimezone());

                                                                    if ($currentDateTime >= $startDate && $currentDateTime <= $endDate) {
                                                                        if (is_array($saleEnableArray) && in_array($pro->id, $saleEnableArray)) {
                                                                            $latestSales[$pro->id] = [
                                                                                'discount_type' => $flashsale->discount_type,
                                                                                'discount_amount' => $flashsale->discount_amount,
                                                                            ];
                                                                        }
                                                                    }
                                                                }
                                                            @endphp
                                                            @foreach ($latestSales as $productId => $saleData)
                                                                <div class="badge">
                                                                    @if ($saleData['discount_type'] == 'flat')
                                                                        -{{ $saleData['discount_amount'] }}{{ $currency_icon }}
                                                                    @elseif ($saleData['discount_type'] == 'percentage')
                                                                        -{{ $saleData['discount_amount'] }}%
                                                                    @endif
                                                                </div>
                                                            @endforeach
                                                        </div>
                                                    </div>
                                                    <div class="show-more-btn">
                                                        <a href="javascript:void(0)"
                                                            class="link-btn dark-link-btn addcart-btn-globaly"
                                                            product_id="{{ $pro->id }}" variant_id="0"
                                                            qty="1">
                                                            {{ __('Add to cart') }}
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="8"
                                                                height="10" viewBox="0 0 4 6" fill="none">
                                                                <path fill-rule="evenodd" clip-rule="evenodd"
                                                                    d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                                                    fill=""></path>
                                                            </svg>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
