<section class="main-home-first-section"
    style="@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="main-banner-image-wrap">
        @foreach ($modern_products as $products)
            <div class="img-wrapper">
                <img src="{{ asset($products->cover_image_path) }}" alt="Banner main image">
                <div class="container">
                    <div class="home-banner-content">
                        <div class="decorative-img">
                            <img src="{{ asset('themes/' . $currentTheme . '/assets/images/decorative.png') }}"
                                alt="">

                        </div>
                        <div class="home-banner-content-inner">
                            <span class="lbl">{{ $products->ProductData->name }} </span>
                            <h2 class="h1">{!! $products->name !!}</h2>
                            <div class="banner-desk">
                                <p class="description">{{ strip_tags($products->description) }}</p>
                            </div>
                            <a href="{{ route('page.product-list', $slug) }}" class="btn">
                                <span class="home-icon"><svg xmlns="http://www.w3.org/2000/svg" width="16"
                                        height="17" viewBox="0 0 16 17" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M2.575 13.15V6.95H1.025V13.15C1.025 14.8621 2.41292 16.25 4.125 16.25H11.875C13.5871 16.25 14.975 14.8621 14.975 13.15V6.95H13.425V13.15C13.425 14.006 12.731 14.7 11.875 14.7H10.325V12.375C10.325 11.0909 9.28406 10.05 8 10.05C6.71594 10.05 5.675 11.0909 5.675 12.375V14.7H4.125C3.26896 14.7 2.575 14.006 2.575 13.15ZM7.225 14.7H8.775V12.375C8.775 11.947 8.42802 11.6 8 11.6C7.57198 11.6 7.225 11.947 7.225 12.375V14.7Z"
                                            fill="#3A1C36" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M4.5943 0.75C3.31305 0.75 2.05032 1.38849 1.35605 2.57142C1.13367 2.95033 0.882254 3.40328 0.68167 3.83608C0.581565 4.05208 0.48285 4.28705 0.406519 4.52103C0.338036 4.73096 0.25 5.05007 0.25 5.4C0.25 6.01779 0.401441 6.80166 0.94832 7.45791C1.53664 8.16389 2.396 8.5 3.35 8.5C4.25567 8.5 4.95751 8.07767 5.40794 7.70064C5.56154 7.57208 5.78846 7.57208 5.94206 7.70064C6.39249 8.07767 7.09433 8.5 8 8.5C8.90567 8.5 9.60751 8.07768 10.0579 7.70064C10.2115 7.57208 10.4385 7.57208 10.5921 7.70064C11.0425 8.07768 11.7443 8.5 12.65 8.5C13.604 8.5 14.4634 8.16389 15.0517 7.45791C15.5986 6.80166 15.75 6.01779 15.75 5.4C15.75 5.05007 15.662 4.73096 15.5935 4.52103C15.5171 4.28705 15.4184 4.05208 15.3183 3.83608C15.1177 3.40328 14.8663 2.95033 14.6439 2.57142C13.9497 1.38849 12.6869 0.75 11.4057 0.75H4.5943ZM4.5943 2.3C3.81693 2.3 3.08631 2.68555 2.69283 3.35598C2.27033 4.07585 1.8 4.97309 1.8 5.4C1.8 6.175 2.1875 6.95 3.35 6.95C4.16508 6.95 4.78967 6.18801 5.09019 5.73256C5.22098 5.53434 5.43751 5.4 5.675 5.4C5.91249 5.4 6.12901 5.53434 6.25981 5.73256C6.56033 6.18801 7.18492 6.95 8 6.95C8.81508 6.95 9.43967 6.18801 9.74019 5.73256C9.87099 5.53434 10.0875 5.4 10.325 5.4C10.5625 5.4 10.779 5.53434 10.9098 5.73256C11.2103 6.18801 11.8349 6.95 12.65 6.95C13.8125 6.95 14.2 6.175 14.2 5.4C14.2 4.97309 13.7297 4.07585 13.3072 3.35598C12.9137 2.68555 12.1831 2.3 11.4057 2.3H4.5943Z"
                                            fill="#3A1C36" />
                                    </svg></span>
                                {{ __('Show products') }}
                                <svg xmlns="http://www.w3.org/2000/svg" width="12" height="5" viewBox="0 0 12 5"
                                    fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M0.75 2.49986C0.749996 2.72077 0.955195 2.89986 1.20832 2.89986L10.1621 2.89998L9.0851 3.81309C8.90354 3.96702 8.89934 4.22026 9.07572 4.37872C9.25209 4.53717 9.54226 4.54084 9.72383 4.38691L11.611 2.78691C11.6999 2.71159 11.75 2.60809 11.75 2.5C11.75 2.3919 11.6999 2.28841 11.611 2.21309L9.72383 0.613092C9.54226 0.45916 9.25209 0.462827 9.07572 0.621283C8.89934 0.779738 8.90354 1.03298 9.0851 1.18691L10.1621 2.09998L1.20834 2.09986C0.955209 2.09986 0.750004 2.27894 0.75 2.49986Z"
                                        fill="#3A1C36" />
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="home-category-section">
        <div class="slides-numbers">
            <div class="category-slide-nav"></div>
            <span class="active">01</span> / <span class="total"></span>
        </div>
        <div class="home-category-slider">
            @foreach ($modern_products as $m_product)
                <div class="modern-cat-col">
                    <div class="modern-cat-inner d-flex">
                        <div class="cat-img">
                            <img src="{{asset($m_product->cover_image_path) }}" alt="default cat img">
                        </div>
                        <div class="modern-cat-info">
                            <div class="info-top">
                                <span class="lbl">{{ $m_product->ProductData->name }}</span>
                                <a href="{{ route('page.product-list', $slug) }}" class="h4 cat-link">
                                    {!! $m_product->name !!}
                                </a>
                            </div>
                            <a href="{{ route('page.product-list', $slug) }}"
                                class="btn-link">{{ __('MORE PRODUCT') }}
                                <svg xmlns="http://www.w3.org/2000/svg" width="17" height="7" viewBox="0 0 17 7"
                                    fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M4.18215e-08 3.49975C-6.03322e-06 3.88635 0.317119 4.19975 0.70832 4.19976L14.546 4.19997L12.8815 5.79791C12.6009 6.06729 12.5944 6.51046 12.867 6.78776C13.1396 7.06505 13.588 7.07147 13.8686 6.80209L16.7852 4.00209C16.9225 3.87028 17 3.68917 17 3.5C17 3.31083 16.9225 3.12972 16.7852 2.99791L13.8686 0.19791C13.588 -0.0714698 13.1396 -0.065052 12.867 0.212245C12.5944 0.489541 12.6009 0.93271 12.8815 1.20209L14.5459 2.79997L0.708342 2.79976C0.317142 2.79975 6.15908e-06 3.11315 4.18215e-08 3.49975Z"
                                        fill="white" />
                                </svg></a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
