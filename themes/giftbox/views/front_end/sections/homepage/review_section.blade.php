<section class="padding-top shop-review-section padding-bottom"style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-lg-4">
                <div class="shop-reviews-left-inner">
                    <div class="disc-labl" id="{{ $section->review->section->sub_title->slug ?? '' }}_preview">
                        {!! $section->review->section->sub_title->text ?? '' !!}
                    </div>
                    <div class="section-title">
                        <h2 class="title" id="{{ $section->review->section->title->slug ?? '' }}_preview">{!! $section->review->section->title->text ?? '' !!}</h2>
                    </div>
                    <p id="{{ $section->review->section->description->slug ?? '' }}_preview">{!! $section->review->section->description->text ?? '' !!}</p>
                    <a href="{{ route('page.product-list', $slug) }}" class="btn" id="{{ $section->review->section->button->slug ?? '' }}_preview">
                        <span class="first-icon"><svg width="11" height="11" viewBox="0 0 11 11" fill="none"
                                xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M8.525 7.15C8.2763 7.15 7.92491 7.18438 7.5591 7.23448C7.3882 7.25788 7.25788 7.3882 7.23448 7.5591C7.18438 7.92491 7.15 8.2763 7.15 8.525C7.15 8.7737 7.18438 9.12509 7.23448 9.4909C7.25788 9.6618 7.3882 9.79212 7.5591 9.81552C7.92491 9.86562 8.2763 9.9 8.525 9.9C8.7737 9.9 9.12509 9.86562 9.4909 9.81552C9.6618 9.79212 9.79212 9.6618 9.81552 9.4909C9.86562 9.12509 9.9 8.7737 9.9 8.525C9.9 8.2763 9.86562 7.92491 9.81552 7.5591C9.79212 7.3882 9.6618 7.25788 9.4909 7.23448C9.12509 7.18438 8.7737 7.15 8.525 7.15ZM7.40984 6.14465C6.74989 6.23503 6.23503 6.74989 6.14465 7.40984C6.09281 7.78836 6.05 8.19949 6.05 8.525C6.05 8.85051 6.09281 9.26165 6.14465 9.64016C6.23503 10.3001 6.74989 10.815 7.40984 10.9053C7.78836 10.9572 8.19949 11 8.525 11C8.85051 11 9.26165 10.9572 9.64016 10.9053C10.3001 10.815 10.815 10.3001 10.9053 9.64016C10.9572 9.26165 11 8.85051 11 8.525C11 8.19949 10.9572 7.78836 10.9053 7.40984C10.815 6.74989 10.3001 6.23503 9.64016 6.14465C9.26165 6.09281 8.85051 6.05 8.525 6.05C8.19949 6.05 7.78836 6.09281 7.40984 6.14465Z"
                                    fill="white"></path>
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M2.475 7.15C2.2263 7.15 1.87491 7.18438 1.5091 7.23448C1.3382 7.25788 1.20788 7.3882 1.18448 7.5591C1.13438 7.92491 1.1 8.2763 1.1 8.525C1.1 8.7737 1.13438 9.12509 1.18448 9.4909C1.20788 9.6618 1.3382 9.79212 1.5091 9.81552C1.87491 9.86562 2.2263 9.9 2.475 9.9C2.7237 9.9 3.07509 9.86562 3.4409 9.81552C3.6118 9.79212 3.74212 9.6618 3.76552 9.4909C3.81562 9.12509 3.85 8.7737 3.85 8.525C3.85 8.2763 3.81562 7.92491 3.76552 7.5591C3.74212 7.3882 3.6118 7.25788 3.4409 7.23448C3.07509 7.18438 2.7237 7.15 2.475 7.15ZM1.35984 6.14465C0.699894 6.23503 0.185033 6.74989 0.0946504 7.40984C0.0428112 7.78836 0 8.19949 0 8.525C0 8.85051 0.0428112 9.26165 0.0946504 9.64016C0.185033 10.3001 0.699894 10.815 1.35984 10.9053C1.73836 10.9572 2.14949 11 2.475 11C2.80051 11 3.21165 10.9572 3.59016 10.9053C4.25011 10.815 4.76497 10.3001 4.85535 9.64016C4.90719 9.26165 4.95 8.85051 4.95 8.525C4.95 8.19949 4.90719 7.78836 4.85535 7.40984C4.76497 6.74989 4.25011 6.23503 3.59016 6.14465C3.21165 6.09281 2.80051 6.05 2.475 6.05C2.14949 6.05 1.73836 6.09281 1.35984 6.14465Z"
                                    fill="white"></path>
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M8.525 1.1C8.2763 1.1 7.92491 1.13438 7.5591 1.18448C7.3882 1.20788 7.25788 1.3382 7.23448 1.5091C7.18438 1.87491 7.15 2.2263 7.15 2.475C7.15 2.7237 7.18438 3.07509 7.23448 3.4409C7.25788 3.6118 7.3882 3.74212 7.5591 3.76552C7.92491 3.81562 8.2763 3.85 8.525 3.85C8.7737 3.85 9.12509 3.81562 9.4909 3.76552C9.6618 3.74212 9.79212 3.6118 9.81552 3.4409C9.86562 3.07509 9.9 2.7237 9.9 2.475C9.9 2.2263 9.86562 1.87491 9.81552 1.5091C9.79212 1.3382 9.6618 1.20788 9.4909 1.18448C9.12509 1.13438 8.7737 1.1 8.525 1.1ZM7.40984 0.0946504C6.74989 0.185033 6.23503 0.699894 6.14465 1.35984C6.09281 1.73836 6.05 2.14949 6.05 2.475C6.05 2.80051 6.09281 3.21165 6.14465 3.59016C6.23503 4.25011 6.74989 4.76497 7.40984 4.85535C7.78836 4.90719 8.19949 4.95 8.525 4.95C8.85051 4.95 9.26165 4.90719 9.64016 4.85535C10.3001 4.76497 10.815 4.25011 10.9053 3.59016C10.9572 3.21165 11 2.80051 11 2.475C11 2.14949 10.9572 1.73836 10.9053 1.35984C10.815 0.699894 10.3001 0.185033 9.64016 0.0946504C9.26165 0.0428112 8.85051 0 8.525 0C8.19949 0 7.78836 0.0428112 7.40984 0.0946504Z"
                                    fill="white"></path>
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M2.475 1.1C2.2263 1.1 1.87491 1.13438 1.5091 1.18448C1.3382 1.20788 1.20788 1.3382 1.18448 1.5091C1.13438 1.87491 1.1 2.2263 1.1 2.475C1.1 2.7237 1.13438 3.07509 1.18448 3.4409C1.20788 3.6118 1.3382 3.74212 1.5091 3.76552C1.87491 3.81562 2.2263 3.85 2.475 3.85C2.7237 3.85 3.07509 3.81562 3.4409 3.76552C3.6118 3.74212 3.74212 3.6118 3.76552 3.4409C3.81562 3.07509 3.85 2.7237 3.85 2.475C3.85 2.2263 3.81562 1.87491 3.76552 1.5091C3.74212 1.3382 3.6118 1.20788 3.4409 1.18448C3.07509 1.13438 2.7237 1.1 2.475 1.1ZM1.35984 0.0946504C0.699894 0.185033 0.185033 0.699894 0.0946504 1.35984C0.0428112 1.73836 0 2.14949 0 2.475C0 2.80051 0.0428112 3.21165 0.0946504 3.59016C0.185033 4.25011 0.699894 4.76497 1.35984 4.85535C1.73836 4.90719 2.14949 4.95 2.475 4.95C2.80051 4.95 3.21165 4.90719 3.59016 4.85535C4.25011 4.76497 4.76497 4.25011 4.85535 3.59016C4.90719 3.21165 4.95 2.80051 4.95 2.475C4.95 2.14949 4.90719 1.73836 4.85535 1.35984C4.76497 0.699894 4.25011 0.185033 3.59016 0.0946504C3.21165 0.0428112 2.80051 0 2.475 0C2.14949 0 1.73836 0.0428112 1.35984 0.0946504Z"
                                    fill="white"></path>
                            </svg></span>
                            {!! $section->review->section->button->text ?? '' !!} <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6"
                            viewBox="0 0 4 6" fill="none">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                fill="white"></path>
                        </svg></a>
                </div>
            </div>
            <div class="col-xl-9 col-lg-8">
                <div class="row itro-list">
                    <div class="col-lg-4 col-md-4 col-12 intro-card">
                        <div class="intro-card-inner">
                            <div class="intro-icon">
                                <div class="back-shape">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="60" height="95"
                                        viewBox="0 0 60 95" fill="none">
                                        <path
                                            d="M47.5 52.5C47.5 78.7335 73.7335 95 47.5 95C21.2665 95 0 73.7335 0 47.5C0 21.2665 21.2665 0 47.5 0C73.7335 0 47.5 26.2665 47.5 52.5Z"
                                            fill="#3A1C36" />
                                    </svg>
                                </div>
                                <span class="icon-circle">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                                        viewBox="0 0 40 40" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M19.9987 33.334C27.3625 33.334 33.332 27.3644 33.332 20.0007C33.332 12.6369 27.3625 6.66732 19.9987 6.66732C12.6349 6.66732 6.66536 12.6369 6.66536 20.0007C6.66536 27.3644 12.6349 33.334 19.9987 33.334ZM19.9987 36.6673C29.2034 36.6673 36.6654 29.2054 36.6654 20.0007C36.6654 10.7959 29.2034 3.33398 19.9987 3.33398C10.794 3.33398 3.33203 10.7959 3.33203 20.0007C3.33203 29.2054 10.794 36.6673 19.9987 36.6673Z"
                                            fill="#3A1C36" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M19.9987 10C20.9192 10 21.6654 10.7462 21.6654 11.6667V19.3096L25.3439 22.9882C25.9947 23.639 25.9947 24.6943 25.3439 25.3452C24.693 25.9961 23.6377 25.9961 22.9869 25.3452L18.8202 21.1785C18.5076 20.8659 18.332 20.442 18.332 20V11.6667C18.332 10.7462 19.0782 10 19.9987 10Z"
                                            fill="#3A1C36" />
                                    </svg>

                                </span>
                            </div>
                            <div class="caption">
                                <h3 class="title">{{ __('Caja de Regalo Dorada 3D de Color Púrpura Asombroso')}} </h3>
                                <p>{{ __('Lorem Ipsum es simplemente el texto ficticio de la industria de impresión y composición tipográfica.
                                    Lorem Ipsum has been the industry`s standard dummy.')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 intro-card">
                        <div class="intro-card-inner">
                            <div class="intro-icon">
                                <div class="back-shape">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="60" height="95"
                                        viewBox="0 0 60 95" fill="none">
                                        <path
                                            d="M47.5 52.5C47.5 78.7335 73.7335 95 47.5 95C21.2665 95 0 73.7335 0 47.5C0 21.2665 21.2665 0 47.5 0C73.7335 0 47.5 26.2665 47.5 52.5Z"
                                            fill="#390C59" />
                                    </svg>
                                </div>
                                <span class="icon-circle">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="37" height="37"
                                        viewBox="0 0 37 37" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M14.6814 13.7731C14.5256 14.1461 14.1744 14.4008 13.7715 14.433L6.44967 15.0177C5.49164 15.0942 5.104 16.2906 5.83509 16.9144L11.4073 21.6691C11.716 21.9325 11.8508 22.3467 11.7563 22.7413L10.0529 29.8551C9.82941 30.7882 10.8436 31.527 11.6633 31.0283L17.94 27.2094C18.2846 26.9997 18.7173 26.9997 19.0619 27.2094L25.3386 31.0283C26.1583 31.527 27.1725 30.7882 26.9491 29.8551L25.2456 22.7413C25.1511 22.3467 25.2859 21.9325 25.5946 21.6691L31.1668 16.9144C31.8979 16.2905 31.5103 15.0942 30.5522 15.0177L23.2305 14.433C22.8275 14.4008 22.4763 14.1461 22.3205 13.7731L19.4968 7.01036C19.1275 6.12588 17.8744 6.12589 17.5051 7.01037L14.6814 13.7731ZM24.6947 11.4568L22.3421 5.82234C20.9176 2.41077 16.0843 2.4108 14.6598 5.82234L12.3072 11.4568L6.20422 11.9442C2.50896 12.2393 1.01378 16.8537 3.83371 19.2599L8.47182 23.2175L7.0543 29.137C6.19244 32.7361 10.1043 35.586 13.2659 33.6624L18.501 30.4773L23.736 33.6624C26.8976 35.5861 30.8095 32.7361 29.9476 29.137L28.5301 23.2175L33.1682 19.2599C35.9882 16.8537 34.4929 12.2393 30.7977 11.9442L24.6947 11.4568Z"
                                            fill="#390C59" />
                                    </svg>
                                </span>
                            </div>
                            <div class="caption">
                                <h3 class="title">{{ __('Caja de Regalo Dorada 3D de Color Púrpura Asombroso')}}</h3>
                                <p>{{ __('Lorem Ipsum es simplemente el texto ficticio de la industria de impresión y composición tipográfica.
                                    Lorem Ipsum has been the industry`s standard dummy.')}}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-12 intro-card">
                        <div class="intro-card-inner">
                            <div class="intro-icon">
                                <div class="back-shape">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="60" height="95"
                                        viewBox="0 0 60 95" fill="none">
                                        <path
                                            d="M47.5 52.5C47.5 78.7335 73.7335 95 47.5 95C21.2665 95 0 73.7335 0 47.5C0 21.2665 21.2665 0 47.5 0C73.7335 0 47.5 26.2665 47.5 52.5Z"
                                            fill="#FF369A" />
                                    </svg>
                                </div>
                                <span class="icon-circle">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="35" height="35"
                                        viewBox="0 0 35 35" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M3.3451 19.3861C3.91462 18.8166 4.83799 18.8166 5.4075 19.3861L10.2096 24.1883C10.7792 24.7578 10.7792 25.6811 10.2096 26.2506C9.64012 26.8202 8.71676 26.8202 8.14724 26.2506L3.3451 21.4485C2.77559 20.879 2.77559 19.9556 3.3451 19.3861Z"
                                            fill="#FF369A" />
                                        <path
                                            d="M31.697 8.28148C32.2433 7.68966 32.2064 6.76703 31.6146 6.22074C31.0228 5.67444 30.1001 5.71134 29.5538 6.30317L13.043 24.1899C12.4967 24.7817 12.5336 25.7044 13.1254 26.2507C13.7173 26.797 14.6399 26.7601 15.1862 26.1682L25.2528 15.2627C25.5337 14.9584 26.0113 14.9489 26.3042 15.2417L28.136 17.0735C28.7055 17.643 29.6289 17.643 30.1984 17.0735C30.7679 16.504 30.7679 15.5806 30.1984 15.0111L28.2428 13.0556C27.9661 12.7788 27.9572 12.333 28.2226 12.0454L31.697 8.28148Z"
                                            fill="#FF369A" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M8.57469 18.7824L6.01188 16.2196C4.30333 14.511 4.30333 11.7409 6.01188 10.0324L11.4914 4.55289C13.1999 2.84435 15.97 2.84435 17.6785 4.55289L20.2414 7.11571C21.9499 8.82425 21.9499 11.5944 20.2414 13.3029L14.7619 18.7824C13.0533 20.4909 10.2832 20.4909 8.57469 18.7824ZM8.07427 14.1572L10.6371 16.72C11.2066 17.2895 12.13 17.2895 12.6995 16.72L18.179 11.2405C18.7485 10.671 18.7485 9.74762 18.179 9.1781L15.6161 6.61529C15.0466 6.04577 14.1233 6.04577 13.5538 6.61529L8.07427 12.0948C7.50476 12.6643 7.50476 13.5877 8.07427 14.1572Z"
                                            fill="#FF369A" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M11.668 29.1673C12.4734 29.1673 13.1263 28.5144 13.1263 27.709C13.1263 26.9036 12.4734 26.2507 11.668 26.2507C10.8626 26.2507 10.2096 26.9036 10.2096 27.709C10.2096 28.5144 10.8626 29.1673 11.668 29.1673ZM11.668 32.084C14.0842 32.084 16.043 30.1252 16.043 27.709C16.043 25.2927 14.0842 23.334 11.668 23.334C9.25172 23.334 7.29297 25.2927 7.29297 27.709C7.29297 30.1252 9.25172 32.084 11.668 32.084Z"
                                            fill="#FF369A" />
                                    </svg>
                                </span>
                            </div>
                            <div class="caption">
                                <h3 class="title">{{ __('Caja de Regalo Dorada 3D de Color Púrpura Asombroso')}}</h3>
                                <p>{{ __('Lorem Ipsum es simplemente el texto ficticio de la industria de impresión y composición tipográfica.
                                    Lorem Ipsum has been the industry`s standard dummy.')}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
