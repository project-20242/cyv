@extends('front_end.layouts.app')
@section('page-title')
    {{ __('Cart') }}
@endsection
@section('content')
    @include('front_end.sections.partision.header_section')

    <section class="cart-page-section padding-bottom">
    </section>   
    @include('front_end.sections.homepage.best_product_section')
    @include('front_end.sections.partision.footer_section')
@endsection
