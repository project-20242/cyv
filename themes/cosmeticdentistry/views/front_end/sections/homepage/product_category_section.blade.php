<section class="today-discounts bg-gray padding-bottom" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="best-tabs">
            <div class="section-title d-flex align-items-center justify-content-between">
                <h2 id="{{ $section->product_category->section->title->slug ?? '' }}_preview"> {!!
                    $section->product_category->section->title->text ?? '' !!}
                </h2>
                <ul class="cat-tab tabs">
                    @foreach ($category_options as $cat_key => $category)
                        <li class="tab-link {{ $cat_key == 0 ? 'active' : '' }}" data-tab="{{ $cat_key }}">
                            <a href="javascript:;">{{ __('All Products') }}</a>
                        </li>
                    @endforeach
                </ul>
            </div>
            @foreach ($category_options as $cat_k => $category)
                <div id="{{ $cat_k }}" class="tab-content {{ $cat_k == 0 ? 'active' : '' }}">
                    <div class="row product-list shop-protab-slider">
                        @foreach ($products as $all_product)
                        @if ($cat_k == '0' || $all_product->ProductData->id == $cat_k)
                        <div class="col-lg-4 col-xl-3 col-md-6 col-sm-6 product-card col-12">
                            <div class="product-card-inner">
                                        <div class="product-card-image">
                                            <a href="{{ url($slug.'/product/'.$all_product->slug) }}">
                                                <img src="{{ asset($all_product->cover_image_path) }}"
                                                    class="default-img">
                                                <div class="new-labl  danger">
                                                    {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $all_product->id) !!}
                                                </div>
                                            </a>
                                        </div>
                                        <div class="product-content">
                                            <div class="product-content-top d-flex align-items-end">
                                                <div class="product-content-left">
                                                    <div class="product-subtitle">{{ $all_product->ProductData->name ?? '' }}</div>
                                                    <h3 class="product-title">
                                                        <a href="{{ url($slug.'/product/'.$all_product->slug) }}" class="short_description">
                                                            {{ $all_product->name }}
                                                        </a>
                                                    </h3>
                                                    <div class="reviews-stars-wrap d-flex align-items-center">
                                                        @if (!empty($all_product->average_rating))
                                                            @for ($i = 0; $i < 5; $i++)
                                                                <i class="fa fa-star {{ $i < $all_product->average_rating ? '' : 'text-warning' }} ">
                                                                </i>
                                                            @endfor
                                                        <span><b>{{ $all_product->average_rating }}.0</b> / 5.0</span>
                                                        @else
                                                            @for ($i = 0; $i < 5; $i++)
                                                                <i class="fa fa-star {{ $i < $all_product->average_rating ? '' : 'text-warning' }} ">
                                                                </i>
                                                            @endfor
                                                        <span><b>{{ $all_product->average_rating }}.0</b> / 5.0</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="product-content-center">
                                                @if ($all_product->variant_product == 0)
                                                    <div class="price">
                                                        <ins>{{ currency_format_with_sym(($all_product->sale_price ?? $all_product->price), $store->id, $currentTheme)}} </ins>
                                                    </div>
                                                @else
                                                    <div class="price">
                                                        <ins>{{ __('In Variant') }}</ins>
                                                    </div>
                                                @endif
                                            </div>
                                            <div class="product-content-bottom d-flex align-items-center justify-content-between">
                                                <button class="btn addcart-btn-globaly" product_id="{{ $all_product->id }}" variant_id="0" qty="1">
                                                    <span>{{ __('Add to cart') }} </span>
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6"
                                                        viewBox="0 0 4 6" fill="none">
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                            d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                                            fill="white"></path>
                                                    </svg>
                                                </button>
                                                    <a href="javascript:void(0)"
                                                        class="wishlist-btn wbwish  wishbtn-globaly" product_id="{{ $all_product->id }}" in_wishlist="{{ $all_product->in_whishlist ? 'remove' : 'add' }}">
                                                        <span class="wish-ic">
                                                            <i class="{{ $all_product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"
                                                                style='color: #000000'></i>
                                                        </span>
                                                    </a>
                                                    <div class="product-btn-wrp">
                                                        @php
                                                            $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                            $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                                        @endphp
                                                        @if(isset($module) && $module->isEnabled())
                                                            {{-- Include the module blade button --}}
                                                            @include('productquickview::pages.button', ['product_slug' => $all_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                        @endif
                                                        @if(isset($compare_module) && $compare_module->isEnabled())
                                                            {{-- Include the module blade button --}}
                                                            @include('productcompare::pages.button', ['product_slug' => $all_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                        @endif
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                            </div>
                        @endif
                        @endforeach
                    </div>
                </div>
            @endforeach
        </div>
        <div class="d-flex justify-content-center see-all-probtn">
            <a href="{{ route('page.product-list',$slug) }}" class="btn"id="{{ $section->category->section->button->slug ?? '' }}_preview"> {!!
                                    $section->category->section->button->text ?? '' !!}
                <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6" viewBox="0 0 4 6" fill="none">
                    <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                        fill="white"></path>
                </svg>
            </a>
        </div>
    </div>
</section>
