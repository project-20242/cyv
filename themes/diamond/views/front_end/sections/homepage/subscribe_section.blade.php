
<section class="subscribe-our-sec" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
                <div class=" container">
                    <div class="sec-head d-flex justify-content-between align-items-center ">
                        <h2 id="{{ $section->subscribe->section->title->slug ?? '' }}_preview">{!! $section->subscribe->section->title->text ?? '' !!}</h2>
                        <div class="insta-pro">
                            <div class="insta-pro-info">
                                <span id="{{ $section->subscribe->section->subtitle->slug ?? '' }}_preview">{!! $section->subscribe->section->subtitle->text ?? '' !!}</span>
                                <a href="https://www.instagram.com/" target="_blank" id="{{ $section->subscribe->section->button->slug ?? '' }}_preview"> {!!
                                     $section->subscribe->section->button->text ?? '' !!}
                                </a>
                            </div>
                            <div class="insta-pro-img">
                                <a href="https://www.instagram.com/">
                                    <img src="{{ asset($section->subscribe->section->background_image->image ?? 'themes/'.$currentTheme.'/assets/img/banner-sec-7.png')}}"
                                        id="{{ $section->subscribe->section->background_image->slug ?? '' }}_preview" alt="insta-pro">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 9 9"
                                        fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M0.746338 3.26117C0.746338 1.90962 1.74238 0.813965 2.97107 0.813965H5.93738C7.16607 0.813965 8.16211 1.90962 8.16211 3.26117V6.52411C8.16211 7.87567 7.16607 8.97132 5.93738 8.97132H2.97107C1.74238 8.97132 0.746338 7.87567 0.746338 6.52411V3.26117ZM2.97107 1.6297C2.15195 1.6297 1.48792 2.36013 1.48792 3.26117V6.52411C1.48792 7.42515 2.15195 8.15558 2.97107 8.15558H5.93738C6.75651 8.15558 7.42054 7.42515 7.42054 6.52411V3.26117C7.42054 2.36013 6.75651 1.6297 5.93738 1.6297H2.97107ZM3.71265 4.89264C3.71265 5.34316 4.04466 5.70838 4.45423 5.70838C4.86379 5.70838 5.1958 5.34316 5.1958 4.89264C5.1958 4.44212 4.86379 4.07691 4.45423 4.07691C4.04466 4.07691 3.71265 4.44212 3.71265 4.89264ZM4.45423 3.26117C3.6351 3.26117 2.97107 3.99161 2.97107 4.89264C2.97107 5.79368 3.6351 6.52411 4.45423 6.52411C5.27335 6.52411 5.93738 5.79368 5.93738 4.89264C5.93738 3.99161 5.27335 3.26117 4.45423 3.26117ZM6.12278 2.44544C5.8156 2.44544 5.56659 2.71935 5.56659 3.05724C5.56659 3.39513 5.8156 3.66904 6.12278 3.66904C6.42995 3.66904 6.67896 3.39513 6.67896 3.05724C6.67896 2.71935 6.42995 2.44544 6.12278 2.44544Z"
                                            fill="white"></path>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>
                    <ul>                      
                        @for ($i = 0; $i < count(objectToArray($section->subscribe->section->image->image)) ?? 1; $i++)                            
                            <li>
                                <a href="https://www.instagram.com/" class="img-wrapper">
                                    <img src="{{ asset($section->subscribe->section->image->image->{$i} ?? '') }}" alt="pic">
                                </a>
                            </li>
                        @endfor
                    </ul>
                </div>
            </section>