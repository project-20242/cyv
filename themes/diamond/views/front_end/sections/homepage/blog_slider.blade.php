<div class="main-blog">
    @foreach ($landing_blogs as $blog)
    <div class="blog-card">
            <div class="blog-card-image">
                <a href="#">
                    <img src="{{asset($blog->cover_image_path )}}" alt="blog-img">
                </a>
            </div>
            <div class="blog-card-content">
            <span>{{ __('ARTICLES') }}</span>
                <h3><a href="#">{{$blog->title}}</a> </h3>
                <p>{{$blog->short_description}}</p>
                <div class="blog-card-bottom">
                    <a href="#" class="btn">
                        {{ __('Read more') }}
                    </a>
                    <span class="date">
                       {{ date("d M Y", strtotime($blog->created_at))}} <br>
                        <a href="#">
                            @john
                        </a>
                    </span>
                </div>
               
            </div>
    </div>
    @endforeach
</div>