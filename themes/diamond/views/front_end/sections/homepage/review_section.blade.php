<section class="testimonials-sec padding-bottom"tyle="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
                <div class="container">
                        <div class="main-title">
                        <h2 id="{{ $section->review->section->title->slug ?? '' }}_preview">{!!
                            $section->review->section->title->text ?? '' !!}</h2>
                        </div>
                    <div class="testi-main">
                        @foreach ($reviews as $review)
                            <div class="testi-slide">
                                <div class="product-card-image">
                                    <a href="{{route('page.product-list',$slug)}}">
                                        <img src="{{asset('/'. !empty($review->ProductData) ? asset($review->ProductData->cover_image_path ) : '' )}}" class="default-img">

                                        <img src="{{ asset('themes/'.$currentTheme.'/assets/images/placholder.png') }}" class="hover-img">
                                    </a>
                                </div>
                                <div class="testi-content">
                                    <div class="title">
                                        <h3>{{$review->title}}</h3>
                                        <div class="star ratings d-flex align-items-center justify-content-end">
                                            @for ($i = 0; $i < 5; $i++)
                                                <i class="ti ti-star {{ $i < $review->rating_no ? 'text-warning' : '' }} "></i>

                                            @endfor
                                            <span>{{ $review->rating_no }}/ 5.0</span>
                                        </div>
                                    </div>
                                    <p>
                                        {{ strip_tags($review->description) }}
                                    </p>
                                    <h6>
                                        {{!empty($review->UserData) ? $review->UserData->first_name : '' }},
                                        <span>client</span>
                                    </h6>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </section>