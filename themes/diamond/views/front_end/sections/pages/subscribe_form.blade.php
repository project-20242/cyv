    <form class="footer-subscribe-form" action="{{ route('newsletter.store', $slug) }}" method="post">
        @csrf
        <div class="input-wrapper">
            <input type="email" placeholder="Ingrese su dirección de correo electrónico...." name="email">
            <button type="submit" class="btn-subscibe">
                     <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 24 24"
                                    fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M22.7071 12.7071C22.8946 12.5196 23 12.2652 23 12C23 11.7348 22.8946 11.4804 22.7071 11.2929L11.7071 0.292894C11.3166 -0.0976306 10.6834 -0.0976306 10.2929 0.292894C9.90237 0.683418 9.90237 1.31658 10.2929 1.70711L20.5858 12L10.2929 22.2929C9.90237 22.6834 9.90237 23.3166 10.2929 23.7071C10.6834 24.0976 11.3166 24.0976 11.7071 23.7071L22.7071 12.7071ZM13.7071 12.7071C13.8946 12.5196 14 12.2652 14 12C14 11.7348 13.8946 11.4804 13.7071 11.2929L2.70711 0.292894C2.31658 -0.0976302 1.68342 -0.0976302 1.29289 0.292894C0.902369 0.683419 0.902369 1.31658 1.29289 1.70711L11.5858 12L1.29289 22.2929C0.90237 22.6834 0.90237 23.3166 1.29289 23.7071C1.68342 24.0976 2.31658 24.0976 2.70711 23.7071L13.7071 12.7071Z"
                                        fill="#fff" />
                                </svg>
                {{__('Subscribe')}}
            </button>
        </div>
        <div class="checkbox">
            <label for="subscibecheck" id="{{ $section->subscribe->section->sub_title->slug ?? '' }}_preview">
            {!! $section->subscribe->section->sub_title->text ?? '' !!}
            </label>
        </div>

    </form>