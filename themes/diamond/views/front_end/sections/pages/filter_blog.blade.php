@foreach ($blogs as $key => $blog)
@if($request->cat_id == '0' || $blog->category_id == $request->cat_id)
<div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 blog-itm">
    <div class="blog-inner">
        <div class="blog-img">
            <span class="new-labl bg-second">{{__('Food')}}</span>
            <a href="{{ route('page.article', ['storeSlug'=> $slug,$blog->id]) }}">
                <img src="{{asset($blog->cover_image_path)}}">
            </a>
        </div>
        <div class="blog-content">
            <h3><a href="{{ route('page.article', ['storeSlug'=> $slug,$blog->id]) }}">{{ $blog->title }}</a>
            </h3>
            <p>{{$blog->short_description }}</p>
            <div class="blog-lbl-row d-flex align-items-center justify-content-between">
                <a class="btn-secondary blog-btn" href="{{ route('page.article', ['storeSlug'=> $slug,$blog->id]) }}">
                    {{__('Read more')}}
                </a>
                <div class="author-info">
                    {{-- <strong class="auth-name">{{__('John Doe,')}}</strong> --}}
                    <span class="date">{{$blog->created_at->format('d M,Y ')}}</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@endforeach