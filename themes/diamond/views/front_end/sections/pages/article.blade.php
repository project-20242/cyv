@extends('front_end.layouts.app')
@section('page-title')
{{ __('Article Page') }}
@endsection
@section('content')
@include('front_end.sections.partision.header_section')
@foreach ($blogs as $blog)
<section class="blog-page-banner common-banner-section"
    style="background-image:url({{ asset('themes/' . $currentTheme . '/assets/images/blog-banner.png')}});">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="common-banner-content">
                    <ul class="blog-cat">
                        <li class="active"><a href="#"> {{ __('Featured')}} </a></li>
                        <li><a href="#"><b> {{ __('Category:')}} </b> {{$blog->category->name}}</a></li>
                        <li><a href="#"><b> {{ __('Date:')}} </b> {{$blog->created_at->format('d M, Y ')}}</a></li>
                    </ul>
                    <div class="section-title">
                        <h3>{{$blog->title}}</h3>
                    </div>
                    <p>{{$blog->short_description}}</p>
                    <a href="#" class="btn-secondary white-btn" tabindex="0">
                        {{__('Go to Article')}}
                        <svg viewBox="0 0 10 5">
                            <path
                                d="M2.37755e-08 2.57132C-3.38931e-06 2.7911 0.178166 2.96928 0.397953 2.96928L8.17233 2.9694L7.23718 3.87785C7.07954 4.031 7.07589 4.28295 7.22903 4.44059C7.38218 4.59824 7.63413 4.60189 7.79177 4.44874L9.43039 2.85691C9.50753 2.78197 9.55105 2.679 9.55105 2.57146C9.55105 2.46392 9.50753 2.36095 9.43039 2.28602L7.79177 0.69418C7.63413 0.541034 7.38218 0.544682 7.22903 0.702329C7.07589 0.859976 7.07954 1.11192 7.23718 1.26507L8.1723 2.17349L0.397965 2.17336C0.178179 2.17336 3.46059e-06 2.35153 2.37755e-08 2.57132Z">
                            </path>
                        </svg>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="article-section padding-bottom padding-top">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="about-user d-flex align-items-center">
                    <div class="abt-user-img">
                        <img src="{{asset('themes/'.$currentTheme.'/assets/images/john.png')}}" />
                    </div>
                    <h6>
                        <span>{{__('John Doe')}},</span>
                        {{__('company.com')}}
                    </h6>
                    <div class="post-lbl"><b> {{ __('Category:')}} </b> {{$blog->category->name}}</div>
                    <div class="post-lbl"><b> {{ __('Date:')}} </b> {{$blog->created_at->format('d M, Y ')}}</div>
                </div>
                {{-- <div class="section-title">
                       <h2>{{$blog->title}}</h2>
            </div> --}}
        </div>
        <div class="col-md-8 col-12">
            <div class="aticleleftbar">
                {!! html_entity_decode($blog->content) !!}
                <div class="art-auther">
                    <div class="art-auther"><b>{{__('Tags:')}} </b> {{$blog->category->name}}</div>
                </div>

                <ul class="article-socials d-flex align-items-center">
                    <h2>{{__('Share:') }}</h2>

                    @for($i=0 ; $i < $section->footer->section->footer_link->loop_number ?? 1;$i++) 
                    <li>
                            <a href="{!!$section->footer->section->footer_link->social_link->{$i} ?? '#' !!}">
                                <img src=" {{  asset($section->footer->section->footer_link->social_icon->{$i}->image ?? 'themes/' . $currentTheme . '/assets/images/youtube.svg') }}"
                                    alt="youtube">
                            </a>

                        </li>
                        @endfor
                </ul>
            </div>
        </div>
        <div class="col-md-4 col-12">
            <div class="articlerightbar">
                <div class="section-title">
                    <h3>{{ __('Related articles')}}</h3>
                </div>
                <div class="row blog-grid">
                    @foreach ($datas->take(2) as $data)

                    <div class="col-md-12 col-sm-6 col-12 blog-widget">
                        <div class="blog-card">
                            <div class="blog-card-inner">
                                <div class="blog-card-image">
                                    <a href="{{route('page.article',[$slug,$data->id])}}">
                                        <img src="{{asset($data->cover_image_path)}}" class="default-img">
                                    </a>
                                </div>
                                <div class="blog-card-content">
                                    <span class="label">{{$blog->category->name}}</span>
                                    <h3>
                                        <a href="{{route('page.article',[$slug,$data->id])}}">
                                            {{$data->title}}
                                        </a>
                                    </h3>
                                    <p>{{$data->short_description}}</p>
                                    <div class="blog-card-bottom">
                                        <a href="{{route('page.article',[$slug,$data->id])}}" class=" btn">
                                            {{__(' Read More')}}
                                        </a>
                                        
                                        <span class="date">{{__('DATE:')}} {{$blog->created_at->format('d M,Y ')}}
                                            <br>
                                            <span>{{__('AUTHOR')}}: {{__('JOHN DOE')}}</span>
                                        </span>
                                    </div>
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endforeach

@include('front_end.sections.partision.footer_section')
@endsection