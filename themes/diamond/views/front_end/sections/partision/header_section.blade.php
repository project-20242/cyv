<!--header start here-->
<header class="site-header header-style-one"
    style="@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide  ?? '' }}" data-section="{{ $option->section_name  ?? '' }}"
    data-store="{{ $option->store_id  ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">

        <div class="custome_tool_bar"></div>
        <div class="announcebar">
            <div class="container">
                <div class="announce-row row align-items-center">
                    <div class="annoucebar-left col-6 d-flex">
                        <p id="{{ $section->header->section->title->slug ?? '' }}_preview">{!!
                            $section->header->section->title->text ?? '' !!}</p>
                    </div>
                    <div class="announcebar-right col-6 d-flex justify-content-end">
                        <button id="announceclose">
                            <svg xmlns="http://www.w3.org/2000/svg" aria-hidden="true" focusable="false"
                                role="presentation" class="icon icon-close" viewBox="0 0 18 17">
                                <path
                                    d="M.865 15.978a.5.5 0 00.707.707l7.433-7.431 7.579 7.282a.501.501 0 00.846-.37.5.5 0 00-.153-.351L9.712 8.546l7.417-7.416a.5.5 0 10-.707-.708L8.991 7.853 1.413.573a.5.5 0 10-.693.72l7.563 7.268-7.418 7.417z"
                                    fill="#000000">
                                </path>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="main-navigationbar">
            <div class="container">
                <div class="navigationbar-row d-flex align-items-center">
                    <div class="menu-items-col">
                        <div class="logo-col  mobile-only">
                            <h1>
                                <a href="{{route('landing_page',$slug)}}">
                                    <img src="{{ asset((isset($theme_logo) && !empty($theme_logo)) ? $theme_logo : 'themes/'.$currentTheme.'/assets/images/logo.png') }}"
                                        alt="d">
                                </a>
                            </h1>
                        </div>
                        <ul class="main-nav">
                        @if (!empty($topNavItems))
                            @foreach ($topNavItems as $key => $nav)
                                @if (!empty($nav->children[0]))
                                    <li class="menu-lnk has-item">
                                        <a href="#">
                                            @if ($nav->title == null)
                                                {{ $nav->title }}
                                            @else
                                                {{ $nav->title }}
                                            @endif
                                        </a>
                                        <div class="menu-dropdown">
                                            <ul>
                                                @foreach ($nav->children[0] as $childNav)
                                                    @if ($childNav->type == 'custom')
                                                        <li><a href="{{ url($childNav->slug) }}" target="{{ $childNav->target }}">
                                                            @if ($childNav->title == null)
                                                                {{ $childNav->title }}
                                                            @else
                                                                {{ $childNav->title }}
                                                            @endif
                                                        </a></li>
                                                    @elseif($childNav->type == 'category')
                                                        <li><a href="{{ url($slug.'/'.$childNav->slug) }}" target="{{ $childNav->target }}">
                                                            @if ($childNav->title == null)
                                                                {{ $childNav->title }}
                                                            @else
                                                                {{ $childNav->title }}
                                                            @endif
                                                        </a></li>
                                                    @else
                                                        <li><a href="{{ url($slug.'/'.$childNav->slug) }}" target="{{ $childNav->target }}">
                                                            @if ($childNav->title == null)
                                                                {{ $childNav->title }}
                                                            @else
                                                                {{ $childNav->title }}
                                                            @endif
                                                        </a></li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                        </div>
                                    </li>
                                @else
                                    @if ($nav->type == 'custom')
                                        <li class="">
                                            <a href="{{ url($nav->slug) }}" target="{{ $nav->target }}">
                                                @if ($nav->title == null)
                                                    {{ $nav->title }}
                                                @else
                                                    {{ $nav->title }}
                                                @endif
                                            </a>
                                        </li>
                                    @elseif($nav->type == 'category')
                                        <li class="">
                                            <a href="{{  url($slug.'/'.$nav->slug) }}" target="{{ $nav->target }}" target="{{ $nav->target }}">
                                                @if ($nav->title == null)
                                                    {{ $nav->title }}
                                                @else
                                                    {{ $nav->title }}
                                                @endif
                                            </a>
                                        </li>
                                    @else
                                        <li class="">
                                            <a href="{{  url($slug.'/'.$nav->slug) }}"
                                                target="{{ $nav->target }}">
                                                @if ($nav->title == null)
                                                    {{ $nav->title }}
                                                @else
                                                    {{ $nav->title }}
                                                @endif
                                            </a>
                                        </li>
                                    @endif
                                @endif
                            @endforeach
                        @endif
                            <li class="menu-lnk has-item">
                                <a href="javascript:void()">
                                    {{__(' Pages')}}
                                </a>
                                <div class="menu-dropdown">
                                    <ul>
                                        <li><a href="{{route('page.faq',['storeSlug' => $slug])}}">{{__('FAQs')}}</a>
                                        </li>
                                        <li><a href="{{route('page.blog',['storeSlug' => $slug])}}">{{__('Blog')}}</a>
                                        </li>
                                        <li><a
                                                href="{{route('page.product-list',['storeSlug' => $slug])}}">{{__('Collection')}}</a>
                                        @if(isset($pages))
                                            @foreach ($pages as $page)
                                            <li><a
                                                    href="{{ route('custom.page',[$slug, $page->page_slug]) }}">{{$page->name}}</a>
                                            </li>
                                            @endforeach
                                        @endif
                                    </ul>
                                </div>

                            </li>
                        </ul>
                        <div class="logo-col desk-only">
                            <h1>
                                <a href="{{route('landing_page',$slug)}}">
                                    <img src="{{ asset(isset($theme_logo) && !empty($theme_logo) ? $theme_logo : 'themes/'.$currentTheme.'/assets/images/logo.png') }}"
                                        alt="d">
                                </a>
                            </h1>
                        </div>
                        <ul class="menu-right d-flex  justify-content-end">
                            @auth('customers')
                            <li class="search-header">
                                <a href="javascript:;">
                                    <span class="icon-lable">{{ __('Search') }}</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="13" height="13"
                                        viewBox="0 0 13 13" fill="none">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M9.47487 10.5131C8.48031 11.2863 7.23058 11.7466 5.87332 11.7466C2.62957 11.7466 0 9.11706 0 5.87332C0 2.62957 2.62957 0 5.87332 0C9.11706 0 11.7466 2.62957 11.7466 5.87332C11.7466 7.23058 11.2863 8.48031 10.5131 9.47487L12.785 11.7465C13.0717 12.0332 13.0717 12.4981 12.785 12.7848C12.4983 13.0715 12.0334 13.0715 11.7467 12.7848L9.47487 10.5131ZM10.2783 5.87332C10.2783 8.30612 8.30612 10.2783 5.87332 10.2783C3.44051 10.2783 1.46833 8.30612 1.46833 5.87332C1.46833 3.44051 3.44051 1.46833 5.87332 1.46833C8.30612 1.46833 10.2783 3.44051 10.2783 5.87332Z"
                                            fill="#173334" />
                                    </svg>
                                </a>
                            </li>
                            <li class="profile-header menu-lnk has-item">
                                <a href="javascript:;">
                                <span>{{ __('My Profile') }}</span>
                                <svg class="myprofile-icon" xmlns="http://www.w3.org/2000/svg" width="16"
                                    height="22" viewBox="0 0 16 22" fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M13.3699 21.0448H4.60183C4.11758 21.0448 3.72502 20.6522 3.72502 20.168C3.72502 19.6837 4.11758 19.2912 4.60183 19.2912H13.3699C13.8542 19.2912 14.2468 18.8986 14.2468 18.4143V14.7756C14.2026 14.2836 13.9075 13.8492 13.4664 13.627C10.0296 12.2394 6.18853 12.2394 2.75176 13.627C2.31062 13.8492 2.01554 14.2836 1.9714 14.7756V20.168C1.9714 20.6522 1.57883 21.0448 1.09459 21.0448C0.610335 21.0448 0.217773 20.6522 0.217773 20.168V14.7756C0.256548 13.5653 0.986136 12.4845 2.09415 11.9961C5.95255 10.4369 10.2656 10.4369 14.124 11.9961C15.232 12.4845 15.9616 13.5653 16.0004 14.7756V18.4143C16.0004 19.8671 14.8227 21.0448 13.3699 21.0448ZM12.493 4.38406C12.493 1.96281 10.5302 0 8.10892 0C5.68767 0 3.72486 1.96281 3.72486 4.38406C3.72486 6.80531 5.68767 8.76812 8.10892 8.76812C10.5302 8.76812 12.493 6.80531 12.493 4.38406ZM10.7393 4.38483C10.7393 5.83758 9.56159 7.01526 8.10884 7.01526C6.6561 7.01526 5.47841 5.83758 5.47841 4.38483C5.47841 2.93208 6.6561 1.75439 8.10884 1.75439C9.56159 1.75439 10.7393 2.93208 10.7393 4.38483Z"
                                        fill="#183A40"></path>
                                </svg>
                                </a>
                                <div class="menu-dropdown">
                                    <ul>
                                        <li><a href="{{ route('my-account.index',$slug) }}"
                                                class="account">{{__('My Account')}}</a></li>
                                        <li>
                                            <a href="javascript:;" title="wish" class="wish-header">
                                                {{__('Wishlist')}}
                                            </a>
                                        </li>
                                        <li>
                                            <form method="POST" action="{{ route('customer.logout',$slug) }}"
                                                id="form_logout">
                                                @csrf
                                                <a href="#"
                                                    onclick="event.preventDefault(); this.closest('form').submit();"
                                                    class="account">{{__('Log Out')}}
                                                </a>
                                            </form>
                                        </li>
                                      
                                    </ul>
                                </div>
                            </li>
                            @endauth
                            @guest('customers')
                            <li class="search-header">
                            <span class="icon-lable">{{ __('Search') }}</span>
                                <a href="javascript:;">
                                    <svg width="11" height="11" viewBox="0 0 11 11" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M8.01711 8.89563C7.17556 9.5498 6.11811 9.93935 4.96967 9.93935C2.225 9.93935 0 7.71435 0 4.96967C0 2.225 2.225 0 4.96967 0C7.71435 0 9.93935 2.225 9.93935 4.96967C9.93935 6.11811 9.5498 7.17556 8.89563 8.01711L10.8179 9.93925C11.0605 10.1819 11.0605 10.5752 10.8179 10.8178C10.5753 11.0604 10.182 11.0604 9.93941 10.8178L8.01711 8.89563ZM8.69693 4.96967C8.69693 7.02818 7.02818 8.69693 4.96967 8.69693C2.91117 8.69693 1.24242 7.02818 1.24242 4.96967C1.24242 2.91117 2.91117 1.24242 4.96967 1.24242C7.02818 1.24242 8.69693 2.91117 8.69693 4.96967Z"
                                            fill="#12131A" />
                                    </svg>
                                </a>
                            </li>
                            <li class="profile-header">
                                <a href="{{ route('customer.login',$slug) }}">
                                    <svg width="11" height="11" viewBox="0 0 11 11" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M5.5 5.95841C3.72809 5.95841 2.29167 7.39483 2.29167 9.16675V10.0834C2.29167 10.3365 2.08646 10.5417 1.83333 10.5417C1.5802 10.5417 1.375 10.3365 1.375 10.0834V9.16675C1.375 6.88857 3.22183 5.04175 5.5 5.04175C7.77817 5.04175 9.625 6.88857 9.625 9.16675V10.0834C9.625 10.3365 9.4198 10.5417 9.16667 10.5417C8.91354 10.5417 8.70833 10.3365 8.70833 10.0834V9.16675C8.70833 7.39483 7.27191 5.95841 5.5 5.95841Z"
                                            fill="#12131A" />
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M5.5 5.04159C6.51252 5.04159 7.33333 4.22077 7.33333 3.20825C7.33333 2.19573 6.51252 1.37492 5.5 1.37492C4.48748 1.37492 3.66667 2.19573 3.66667 3.20825C3.66667 4.22077 4.48748 5.04159 5.5 5.04159ZM5.5 5.95825C7.01878 5.95825 8.25 4.72704 8.25 3.20825C8.25 1.68947 7.01878 0.458252 5.5 0.458252C3.98122 0.458252 2.75 1.68947 2.75 3.20825C2.75 4.72704 3.98122 5.95825 5.5 5.95825Z"
                                            fill="#12131A" />
                                    </svg>
                                </a>
                            </li>
                            @endguest
                            <li class="cart-header">
                                <a href="javascript:;"  class="btn">
                                    <span class="count">{!! \App\Models\Cart::CartCount($slug) !!}
                                    </span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="15" height="13"
                                    viewBox="0 0 15 13" fill="none">
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                        d="M12.2919 8.13529H5.50403C4.58559 8.13555 3.79748 7.48102 3.6291 6.57815L2.74566 1.79231C2.68975 1.48654 2.42089 1.26607 2.11009 1.27114H0.63557C0.284554 1.27114 0 0.986585 0 0.63557C0 0.284554 0.284554 7.71206e-08 0.63557 7.71206e-08H2.1228C3.04124 -0.000259399 3.82935 0.654274 3.99773 1.55715L4.88117 6.34299C4.93708 6.64875 5.20595 6.86922 5.51674 6.86415H12.2983C12.6091 6.86922 12.8779 6.64875 12.9338 6.34299L13.7347 2.02111C13.769 1.8338 13.7175 1.64099 13.5944 1.49571C13.4712 1.35044 13.2895 1.26802 13.0991 1.27114H5.72013C5.36911 1.27114 5.08456 0.986585 5.08456 0.63557C5.08456 0.284554 5.36911 7.71206e-08 5.72013 7.71206e-08H13.0927C13.6597 -0.000160116 14.1974 0.252022 14.5597 0.688096C14.9221 1.12417 15.0716 1.6989 14.9677 2.25627L14.1668 6.57815C13.9985 7.48102 13.2104 8.13555 12.2919 8.13529ZM8.26269 10.678C8.26269 9.62499 7.40902 8.77133 6.35598 8.77133C6.00496 8.77133 5.72041 9.05588 5.72041 9.4069C5.72041 9.75791 6.00496 10.0425 6.35598 10.0425C6.70699 10.0425 6.99155 10.327 6.99155 10.678C6.99155 11.0291 6.70699 11.3136 6.35598 11.3136C6.00496 11.3136 5.72041 11.0291 5.72041 10.678C5.72041 10.327 5.43585 10.0425 5.08484 10.0425C4.73382 10.0425 4.44927 10.327 4.44927 10.678C4.44927 11.7311 5.30293 12.5847 6.35598 12.5847C7.40902 12.5847 8.26269 11.7311 8.26269 10.678ZM12.0765 11.9491C12.0765 11.5981 11.7919 11.3135 11.4409 11.3135C11.0899 11.3135 10.8053 11.029 10.8053 10.6779C10.8053 10.3269 11.0899 10.0424 11.4409 10.0424C11.7919 10.0424 12.0765 10.3269 12.0765 10.6779C12.0765 11.029 12.361 11.3135 12.712 11.3135C13.063 11.3135 13.3476 11.029 13.3476 10.6779C13.3476 9.6249 12.4939 8.77124 11.4409 8.77124C10.3878 8.77124 9.53418 9.6249 9.53418 10.6779C9.53418 11.731 10.3878 12.5847 11.4409 12.5847C11.7919 12.5847 12.0765 12.3001 12.0765 11.9491Z"
                                        fill="#F2DFCE" />
                                    </svg>
                                </a>
                            </li>
                            <li class="menu-lnk has-item lang-dropdown">
                                <a href="#">
                                    <span class="drp-text">{{ Str::upper($currantLang) }}</span>
                                    <div class="lang-icn">
                                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:svg="http://www.w3.org/2000/svg"
                                            version="1.1" id="svg2223" xml:space="preserve" width="682.66669"
                                            height="682.66669" viewBox="0 0 682.66669 682.66669">
                                            <g id="g2229" transform="matrix(1.3333333,0,0,-1.3333333,0,682.66667)">
                                                <g id="g2231">
                                                    <g id="g2233" clip-path="url(#clipPath2237)">
                                                        <g id="g2239" transform="translate(497,256)">
                                                            <path
                                                                d="m 0,0 c 0,-132.548 -108.452,-241 -241,-241 -132.548,0 -241,108.452 -241,241 0,132.548 108.452,241 241,241 C -108.452,241 0,132.548 0,0 Z"
                                                                style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2241" />
                                                        </g>
                                                        <g id="g2243" transform="translate(376,256)">
                                                            <path
                                                                d="m 0,0 c 0,-132.548 -53.726,-241 -120,-241 -66.274,0 -120,108.452 -120,241 0,132.548 53.726,241 120,241 C -53.726,241 0,132.548 0,0 Z"
                                                                style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2245" />
                                                        </g>
                                                        <g id="g2247" transform="translate(256,497)">
                                                            <path d="M 0,0 V -482"
                                                                style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2249" />
                                                        </g>
                                                        <g id="g2251" transform="translate(15,256)">
                                                            <path d="M 0,0 H 482"
                                                                style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2253" />
                                                        </g>
                                                        <g id="g2255" transform="translate(463.8926,136)">
                                                            <path d="M 0,0 H -415.785"
                                                                style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2257" />
                                                        </g>
                                                        <g id="g2259" transform="translate(48.1079,377)">
                                                            <path d="M 0,0 H 415.785"
                                                                style="fill:none;stroke:#000000;stroke-width:30;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                                                id="path2261" />
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </svg>
                                    </div>
                                </a>
                                <div class="menu-dropdown">
                                    <ul>
                                        @foreach ($languages as $code => $language)
                                        <li><a href="{{ route('change.languagestore', [$code]) }}"
                                                class="@if ($language == $currantLang) active-language text-primary @endif">{{ ucFirst($language) }}</a>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        <div class="mobile-menu">
                            <button class="mobile-menu-button" id="menu">
                                <div class="one"></div>
                                <div class="two"></div>
                                <div class="three"></div>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mobile menu start here -->
        <div class="mobile-menu-wrapper">
            <div class="menu-close-icon">
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="18" viewBox="0 0 20 18">
                    <path fill="#24272a"
                        d="M19.95 16.75l-.05-.4-1.2-1-5.2-4.2c-.1-.05-.3-.2-.6-.5l-.7-.55c-.15-.1-.5-.45-1-1.1l-.1-.1c.2-.15.4-.35.6-.55l1.95-1.85 1.1-1c1-1 1.7-1.65 2.1-1.9l.5-.35c.4-.25.65-.45.75-.45.2-.15.45-.35.65-.6s.3-.5.3-.7l-.3-.65c-.55.2-1.2.65-2.05 1.35-.85.75-1.65 1.55-2.5 2.5-.8.9-1.6 1.65-2.4 2.3-.8.65-1.4.95-1.9 1-.15 0-1.5-1.05-4.1-3.2C3.1 2.6 1.45 1.2.7.55L.45.1c-.1.05-.2.15-.3.3C.05.55 0 .7 0 .85l.05.35.05.4 1.2 1 5.2 4.15c.1.05.3.2.6.5l.7.6c.15.1.5.45 1 1.1l.1.1c-.2.15-.4.35-.6.55l-1.95 1.85-1.1 1c-1 1-1.7 1.65-2.1 1.9l-.5.35c-.4.25-.65.45-.75.45-.25.15-.45.35-.65.6-.15.3-.25.55-.25.75l.3.65c.55-.2 1.2-.65 2.05-1.35.85-.75 1.65-1.55 2.5-2.5.8-.9 1.6-1.65 2.4-2.3.8-.65 1.4-.95 1.9-1 .15 0 1.5 1.05 4.1 3.2 2.6 2.15 4.3 3.55 5.05 4.2l.2.45c.1-.05.2-.15.3-.3.1-.15.15-.3.15-.45z" />
                </svg>
            </div>
            <div class="mobile-menu-bar">
                <ul>
                    <li class="mobile-item has-children">
                        <a href="#" class="acnav-label">
                            {{__('Shop All')}}
                            <svg class="menu-open-arrow" xmlns="http://www.w3.org/2000/svg" width="20" height="11"
                                viewBox="0 0 20 11">
                                <path fill="#24272a"
                                    d="M.268 1.076C.373.918.478.813.584.76l.21.474c.79.684 2.527 2.158 5.21 4.368 2.738 2.21 4.159 3.316 4.264 3.316.474-.053 1.158-.369 1.947-1.053.842-.631 1.632-1.42 2.474-2.368.895-.948 1.737-1.842 2.632-2.58.842-.789 1.578-1.262 2.105-1.42l.316.684c0 .21-.106.474-.316.737-.053.21-.263.421-.474.579-.053.052-.316.21-.737.474l-.526.368c-.421.263-1.105.947-2.158 2l-1.105 1.053-2.053 1.947c-1 .947-1.579 1.421-1.842 1.421-.263 0-.684-.263-1.158-.895-.526-.631-.842-1-1.052-1.105l-.737-.579c-.316-.316-.527-.474-.632-.474l-5.42-4.315L.267 2.339l-.105-.421-.053-.369c0-.157.053-.315.158-.473z" />
                            </svg>
                            <svg class="close-menu-ioc" xmlns="http://www.w3.org/2000/svg" width="20" height="18"
                                viewBox="0 0 20 18">
                                <path fill="#24272a"
                                    d="M19.95 16.75l-.05-.4-1.2-1-5.2-4.2c-.1-.05-.3-.2-.6-.5l-.7-.55c-.15-.1-.5-.45-1-1.1l-.1-.1c.2-.15.4-.35.6-.55l1.95-1.85 1.1-1c1-1 1.7-1.65 2.1-1.9l.5-.35c.4-.25.65-.45.75-.45.2-.15.45-.35.65-.6s.3-.5.3-.7l-.3-.65c-.55.2-1.2.65-2.05 1.35-.85.75-1.65 1.55-2.5 2.5-.8.9-1.6 1.65-2.4 2.3-.8.65-1.4.95-1.9 1-.15 0-1.5-1.05-4.1-3.2C3.1 2.6 1.45 1.2.7.55L.45.1c-.1.05-.2.15-.3.3C.05.55 0 .7 0 .85l.05.35.05.4 1.2 1 5.2 4.15c.1.05.3.2.6.5l.7.6c.15.1.5.45 1 1.1l.1.1c-.2.15-.4.35-.6.55l-1.95 1.85-1.1 1c-1 1-1.7 1.65-2.1 1.9l-.5.35c-.4.25-.65.45-.75.45-.25.15-.45.35-.65.6-.15.3-.25.55-.25.75l.3.65c.55-.2 1.2-.65 2.05-1.35.85-.75 1.65-1.55 2.5-2.5.8-.9 1.6-1.65 2.4-2.3.8-.65 1.4-.95 1.9-1 .15 0 1.5 1.05 4.1 3.2 2.6 2.15 4.3 3.55 5.05 4.2l.2.45c.1-.05.2-.15.3-.3.1-.15.15-.3.15-.45z" />
                            </svg>
                        </a>
                        @if ($has_subcategory)
                        <ul class="mobile_menu_inner acnav-list">
                            @foreach ($categories as $category)
                            <li class="menu-h-link">
                                <ul>
                                    <li>
                                        <span>{{$category->name}}
                                    </li>
                                    <li>
                                        <a
                                            href="{{route('page.product-list',[$slug,'main_category' => $category->id ])}}">{{ __('All') }}</a>
                                    </li>
                                    @foreach ($SubCategoryList as $cat)
                                    @if ($cat->maincategory_id == $category->id)
                                    <li><a
                                            href="{{route('page.product-list',[$slug,'main_category' => $category->id,'sub_category' => $cat->id ])}}">{{$cat->name}}</a>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                            </li>
                            @endforeach
                        </ul>
                        @else
                        <ul class="mobile_menu_inner acnav-list">
                            <li class="menu-h-link">
                                <ul>
                                    @foreach ($categories as $category)
                                    <li>
                                        <a
                                            href="{{route('page.product-list',[$slug,'main_category' => $category->id ])}}">{{$category->name}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                        @endif
                    </li>
                    <li class="mobile-item">
                        <a href="{{route('page.product-list',$slug)}}"> {{ __('Collection')}} </a>
                    </li>
                    <li class="mobile-item has-children">
                        <a href="#" class="acnav-label">
                            {{__('Pages')}}
                            <svg class="menu-open-arrow" xmlns="http://www.w3.org/2000/svg" width="20" height="11"
                                viewBox="0 0 20 11">
                                <path fill="#24272a"
                                    d="M.268 1.076C.373.918.478.813.584.76l.21.474c.79.684 2.527 2.158 5.21 4.368 2.738 2.21 4.159 3.316 4.264 3.316.474-.053 1.158-.369 1.947-1.053.842-.631 1.632-1.42 2.474-2.368.895-.948 1.737-1.842 2.632-2.58.842-.789 1.578-1.262 2.105-1.42l.316.684c0 .21-.106.474-.316.737-.053.21-.263.421-.474.579-.053.052-.316.21-.737.474l-.526.368c-.421.263-1.105.947-2.158 2l-1.105 1.053-2.053 1.947c-1 .947-1.579 1.421-1.842 1.421-.263 0-.684-.263-1.158-.895-.526-.631-.842-1-1.052-1.105l-.737-.579c-.316-.316-.527-.474-.632-.474l-5.42-4.315L.267 2.339l-.105-.421-.053-.369c0-.157.053-.315.158-.473z" />
                            </svg>
                            <svg class="close-menu-ioc" xmlns="http://www.w3.org/2000/svg" width="20" height="18"
                                viewBox="0 0 20 18">
                                <path fill="#24272a"
                                    d="M19.95 16.75l-.05-.4-1.2-1-5.2-4.2c-.1-.05-.3-.2-.6-.5l-.7-.55c-.15-.1-.5-.45-1-1.1l-.1-.1c.2-.15.4-.35.6-.55l1.95-1.85 1.1-1c1-1 1.7-1.65 2.1-1.9l.5-.35c.4-.25.65-.45.75-.45.2-.15.45-.35.65-.6s.3-.5.3-.7l-.3-.65c-.55.2-1.2.65-2.05 1.35-.85.75-1.65 1.55-2.5 2.5-.8.9-1.6 1.65-2.4 2.3-.8.65-1.4.95-1.9 1-.15 0-1.5-1.05-4.1-3.2C3.1 2.6 1.45 1.2.7.55L.45.1c-.1.05-.2.15-.3.3C.05.55 0 .7 0 .85l.05.35.05.4 1.2 1 5.2 4.15c.1.05.3.2.6.5l.7.6c.15.1.5.45 1 1.1l.1.1c-.2.15-.4.35-.6.55l-1.95 1.85-1.1 1c-1 1-1.7 1.65-2.1 1.9l-.5.35c-.4.25-.65.45-.75.45-.25.15-.45.35-.65.6-.15.3-.25.55-.25.75l.3.65c.55-.2 1.2-.65 2.05-1.35.85-.75 1.65-1.55 2.5-2.5.8-.9 1.6-1.65 2.4-2.3.8-.65 1.4-.95 1.9-1 .15 0 1.5 1.05 4.1 3.2 2.6 2.15 4.3 3.55 5.05 4.2l.2.45c.1-.05.2-.15.3-.3.1-.15.15-.3.15-.45z" />
                            </svg>
                        </a>
                        <ul class="mobile_menu_inner acnav-list">
                            <li class="menu-h-link">
                                <ul>
                                    @foreach ($pages as $page)
                                    <li><a
                                            href="{{ route('custom.page',[$slug, $page->page_slug]) }}">{{$page->name}}</a>
                                    </li>
                                    @endforeach
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="mobile-item">
                        <a href="#">
                            {{ __('Contact') }}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <!-- Mobile menu end here -->
        <div class="search-popup">
            <div class="close-search">
                <svg xmlns="http://www.w3.org/2000/svg" width="50" height="50" viewBox="0 0 50 50" fill="none">
                    <path
                        d="M27.7618 25.0008L49.4275 3.33503C50.1903 2.57224 50.1903 1.33552 49.4275 0.572826C48.6647 -0.189868 47.428 -0.189965 46.6653 0.572826L24.9995 22.2386L3.33381 0.572826C2.57102 -0.189965 1.3343 -0.189965 0.571605 0.572826C-0.191089 1.33562 -0.191186 2.57233 0.571605 3.33503L22.2373 25.0007L0.571605 46.6665C-0.191186 47.4293 -0.191186 48.666 0.571605 49.4287C0.952952 49.81 1.45285 50.0007 1.95275 50.0007C2.45266 50.0007 2.95246 49.81 3.3339 49.4287L24.9995 27.763L46.6652 49.4287C47.0465 49.81 47.5464 50.0007 48.0463 50.0007C48.5462 50.0007 49.046 49.81 49.4275 49.4287C50.1903 48.6659 50.1903 47.4292 49.4275 46.6665L27.7618 25.0008Z"
                        fill="white"></path>
                </svg>
            </div>
            <div class="search-form-wrapper">
                <form>
                    <div class="form-inputs">
                        <input type="search" placeholder="Buscar Productos..." class="form-control search_input"
                            list="products" name="search_product" id="product">
                        <datalist id="products">
                        @foreach ($search_products as $pro_id => $pros)
                            <option value="{{$pros}}"></option>
                        @endforeach
                        </datalist>

                        <button type="submit" class="btn search_product_globaly">
                            <svg>
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M0.000169754 6.99457C0.000169754 10.8576 3.13174 13.9891 6.99473 13.9891C8.60967 13.9891 10.0968 13.4418 11.2807 12.5226C11.3253 12.6169 11.3866 12.7053 11.4646 12.7834L17.0603 18.379C17.4245 18.7432 18.015 18.7432 18.3792 18.379C18.7434 18.0148 18.7434 17.4243 18.3792 17.0601L12.7835 11.4645C12.7055 11.3864 12.6171 11.3251 12.5228 11.2805C13.442 10.0966 13.9893 8.60951 13.9893 6.99457C13.9893 3.13157 10.8577 0 6.99473 0C3.13174 0 0.000169754 3.13157 0.000169754 6.99457ZM1.86539 6.99457C1.86539 4.1617 4.16187 1.86522 6.99473 1.86522C9.8276 1.86522 12.1241 4.1617 12.1241 6.99457C12.1241 9.82743 9.8276 12.1239 6.99473 12.1239C4.16187 12.1239 1.86539 9.82743 1.86539 6.99457Z">
                                </path>
                            </svg>
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!--header end here-->
    </header>

    @push('page-script')
    @endpush
