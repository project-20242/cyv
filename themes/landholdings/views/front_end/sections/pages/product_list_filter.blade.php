<div class="row">
    @foreach ($products as $product)
    <div class="col-lg-4 col-xl-4 col-md-4 col-sm-6 col-12 product-card">
    <div class="product-card-inner no-back">
        <div class="product-card-image">
            <a href="{{url($slug.'/product/'.$product->slug)}}">
                <img src="{{ asset('storage/app/public/' . $product->cover_image_path) }}" alt="Cover Image" width="100" height="100" class="default-img">
                @if($product->Sub_image($product->id)['status'] == true)
                    <img src="{{ asset($product->Sub_image($product->id)['data'][0]->image_path) }}" class="hover-img">
                @else
                    <img src="{{ asset($product->Sub_image($product->id)) }}" class="hover-img">
                @endif
            </a>
            <div class="new-labl">
            {{!empty($product->ProductData()) ? $product->ProductData->name : ''}}
            </div>
        </div>
        <div class="product-content">
            <div class="product-content-top ">

                {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $product->id) !!}

                <h3 class="product-title">
                    <a href="{{url($slug.'/product/'.$product->slug)}}">
                        {{$product->name}}
                    </a>
                </h3>
                <div class="reviews-stars-wrap d-flex align-items-center justify-content-center">
                    @if(!empty($product->average_rating))
                        @for ($i = 0; $i < 5; $i++)
                            <i class="fa fa-star {{ $i < $product->average_rating ? '' : 'text-warning' }} "></i>
                        @endfor
                        <span class="review-gap"><b>{{ $product->average_rating }}.0</b> / 5.0</span>
                    @else
                        @for ($i = 0; $i < 5; $i++)
                            <i class="fa fa-star {{ $i < $product->average_rating ? '' : 'text-warning' }} "></i>
                        @endfor
                        <span class="review-gap"><b>{{ $product->average_rating }}.0</b> / 5.0</span>
                    @endif
                </div>
            </div>
            <div class="product-content-center">
                @if ($product->variant_product == 0)
                    <div class="price">
                        <ins>{{ currency_format_with_sym(($product->sale_price ?? $product->price) , $store->id, $currentTheme) }}</ins>
                        <del>{{$product->price}} {{$currency}}</del>
                    </div>
                @else
                    <div class="price">
                        <ins>{{ __('In Variant') }}</ins>
                    </div>
                @endif
            </div>
            <div class="product-content-bottom d-flex align-items-center justify-content-between">
                <div class="bottom-select  d-flex align-items-center justify-content-between">
                    <div class="cart-btn-wrap">
                        <button class="btn addcart-btn-globaly" product_id="{{ $product->id }}" variant_id="0" qty="1">
                            <span> {{ __('Add to cart')}}</span>
                            <svg xmlns="http://www.w3.org/2000/svg" width="4" height="6" viewBox="0 0 4 6" fill="none">
                                <path fill-rule="evenodd" clip-rule="evenodd" d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z" fill="white"></path>
                            </svg>
                        </button>
                    </div>
                </div>
                    <button href="javascript:void(0)" class="wishlist-btn wbwish  wishbtn-globaly" product_id="{{$product->id}}" in_wishlist="{{ $product->in_whishlist ? 'remove' : 'add'}}">
                        <span class="wish-ic">
                            <i class="{{ $product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"
                                style='color: #000000'></i>
                        </span>
                        <div class="product-btn-wrp">
                            @php
                                $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                            @endphp
                            @if(isset($module) && $module->isEnabled())
                                {{-- Include the module blade button --}}
                                @include('productquickview::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                            @endif
                            @if(isset($compare_module) && $compare_module->isEnabled())
                                {{-- Include the module blade button --}}
                                @include('productcompare::pages.button', ['product_slug' => $product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                            @endif
                        </div>
                    </button>
            </div>
        </div>
    </div>
</div>

    @endforeach
</div>

@php
$page_no = !empty($page) ? $page : 1;
@endphp
<div class="d-flex justify-content-end col-12">
    <nav class="dataTable-pagination">
        <ul class="dataTable-pagination-list">
            <li class="pagination">
                {{ $products->onEachSide(0)->links('pagination::bootstrap-4') }}
            </li>
        </ul>
    </nav>
</div>
