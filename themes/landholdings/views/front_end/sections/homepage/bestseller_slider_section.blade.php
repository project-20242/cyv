
<section class="today-discounts padding-bottom" style="position: relative;@if (isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"
    data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
        <div class="tabs-wrapper">
            <div class="section-title d-flex align-items-center justify-content-between">

                    <h2 class="title title-white" id="{{ $section->bestseller_slider->section->title->slug ?? '' }}_preview">{!! $section->bestseller_slider->section->title->text ?? '' !!}</h2>

                <ul class="tabs d-flex">
                    @foreach ($category_options as $cat_key => $category)
                        <li class="tab-link  {{ $cat_key == 0 ? 'active' : '' }}"
                            data-tab="{{ $cat_key }}">
                            <a href="javascript:;">
                                <span class="tab-icon">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="19" height="19"
                                        viewBox="0 0 19 19" fill="none">
                                        <g clip-path="url(#clip0_1_8022)">
                                            <path
                                                d="M18.6561 7.66276L9.71775 1.11831C9.54539 0.992137 9.31123 0.992137 9.13894 1.11831L0.200557 7.66276C-0.0177672 7.82264 -0.0652033 8.1292 0.0946725 8.34753C0.254548 8.56585 0.561152 8.61321 0.779439 8.45341L9.42831 2.12084L18.0772 8.45337C18.1644 8.51727 18.2657 8.54803 18.3662 8.54803C18.5171 8.54803 18.6659 8.4786 18.7619 8.34749C18.9218 8.1292 18.8744 7.82264 18.6561 7.66276Z"
                                                fill="white"></path>
                                            <path
                                                d="M16.2876 8.56421C16.0171 8.56421 15.7977 8.78356 15.7977 9.05415V16.8527H11.8782V12.5958C11.8782 11.2449 10.7792 10.1459 9.42834 10.1459C8.07752 10.1459 6.97846 11.2449 6.97846 12.5958V16.8527H3.05898V9.05419C3.05898 8.7836 2.83959 8.56425 2.56904 8.56425C2.29849 8.56425 2.0791 8.7836 2.0791 9.05419V17.3427C2.0791 17.6133 2.29849 17.8327 2.56904 17.8327H7.4684C7.72606 17.8327 7.9369 17.6336 7.95642 17.3809C7.9576 17.3694 7.95834 17.3569 7.95834 17.3427V12.5958C7.95834 11.7852 8.61777 11.1258 9.42834 11.1258C10.2389 11.1258 10.8983 11.7853 10.8983 12.5958V17.3427C10.8983 17.3568 10.8991 17.3691 10.9003 17.3804C10.9196 17.6333 11.1305 17.8327 11.3883 17.8327H16.2876C16.5582 17.8327 16.7776 17.6133 16.7776 17.3427V9.05419C16.7775 8.78356 16.5582 8.56421 16.2876 8.56421Z"
                                                fill="white"></path>
                                        </g>
                                        <defs>
                                            <clipPath id="clip0_1_8022">
                                                <rect width="18.8566" height="18.8566" fill="white"></rect>
                                            </clipPath>
                                        </defs>
                                    </svg>
                                </span>
                                {{ $category }}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            @foreach ($category_options as $cat_k => $category)
                <div class="tabs-container">
                    <div id="{{ $cat_k }}" class="tab-content {{ $cat_k == 0 ? 'active' : '' }}">
                        <div class="shop-protab-slider row">
                            @foreach ($MainCategoryList->take(1) as $category)
                                <div class="col-lg-4 col-xl-3 col-md-6 col-sm-6 col-12">
                                    <div class="category-card second-style">
                                        <div class="category-img">
                                            <img src="{{ asset($category->image_path) }}" alt="">
                                        </div>
                                        <div class="category-card-body">
                                            <div class="title-wrapper">
                                                <h3 class="title">{{ $category->name }}</h3>
                                            </div>
                                            <div class="btn-wrapper">
                                                <a href="{{ route('page.product-list', [$slug, 'main_category' => $category->id]) }}"
                                                    class="btn">
                                                    {{ __('Show more') }}
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="4"
                                                        height="6" viewBox="0 0 4 6" fill="none">
                                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                                            d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                                            fill="white"></path>
                                                    </svg>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                            @foreach ($home_products as $modern_product)

                                @if ($cat_k == '0' || $modern_product->ProductData->id == $cat_k)
                                    <div class="col-lg-4 col-xl-3 col-md-6 col-sm-6 col-12 product-card">
                                        <div class="product-card-inner no-back">
                                            <div class="product-card-image">
                                                <a href="{{ url($slug.'/product/'.$modern_product->slug) }}">
                                                    <img src="{{ asset($modern_product->cover_image_path) }}"
                                                        class="default-img">
                                                    @if ($modern_product->Sub_image($modern_product->id)['status'] == true)
                                                        <img src="{{ asset($modern_product->Sub_image($modern_product->id)['data'][0]->image_path) }}"
                                                            class="hover-img">
                                                    @else
                                                        <img src="{{ asset($modern_product->Sub_image($modern_product->id)) }}"
                                                            class="hover-img">
                                                    @endif
                                                </a>
                                                <div class="new-labl">
                                                    {{ $modern_product->ProductData->name }}
                                                </div>
                                            </div>
                                            <div class="product-content">
                                                <div class="product-content-top ">
                                                    <div class="custom-output">
                                                    {!! \App\Models\Product::productSalesPage($currentTheme, $slug, $modern_product->id) !!}
                                                    </div>
                                                    <h3 class="product-title">
                                                        <a href="{{ url($slug.'/product/'.$modern_product->slug) }}">
                                                            {{ $modern_product->name }}
                                                        </a>
                                                    </h3>
                                                    <div
                                                        class="reviews-stars-wrap d-flex align-items-center justify-content-center">
                                                        @if (!empty($modern_product->average_rating))
                                                            @for ($i = 0; $i < 5; $i++)
                                                                <i
                                                                    class="fa fa-star {{ $i < $modern_product->average_rating ? '' : 'text-warning' }} "></i>
                                                            @endfor
                                                            <span><b>{{ $modern_product->average_rating }}.0</b> /
                                                                5.0</span>
                                                        @else
                                                            @for ($i = 0; $i < 5; $i++)
                                                                <i
                                                                    class="fa fa-star review-stars {{ $i < $modern_product->average_rating ? '' : 'text-warning' }} "></i>
                                                            @endfor
                                                            <span><b>{{ $modern_product->average_rating }}.0</b> /
                                                                5.0</span>
                                                        @endif
                                                    </div>
                                                </div>
                                                <div class="product-content-center">
                                                    @if ($modern_product->variant_product == 0)
                                                        <div class="price">

                                                            <ins>{{ currency_format_with_sym(($modern_product->sale_price ?? $modern_product->price) , $store->id, $currentTheme) }}
                                                               </ins>
                                                            <del>{{ $modern_product->price }}
                                                                {{ $currency }}</del>
                                                        </div>
                                                    @else
                                                        <div class="price">
                                                            <ins>{{ __('In Variant') }}</ins>
                                                        </div>
                                                    @endif
                                                </div>
                                                <div
                                                    class="product-content-bottom d-flex align-items-center justify-content-between">
                                                    <div
                                                        class="bottom-select  d-flex align-items-center justify-content-between">
                                                        <div class="cart-btn-wrap">
                                                            <button class="btn addcart-btn-globaly"
                                                                product_id="{{ $modern_product->id }}"
                                                                variant_id="0" qty="1">
                                                                {{ __('Add to cart') }}
                                                                <svg xmlns="http://www.w3.org/2000/svg"
                                                                    width="4" height="6"
                                                                    viewBox="0 0 4 6" fill="none">
                                                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                                                        d="M0.65976 0.662719C0.446746 0.879677 0.446746 1.23143 0.65976 1.44839L2.18316 3L0.65976 4.55161C0.446747 4.76856 0.446747 5.12032 0.65976 5.33728C0.872773 5.55424 1.21814 5.55424 1.43115 5.33728L3.34024 3.39284C3.55325 3.17588 3.55325 2.82412 3.34024 2.60716L1.43115 0.662719C1.21814 0.445761 0.872773 0.445761 0.65976 0.662719Z"
                                                                        fill="white"></path>
                                                                </svg>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <button href="javascript:void(0)"
                                                        class="wishlist-btn wbwish  wishbtn-globaly"
                                                        product_id="{{ $modern_product->id }}"
                                                        in_wishlist="{{ $modern_product->in_whishlist ? 'remove' : 'add' }}">
                                                        <span class="wish-ic">
                                                            <i class="{{ $modern_product->in_whishlist ? 'fa fa-heart' : 'ti ti-heart' }}"
                                                                style='color: #000000'></i>
                                                        </span>
                                                    </button>
                                                    <div class="product-btn-wrp">
                                                        @php
                                                            $module = Nwidart\Modules\Facades\Module::find('ProductQuickView');
                                                            $compare_module = Nwidart\Modules\Facades\Module::find('ProductCompare');
                                                        @endphp
                                                        @if(isset($module) && $module->isEnabled())
                                                            {{-- Include the module blade button --}}
                                                            @include('productquickview::pages.button', ['product_slug' => $modern_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                        @endif
                                                        @if(isset($compare_module) && $compare_module->isEnabled())
                                                            {{-- Include the module blade button --}}
                                                            @include('productcompare::pages.button', ['product_slug' => $modern_product->slug ?? null, 'slug' => $slug ?? null, 'currentTheme' => $currentTheme])
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
