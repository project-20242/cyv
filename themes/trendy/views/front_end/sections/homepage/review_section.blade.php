<section class="testimonial-section padding-bottom" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
        <div class="container">
                <div class="section-title">
                    <div class="subtitle" id="{{ $section->review->section->sub_title->slug ?? '' }}_preview">{!! $section->review->section->sub_title->text ?? '' !!}</div>
                    <h2 id="{{ $section->review->section->title->slug ?? '' }}">{!!
                    $section->review->section->title->text ?? '' !!}</h2>
                </div>
                <div class="testimonial-slider white-dots">
                    @foreach ($reviews as $review)
                        <div class="testimonial-itm">
                            <div class="testimonial-itm-inner">
                                <div class="testm-top">
                                    <div class="review-title-wrp d-flex align-items-center justify-content-between">
                                        <h3 class="description" id="{{ $section->review->section->title->slug ?? '' }}">{!!$section->review->section->title->text ?? '' !!}</h3>
                                        <div class="review-stars d-flex align-items-center">
                                            @for ($i = 0; $i < 5; $i++)
                                                <i
                                                    class="ti ti-star {{ $i < $review->rating_no ? '' : 'text-warning' }} "></i>
                                            @endfor
                                            <span><b>{{ $review->rating_no }}.0</b> / 5.0</span>
                                        </div>
                                        <p class="descriptions">{{ $review->description }}</p>
                                    </div>
                    </div>
                                <div class="testim-bottom">
                                    <div class="testi-auto d-flex align-items-center">
                                        <div class="testi-img">
                                            <img src="{{ asset(!empty($review->ProductData) ? $review->ProductData->cover_image_path : '') }}"
                                                alt="reviewer-img">
                                        </div>
                                        <div class="test-auth-detail">
                                            <h4 id="{{ $section->review->section->title->slug ?? '' }}">{!!$section->review->section->title->text ?? '' !!}
                                            </h4>
                                            <span>Cliente</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
        </div>
    </section>