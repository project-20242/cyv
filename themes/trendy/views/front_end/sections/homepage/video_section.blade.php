<section class="vedio-center-section" style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif" data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}" data-hide="{{ $option->is_hide ?? '' }}" data-section="{{ $option->section_name ?? '' }}"  data-store="{{ $option->store_id ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
            <div class="video-right-wrap">
                <video class="video--tag" id="img-vid"
                    poster="{{ asset('themes/' . $currentTheme . '/assets/images/video-bnr.jpg') }}">
                    <source src="{!! asset('themes/' . $currentTheme . '/assets/images/fashion.mp4') !!}" type="video/mp4">
                </video>

                <div class="play-vid" data-click="0">
                    <div class="d-flex align-items-center">
                        <svg width="76" height="76" viewBox="0 0 76 76">
                            <path fill-rule="evenodd" clip-rule="evenodd"
                                d="M6.33333 38C6.33333 20.511 20.511 6.33333 38 6.33333C55.489 6.33333 69.6667 20.511 69.6667 38C69.6667 55.489 55.489 69.6667 38 69.6667C20.511 69.6667 6.33333 55.489 6.33333 38ZM38 0C17.0132 0 0 17.0132 0 38C0 58.9868 17.0132 76 38 76C58.9868 76 76 58.9868 76 38C76 17.0132 58.9868 0 38 0ZM33.4232 22.6985C32.4515 22.0507 31.2021 21.9903 30.1725 22.5414C29.1428 23.0924 28.5 24.1655 28.5 25.3333V50.6667C28.5 51.8345 29.1428 52.9076 30.1725 53.4586C31.2021 54.0097 32.4515 53.9493 33.4232 53.3015L52.4232 40.6348C53.3042 40.0475 53.8333 39.0588 53.8333 38C53.8333 36.9412 53.3042 35.9525 52.4232 35.3652L33.4232 22.6985ZM44.9579 38L34.8333 44.7497V31.2503L44.9579 38Z">
                            </path>
                        </svg>
                        <span><b>{{ __('CLICK HERE') }} </b> {{ __('TO WATCH') }}</span>
                    </div>
                </div>
            </div>
    </section>