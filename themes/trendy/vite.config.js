import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";
import path from "path";



export default defineConfig({
    plugins: [
        laravel({
            input: [
                "themes\menswear\sass/app.scss",
                "themes\menswear\js/app.js"
            ],
            buildDirectory: "menswear",
        }),
        
        
        {
            name: "blade",
            handleHotUpdate({ file, server }) {
                if (file.endsWith(".blade.php")) {
                    server.ws.send({
                        type: "full-reload",
                        path: "*",
                    });
                }
            },
        },
    ],
    resolve: {
        alias: {
            '@': '/themes\menswear\js',
            '~bootstrap': path.resolve('node_modules/bootstrap'),
        }
    },
    
});
