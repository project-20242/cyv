@extends('front_end.layouts.app')
@section('page-title')
{{ __('Article Page') }}
@endsection
@section('content')
@include('front_end.sections.partision.header_section')
@foreach ($blogs as $blog)
<section class="blog-page-banner common-banner-section"
    style="background-image: url({{asset('themes/'.$currentTheme.'/assets/images/blog-banner.jpg')}})">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-12">
                <div class="common-banner-content">
                    <ul class="blog-cat">
                        <li class="active">{{ __('Destacado')}}</li>
                        <li><b> {{__('Category')}}: </b> {{ $blog->category->name }}</li>
                        <li><b>{{__('Date')}}:</b> {{ $blog->created_at->format('d M,Y ') }}</li>
                    </ul>
                    <div class="section-title">
                        <h2>{!! $blog->title !!}</h2>
                    </div>
                    <p class="description">{!!$blog->short_description!!}</p>
                    <a href="" class="btn">
                        <span class="btn-txt">{{__('READ MORE')}}</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="article-section padding-top">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="about-user d-flex align-items-center">
                    <div class="abt-user-img">
                        <img src="{{asset('themes/'.$currentTheme.'/assets/images/john.png')}}">
                    </div>
                    <h3>
                        <span>John Doe,</span>
                        company.com
                    </h3>
                    <div class="post-lbl"><b>{{__('Category')}}:</b>{{$blog->category->name}}</div>
                    <div class="post-lbl"><b>{{__('Date')}}:</b>{{$blog->created_at->format('d M, Y ')}}</div>
                </div>

            </div>
            <div class="col-md-8 col-12">
                <div class="aticleleftbar">

                    {!! $blog->content !!}


                    <div class="art-auther"><b>John Doe</b>, <a href="company.com">company.com</a></div>
                    <div class="art-auther"><b>{{__('Tags:')}}</b> {{$blog->category->name}}</div>

                    <ul class="article-socials d-flex align-items-center">


                        <li><span>{{ __('Share :') }}</span></li>
                        @for($i=0 ; $i <$section->footer->section->footer_link->loop_number ?? 1;$i++) <li>
                                <a href="{!!$section->footer->section->footer_link->social_link->{$i} ?? '#' !!}">
                                    <img src=" {{  asset($section->footer->section->footer_link->social_icon->{$i}->image ?? 'themes/' . $currentTheme . '/assets/images/youtube.svg') }}"
                                        alt="youtube" class="mb-0" style="width: 60%;">
                                </a>

                            </li>
                            @endfor

                    </ul>
                </div>
            </div>
            <div class="col-md-4 col-12">
                <div class="articlerightbar blog-grid-section">
                    <div class="section-title">
                        <h3>{{ __('Related articles')}}</h3>
                    </div>
                    @foreach($datas->take(2) as $data)
                    <div class="blog-itm-card">

                        <div class="blog-card-inner">
                            <div class="blog-card-image">
                                <a href="{{route('page.article',[$slug,$data->id])}}" tabindex="0">
                                    <img src="{{ asset($data->cover_image_path) }}" class="default-img">
                                </a>
                                <div class="blog-labl">
                                    {{$data->category->name}}
                                </div>
                                <div class="date-labl">
                                    {{ $data->created_at->format('d M,Y ') }}
                                </div>
                            </div>
                            <div class="blog-product-content">
                                <h4 class="product-title">
                                    <a href="{{route('page.article',[$slug,$data->id])}}" tabindex="0"
                                        class="description">
                                        {!! $data->title !!}
                                    </a>
                                </h4>
                            </div>
                            <p class="descriptions">{{$data->short_description}}</p>
                            <div class="read-more-btn">
                                <a href="{{route('page.article',[$slug,$data->id])}}" class="btn-primary add-cart-btn"
                                    tabindex="0">
                                    {{__('Read more')}}
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>


@endforeach

<section class="related-blogs-section padding-top padding-bottom">
    <div class="container">

        <div class="section-title d-flex justify-content-between align-items-center">
            <div class="section-title-left">
                <h2>{{ __('Latest Blogs')}}</h2>
            </div>
            <div class="section-title-right">
                <a href="{{route('page.product-list',$slug)}}" class="btn-primary btn-transparent">
                    {{ __('Read More') }}
                   
                </a>
            </div>
        </div>
        <div class="blog-grid-row related-blogs-slider">
            @foreach ($l_articles as $blog)

            <div class="blog-itm-card">
                <div class="blog-card-inner">
                    <div class="blog-card-image">
                        <a href="{{route('page.article',[$slug,$blog->id])}}" tabindex="0">
                            <img src="{{ asset($blog->cover_image_path) }}" class="default-img">
                        </a>
                        <div class="blog-labl">
                            {{ $blog->category->name }}
                        </div>
                        <div class="date-labl">
                            {{ $blog->created_at->format('d M,Y ') }}
                        </div>
                    </div>
                    <div class="blog-product-content">
                        <h3 class="product-title">
                            <a href="{{route('page.article',[$slug,$blog->id])}}" tabindex="0" class="description">
                                {!! $blog->title !!}
                            </a>
                        </h3>
                    </div>
                    <p class="descriptions">{{$blog->short_description}}</p>
                    <div class="read-more-btn">
                        <a href="{{route('page.article',[$slug,$blog->id])}}" class="btn-primary add-cart-btn"
                            tabindex="0">
                            {{ __('READ MORE')}}
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>


@include('front_end.sections.partision.footer_section')
@endsection