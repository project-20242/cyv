@foreach ($landing_blogs as $blog)
    <div class="blog-card-itm">
        <div class="blog-card-itm-inner">
            <div class="blog-card-image">
                <a href="{{route('page.article',[$slug,$blog->id])}}" tabindex="0">
                    <img src="{{asset($blog->cover_image_path )}}" class="default-img">
                </a>
                <div class="tip-lable">
                    <span>NUEVO</span>
                </div>
            </div>
            <div class="blog-card-content">
                <div class="blog-card-heading-detail">
                    <span>{{ date("d M Y", strtotime($blog->created_at))}}</span>
                </div>
                <h4>
                    <a href="{{route('page.article',[$slug,$blog->id])}}" tabindex="0">
                        {{ $blog->title }}
                    </a>
                </h4>
                <p>
                    {{ $blog->short_description }}
                </p>
                <div class="blog-card-bottom">
                    <a href="{{route('page.article',[$slug,$blog->id])}}" class="btn" tabindex="0">
                        {{__(' Read More')}}
                    </a>
                </div>
            </div>
        </div>
    </div>
@endforeach

