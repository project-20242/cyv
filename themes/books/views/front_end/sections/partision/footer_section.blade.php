<footer class="site-footer"  style="position: relative;@if(isset($option) && $option->is_hide == 1) opacity: 0.5; @else opacity: 1; @endif"
    data-index="{{ $option->order ?? '' }}" data-id="{{ $option->order ?? '' }}" data-value="{{ $option->id ?? '' }}"
    data-hide="{{ $option->is_hide  ?? '' }}" data-section="{{ $option->section_name  ?? '' }}"
    data-store="{{ $option->store_id  ?? '' }}" data-theme="{{ $option->theme_id ?? '' }}">
    <div class="custome_tool_bar"></div>
    <div class="container">
         @if(isset($whatsapp_setting_enabled) && !empty($whatsapp_setting_enabled))
        <div class="floating-wpp"></div>
        @endif
        <div class="footer-row">
             <div class="footer-col footer-subscribe-col">
                <div class="footer-widget">
                    <div class="footer-subscribe">
                        <h3 id="{{ $section->footer->section->newsletter_title->slug ?? '' }}_preview">
                            {!! $section->footer->section->newsletter_title->text ?? '' !!} </h3>
                    </div>
                    <p id="{{ $section->footer->section->newsletter_description->slug ?? '' }}_preview">
                            {!! $section->footer->section->newsletter_description->text ?? '' !!}</p>
                    <form class="footer-subscribe-form" action='{{ route("newsletter.store",$slug) }}' method="post">
                        @csrf
                        <div class="input-wrapper">
                            <input type="email" placeholder="ESCRIBA SU DIRECCION DE CORREO ELECTRONICO..." name="email">
                            <button type="submit" class="btn-subscibe"> {{__('SUBSCRIBE')}}
                            </button>
                        </div>
                        <div class="">
                            <label for="subscibecheck"
                                        id="{{ $section->footer->section->newsletter_sub_title->slug ?? '' }}_preview">
                                        {!! $section->footer->section->newsletter_sub_title->text ?? '' !!}
                            </label>
                        </div>
                    </form>
                </div>
            </div>
            @if(isset($section->footer->section->footer_menu_type))
            @for ($i = 0; $i < $section->footer->section->footer_menu_type->loop_number ?? 1; $i++)
                @if(isset($section->footer->section->footer_menu_type->footer_title->{$i}))
                <div class="footer-col footer-link footer-link-{{$i+1}}">
                    <div class="footer-widget">
                        <h4> {{ $section->footer->section->footer_menu_type->footer_title->{$i} ?? ''}} </h4>
                        @php
                        $footer_menu_id = $section->footer->section->footer_menu_type->footer_menu_ids->{$i} ?? '';
                        $footer_menu = get_nav_menu($footer_menu_id);
                        @endphp
                        <ul>
                            @if (!empty($footer_menu))
                            @foreach ($footer_menu as $key => $nav)
                            @if ($nav->type == 'custom')
                            <li><a href="{{ url($nav->slug) }}" target="{{ $nav->target }}">
                                    @if ($nav->title == null)
                                    {{ $nav->title }}
                                    @else
                                    {{ $nav->title }}
                                    @endif
                                </a></li>
                            @elseif($nav->type == 'category')
                            <li><a href="{{ route('themes.page', [$slug, $nav->slug]) }}" target="{{ $nav->target }}">
                                    @if ($nav->title == null)
                                    {{ $nav->title }}
                                    @else
                                    {{ $nav->title }}
                                    @endif
                                </a></li>
                            @else
                            <li><a href="{{ route('themes.page', [$slug, $nav->slug]) }}" target="{{ $nav->target }}">
                                    @if ($nav->title == null)
                                    {{ $nav->title }}
                                    @else
                                    {{ $nav->title }}
                                    @endif
                                </a>
                            </li>
                            @endif
                            @endforeach
                            @endif
                        </ul>
                    </div>
                </div>
                @endif
                @endfor
                @endif
                <div class="footer-col footer-link footer-link-3">
                    <div class="footer-widget">
                        <h4 id="{{ $section->footer->section->title->slug ?? '' }}_preview"> {!!
                            $section->footer->section->title->text ?? '' !!}</h4>
                        @if(isset($section->footer->section->footer_link))
                        <ul class="footer-list-social">
                            @for ($i = 0; $i < $section->footer->section->footer_link->loop_number ?? 1; $i++)
                                <li>
                                    <a href="{{ $section->footer->section->footer_link->social_link->{$i} ?? '#'}}"
                                        target="_blank" id="social_link_{{ $i }}">
                                        <img src="{{ asset($section->footer->section->footer_link->social_icon->{$i}->image ?? 'themes/' . $currentTheme . '/assets/images/youtube.svg') }}"
                                            class="{{ 'social_icon_'. $i .'_preview' }}" alt="icon"
                                            id="social_icon_{{ $i }}">
                                    </a>
                                </li>
                                @endfor
                        </ul>
                        @endif
                    </div>
                </div>
        </div>
    </div>
</footer>
<!--footer end here-->
<div class="overlay"></div>
<!--cart popup start here-->
<div class="overlay cart-overlay"></div>
<div class="cartDrawer cartajaxDrawer"></div>
<div class="overlay wish-overlay"></div>
<div class="wishDrawer wishajaxDrawer"></div>