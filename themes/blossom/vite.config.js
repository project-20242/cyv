import { defineConfig } from "vite";
import laravel from "laravel-vite-plugin";
import path from "path";



export default defineConfig({
    plugins: [
        laravel({
            input: [
                "themes\landholdings\sass/app.scss",
                "themes\landholdings\js/app.js"
            ],
            buildDirectory: "landholdings",
        }),
        
        
        {
            name: "blade",
            handleHotUpdate({ file, server }) {
                if (file.endsWith(".blade.php")) {
                    server.ws.send({
                        type: "full-reload",
                        path: "*",
                    });
                }
            },
        },
    ],
    resolve: {
        alias: {
            '@': '/themes\landholdings\js',
            '~bootstrap': path.resolve('node_modules/bootstrap'),
        }
    },
    
});
